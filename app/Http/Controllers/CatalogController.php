<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roipal\Eloquent\Catalog;
use App\Roipal\Repositories\CatalogRepository;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Transformers\Catalogs\MissionTypeTransformer;

class CatalogController extends Controller
{
    private $catalog;

    public function __construct(CatalogRepository $catalog)
    {
        $this->catalog = $catalog;
    }

    public function show(Request $request)
    {
        $type = $request->catalog;

        $catalogs = $this->catalog->getByType($type);

        $data = [
            'message' => 'Catalogs list',
        ] + Fractal::collection($catalogs, new MissionTypeTransformer, 'data');

        return response()->json($data);
    }
}
