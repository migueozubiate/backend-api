<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\LineBusinessRepository;
use App\Roipal\Transformers\LineBusinessTransformer;

class LineBusinessCatalogController extends Controller
{
    protected $repository;

    public function __construct(LineBusinessRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $lineBusinessCatalog = $this->repository->index($request->all());

        $data = [
            'message' => 'Line business'
        ] + Fractal::simplePaginate($lineBusinessCatalog, new LineBusinessTransformer, 'data');

        return response()->json($data);
    }

    public function info(Request $request, $sector)
    {
        $lineBusinessBySector = 
                $this->repository->lineBusinessBySector($request->all(), $sector);

        $data = [
            'message' => 'Line business',
        ] + Fractal::simplePaginate($lineBusinessBySector, new LineBusinessTransformer, 'data');

        return response()->json($data);
    }
}
