<?php
namespace App\Http\Controllers;

use Lcobucci\JWT\Parser;
use App\Roipal\Eloquent\User;
use Illuminate\Support\Facades\App;
use App\Http\Requests\UserLoginRequest;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Transformers\UserTransformer;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response as Psr7Response;
use App\Roipal\Transformers\CompanyTransformer;
use Laravel\Passport\Http\Controllers\AccessTokenController as PassortAccessTokenController;

class AccessTokenController extends PassortAccessTokenController
{
    public function issueCustomToken(UserLoginRequest $req, ServerRequestInterface $request)
    {
        $tokenResponse = $this->server->respondToAccessTokenRequest($request, new Psr7Response);

        $token = $tokenResponse->getBody();

        $token = json_decode($token, true);

        $profile = User::where('email', '=', $req->username)->first();

        $includes = $req->includes ?? '';

        $bearerToken = $token['access_token'];
        $tokenId= (new Parser())->parse($bearerToken)->getHeader('jti');

        $userTokens = $profile->tokens->whereNotIn('id', $tokenId);

        foreach ($userTokens as $userToken) {
            $userToken->revoke();
        }

        $profile->update(['lang' => App::getLocale()]);

        Fractal::with($includes);

        $profile = Fractal::item($profile, new UserTransformer);

        $relationships = [];

        foreach ($profile as $key => $value) {
            if (is_array($value)) {
                $relationships[$key] = array_pull($profile, $key);
            }
        }

        $relationships['profile'] = $profile;

        $data = [
            'message' => 'Success authentication',
            'data' => [
                'token' => $token
            ] + $relationships
        ];

        return response()->json($data);
    }
}