<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use App\Roipal\Eloquent\Executive;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Strategies\RoipalStarStrategy;
use App\Roipal\Jobs\ProcessDeleteRoipalStarJob;
use App\Roipal\Repositories\ExecutiveAssestmentRepository;
use App\Roipal\Transformers\Assestments\ExecutiveAssestmentTransformer;


class RoipalStar extends Controller
{
    protected $repository;

    public function __construct(RoipalStarStrategy $strategy, ExecutiveAssestmentRepository $repository)
    {
        $this->strategy = $strategy;
        $this->repository = $repository;
    }

    public function index(Executive $executive)
    {
        $executiveAssesment = $this->repository->getScore($executive);

        if (empty($executiveAssesment)) {
            return response()->json([
                'message' => 'Not complete the DISC evaluation',
                'code' => 'NOT_DISC_EVALUATION'
            ]);
        }

        $filePath = $this->strategy->drawStar($executiveAssesment);

        $route = 'https://media-roipal.nulldata.com/api/generic/downloads?file=' . $filePath;

        $data = [
            'message' => 'Success',
            'data' => [
                'roipal_star_url' => $route,
                'assessment' => Fractal::item($executiveAssesment, new ExecutiveAssestmentTransformer)
            ]
        ];

        return response()->json($data);
    }
}
