<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LanguageRequest;
use App\Roipal\Transformers\LanguageTransformer;
use App\Roipal\Repositories\LanguageRepository;
use Waavi\Translation\Models\Language;
use Xaamin\Fractal\Facades\Laravel\Fractal;


class LanguageController extends Controller
{
   private $languageRepository;

   public function __construct(LanguageRepository $repository)
   {
        $this->languageRepository = $repository;
   }

   public function index(Request $request)
   {

        $limit = array_get($request,'limit');

        $result = $this->languageRepository->index($limit);

        $data = [
            'message' => 'Languages',
        ] + Fractal::simplePaginate($result, new LanguageTransformer, 'data');


        return response()->json($data);
   }

   public function store(LanguageRequest $request)
   {
        $data = $request->all();

        $languageInformation = $this->languageRepository->store($data);

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($languageInformation, new LanguageTransformer)
        ];

        return response()->json($data);
   }

   public function update(LanguageRequest $request, Language $language)
   {
        $result = $this->languageRepository->update($request->all(),$language);

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($result, new LanguageTransformer)
        ];

        return response()->json($data);
   }

   public function info(Request $request, $locale)
   {
     $traslation = trans($request->item);
     
     return response()->json([
          $traslation
     ]);
   }
}
