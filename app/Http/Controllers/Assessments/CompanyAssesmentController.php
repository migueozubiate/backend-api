<?php
namespace App\Http\Controllers\Assessments;

use Illuminate\Http\Request;
use App\Roipal\Eloquent\Mission;
use App\Http\Controllers\Controller;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\CompanyAssestmentRepository;
use App\Roipal\Strategies\Missions\MissionInvitationStrategy;
use App\Roipal\Transformers\Assestments\AssestmentTransformer;
use App\Roipal\Transformers\Assestments\ExecutiveAssestmentTransformer;
use App\Roipal\Strategies\PushNotifications\MissionNotificationStrategy;

class CompanyAssesmentController extends Controller
{
    protected $repository;

    public function __construct(CompanyAssestmentRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request, Mission $mission)
    {
        $asessmentCompany = $this->repository->getAsessment($mission);

        Fractal::with($request->input('includes') ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::collection($asessmentCompany, new AssestmentTransformer)
        ];

        return response()->json($data);
    }

    public function store(
        Request $request, 
        Mission $mission, 
        MissionInvitationStrategy $invitation,
        MissionNotificationStrategy $notification
        )
    {
        $user = $request->user();
        
        $executiveAssessment = $this->repository->evaluationAssessment(
                                    $request->all(), 
                                    $mission, 
                                    $user->executive
                                );

        $data = $invitation->invitationStatus(
            $user->executive, 
            $mission, 
            $executiveAssessment,
            $notification
        );

        return response()->json($data);
    }
}
