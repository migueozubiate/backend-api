<?php
namespace App\Http\Controllers\Assessments;

use UUID;
use ArrayIterator;
use League\Csv\Writer;
use Illuminate\Http\Request;
use App\Roipal\Eloquent\Assestment;
use App\Http\Controllers\Controller;
use App\Roipal\Scopes\AssesmentScope;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\AssestmentRepository;
use App\Roipal\Strategies\Assessments\AssessmentFactory;
use App\Http\Requests\Assestment\AssessmentUpdateRequest;
use App\Roipal\Repositories\ExecutiveAssestmentRepository;
use App\Roipal\Transformers\Assestments\AssestmentTransformer;
use App\Roipal\Transformers\Assestments\ExecutiveAssestmentTransformer;

class AssestmentController extends Controller
{
    protected $repository;

    protected $scope;

    public function __construct(AssestmentRepository $repository, AssesmentScope $scope)
    {
        $this->repository = $repository;
        $this->scope = $scope;
    }

    public function index(Request $request, ExecutiveAssestmentRepository $respository)
    {
        if ($request->download) {
            return $this->download($request, $respository);
        }

        $assestments = $respository->paginateByType($request->type, $request->all());

        $data = [
            'message' => 'Assestment evaluations list',
        ] + Fractal::with($request->includes ?? '')->paginate($assestments, new ExecutiveAssestmentTransformer, 'data');

        return response()->json($data);
    }

    protected function download(Request $request, ExecutiveAssestmentRepository $respository)
    {
        $data = $respository->getByType($request->type, $request->all());

        foreach ($data as $index => $eval) {
            $profiles = ['H', 'P', 'F', 'C'];

            foreach ($profiles as $profile) {
                $data[$index][$profile] = $eval->answers->where('profile', $profile)->sum('points');
            }

            foreach ($eval->answers as $answer) {
                $data[$index]["answer_{$answer->number}"] = "{$answer->profile}{$answer->points}";
            }

            unset($data[$index]['answers']);
        }

        $data = $data->toArray();

        $filename = UUID::generate(4) . '.csv';

        $writer = Writer::createFromPath('php://memory', 'w');

        $firstRow = null;

        $types = [0, 1, 2, 3, 4];

        if ($request->only_billing_invoices) {
            $types = [5, 6, 7, 8, 9];
        }

        if (!$firstRow and count($data)) {
            $firstRow = $data[0];

            $writer->insertOne(array_keys($firstRow));
        }

        $writer->insertAll(new ArrayIterator($data));

        $writer->output($filename);

        return response("\r\n\r\n")
            ->header('Transfer-Encoding', 'chunked')
            ->header('Content-Encoding', 'none')
            ->header('Content-Type', 'text/csv; charset=UTF-8')
            ->header('Content-Description', 'File Transfer')
            ->header('Content-Disposition', 'attachment; filename="' . $filename . '"');
    }

    public function info(Request $request, $type, AssessmentFactory $factory)
    {
        $assessment = $this->repository->getAssestmentByType($request->type, $request->all());

        $tranformer = Fractal::with($request->get('includes', ''));

        $assessment->questions = $factory->getQuestions($assessment, $request->all());

        return response()->json([
            'message' => 'Assestment list',
            'data' => $tranformer->item($assessment, new AssestmentTransformer),
        ]);
    }

    public function show(Request $request)
    {
        $assessments = $this->repository->show($request->all());

        Fractal::with($request->includes ?? '');

        $data = [
            'message' => 'Success'
        ] + Fractal::simplePaginate($assessments, new AssestmentTransformer, 'data');

        return response()->json($data, 200);
    }

    public function update(AssessmentUpdateRequest $request, Assestment $assessment)
    {
        $getAssessment = $this->repository->update($request->all(), $assessment);

        Fractal::with($request->includes ?? '');

        $data = [
            'message' => 'Successful assessment update',
            'data' => Fractal::item($getAssessment, new AssestmentTransformer)
        ];

        return response()->json($data, 200);
    }
}
