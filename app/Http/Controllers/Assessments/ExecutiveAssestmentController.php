<?php
namespace App\Http\Controllers\Assessments;

use App\Http\Controllers\Controller;
use App\Http\Requests\Assestment\ExecutiveAssestmentStoreRequest;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Repositories\AssestmentRepository;
use App\Roipal\Repositories\ExecutiveAssestmentRepository;
use App\Roipal\Strategies\Assessments\AssessmentFactory;
use App\Roipal\Transformers\Assestments\ExecutiveAssestmentTransformer;
use Illuminate\Http\Request;
use Xaamin\Fractal\Facades\Laravel\Fractal;

class ExecutiveAssestmentController extends Controller
{
    /**
     * @var AssestmentRepository
     */
    protected $repository;

    protected $assestment;

    public function __construct(ExecutiveAssestmentRepository $repository, AssestmentRepository $assestment)
    {
        $this->repository = $repository;
        $this->assestment = $assestment;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    public function info(Request $request, $uuid)
    {
        $assessment = $this->repository->getByUuid($uuid);

        $tranformer = Fractal::with($request->includes ?? '');

        return response()->json([
            'message' => strtolower($assessment->name),
            'data' => $tranformer->item($assessment, new ExecutiveAssestmentTransformer),
        ]);
    }

    public function store(ExecutiveAssestmentStoreRequest $request, Executive $executive, AssessmentFactory $factory)
    {  
        $assessment = $this->assestment->getAssestmentByType($request->type);
        $repository = $factory->build($request->type);
        $assessment = $repository->store($assessment, $executive, $request->all());

        $includes = $request->includes;

        Fractal::with($includes ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($assessment, new ExecutiveAssestmentTransformer),
        ];

        return response()->json($data);
    }
}
