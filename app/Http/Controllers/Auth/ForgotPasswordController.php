<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Roipal\Eloquent\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Transformers\UserTransformer;
use App\Roipal\Repositories\UserRepository;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;


class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
     */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
        $this->middleware('guest');
    }

    public function verifyPassword(Request $request)
	{

  	    $user = $this->repository->resetPassword($request->all());

        if (!$user) {
            return response()->json([
                'message' => 'The email not exist',
                'code' => 'FAILED_MAIL_SEND', 
            ]);
        }

        $this->sendResetLinkEmail($request);

        $data = [
            'message' => 'Success',
            'data' => Fractal::including('checklist')->item($user, new UserTransformer)
        ];

        return response()->json($data, 200);
	}

	public function sendResetLinkEmail(Request $request)
    {
        $this->validateEmail($request);

        $response = $this->broker()->sendResetLink(
            $request->only('email')
        );

        return $response == Password::RESET_LINK_SENT
                    ? $this->sendResetLinkResponse($request, $response)
                    : $this->sendResetLinkFailedResponse($request, $response);
    }


}
