<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Roipal\Eloquent\User;
use App\Mail\ResetPasswordMail;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Mail;
use App\Http\Requests\PasswordRequest;
use App\Roipal\Repositories\UserRepository;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Transformers\UserTransformer;

class PasswordController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $repository;

    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('auth.passwords.confirmation_password');
    }


}
