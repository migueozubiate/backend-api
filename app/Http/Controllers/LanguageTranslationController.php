<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Waavi\Translation\Models\Language;
use Waavi\Translation\Models\Translation;
use App\Roipal\Transformers\LanguageTranslationTransformer;
use App\Http\Requests\LanguageTranslationRequest;
use App\Roipal\Repositories\LanguageTranslationRepository;
use Xaamin\Fractal\Facades\Laravel\Fractal;

class LanguageTranslationController extends Controller
{
    private $translationRepository;

    public function __construct(LanguageTranslationRepository $repository)
    {
         $this->translationRepository = $repository;
    }

    public function index(Request $request)
    {
        $limit = array_get($request,'limit');

        $result = $this->translationRepository->index($limit);
        
 
        $data = [
            'message' => 'Tranlations',
        ] + Fractal::simplePaginate($result, new LanguageTranslationTransformer, 'data');


        return response()->json($data);
    } 

    public function store(LanguageTranslationRequest $request, Language $language)
    {
        
        $trasnlation = $this->translationRepository->store($request->all(), $language);

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($trasnlation, new LanguageTranslationTransformer )
        ]; 

        return response()->json($data);
    }

    public function update(LanguageTranslationRequest $request, Translation $translation)
    {
        
        $result = $this->translationRepository->update($request->all(),$translation);

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($result, new LanguageTranslationTransformer)
        ];

        return response()->json($data);
    }

    public function getByNamespaceAndGroup(Request $request)
    {
        $getCatalog = $this->translationRepository->getByNamespaceAndGroup($request->all());

        $data = [
            'message' => 'Catalogs list',
            'data' => Fractal::collection($getCatalog, new LanguageTranslationTransformer)
        ];

        return response()->json($data);
    }
}
  