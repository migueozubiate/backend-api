<?php
namespace App\Http\Controllers;

use Xaamin\WebsiteInfo\SiteInfoReader;
use App\Http\Requests\WebsiteInfoRequest;

class WebsiteInfoController extends Controller
{
    public function index(WebsiteInfoRequest $request, SiteInfoReader $reader)
    {
        $data = [];
        $code = 200;
        $info = null;

        try {
            $info = $reader->parse($request->url);

            $data = [
                'message' => 'Success',
                'data' => $info
            ];
        } catch (Exception $e) {
            $code = 400;
            $data['message'] = 'Web site not exists or is not available at this moment';
        }

        return response()->json($data, $code);
    }
}