<?php
namespace App\Http\Controllers;

use Fractal;
use Illuminate\Http\Request;
use App\Roipal\Eloquent\User;
use App\Roipal\Scopes\UserScope;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Roipal\Repositories\UserRepository;
use App\Roipal\Transformers\UserTransformer;

class UserController extends Controller
{
    protected $repository;

    protected $scope;

    public function __construct(UserRepository $repository, UserScope $scope)
    {
        $this->repository = $repository;
        $this->scope = $scope;
    }

    public function index(Request $request)
    {
        $user = $request->user();

        $this->scope->setUser($user);

        Fractal::with($request->includes ?? '');

        $users = $this->repository->index($request->all(), $this->scope);

        $data = [
            'message' => 'Users',
        ] + Fractal::simplePaginate($users, new UserTransformer, 'data');

        return response()->json($data);
    }

    public function show(Request $request, User $user)
    {
        $data = [
            'message' => 'Users info',
            'data' => Fractal::with($request->includes ?? '')->item($user, new UserTransformer)
        ];

        return response()->json($data);
    }

    public function sendMailVerificationAgain(Request $request)
    {
        $user = $request->user();
        event(new Registered($user));

        $data = [
            'message' => 'Send mail again',
            'data' => Fractal::item($user, new UserTransformer)
        ];

        return response()->json($data, 200);
    }
}