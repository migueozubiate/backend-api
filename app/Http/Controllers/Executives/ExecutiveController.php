<?php
namespace App\Http\Controllers\Executives;

use App\Http\Controllers\Controller;
use App\Http\Requests\Executives\ExecutiveCreateRequest;
use App\Http\Requests\Executives\ExecutiveUpdateRequest;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Repositories\ExecutiveRepository;
use App\Roipal\Strategies\TokenWithNewUser;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Xaamin\Fractal\Facades\Laravel\Fractal;

class ExecutiveController extends Controller
{
    use TokenWithNewUser;

    /** @var ExecutiveRepository */
    protected $repository;

    public function __construct(ExecutiveRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $executive = $this->repository->index();
        $includes = array_get($request->toArray(), 'includes', '');

        $data = [
            'message' => 'Executives',
        ] + Fractal::With($includes)->collection($executive, new ExecutiveTransformer, 'data');

        return response()->json($data);
    }

    public function store(ExecutiveCreateRequest $request)
    {
        $executive = $this->repository->store($request->only(['name', 'email', 'password']));
        event(new Registered($executive));
        $token = $this->geTokenFromNewUser($executive, $request->get('password'), ['executive']);

        $includes = 'profile,checklist';

        Fractal::with($includes);

        $profile = Fractal::item($executive, new ExecutiveTransformer);

        $relationships = [];

        foreach ($profile as $key => $value) {
            if (is_array($value)) {
                $relationships[$key] = array_pull($profile, $key);
            }
        }

        $relationships['profile'] = $profile;

        $data = [
            'message' => 'Success',
            'data' => [
                'token' => $token,
            ] + $relationships,
        ];

        return response()->json($data, 201);
    }

    public function show(Request $request, Executive $executive)
    {
        $data = [
            'message' => 'Success',
            'data' => Fractal::with($request->get('includes', []))
                        ->item($executive, new ExecutiveTransformer)
        ];

        return response()->json($data, 200);
    }

    public function showMissions(Executive $executive, Mission $mission)
    {
        $mission = $executive->missions()
            ->where('missions.uuid', '=', $mission->uuid)
            ->get();
            
        return $mission;
    }

    public function update(ExecutiveUpdateRequest $request, Executive $executive)
    {
        $executive = $this->repository->update($request->all(), $executive);
        
        $includes = ['profile', 'limitations'];
        
        $includesRequest = explode(',', $request->includes);
    
        $newIncludes = array_merge($includes, $includesRequest);

        Fractal::with($newIncludes ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($executive, new ExecutiveTransformer)
        ];
     
        return response()->json($data, 200);
        
    }
}
