<?php

namespace App\Http\Controllers\Executives;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Roipal\Eloquent\PhysicalLimitation;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\PhysicalLimitationRepository;
use App\Roipal\Transformers\PhysicalLimitationsTransformer;

class ExecutivePhysicalLimitationController extends Controller
{
    protected $repository;

    public function __construct(PhysicalLimitationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index()
    {
        $limitations = $this->repository->index();

        $data = [
            'message' => 'Success',
            'data' => Fractal::collection($limitations, new PhysicalLimitationsTransformer)
        ];

        return response()->json($data);
    }
}
