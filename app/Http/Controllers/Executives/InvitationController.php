<?php

namespace App\Http\Controllers\Executives;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Repositories\MissionInvitationRepository;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Transformers\MissionInvitationTransformer;

class InvitationController extends Controller
{
    protected $repository;

    public function __construct(MissionInvitationRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request, Executive $executive)
    {
        $invitations = $this->repository->getByExecutive($executive, $request->all());

        Fractal::with($request->includes ?? '');

        $data = [
            'message' => 'Invitations'
        ] + Fractal::simplePaginate($invitations, new MissionInvitationTransformer, 'data');

        return response()->json($data);
    }
}
