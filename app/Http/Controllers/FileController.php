<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Roipal\Strategies\StoreFileStrategy;

class FileController extends Controller
{
    public function store(Request $request, StoreFileStrategy $storeFile)
    {
        $data = $storeFile->store($request->all());

        return response()->json($data);
    }

    public function delete(Request $request)
    {
        $storagePath = storage_path($request->file);

        if (!\file_exists($storagePath)) {
            return response()->json([
                'message' => 'File not found',
                'error' => 404
            ]);
        }

        return response()->download($storagePath);
    }

    public function download(Request $request)
    {
        $storagePath = storage_path($request->file);

        if (!\file_exists($storagePath)) {
            return response()->json([
                'message' => 'File not found',
                'error' => 404
            ]);
        }

        return response()->download($storagePath);
    }
}
