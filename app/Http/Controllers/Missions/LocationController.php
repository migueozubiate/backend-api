<?php

namespace App\Http\Controllers\Missions;

use Illuminate\Http\Request;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Location;
use App\Http\Controllers\Controller;
use App\Roipal\Scopes\LocationScope;
use App\Http\Requests\LocationRequest;
use App\Roipal\Eloquent\MissionLocation;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\LocationRepository;
use App\Roipal\Transformers\LocationTransformer;
use App\Http\Requests\Mission\MissionLocationRequest;
use App\Roipal\Jobs\CreateInvitationByNearProfileJob;
use App\Roipal\Transformers\MissionLocationTransformer;

class LocationController extends Controller
{
    protected $location;

    protected $scope;

    public function __construct(LocationRepository $location, LocationScope $scope)
    {
      $this->location = $location;
      $this->scope = $scope;
    }

    public function index(Request $request, Company $company, Mission $mission, MissionLocation $location)
    {
        Fractal::with($request->input('includes')??'');

        $data = [
          'message' => 'Success',
          'data' => Fractal::item($location, new MissionLocationTransformer) 
        ];

        return $data;
    }

    public function store(LocationRequest $request, Company $company)
    {
      $locations = $this->location->store($request->all(), $company);

      $includes = $request->includes;

      Fractal::with($includes ?? '');

      $data = [
        'message' => 'Success',
        'data' => Fractal::item($locations, new LocationTransformer) 
      ];

      return response()->json($data);
    }

    public function show(Request $request)
    {
      $location = new Location();

      Fractal::with($request->input('includes')??'');

      $user = $request->user();

      $this->scope->setUser($user);

      $includes = $request->input('includes');
      $includes = explode(',', $includes);

      $locationIncludes = $location->getRelationshipsToBeLoaded($includes);

      array_add($request, 'relationships', $locationIncludes);

      $locations = $this->location->show($request->all(), $this->scope);
      $data = [
        'message' => 'locations',
      ] + Fractal::simplePaginate($locations, new LocationTransformer, 'data');

      return response()->json($data);
    }

    public function info(Location $location)
    {
      $getLocation = $this->location->getLocationById($location);

      $data = [
        'data' => Fractal::collection($getLocation, new LocationTransformer)
      ];

      return response()->json($data);
    }

    public function update(LocationRequest $request, Company $company, Location $location)
    {

      $locations = $this->location->update($request->all(), $company, $location);

      $data = [
        'message' => 'Success',
        'data' => Fractal::item($locations, new LocationTransformer)
      ];

      return response()->json($data);
    }

    public function storeByMission(MissionLocationRequest $request, Company $company, Mission $mission)
    {
        $missionLocation = $this->location->storeByMission($request->all(), $company, $mission);

        Fractal::with($request->includes ?? '');

        $data = [
          'message' => 'Success',
          'data' => Fractal::collection($missionLocation, new MissionLocationTransformer)
        ];

        return response()->json($data);
    }

    public function getLocationsThatNotInMission(Request $request, Company $company, Mission $mission)
    {
        $locations = $this->location->getLocationsThatNotInMission(
                                        $request->all(), 
                                        $company, 
                                        $mission
                                      );

        $includes = $request->includes;

        Fractal::with($includes ?? '');

        $data = [
          'message' => 'Success',
        ] + Fractal::simplePaginate($locations, new LocationTransformer, 'data');

        return response()->json($data, 200);
    }
}
