<?php
namespace App\Http\Controllers\missions;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SuggestionController extends Controller
{
    public function show(Request $request)
    {
        $data = [
            'message' => 'Suggested profile',
            'data' => [
                'key' => 'promotor',
                'profile' => 'Promotor de ventas',
                'time' => 2,
                'time_unit' => trans("validation.time"),
                'currency' => 'MXN',
                'min_time' => 2,
                'max_time' => 50,
                'fare_by_time' => 200.00,
                'estimations' => [
                    [
                        'min' => 1,
                        'max' => 10,
                        'fare' => 200.00
                    ],
                    [
                        'min' => 11,
                        'max' => 15,
                        'fare' => 180.00
                    ]
                ]

            ]
        ];

        return response()->json($data);
    }
}
