<?php

namespace App\Http\Controllers\Missions;

use Ramsey\Uuid\Uuid;
use Illuminate\Http\Request;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use App\Roipal\Constants\ActivityStatus;
use App\Roipal\Eloquent\ActivityHistory;
use App\Roipal\Eloquent\MissionActivity;
use League\Flysystem\AwsS3v2\AwsS3Adapter;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\ActivityHistoryRepository;
use App\Roipal\Transformers\ActivityHistoryTransformer;
use App\Http\Requests\MissionActivities\ActivityHistoryRequest;
use App\Roipal\Strategies\PushNotifications\ActivityNotificationStrategy;

class ActivityHistoryController extends Controller
{
    protected $repository;

    public function __construct(ActivityHistoryRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request, Executive $executive, Mission $mission, MissionActivity $activity)
    {
        Fractal::with($request->input('includes') ?? '');

        $activityHistory = $this->repository->index($request->all(), $activity);
        $data = [
            "message" => "History Activities"
        ] + Fractal::simplePaginate($activityHistory, new ActivityHistoryTransformer, 'data');

        return response()->json($data);   
    }

    public function store(
        ActivityHistoryRequest $request, 
        Executive $executive, 
        Mission $mission, 
        MissionActivity $activity,
        ActivityNotificationStrategy $notification
    )
    {
        $includes = $request->includes;

        Fractal::with($includes ?? '');

        $activityHistory = $this->repository->store($request->all(), $activity);

        if ($activity->status == ActivityStatus::FINISHED) {
            $notification->notifyToCompanyWhenActivityFinished($activity);
        }

        if ($activity->status == ActivityStatus::STARTED) {
            $notification->notifyToCompanyWhenActivityStarted($activity);
        }

        $data = [
            "message" => "Success",
            "data" => Fractal::item($activityHistory, new ActivityHistoryTransformer)
        ];

        return response()->json($data);
    }
}
