<?php
namespace App\Http\Controllers\Missions;

use Illuminate\Http\Request;
use App\Roipal\Eloquent\Mission;
use App\Http\Controllers\Controller;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\ExecutiveRepository;
use App\Http\Requests\Mission\MissionProfilesRequest;
use App\Roipal\Strategies\Missions\MissionProfilesStrategy;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;

class ProfilingController extends Controller
{
    private $strategy;

    public function __construct(MissionProfilesStrategy $strategy)
    {
      $this->strategy = $strategy;
    }

    public function index(Request $request, Mission $mission)
    {
        $user = $request->user();
        $meta = $request->all();
        $profiles = $this->strategy->getAvailable($meta, $mission, $user);

        $data = [
            'message' => 'Profiles',
            'data' => $profiles
        ];

        return response()->json($data);
    }
}
