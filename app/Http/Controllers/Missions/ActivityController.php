<?php
namespace App\Http\Controllers\Missions;

use Fractal;
use Illuminate\Http\Request;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Scopes\ActivityScope;
use App\Roipal\Eloquent\MissionActivity;
use App\Roipal\Repositories\ActivityRepository;
use App\Roipal\Transformers\MissionActivityTransformer;
use App\Http\Controllers\Controller;
use App\Http\Requests\MissionActivities\MissionActivityStoreRequest;

class ActivityController extends Controller
{
    protected $repository;

    protected $scope;

    public function __construct(ActivityRepository $repository, ActivityScope $scope)
    {
        $this->repository = $repository;
        $this->scope = $scope;
    }

    public function index(Request $request, Executive $executive, Mission $mission)
    {
        Fractal::with($request->input('includes') ?? '');

        $missionActivity = $this->repository->index($request->all(), $mission, $executive);

        $data = [
            'message' => 'Activities',
        ] + Fractal::simplePaginate($missionActivity, new MissionActivityTransformer, 'data');

        return response()->json($data); 
    }

    public function show(Request $request)
    {
        $missionActivity = new MissionActivity();

        $user = $request->user();

        $this->scope->setUser($user);

        Fractal::with($request->input('includes') ?? '');

        $includes = $request->input('includes');
        $includes = explode(',', $includes);

        $activityIncludes = $missionActivity->getRelationshipsToBeLoaded($includes);

        array_add($request, 'relationships', $activityIncludes);

        $activity = $this->repository->show($request->all(), $this->scope);

        $data = [
            'message' => 'Activities',
        ] + Fractal::simplePaginate($activity, new MissionActivityTransformer, 'data');

        return response()->json($data);
    }

    public function store(MissionActivityStoreRequest $request, Executive $executive, Mission $mission)
    {
        $missionActivity = $this->repository->store($request->all(), $executive, $mission);

        $includes = $request->includes;

        Fractal::with($includes ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($missionActivity, new MissionActivityTransformer)
        ];

        return response()->json($data);
    }
}