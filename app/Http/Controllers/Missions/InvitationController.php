<?php
namespace App\Http\Controllers\Missions;

use Illuminate\Http\Request;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Support\Firebase;
use App\Http\Controllers\Controller;
use App\Roipal\Scopes\InvitationScope;
use App\Roipal\Constants\MissionStatus;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Eloquent\MissionInvitation;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Transformers\MissionTransformer;
use App\Roipal\Jobs\ProcessInvitationCanceledJob;
use App\Roipal\Jobs\CreateInvitationByNearProfileJob;
use App\Roipal\Transformers\MissionLocationTransformer;
use App\Roipal\Repositories\MissionInvitationRepository;
use App\Http\Requests\Invitations\InvitationStoreRequest;
use App\Roipal\Transformers\MissionInvitationTransformer;
use App\Roipal\Strategies\Missions\MissionInvitationStrategy;
use App\Roipal\Strategies\PushNotifications\MissionNotificationStrategy;
use App\Roipal\Strategies\PushNotifications\InvitationNotificationStrategy;

class InvitationController extends Controller
{
    protected $repository;
    protected $scope;
    protected $firebase;
    protected $strategy;
    public function __construct(Firebase $firebase,
        MissionInvitationRepository $repository,
        InvitationScope $scope,
        MissionInvitationStrategy $strategy) {
        $this->repository = $repository;
        $this->scope = $scope;
        $this->firebase = $firebase;
        $this->strategy = $strategy;
    }

    public function index(Request $request)
    {
        $missionInvitation = new MissionInvitation();

        $user = $request->user();

        $this->scope->setUser($user);

        $includes = $request->input('includes');
        $includes = explode(',', $includes);

        $invitationIncludes = $missionInvitation->getRelationshipsToBeLoaded($includes);

        array_add($request, 'relationships', $invitationIncludes);

        $invitation = $this->repository->index($request->all(), $this->scope);

        Fractal::With($request->input('includes') ?? '');

        $data = [
            'message' => 'Invitation',
        ] + Fractal::simplePaginate($invitation, new MissionInvitationTransformer, 'data');

        return response()->json($data);

    }

    public function store(InvitationStoreRequest $request, Mission $mission)
    {
        $data = $request->all();

        $locations = array_get($data, 'locations');

        foreach ($locations as $location) {
             $executives = array_get($location, 'executives_requested');
             $getLocation = $this->strategy->getUuidMissionLocation($location);

             $invitations = $getLocation->invitations->count();

             if ($executives <= $invitations) {
                return response()->json([
                    'message' => 'The required executives must be greater than the existing one in this location.',
                    'code' => 'EXECUTIVES_REQUESTED_MUST_BE_GREATER'
                ]);
             }

             $oldExecutives = $getLocation->executives_requested;
             
             $missionLocation = $this->repository->getMissionLocationByUuid($location, $mission, $oldExecutives);
             
             $newExecutives = $missionLocation->executives_requested;

             CreateInvitationByNearProfileJob::dispatch(
                 $missionLocation, 
                 $newExecutives - $oldExecutives,
                 [0])
                 ->onQueue('mission'); 
             
        } 
 
        Fractal::with($request->includes ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($mission, new MissionTransformer),
        ];
       

        return response()->json($data);
    }

    public function show(Request $request, MissionInvitation $invitation)
    {
        Fractal::with($request->includes ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($invitation, new MissionInvitationTransformer),
        ];

        return response()->json($data);
    }

    public function status(Request $request, MissionInvitation $invitation, MissionNotificationStrategy $notification)
    {
        $invitation = $this->repository->status($request->all(), $invitation);

        // User accepts the mission
        if ($invitation->status == MissionStatus::STARTED) {
            $message = 'Accepted';

            $notification->notifyToCompanyWhenExecutiveAcceptasAnInvitation(
                $invitation->mission,
                $request->user()
            );
        }

        // User did not accept the mission
        if ($invitation->status == MissionStatus::CANCELED) {
            $message = 'Rejected';

            ProcessInvitationCanceledJob::dispatch($invitation)->onQueue('invitation');
            $notification->notifyToCompanyWhenExecutiveCanceledAnInvitation(
                $invitation->mission,
                $request->user()
            );
        }

        $response = [
            'message' => $message,
            'data' => Fractal::item($invitation, new MissionInvitationTransformer),
        ];

        return response()->json($response, 200);
    }
}
