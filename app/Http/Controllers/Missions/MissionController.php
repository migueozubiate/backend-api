<?php
namespace App\Http\Controllers\Missions;

use Illuminate\Http\Request;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Scopes\MissionScope;
use App\Http\Controllers\Controller;
use App\Roipal\Constants\MissionStatus;
use App\Roipal\Constants\ExecutiveStatus;
use App\Roipal\Jobs\FindMissionProfilesJob;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\MissionRepository;
use App\Roipal\Transformers\MissionTransformer;
use App\Http\Requests\Mission\MissionCreateRequest;
use App\Http\Requests\Mission\MissionStatusRequest;
use App\Roipal\Repositories\CompanyAssestmentRepository;
use App\Roipal\Strategies\Missions\MissionProfilesStrategy;
use App\Http\Requests\Executives\ExecutiveMissionStatusRequest;
use App\Roipal\Strategies\PushNotifications\CompanyNotificationStrategy;
use App\Roipal\Strategies\PushNotifications\MissionNotificationStrategy;

class MissionController extends Controller
{
    private $repository;
    private $scope;
    private $assessmentRepository;

    public function __construct(
        MissionRepository $repository,
        MissionScope $scope,
        CompanyAssestmentRepository $assessment
    ) {
        $this->repository = $repository;
        $this->scope = $scope;
        $this->assessmentRepository = $assessment;
    }

    public function index(Request $request)
    {
        $mission = new Mission();

        $user = $request->user();

        $this->scope->setUser($user);

        $includes = $request->input('includes');
        $includes = explode(',', $includes);

        $missionIncludes = $mission->getRelationshipsToBeLoaded($includes);

        array_add($request, 'relationships', $missionIncludes);

        $misions = $this->repository->paginate($request->all(), $this->scope);

        Fractal::with($request->input('includes') ?? '');

        $data = [
            'message' => 'Missions',
        ] + Fractal::simplePaginate($misions, new MissionTransformer, 'data');

        return response()->json($data);
    }

    public function store(
        MissionCreateRequest $request,
        Company $company,
        MissionProfilesStrategy $strategy
    ) {
        $user = $request->user();
        $data = $request->all();
        $locations = array_get($data, 'locations');

        foreach ($locations as $key => $location) {
            $numProfiles[] = array_get($location, 'executives_requested');
        }

        $includes = $request->includes;

        Fractal::with($includes ?? '');

        $mission = $this->repository->store($company, $user, $data);

        FindMissionProfilesJob::dispatch($mission, $numProfiles)->onQueue('mission');

        $this->assessmentRepository->store($mission);

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($mission, new MissionTransformer),
        ];

        return response()->json($data, 201);
    }

    public function show(Request $request, Company $company, Mission $mission)
    {
        Fractal::with($request->input('includes') ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($mission, new MissionTransformer),
        ];

        return response()->json($data);
    }

    public function status(
        MissionStatusRequest $request,
        Company $company,
        Mission $mission,
        MissionNotificationStrategy $notification
    ) {
        $message = $this->repository->status($request->all(), $company, $mission);

        if (MissionStatus::isAllowedStatusUpdate($mission->status)) {
            $error = [
                'message' => 'Mission not available for update',
                'code' => 'MISSION_INVALID_STATUS_UPDATE',
            ];

            return response()->json($error, 400);
        }

        if ($mission->status == MissionStatus::STARTED) {
            $notification->notifyToExecutivesWhenCompanyStartedMission($mission);
        }

        if ($mission->status == MissionStatus::CANCELED) {
            $notification->notifyToExecutivesWhenCompanyCanceledMission($mission);
        }

        if ($mission->status == MissionStatus::COMPLETED) {
            $notification->notifyToExecutivesWhenCompanyFinishedMission($mission);
        }

        Fractal::with($request->input('includes') ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($mission, new MissionTransformer),
        ];

        return response()->json($data);
    }

    public function rate(Request $request, Mission $mission, Executive $executive)
    {
        if (!MissionStatus::ratingIsAllowed($mission->status)) {
            return response()->json([
                'message' => 'Solo puedes calificar una mission cerrada o cancelada',
                'code' => 'RATING_NOT_ALLOWED',
            ], 400);
        }

        $mission = $this->repository->rateMission($request->all(), $mission, $executive);

        Fractal::with($request->input('includes') ?? '');

        $data = [
            'message' => 'La mission fue calificada correctamente',
            'data' => Fractal::item($mission, new MissionTransformer),
        ];

        return response()->json($data);
    }

    public function executiveStatus(
        ExecutiveMissionStatusRequest $request, 
        Executive $executive, 
        Mission $mission,
        MissionNotificationStrategy $notification)
    {
        $user = $request->user();

        if (MissionStatus::isAllowedStatusUpdateExecutive($mission->status)) {
            return response()->json([
                'message' => 'Mission not available for update',
                'code' => 'MISSION_INVALID_STATUS_UPDATE',
            ], 400);
        }

        if ($request->status == MissionStatus::REJECT) {
            if (!MissionStatus::isAllowedStatusRejectExecutive($mission->status)) {
                return response()->json([
                    'message' => 'Status not available for update',
                    'code' => 'INVALID_STATUS',
                ], 400);
            }
        }

        $executiveMission = $this->repository->executiveMissionStatus(
            $request->all(), 
            $executive, 
            $mission
        );

        if ($executiveMission->pivot->status == MissionStatus::CANCELED) {
            $notification->notifyWhenCanceledMission($mission, $user, $executive);
        }

        if ($executiveMission->pivot->status == MissionStatus::COMPLETED) {
            $notification->notifyWhenCompletedMission($mission, $user, $executive);
        }

        if ($executiveMission->pivot->status == MissionStatus::REJECT) {
            $notification->notifyExecutiveWhenCompanyRejected($mission, $executive);
        }

        Fractal::with($request->input('includes') ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($mission, new MissionTransformer),
        ];

        return response()->json($data, 200);
    }
}
