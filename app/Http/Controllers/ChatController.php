<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Roipal\Constants\ChatApp;
use App\Roipal\Repositories\ChatRepository;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Transformers\ChatTransformer;
use App\Roipal\Strategies\PushNotifications\ChatNotificationStrategy;


class ChatController extends Controller
{
    private $chatRepository;

    public function __construct(ChatRepository $repository)
    {
        $this->chatRepository = $repository;
    }

    public function getHistoryConversation(Request $request)
    {
        $getConversation = $this->chatRepository->getHistoryConversation($request->all());

        $data = [
           'data' =>  Fractal::collection($getConversation, new ChatTransformer)
        ];

        return response()->json($data);    
    }

    public function notification(Request $request, ChatNotificationStrategy $notification)
    {
        $app = $request->app;

        if ($app == ChatApp::BUSSINESS_APP) {
            $notification->sendExecutiveChatNotification($request->all());
        }

        if ($app == ChatApp::EXECUTIVE_APP) {
            $notification->sendBusinessChatNotification($request->all());
        }

        $data = [
            'message' => 'Success',
            'code' => 'SEND_CHAT_MESSAGE'
        ];
        
        return response()->json($data);
    }


}
