<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Roipal\Repositories\MatiRepository;

class MatiController extends Controller
{
    private $repository;

    public function __construct(MatiRepository $repository)
    {
        $this->repository = $repository;
    }
    
    public function verifyUser(Request $request)
    { 
        if ($request->eventName == "verification_completed") {
            
            $getResource = $this->getResourceUsetMati($request->resource);
        
            $userVerify = $this->repository->store($getResource);
                               
            return response()->json([],200);

        }
    }

    public function getResourceUsetMati($resource){
      
        $client = new Client();

        $getAccessToken = $client->request('POST', "https://api.getmati.com/oauth/token", [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Authorization' => 'Basic NWJlZjA0NWFhNjgxYzY1YTZkY2FiMGI4OlEwMzBHM1JENE0wSjVKWjNVM0s4RFFVVjAyTUYxN1Yx',
            ],
            'form_params' => [
                'grant_type' => 'client_credentials',
                'scope' => 'identity',
            ]
          ]);

        $getAccessToken = json_decode($getAccessToken->getBody()->getContents());
          
        $getResource = $client->request('GET', $resource, [
            'headers' => [ 
                'Authorization' => "Bearer {$getAccessToken->access_token}",
            ]
          ]); 

        $resourceRecived = json_decode($getResource->getBody()->getContents(), true);

        return $resourceRecived;
    }
}
