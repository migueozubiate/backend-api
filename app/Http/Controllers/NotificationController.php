<?php

namespace App\Http\Controllers;

use App\Roipal\Repositories\NotificationRepository;
use App\Roipal\Strategies\PushNotifications\NotificationTestStrategy;
use App\Roipal\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Xaamin\Fractal\Facades\Laravel\Fractal;

class NotificationController extends Controller
{
    protected $repository;

    public function __construct(NotificationRepository $repository)
    {
        $this->repository = $repository;

    }

    public function store(Request $request)
    {
        $user = $request->user();
        $userInformation = $this->repository->store($request->all(), $user);

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($userInformation, new UserTransformer),
        ];

        return response()->json($data);

    }

    public function sendNotificationTest(Request $request, NotificationTestStrategy $notification)
    {
        $notification->sendTestNotificationExcutive($request->all());
        $notification->sendTestNotificationBusiness($request->all());

        $data = [
            "message" => "Success",
            "data" => array_get($request, 'data'),
        ];

        return response()->json($data);
    }

    public function block(Request $request)
    {
        $user = $this->repository->deleteToken($request->all());

        if (!$user) {
            return response()->json([
                'message' => 'User not exists in database',
                'code' => 'USER_NOT_EXISTS'
            ]);
        }

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($user, new UserTransformer)
        ];

        return response()->json($data, 200);
    }
}
