<?php
namespace App\Http\Controllers\Companies;

use Fractal;
use Illuminate\Http\Request;
use App\Roipal\Eloquent\Company;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use App\Roipal\Strategies\TokenWithNewUser;
use App\Roipal\Strategies\StoreFileStrategy;
use App\Roipal\Transformers\UserTransformer;
use App\Roipal\Repositories\CompanyRepository;
use App\Roipal\Transformers\CompanyTransformer;
use App\Http\Requests\Companies\CompanyStoreRequest;
use App\Http\Requests\Companies\CompanyUpdateRequest;
use App\Roipal\Transformers\UserChecklistTransformer;

class CompanyController extends Controller
{
    use TokenWithNewUser;

    protected $repository;

    public function __construct(CompanyRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $company = Company::all();

        Fractal::with($request->input('includes') ?? '');

        $data = [
            'message' => 'Companies',
        ] + Fractal::collection($company, new CompanyTransformer, 'data');

        return response()->json($data, 200);
    }

    public function show(Request $request, Company $company)
    {
        $includes = array_get($request, 'includes');

        $companyUsers = $this->repository->show($request->all(), $company);

        Fractal::with($includes ?? '');

        $data = [
            'message' => 'Success',
        ] + Fractal::collection($companyUsers, new UserTransformer, 'data');

        return response()->json($data, 200);
    }

    public function store(CompanyStoreRequest $request)
    {
        $company = $this->repository->store($request->all());

        $user = $company->owner;

        event(new Registered($user));

        $token = $this->geTokenFromNewUser($user, $request->get('password'), ['company']);

        $data = [
            'message' => 'Success',
            'data' => [
                'token' => $token,
                'profile' => Fractal::item($user, new UserTransformer),
                'checklist' => Fractal::collection($user->checklist, new UserChecklistTransformer),
                'company' => Fractal::item($company, new CompanyTransformer),
            ],
        ];

        return response()->json($data, 201);
    }

    public function update(CompanyUpdateRequest $request, Company $company, StoreFileStrategy $storeFiles)
    {
        $result = $this->repository->update($request->all(), $company);

        Fractal::with($request->input('includes') ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($result, new CompanyTransformer),
        ];

        return response()->json($data);
    }

    public function info(Request $request, Company $company)
    {
        $includes = array_get($request, 'includes');

        Fractal::with($includes ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($company, new CompanyTransformer)
        ];

        return response()->json($data, 200);
    }
}
