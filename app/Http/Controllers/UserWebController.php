<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Mail\RoipalWebMail;
use Illuminate\Http\Request;
use App\Roipal\Eloquent\UserWeb;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Cache;
use App\Http\Requests\UserWebStoreRequest;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\UserWebRepository;
use App\Roipal\Transformers\UserWebTransformer;

class UserWebController extends Controller
{
    protected $repository;

    public function __construct(UserWebRepository $repository)
    {
        $this->repository = $repository;
    }

    public function store(UserWebStoreRequest $request)
    {
        $userWeb = $this->repository->store($request->all());

        $expiresAt = Carbon::now()->addMinutes(30);

        Cache::put($userWeb->uuid, $userWeb->email, $expiresAt);

            Mail::to($userWeb->email)
            ->send(new RoipalWebMail($userWeb));

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($userWeb, new UserWebTransformer)
        ];

        return response()->json($data);
    }
}
