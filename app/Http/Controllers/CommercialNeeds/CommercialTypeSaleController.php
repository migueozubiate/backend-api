<?php

namespace App\Http\Controllers\CommercialNeeds;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Roipal\Eloquent\CommercialNeed;
use App\Roipal\Eloquent\CommercialTypeSale;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\CommercialNeedRepository;
use App\Roipal\Transformers\CommercialNeeds\CommercialTypeSalesTransformer;

class CommercialTypeSaleController extends Controller
{
    private $repository;

    public function __construct(CommercialNeedRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request, CommercialNeed $commercialNeed)
    {
        $includes = $request->includes;

        $typeSales = $this->repository->getTypeSalesByCommercialNeedUuid(
            $request->all(), 
            $commercialNeed
        );

        Fractal::with($includes ?? '');

        $data = [
            'message' => 'Success',
        ] + Fractal::simplePaginate($typeSales, new CommercialTypeSalesTransformer, 'data');

        return response()->json($data, 200);
    }

    public function getTypeSale(Request $request, CommercialNeed $commercialNeed, CommercialTypeSale $typeSale)
    {
        $getTypeSale = $this->repository->getTypeSale($request->all(), $typeSale);

        Fractal::with($request->includes ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($getTypeSale, new CommercialTypeSalesTransformer)
        ];

        return response()->json($data, 200);
    }
}
