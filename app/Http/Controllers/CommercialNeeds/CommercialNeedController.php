<?php

namespace App\Http\Controllers\CommercialNeeds;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Roipal\Eloquent\CommercialNeed;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Repositories\CommercialNeedRepository;
use App\Roipal\Transformers\CommercialNeeds\CommercialNeedTransformer;

class CommercialNeedController extends Controller
{
    private $repository;

    public function __construct(CommercialNeedRepository $repository)
    {
        $this->repository = $repository;
    }

    public function index(Request $request)
    {
        $includes = $request->includes;

        $commercialNeed = $this->repository->getCommercialNeeds($request->all());

        Fractal::with($includes ?? '');

        $data = [
            'message' => 'Success',
        ] + Fractal::simplePaginate($commercialNeed, new CommercialNeedTransformer, 'data');

        return response()->json($data, 200);
    }

    public function show(Request $request, CommercialNeed $commercialNeed)
    {
        $includes = $request->includes;

        Fractal::with($includes ?? '');

        $data = [
            'message' => 'Success',
            'data' => Fractal::item($commercialNeed, new CommercialNeedTransformer)
        ];

        return response()->json($data, 200);
    }
}
