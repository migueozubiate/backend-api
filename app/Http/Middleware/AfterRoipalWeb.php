<?php

namespace App\Http\Middleware;

use Closure;
use App\Roipal\Eloquent\UserWeb;

class AfterRoipalWeb
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        \App::setLocale($request->language);
        
        $userEmail = $request->email;
        $emailExist = UserWeb::whereEmail($userEmail)
                            ->exists();
        
        if ($emailExist) {
            return response()->json([
                'message' => trans('validation.conflict409'),
                'code' => 409,
            ], 409);
        }

        return $next($request);
    }
}
