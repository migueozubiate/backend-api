<?php

namespace App\Http\Middleware;

use Closure;
use Lcobucci\JWT\Parser;
use Laravel\Passport\Token;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\TokenDetectionException;

class TokenDetector
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $bearerToken = $request->bearerToken();
        
        if ($bearerToken) {
            $tokenId = (new Parser())->parse($bearerToken)->getHeader('jti');
            $token = Token::find($tokenId);

            if (!$token->user) {
                $token->revoke();

                throw new TokenDetectionException('SESSION_CLOSED');
            }

            if ($token->revoked) {
                throw new TokenDetectionException('DUPLICATE_SESSION');
            }
        }
        
        return $next($request);
    }
}
