<?php
namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Application;

class TranslationMiddleware
{
    protected $app;

    /**
     *  Constructor
     *
     *  @param  Illuminate\Foundation\Application                   $app
     */
    public function __construct(Application $app)
    {
        $this->app                = $app;
    }

    /**
     * Handle an incoming request.
     *
     *  @param  \Illuminate\Http\Request  $request
     *  @param  \Closure  $next
     *  @param  integer $segment     Index of the segment containing locale info
     *  @return mixed
     */
    public function handle($request, Closure $next, $segment = 0)
    {
        $locale = $request->header('x-preferred-language');

        // Set app locale
        if (!$locale) {
            $this->app->setLocale('en');
        } else {
            $this->app->setLocale($locale);
        }

        return $next($request);
    }
}