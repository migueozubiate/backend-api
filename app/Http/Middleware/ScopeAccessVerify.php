<?php

namespace App\Http\Middleware;

use Closure;
use App\Roipal\Eloquent\User;
use Illuminate\Support\Facades\Hash;

class ScopeAccessVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userEmail = $request->username;
        $user = User::whereEmail($userEmail)->first();

        if (!$user or !Hash::check($request->password, $user->password)) {
            return response()->json([
                'message' => trans('validation.incorrect_credentials'),
                'code' => 401,
            ], 400);
        }

        if ($request->scope != $user->classification) {
            return response()->json([
                'message' => trans('validation.incorrect_scope'),
                'code' => 401,
            ], 400);
        }

        return $next($request);
    }
}
