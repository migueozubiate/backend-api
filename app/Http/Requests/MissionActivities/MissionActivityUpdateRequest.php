<?php

namespace App\Http\Requests\MissionActivities;

use Illuminate\Foundation\Http\FormRequest;

class MissionActivityUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'activity.activity' => ['required', 'string'],
            'activity.place' => ['required', 'string'],
            'activity.location' => ['required','array'],
            'activity.location.latitude' => ['required', 'numeric', 'between:-180.00,180.00'],
            'activity.location.longitude' => ['required', 'numeric', 'between:-180.00,180.00']
        ];
    }
}
