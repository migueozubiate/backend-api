<?php

namespace App\Http\Requests\MissionActivities;

use App\Roipal\Eloquent\MissionActivity;
use Illuminate\Foundation\Http\FormRequest;

class MissionActivityStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * TODO
         *
         * Temporarily allow it, remove it
         */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //activity
            'title' => ['required', 'string'],
            'description' => ['required', 'string']
        ];
    }
}