<?php

namespace App\Http\Requests\MissionActivities;

use Illuminate\Foundation\Http\FormRequest;

class ActivityHistoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => ['required_without:images', 'string'],
            'comment' => ['string'],
            'images' => ['string']
        ];
    }
}
