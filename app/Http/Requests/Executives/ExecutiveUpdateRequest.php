<?php
namespace App\Http\Requests\Executives;

use Illuminate\Foundation\Http\FormRequest;

class ExecutiveUpdateRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $regex = '/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/';
        return [
            'password' => ['filled', 'string', 'min:8'],
            'video_bio_url' => ['filled', 'string'],
            'photo_bio_url' => ['filled', 'string'],

            'bio' => ['filled', 'string'],
            'website' => ['filled', 'url'],
            'address' => ['filled', 'string'],
            'phone' => ['filled', 'numeric'],
            'country' => ['string'],
            'position' => ['sometimes', 'array', 'size:2'],
            'position.latitude' => ['required_with:position', 'numeric', 'between:-90.00,90.00'],
            'position.longitude' => ['required_with:position', 'numeric', 'between:-180.00,180.00'],

            'limitations' => ['nullable', 'array'],
            'limitations.*' => ['string'],

            'payment' => ['array'],
            'payment.owner' => ['required_with:payment', 'string'],
            'payment.type_payment' => ['required_with:payment','string'],
            'payment.card_number' => ['string'],
            'payment.type_card' => ['string'],
            'payment.clabe' => ['string'],
            'payment.email' => ['email'],
            'payment.bank' => ['string']
        ];
    }

}
