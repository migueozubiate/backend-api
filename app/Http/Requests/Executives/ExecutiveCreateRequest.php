<?php
namespace App\Http\Requests\Executives;

use App\Roipal\Eloquent\ExecutiveProfile;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Foundation\Http\FormRequest;

class ExecutiveCreateRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:8',
            'terms_accepted' => 'accepted',
        ];
    }

    /**
     * @return ExecutiveProfile
     */
    public function getExecutiveProfile()
    {
        $executive_profile = new ExecutiveProfile();
        $executive_profile->forceFill($this->only([
            'video_bio', 'video_bio_url', 'bio', 'website', 'terms_accepted', 'address'
        ]));

        $position = $this->get('position');
        $executive_profile->position = new Point($position['latitude'], $position['longitude']);

        return $executive_profile;
    }
}
