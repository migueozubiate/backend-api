<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'address' => ['required', 'string'],
            'position' => ['required', 'array'],
            'position.latitude' => ['required', 'numeric', 'between:-90.00,90.00'],
            'position.longitude' => ['required', 'numeric', 'between:-180.00,180.00'],
        ];
    }
}
