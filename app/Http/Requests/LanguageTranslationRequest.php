<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LanguageTranslationRequest extends FormRequest
{
    
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'namespace' => ['required', 'string'],
            'group' => ['required', 'string'],
            'item' => ['required', 'string'],
            'text' => ['required', 'string'],
        ];
    }
}
