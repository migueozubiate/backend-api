<?php

namespace App\Http\Requests\Mission;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MissionLocationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'locations' => ['required', 'array'],
            'locations.*.uuid' => ['required_without:locations.*.name',
                        'required_without:locations.*.address',
                        'required_without:locations.*.latitude',
                        'required_without:locations.*.longitude',
                        'uuid'],
            'locations.*.name' => ['required_without:locations.*.uuid', 'string'],
            'locations.*.address' => ['required_without:locations.*.uuid', 'string'],
            'locations.*.latitude' => ['required_without:locations.*.uuid', 'numeric'],
            'locations.*.longitude' => ['required_without:locations.*.uuid', 'numeric'],
        ];
    }
}
