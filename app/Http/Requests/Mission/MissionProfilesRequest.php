<?php
namespace App\Http\Requests\Mission;

use Illuminate\Foundation\Http\FormRequest;

class MissionProfilesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mission_type' => [
                'required', 'string'
            ],
            'profile' => [
                'required', 'string'
            ],
            'locations' => [
                'required', 'array', 'min:1'
            ],
            'locations.*.uuid' => [
                'required', 'string', 'min:5'
            ],
            'locations.*.address' => [
                'required', 'string', 'min:5'
            ],
            'locations.*.latitude' => [
                'required', 'numeric'
            ],
            'locations.*.longitude' => [
                'required', 'numeric'
            ],
            'locations.*.executives_requested' => [
                'required', 'integer'
            ]
        ];
    }
}
