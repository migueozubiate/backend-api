<?php

namespace App\Http\Requests\Mission;

use App\Roipal\Eloquent\Mission;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MissionCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'date' => ['required', 'date'],
            'description' => ['required', 'string'],
            'type' => ['required', 'string'],
            'commercial_need' => ['required', 'string'],
            'profile' => ['required', 'string'],
            'time' => 'required|numeric',
            'time_unit' => 'required|in:hours,horas,heures,days',
            'fare_by_time' => 'required|numeric|regex:/^\d*(\.\d{1,2})?$/',
            'subtotal' => ['required', 'numeric', 'min: 0.01'],
            'taxes' => ['required', 'numeric'],
            'total' => ['required', 'numeric', 'min: 0.01'],
            'currency' => 'required',
            'suggested' => ['required', 'array'],
            'suggested.profile' => ['required', 'string'],
            'suggested.time' => ['required', 'numeric', 'min:1'],
            'suggested.time_unit' => ['required', 'string', 'min:1'],
            'suggested.min_time' => ['required', 'numeric', 'min:1'],
            'suggested.max_time' => ['required', 'numeric', 'min:1'],
            'suggested.fare_by_time' => ['required', 'numeric', 'min:1'],
            'suggested.estimations.*.min' => ['required', 'numeric', 'min:1'],
            'suggested.estimations.*.max' => ['required', 'numeric', 'min:1'],
            'suggested.estimations.*.fare' => ['required', 'numeric', 'min:1'],

            'locations' => ['required', 'array'],
            'locations.*.uuid' => ['required_without:locations.*.name',
                'required_without:locations.*.address',
                'required_without:locations.*.latitude',
                'required_without:locations.*.longitude',
                'uuid'],
            'locations.*.name' => ['required_without:locations.*.uuid', 'string'],
            'locations.*.address' => ['required_without:locations.*.uuid', 'string'],
            'locations.*.latitude' => ['required_without:locations.*.uuid', 'numeric'],
            'locations.*.longitude' => ['required_without:locations.*.uuid', 'numeric'],
            'locations.*.executives_requested' => ['required', 'numeric', 'min:1'],

            'type_sale' => ['required', 'array'],
            'type_sale.uuid' => ['required_without:type_sale', 'uuid'],
        ];
    }
}
