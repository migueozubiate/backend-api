<?php

namespace App\Http\Requests\Assestment;

use Illuminate\Foundation\Http\FormRequest;

class AssessmentUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'questions' => ['required', 'array'],
            'questions.*.number' => ['required_with:questions', 'numeric'],
            'questions.*.question' => ['string'],
            'questions.*.options' => ['required_with:questions', 'array'],
            'questions.*.options.A' => ['required_with:questions.*.options', 'string'],
            'questions.*.options.B' => ['required_with:questions.*.options', 'string'],
            'questions.*.options.C' => ['required_with:questions.*.options', 'string'],
            'questions.*.options.D' => ['required_with:questions.*.options', 'string'],
        ];
    }
}
