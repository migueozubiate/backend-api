<?php
namespace App\Http\Requests\Assestment;

use App\Roipal\Eloquent\Assestment;
use Illuminate\Foundation\Http\FormRequest;

class ExecutiveAssestmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * TODO
         *
         * Temporarily allow it, remove it
         */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $disc = Assestment::where('name', '=', $this->type)->firstOrFail();;

        return [
            'answers' => ['required', 'array'],
            'answers.*.number' => ['required', 'numeric'],
            'answers.*.answer' => ['required', 'string'] //  'size:4'
        ];
    }
}
