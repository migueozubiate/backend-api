<?php

namespace App\Http\Requests\Assestment;

use Illuminate\Foundation\Http\FormRequest;

class CompanyAssestmentStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "assesment.name" => [
                'required', 'string', 'min:5'
            ],
            "assesment.summary" => [
                'required', 'string', 'min:10'
            ],
            "assesment.questions" => [
                'required', 'array'
            ],
            "assesment.questions.*.number" => [
                'required', 'integer', 'min:1'
            ],
            "assesment.questions.*.question" => [
                'required', 'string', 'min:5'
            ],
            "assesment.questions.*.options" => [
                'required', 'array', 'min:2'
            ],
            "assesment.questions.*.answer" => [
                'required', 'string', 'min:1'
            ],
        ];
    }
}
