<?php

namespace App\Http\Requests\Companies;

use Illuminate\Foundation\Http\FormRequest;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * TODO
         *
         * Temporarily allow it, remove it
         */
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //Company
            'business_name' => ['string'],
            'name' => ['string'],
            'vertical' => ['string'],

            //user profile
            'user_profile' => ['array'],
            'user_profile.name' => ['string'],
            'user_profile.avatar' => ['string'],
            'user_profile.password' => ['string', 'min:8'],
            'user_profile.new_password' => ['required_with:user_profile.password', 'string', 'min:8'],

            //profile
            'profile' => ['array'],
            'profile.video_bio_url' => ['required_with:profile.video_bio', 'string'],
            'profile.bio' => ['required_with:profile', 'string'],
            'profile.website' => ['required_with:profile', 'string'],
            'profile.vertical' => ['required_with:profile', 'string'],
            'profile.address' => ['required_with:profile', 'string'],
            'profile.country' => ['required_with:profile', 'string'],
            'profile.position' => ['required_with:profile', 'array'],
            'profile.position.latitude' => ['required_with:profile.position.longitude', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'profile.position.longitude' => ['required_with:profile.position.latitude', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'],

            //company payment profile
            'payment' => ['array'],
            'payment.owner' => ['required_with:payment', 'string'],
            'payment.card_number' => ['required_with:payment', 'string'],
            'payment.exp' => ['required_with:payment', 'regex:/^[0-8]{2}\/[0-9]{2}$/'],
            'payment.ccv' => ['required_with:payment', 'string', 'regex:/^[0-9]{3,4}$/'],

            //company contact profile
            'contact' => ['array'],
            'contact.name' => ['required_with:contact', 'filled', 'string'],
            'contact.phone' => ['required_with:contact', 'filled', 'numeric'],
            'contact.job_title' => ['required_with:contact', 'filled', 'string']
        ];
    }
}