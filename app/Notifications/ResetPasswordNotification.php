<?php

namespace App\Notifications;

use config;
use Illuminate\Bus\Queueable;
use Illuminate\Support\Facades\Lang;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPasswordNotification extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $token;

    public function __construct($token)
    {
       $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
        ->subject(Lang::getFromJson(trans("mail.reset_password_subject")))
        ->line(Lang::getFromJson(trans("mail.reset_password_first_line")))
        ->action(Lang::getFromJson(trans("mail.reset_password_action")), url(config('app.url').route('password.reset', $this->token, false)))
        ->line(Lang::getFromJson(trans("mail.reset_password_second_line"), ['count' => config('auth.passwords.users.expire')]))
        ->line(Lang::getFromJson(trans("mail.reset_password_thrid_line")));

    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
