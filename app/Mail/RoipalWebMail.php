<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RoipalWebMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    private $userWeb;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($userWeb)
    {
        $this->userWeb = $userWeb;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $language = $this->userWeb->language;

        if ($language === 'es') {
            return $this->view('emails.roipal_web_es');
        }

        if ($language === 'en') {
            return $this->view('emails.roipal_web_en');
        }

        if($language === 'fr') {
            return $this->view('emails.roipal_web_fr');
        }
    }
}
