<?php
namespace App\Providers;

use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Employeer;
use App\Roipal\Eloquent\LineBusiness;
use App\Roipal\Observers\UserObserver;
use App\Roipal\Eloquent\CommercialNeed;
use App\Roipal\Eloquent\CompanyProfile;
use Illuminate\Support\ServiceProvider;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Eloquent\ExecutiveMission;
use App\Roipal\Eloquent\ExecutiveProfile;
use App\Roipal\Observers\CompanyObserver;
use App\Roipal\Observers\MessageObserver;
use App\Roipal\Eloquent\MissionInvitation;
use App\Roipal\Eloquent\MissionSuggestion;
use App\Roipal\Eloquent\CommercialTypeSale;
use App\Roipal\Eloquent\ExecutiveAssestment;
use App\Roipal\Observers\InvitationObserver;
use App\Roipal\Observers\LineBusinessObserver;
use Laravel\Passport\Client as PassportClient;
use App\Roipal\Observers\CommercialNeedObserver;
use App\Roipal\Observers\CompanyProfileObserver;
use App\Roipal\Observers\PassportClientObserver;
use App\Roipal\Observers\MissionLocationObserver;
use App\Roipal\Observers\ExecutiveMissionObserver;
use App\Roipal\Observers\ExecutiveProfileObserver;
use App\Roipal\Observers\MissionSuggestionObserver;
use App\Roipal\Observers\AssestmentQuestionObserver;
use App\Roipal\Observers\CommercialTypeSaleObserver;
use App\Roipal\Observers\ExecutiveAssestmentObserver;
use App\Roipal\Observers\AssestmentAnswersPollObserver;
use App\Roipal\Observers\ExecutiveAssestmentAnswerObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe(UserObserver::class);
        Employeer::observe(UserObserver::class);
        ExecutiveAssestment::observe(ExecutiveAssestmentObserver::class);
        PassportClient::observe(PassportClientObserver::class);
        Company::observe(CompanyObserver::class);
        MissionInvitation::observe(InvitationObserver::class);
        MissionLocation::observe(MissionLocationObserver::class);
        CompanyProfile::observe(CompanyProfileObserver::class);
        ExecutiveProfile::observe(ExecutiveProfileObserver::class);
        MissionSuggestion::observe(MissionSuggestionObserver::class);
        LineBusiness::observe(LineBusinessObserver::class);
        ExecutiveMission::observe(ExecutiveMissionObserver::class);
        CommercialNeed::observe(CommercialNeedObserver::class);
        CommercialTypeSale::observe(CommercialTypeSaleObserver::class);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
