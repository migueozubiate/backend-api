<?php
namespace App\Providers;

use Xaamin\Fractal\Transformer;
use Illuminate\Support\ServiceProvider;
use App\Roipal\Fractal\ArraySerializer;

class FractalServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {

    }
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Xaamin\Fractal\Transformer', function ($app) {
            return new Transformer(new ArraySerializer);
        });
    }
}