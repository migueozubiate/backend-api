<?php

namespace App\Roipal\Observers;

use Ramsey\Uuid\Uuid;
use App\Roipal\Eloquent\CommercialNeed;

class CommercialNeedObserver
{
    /**
     *
     * @param  CommercialNeed  $commercialNeed
     * @return void
     */
    public function creating(CommercialNeed $commercialNeed)
    {
        if (!$commercialNeed->uuid) {
            $commercialNeed->uuid = Uuid::uuid4()->toString();
        }
    }
}
