<?php
namespace App\Roipal\Observers;

use UUID;
use App\Roipal\Eloquent\ExecutiveAssestment;

class ExecutiveAssestmentObserver
{
    /**
     * Handle the assestment question "created" event.
     *
     * @param  ExecutiveAssestment  $answer
     * @return void
     */
    public function creating(ExecutiveAssestment $answer)
    {
        if (!$answer->uuid) {
            $answer->uuid = UUID::generate(4)->string;
        }
    }
}
