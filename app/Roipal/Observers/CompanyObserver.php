<?php
namespace App\Roipal\Observers;

use UUID;
use App\Roipal\Eloquent\Company;

class CompanyObserver
{
    /**
     * Handle the assestment question "created" event.
     *
     * @param  Company  $company
     * @return void
     */
    public function creating(Company $company)
    {
        if (!$company->uuid) {
            $company->uuid = UUID::generate(4)->string;
        }
    }
}
