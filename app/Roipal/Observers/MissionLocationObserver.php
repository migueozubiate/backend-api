<?php
namespace App\Roipal\Observers;

use UUID;
use App\Roipal\Eloquent\MissionLocation;

class MissionLocationObserver
{
    /**
     * Handle the assestment question "created" event.
     *
     * @param  MissionLocation  $missionLocation
     * @return void
     */
    public function creating(MissionLocation $missionLocation)
    {
        if (!$missionLocation->uuid) {
            $missionLocation->uuid = UUID::generate(4)->string;
        }
    }
}
