<?php
namespace App\Roipal\Observers;

use UUID;
use App\Roipal\Eloquent\MissionSuggestion;

class MissionSuggestionObserver
{
    /**
     * Handle the assestment question "created" event.
     *
     * @param  MissionSuggestion  $suggestion
     * @return void
     */
    public function creating(MissionSuggestion $suggestion)
    {
        if (is_array($suggestion->estimations)) {
            $suggestion->estimations = json_encode($suggestion->estimations);
        }
    }
}
