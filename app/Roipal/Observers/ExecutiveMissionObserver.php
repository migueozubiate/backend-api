<?php
namespace App\Roipal\Observers;

use Ramsey\Uuid\Uuid;
use App\Roipal\Eloquent\ExecutiveMission;

class ExecutiveMissionObserver
{
    /**
     * Handle the assestment question "created" event.
     *
     * @param  Company  $company
     * @return void
     */
    public function creating(ExecutiveMission $executiveMission)
    {
        if (!$executiveMission->uuid) {
            $executiveMission->uuid = Uuid::uuid4()->toString();
        }
    }
}