<?php
/**
 * Created by PhpStorm.
 * User: genis
 * Date: 2018-11-30
 * Time: 13:46
 */

namespace App\Roipal\Observers;


use App\Roipal\Eloquent\ExecutiveProfile;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class ExecutiveProfileObserver
{
    public function saving(ExecutiveProfile $profile) {
        if(is_string($profile->position)) {
            // latitude longitude
            $profile->position = Point::fromPair($profile->position);
        }
    }

}