<?php
namespace App\Roipal\Observers;

use App\Roipal\Eloquent\CompanyProfile;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class CompanyProfileObserver
{
    /**
     * Handle the assestment question "created" event.
     *
     * @param  CompanyProfile  $client
     * @return void
     */
    public function saving(CompanyProfile $companyProfile)
    {
        if(is_string($companyProfile->position)) {
            $companyProfile->position = Point::fromPair($companyProfile->position);
        }
    }

    /**
     * Handle the assestment question "created" event.
     *
     * @param  CompanyProfile  $client
     * @return void
     */
    public function updating(CompanyProfile $companyProfile)
    {
        if(is_string($companyProfile->position)) {
            $companyProfile->position = Point::fromPair($companyProfile->position);
        }
    }
}
