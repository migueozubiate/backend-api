<?php
namespace App\Roipal\Observers;

use UUID;

class UserObserver
{
    /**
     * Handle the assestment question "created" event.
     *
     * @param mix $user
     * @return void
     * @throws \Exception
     */
    public function creating($user)
    {
        if (!$user->uuid) {
            $user->uuid = UUID::generate(4)->string;
        }
    }
}
