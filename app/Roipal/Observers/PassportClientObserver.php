<?php
namespace App\Roipal\Observers;

use UUID;
use Laravel\Passport\Client as PassportClient;

class PassportClientObserver
{
    /**
     * Handle the assestment question "created" event.
     *
     * @param  PassportClient  $client
     * @return void
     */
    public function creating(PassportClient $client)
    {
        $client->incrementing = false;
        $client->id = UUID::generate(4)->string;
    }
}
