<?php
namespace App\Roipal\Observers;

use Ramsey\Uuid\Uuid;
use App\Roipal\Eloquent\MissionInvitation;

class InvitationObserver
{
    /**
     * Handle the assestment question "created" event.
     *
     * @param MissionInvitation $invitation
     * @return void
     * @throws \Exception
     */
    public function creating(MissionInvitation $invitation)
    {
        if (!$invitation->uuid) {
            $invitation->uuid = UUID::uuid4()->toString();
        }
    }
}
