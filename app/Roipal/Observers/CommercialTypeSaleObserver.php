<?php

namespace App\Roipal\Observers;

use Ramsey\Uuid\Uuid;
use App\Roipal\Eloquent\CommercialTypeSale;

class CommercialTypeSaleObserver
{
    /**
     *
     * @param  CommercialTypeSale  $commercialType
     * @return void
     */
    public function creating(CommercialTypeSale $commercialType)
    {
        if (!$commercialType->uuid) {
            $commercialType->uuid = Uuid::uuid4()->toString();
        }
    }
}
