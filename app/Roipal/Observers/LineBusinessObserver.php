<?php

namespace App\Roipal\Observers;

use Ramsey\Uuid\Uuid;
use App\Roipal\Eloquent\LineBusiness;

class LineBusinessObserver
{
    public function creating(LineBusiness $lineBusiness)
    {
        if (!$lineBusiness->uuid) {
            $lineBusiness->uuid = Uuid::uuid4()->toString();
        }
    }
}
