<?php

namespace App\Roipal\Traits;

trait AssessmentDiscDescriptionTrait
{
    public function getDiscDecription(array $getProfile)
    {
        $profile = implode("-", $getProfile);

        $description = $this->descriptionByProfile();
        $profileDescription = $description[$profile];

        return $profileDescription;
    }

    public function descriptionByProfile()
    {
        return [
            'C' => [
                'profile' => 'Calculator',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Habilidad para realizar bien a la primera las tareas difíciles.',
                    'Es sensible a los errores cuando se requiere precisión y exactitud.',
                    'Cuando trabaja en su especialización, adopta un enfoque profesional y disciplinado.',
                    'Posee habilidades organizativas y gestiona bien el tiempo.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Fuera menos perfeccionista.' ,
                    'Se olvidara más de los “manuales".', 
                    'Se fijara menos en los detalles y fuera más entusiasta.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Precisión y Calidad.',
                    'search_in_others' => 'Busca en los demás: Los resultados correctos, la presentación de pruebas y hechos.',
                    'influence' => 'Influencia a los demás por: El uso de datos y su exactitud.',
                    'contribution_company' => 'Lo que aporta a la empresa: Elevados estándares para él/ella y su equipo; muy disciplinado/a.',
                    'excessive' => 'Excesivo con: Las reglas y normas.',
                    'stress' => 'Bajo estrés: Se vuelve excesivamente crítico/a consigo mismo/a y con los demás.',
                    'fears' => 'Temores: Las decisiones de elevado riesgo.'
                ]
            ],
            'C-F' => [
                'profile' => 'Calculator y Farmer',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Habilidad para establecer y alcanzar elevados estándares de comportamiento y de trabajo.',
                    'Sensible a los problemas, normas, errores y procedimientos.',
                    'Habilidad para tomar decisiones difíciles sin dejar que interfieran las emociones.',
                    'Habilidad para comprender y preservar la necesidad de los sistemas de calidad.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Reflejara sus verdaderos sentimientos.',
                    'Se preocupara menos de la incidencia del cambio en las relaciones y en la calidad.', 
                    'Tuviera más confianza e interdependencia.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Seguridad y destreza.',
                    'search_in_others' => 'Busca en los demás: La precisión que tienen en los estándares.',
                    'influence' => 'Influencia a los demás por: Su responsabilidad y atención a los detalles.',
                    'contribution_company' => 'Lo que aporta a la empresa: Perseverancia; mantiene los estándares.',
                    'excessive' => 'Excesivo con: Dependencia de los procedimientos',
                    'stress' => 'Bajo estrés: Se vuelve introvertido/a y obstinado/a',
                    'fears' => 'Temores: El antagonismo'
                ]
            ],
            'C-H' => [
                'profile' => 'Calculator y Hunter',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Habilidad para hacer un trabajo de calidad, a la vez que explora nuevas formas para incrementar la calidad.',
                    'Habilidad para tomar decisiones difíciles utilizando la reflexión y los hechos y sin dejarse llevar por las emociones.',
                    'Habilidad para persistir hasta encontrar soluciones válidas y aceptables a los problemas.',
                    'Reta al equipo a conseguir mejores resultados y espera lo mejor.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Fuera más sensible a los sentimientos de los demás.',
                    'Fuera menos directo/a.', 
                    'Se manifestara más abiertamente.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Diseño de sistemas.',
                    'search_in_others' => 'Busca en los demás: Sus propios elevados estándares.',
                    'influence' => 'Influencia a los demás por: Su capacidad para avanzar en el desarrollo de sistemas.',
                    'contribution_company' => 'Lo que aporta a la empresa: Precisión; Trabajador/a perseverante',
                    'excessive' => 'Excesivo con: Los hechos, números y datos.',
                    'stress' => 'Bajo estrés: Pierde la calma con facilidad.',
                    'fears' => 'Temores: Desorganización.'
                ]
            ],
            'C-P' => [
                'profile' => 'Calculator y Promotor',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Promotor/a de Sistemas de Calidad.',
                    'Equilibra el sentido de urgencia con el mantenimiento de elevados estándares.',
                    'Organizado/a incluso en sus relaciones. Aprecia la compañía de las personas que poseen ideas similares.',
                    'Es sensible al cambio en el entorno laboral y socia.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Aceptara más las ideas y creencias de los demás.',
                    'Estableciera objetivos realistas.', 
                    'No se dejara influenciar tanto por los comentarios de los demás.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Diplomacia.',
                    'search_in_others' => 'Busca en los demás: La gente a la que conocen, el prestigio y los logros que alcanzan.',
                    'influence' => 'Influencia a los demás por: Las buenas relaciones que mantiene.',
                    'contribution_company' => 'Lo que aporta a la empresa: Crea un buen ambiente de trabajo.',
                    'excessive' => 'Excesivo con:  El tacto.',
                    'stress' => 'Bajo estrés: Se vuelve demasiado adulador/a.',
                    'fears' => 'Temores: Tener que renunciar a la calidad para mantener buenas relaciones.'
                ]
            ],
            'H' => [
                'profile' => 'Hunter',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Habilidad para hacer frente a los problemas que implican muchos factores.',
                    'Orientado/a al futuro, agresivo/a y competitivo/a.',
                    'Habilidad para trabajar en un entorno variado y con cambios frecuentes.',
                    'Promueve las actividades y establece el ritmo necesario para conseguir los resultados deseados.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Fuera menos nervioso/a, testarudo/a y directo/a.',
                    'No forzara el compromiso de los demás hacia un proyecto.', 
                    'Fuera más paciente y humilde y se preocupara más de la gente.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Dominancia e independencia.',
                    'search_in_others' => 'Busca en los demás: La habilidad que tienen para hacer las cosas con rapidez.',
                    'influence' => 'Influencia a los demás por: Su persistencia y fuerza de carácter.',
                    'contribution_company' => 'Lo que aporta a la empresa: Muestra a los demás su actitud.',
                    'excessive' => 'Excesivo con: El reto y la competitividad.',
                    'stress' => 'Bajo estrés: Se vuelve callado/a y analítico/a.',
                    'fears' => 'Temores: Perder el control '
                ]
            ],
            'H-P' => [
                'profile' => 'Hunter y Promotor',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Orientado/a a resultados y con sentido de urgencia para conseguir los objetivos y cumplir los plazos.',
                    'Decidido/a y agresivo/a ante los retos.',
                    'Promueve actividades a través de los demás para conseguir los resultados.',
                    'Busca activamente la relación con gran variedad de personas.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'No se irritara tanto cuando no se cumplen los plazos.',
                    'No asumiera tantas responsabilidades al mismo tiempo .', 
                    'Hiciera un mayor seguimiento y no tuviera expectativas tan elevadas.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Seguridad para ganar.',
                    'search_in_others' => 'Busca en los demás: La habilidad que tienen para comunicarse y pensar.',
                    'influence' => 'Influencia a los demás por: Su amabilidad y su deseo de conseguir resultados.',
                    'contribution_company' => 'Lo que aporta a la empresa: Buen/a planificador/a, capacidad para resolver problemas y buscar recursos.',
                    'excessive' => 'Excesivo con: Su posición y su propio estilo.',
                    'stress' => 'Bajo estrés: Se vuelve nervioso/a, impaciente e insensible.',
                    'fears' => 'Temores: Perder y equivocarse.'
                ]
            ],
            'H-C' => [
                'profile' => 'Hunter y Calculator',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Establece estándares elevados para sí mismo/a y para los demás desde un punto de vista de desempeño y trabajo en equipo.',
                    'Se preocupa por el coste de los errores y equivocaciones.',
                    'Estructurado/a en el uso del tiempo.',
                    'Resuelve los problemas sistemáticamente sin dejar que las emociones le influencien.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Fuera más acogedor/a y apreciara más a los miembros del equipo.',
                    'Equilibrara más los factores de calidad y cantidad en sus decisiones.', 
                    'Fuera menos directo/a y crítico/a con las personas que no cumplen sus estándares.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Dominancia y promover proyectos, ideas y/o situaciones.',
                    'search_in_others' => 'Busca en los demás: Sus propios estándares y las ideas progresistas que aportan.',
                    'influence' => 'Influencia a los demás por: Su competitividad y su capacidad de asumir retos.',
                    'contribution_company' => 'Lo que aporta a la empresa: Promueve el cambio.',
                    'excessive' => 'Excesivo con: La franqueza y la crítica.',
                    'stress' => 'Bajo estrés: Se vuelve autoritario/a y exigente.',
                    'fears' => 'Temores: No ser influyente.'
                ]
            ],
            'H-F' => [
                'profile' => 'Hunter y Farmer',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Habilidad para generar nuevas ideas y llevarlas a la práctica hasta su finalización.',
                    'Aprecia a aquellos que son “jugadores de equipo”.',
                    'Capacidad para analizar los temas tanto desde un punto de vista global como pormenorizado.',
                    'Determinación y persistencia.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'No se centrara tanto en un único aspecto olvidando otras oportunidades.',
                    'No se preocupara tanto de sus propios estándares.', 
                    'Revisara sus prioridades con los demás.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Tener decisión para actuar.',
                    'search_in_others' => 'Busca en los demás: La cantidad de trabajo que finalizan.',
                    'influence' => 'Influencia a los demás por: Su tenacidad y persistencia.',
                    'contribution_company' => 'Lo que aporta a la empresa: Orientación a resultados buscando la consistencia.',
                    'excessive' => 'Excesivo con: La autosuficiencia.',
                    'stress' => 'Bajo estrés: Se vuelve testarudo/a, callado/a e inexpresivo/a.',
                    'fears' => 'Temores: Verse involucrado/a con demasiadas personas.'
                ]
            ],
            'P' => [
                'profile' => 'Promotor',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Muy optimista y con un positivo sentido del humor.',
                    'Se centra en las personas y confía mucho en las relaciones.',
                    'Disfruta creando redes de contacto y establece buenas relaciones fácilmente.',
                    'Busca el consenso en la toma de decisiones.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Se centrara un poco más en los objetivos.',
                    'No se preocupara tanto de los sentimientos de los demás.', 
                    'Fuera más organizado y tuviera una actitud más realista'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Ser servicial y complaciente.',
                    'search_in_others' => 'Busca en los demás: La calidez humana que muestran.',
                    'influence' => 'Influencia a los demás por: Su amigabilidad y habilidades interpersonales.',
                    'contribution_company' => 'Lo que aporta a la empresa: Transmite el objetivo global y crea equipo.',
                    'excessive' => 'Excesivo con: El optimismo y la dependencia en los demás.',
                    'stress' => 'Bajo estrés: Es muy emocional y confía en exceso en los demás.',
                    'fears' => 'Temores: No ser valorado/a y apreciado/a por los demás.'
                ]
            ],
            'P-H' => [
                'profile' => 'Promotor y Hunter',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Capacidad para convencer a los demás de su punto de vista.',
                    'Comunica abiertamente.',
                    'Capacidad para reducir la tensión en las situaciones de conflicto.',
                    'Capacidad para promover nuevas ideas o productos.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'No se basara tanto en las emociones para tomar decisiones.',
                    'Estuviera dispuesto/a a entrar en una confrontación cuando fuera necesario.', 
                    'Estableciera plazos de tiempo realistas y gestionara mejor el tiempo.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Mantener las amistades.',
                    'search_in_others' => 'Busca en los demás: La amplitud de contactos que tienen y su compromiso.',
                    'influence' => 'Influencia a los demás por: Su carisma.',
                    'contribution_company' => 'Lo que aporta a la empresa: Estabilidad, responsabilidad y un amplio abanico de amistades.',
                    'excessive' => 'Excesivo con: El entusiasmo.',
                    'stress' => 'Bajo estrés: Habla demasiado.',
                    'fears' => 'Temores: Fracasar o equivocarse.'
                ]
            ],
            'P-F' => [
                'profile' => 'Promotor y Farmer',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Habilidad para ayudar a los demás utilizando la calidez, la empatía y al comprensión.',
                    'Valora y se preocupa tanto de la gente como de las cosas.',
                    'Buena capacidad de escucha y de comunicación.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Fuera más asertivo/a y decidido/a en determinadas situaciones',
                    'No evitara la confrontación, incluso en las situaciones más arriesgadas.', 
                    'Tuviera más iniciativa y desarrollara más su sentido de urgencia.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Mantener amistades a largo plazo.',
                    'search_in_others' => 'Busca en los demás: La lealtad que le demuestran',
                    'influence' => 'Influencia a los demás por: Su relación personal y demostrando con el ejemplo.',
                    'contribution_company' => 'Lo que aporta a la empresa: Capacidad de escucha y paciencia con los demás.',
                    'excessive' => 'Excesivo con: La tolerancia.',
                    'stress' => 'Bajo estrés: Se vuelve rencoroso/a e inquieto/a.',
                    'fears' => 'Temores:  La confrontación.'
                ]
            ],
            'P-C' => [
                'profile' => 'Promotor y Calculator',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Capacidad para gestionar con tacto las situaciones difíciles y sensible a las necesidades de los demás',
                    'Capacidad para crear una atmósfera agradable y confortable.',
                    'Promueve eficazmente las ideas.',
                    'Prefiere un ritmo rápido de trabajo.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Fuera menos analítico/a.',
                    'Diera menos información cuando se trata de vender ideas o productos.', 
                    'Fuera más asertivo/a.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: La aprobación y aceptación de los demás.',
                    'search_in_others' => 'Busca en los demás: La habilidad que tienen para entender los mensajes verbales y no verbales.',
                    'influence' => 'Influencia a los demás por: Su aplomo y confianza.',
                    'contribution_company' => 'Lo que aporta a la empresa: Elimina la tensión y promueve los proyectos y a las personas.',
                    'excessive' => 'Excesivo con: El control de la conversación.',
                    'stress' => 'Bajo estrés: Se vuelve irónico/a con los demás.',
                    'fears' => 'Temores: Pérdida de la propia individualidad.'
                ] 
            ],
            'F' => [
                'profile' => 'Farmer',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Capacidad para proyectar una imagen de calma y control y para concentrarse con el fin de escuchar y aprender.',
                    'Capacidad para persistir en las tareas que aportan una mayor contribución a la organización.',
                    'Miembro del equipo que puede mostrarse abierto/a, paciente y tolerante con las diferencias .',
                    'Disfruta alabando a los demás.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Demostrara un sentido de urgencia cuando la ocasión lo requiere.',
                    'Se centrara menos en la rutina.', 
                    'Tuviera más iniciativa y aceptara mejor los cambios'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Formalidad y estabilidad.',
                    'search_in_others' => 'Busca en los demás: La consistencia que muestran.',
                    'influence' => 'Influencia a los demás por: Su disposición agradable y de servicio.',
                    'contribution_company' => 'Lo que aporta a la empresa:  Estabiliza el entorno de una forma amigable.',
                    'excessive' => 'Excesivo con: La compostura.',
                    'stress' => 'Bajo estrés: Se vuelve inexpresivo/a.',
                    'fears' => 'Temores: Lo desconocido y no ser apreciado/a por los demás.'
                ]
            ],
            'F-C' => [
                'profile' => 'Farmer y Calculator',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Capacidad para iniciar proyectos y trabajar en ellos hasta su finalización.',
                    'Dispuesto/a a trabajar para un líder y una causa.',
                    'Mucha habilidad para buscar soluciones a los problemas de una forma lógica, consiguiendo la comprensión y aceptación de todos los que están involucrados.',
                    'Demuestra un liderazgo positivo, mostrando consideración por los sentimientos de todos los miembros del equipo.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Aprendiera a tener más iniciativa.',
                    'Utilizara un enfoque más directo.', 
                    'Mostrara más sus preocupaciones y sentimientos.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Alcanzar los elevados estándares que establece.',
                    'search_in_others' => 'Busca en los demás: La utilización que hacen de sus conocimientos.',
                    'influence' => 'Influencia a los demás por: Su habilidad para hacer un seguimiento.',
                    'contribution_company' => 'Lo que aporta a la empresa: Aporta lógica y concentración de esfuerzos a las necesidades existentes.',
                    'excessive' => 'Excesivo con: La resistencia al cambio.',
                    'stress' => 'Bajo estrés: Se vuelve decidido/a y testarudo/a',
                    'fears' => 'Temores: No dar respuesta a las necesidades y/o expectativas'
                ]
            ],
            'F-P' => [
                'profile' => 'Farmer y Promotor',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Buena capacidad de escucha y de empatía con las personas.',
                    'Habilidad para prestar apoyo y ayudar a los demás en la consecución de sus objetivos y aspiraciones.',
                    'Acepta los sentimientos, valores y creencias de los demás.',
                    'Capacidad para crear un ambiente donde la gente se sienta importante.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'uera más asertivo/a y decidido/a.',
                    'No aceptara tanto el “status quo”.', 
                    'Fuera más firme y seguro/a de sí mismo/a.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: Aceptación.',
                    'search_in_others' => 'Busca en los demás: La lealtad, sinceridad y formalidad que muestran.',
                    'influence' => 'Influencia a los demás por: Su comprensión y amigabilidad.',
                    'contribution_company' => 'Lo que aporta a la empresa: Apoyo, crea estabilidad y armonía cuando hay presión.',
                    'excessive' => 'Excesivo con: La compasión y amabilidad.',
                    'stress' => 'Bajo estrés: Se retira.',
                    'fears' => 'Temores: El conflicto, el desacuerdo y no ser apreciado/a por los demás.'
                ]
            ],
            'F-H' => [
                'profile' => 'Farmer y Hunter',
                'strongholds' => 'FORTALEZAS',
                'strongholds_steps' => [
                    'Capacidad para coger un problema y trabajar en él hasta solucionarlo.',
                    'Persistente, tenaz y lógico/a en su búsqueda de resultados.',
                    'Muy hábil para mantener relaciones tanto dentro como fuera del trabajo.',
                    'Miembro del equipo que mostrará habilidades de liderazgo, defendiendo de forma agresiva lo que cree.'
                ],
                'improvement_areas' => 'AREAS DE MEJORA',
                'improvement_areas_steps' => [
                    'Demostrara un comportamiento más activo, incluso cuando peligra su seguridad.',
                    'Utilizara un pensamiento más creativo y nuevo cuando resuelve problemas.', 
                    'No mostrara tanta resistencia a las nuevas situaciones que le obligan a salir de su zona cómoda.'
                ],
                'big_trends' => 'GRANDES TENDENCIAS',
                'big_trends_steps' => [
                    'peculiarity' => 'Particularidad: : El logro personal.',
                    'search_in_others' => 'Busca en los demás: Los éxitos y logros que alcanzan.',
                    'influence' => 'Influencia a los demás por: Su perseverancia.',
                    'contribution_company' => 'Lo que aporta a la empresa: Trabaja de forma independiente y le gustan los retos.',
                    'excessive' => 'Excesivo con: La franqueza.',
                    'stress' => 'Bajo estrés: Se vuelve obstinado/a, inflexible e incesante.',
                    'fears' => 'Temores: No conseguir los resultados deseados.'
                ]
            ]
        ];
    }
}