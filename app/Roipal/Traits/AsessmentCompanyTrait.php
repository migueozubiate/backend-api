<?php

namespace App\Roipal\Traits;

use Faker\Factory as Faker;
use App\Roipal\Eloquent\Catalog;
use App\Roipal\Eloquent\Mission;

trait AsessmentCompanyTrait
{
    protected function getAsessmentCompany(Mission $mission)
    {
        $faker = Faker::create('es_ES');

        $hours = $mission->time;

        $missionHours = $this->getOptionsHoursMission($faker, $hours);

        $missionNames = $this->getOptionsNameMission();
        shuffle($missionNames);

        $missionTypes = $this->getOptionsTypeMission($mission);
        shuffle($missionTypes);

        return [
            'name' => "Cuestionario de la misión {$mission->name}",
            'summary' => 'Este es un cuestionario de conceptos básicos sobre la compañia y la misión que acaba de aceptar',
            'questions' => [
                [
                    'number' => 1,
                    'question' => '¿Cuál es el nombre de la misión?',
                    'item_question' => 'mission_name_question',
                    'options' => [
                        'A' => $missionNames[0],
                        'B' => $mission->name,
                        'C' => $missionNames[2],
                        'D' => $missionNames[3]
                    ],
                    'answer' => 'B'
                ],
                [
                    'number' => 2,
                    'question' => '¿Cuál es el nombre de la compañia?',
                    'item_question' => 'company_name_question',
                    'options' => [
                        'A' => $faker->unique()->company,
                        'B' => $faker->unique()->company,
                        'C' => $faker->unique()->company,
                        'D' => $mission->company->business_name
                    ],
                    'answer' => 'D'
                ],
                [
                    'number' => 3,
                    'question' => 'Selecciona el sitio web de la compañia',
                    'item_question' => 'company_web_question',
                    'options' => [
                        'A' => $faker->unique()->domainName,
                        'B' => $faker->unique()->domainName,
                        'C' => $mission->company->profile->website,
                        'D' => $faker->unique()->domainName
                    ],
                    'answer' => 'C'
                ],
                [
                    'number' => 4,
                    'question' => '¿Cuál es el eslogan de la compañia?',
                    'item_question' => 'company_slogan_question',
                    'options' => [
                        'A' => $faker->unique()->catchPhrase,
                        'B' => $mission->company->profile->slogan,
                        'C' => $faker->unique()->catchPhrase,
                        'D' => $faker->unique()->catchPhrase
                    ],
                    'answer' => 'B'
                ],
                [
                    'number' => 5,
                    'question' => '¿Cuál es el tipo de misión a participar?',
                    'item_question' => 'mission_type_question',
                    'options' => [
                        'A' => $missionTypes[0],
                        'B' => $missionTypes[1],
                        'C' => $mission->type,
                        'D' => $missionTypes[2]
                    ],
                    'answer' => 'C'
                ],
                [
                    'number' => 6,
                    'question' => '¿Cuál es la duración de la misión?',
                    'item_question' => 'mission_time_question',
                    'options' => [
                        'A' => "{$missionHours[0]} {$mission->time_unit}",
                        'B' => "{$missionHours[1]} {$mission->time_unit}",
                        'C' => "{$missionHours[2]} {$mission->time_unit}",
                        'D' => "{$mission->time} {$mission->time_unit}"
                    ],
                    'answer' => 'D'
                ]
            ]
        ];
    }

    protected function getOptionsNameMission()
    {
        return [
            'Venta de electrodomesticos a mayoreo',
            'Venta de computadoras portatiles',
            'Vender peliculas HD',
            'Marketing para Halloween, las campañas más terroríficas del año',
            'Venta de hamburguesas',
            'Venta de tacos'
        ];
    }

    protected function getOptionsTypeMission(Mission $mission)
    {
        $types = Catalog::inRandomOrder()
                    ->where('value', '!=' ,$mission->type)
                    ->get();

        $missionTypes = [];

        foreach ($types as $value) {
            $missionTypes[] = $value->value;
        }

        return $missionTypes;
    }

    protected function getOptionsHoursMission($faker, $hours)
    {
        $optionsHours = [
            $faker->unique()->randomNumber($nbDigits = 2),
            $faker->unique()->randomNumber($nbDigits = 1),
            $faker->unique()->randomNumber($nbDigits = 2),
            $faker->unique()->randomNumber($nbDigits = 1),
        ];

        $getHours = [];

        foreach ($optionsHours as $value) {
            if ($value !== $hours) {
                $getHours[] = $value;
            }
        }

        return $getHours;
    }
}