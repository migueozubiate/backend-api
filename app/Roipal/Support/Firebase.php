<?php
namespace App\Roipal\Support;

use FCM;
use Exception;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use Illuminate\Support\Facades\Config;

class Firebase
{
    protected $allowed = [
        'executive',
        'company',
        'employeer'
    ];

    public function notification(array $payload, $token)
    {
        $type = array_pull($payload, 'app');

        if (!in_array($type, $this->allowed)) {
            throw new Exception("Type {$type} is not allowed for firebase notifications");
        }

        $tokens = !is_array($token) ? [$token] : $token;

        $this->swapAppConfigForFCM($type);

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60*20);
        $optionBuilder->setContentAvailable(true);

        $data = array_get($payload, 'data', []);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);

        $notificationBuilder = new PayloadNotificationBuilder($payload['title']);
        $notificationBuilder
            ->setBody($payload['body'])
            ->setSound('default');

        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();

        return FCM::sendTo($tokens, $option, $notification, $data);
    }

    public function swapAppConfigForFCM($app)
    {
        $config = Config::get("firebase.{$app}");

        Config::set('fcm.http.server_key', $config['server_key']);
    }
}