<?php

namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\CommercialNeed;
use App\Roipal\Eloquent\CommercialTypeSale;

class CommercialNeedRepository
{
    public function getCommercialNeeds(array $meta)
    {
        $limit = array_get($meta, 'limit', 25);
        $search = array_get($meta, 'search');
        $field = array_get($meta, 'field');

        $commercialNeed = CommercialNeed::query();

        if (array_get($meta, 'type')) {
            $type = array_get($meta, 'type');
            $commercialNeed->where('commercial_need', $type);
        }

        if ($search) {
            if ($field) {
                $commercialNeed->where($field, $search);
            } else {
                $commercialNeed->searchCommercialNeed($search);
            }
        }

        return $commercialNeed->simplePaginate($limit);
    }

    public function getTypeSalesByCommercialNeedUuid(array $meta, CommercialNeed $commercialNeed)
    {
        $limit = array_get($meta, 'limit', 25);
        $search = array_get($meta, 'search');
        $field = array_get($meta, 'field');

        $typeSales = 
            CommercialTypeSale::query()
            ->whereCommercialNeedId($commercialNeed->id);

        if ($search) {
            if ($field) {
                $typeSales->where($field, $search);
            } else {
                $typeSales->searchCommercialTypeSales($search);
            }
        }
    
        return $typeSales->simplePaginate($limit);
    }

    public function getTypeSale(array $meta, CommercialTypeSale $typeSale)
    {
        $getTypeSale = CommercialTypeSale::find($typeSale->id);

        return $getTypeSale;
    }
}