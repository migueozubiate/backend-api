<?php
namespace App\Roipal\Repositories;

use Carbon\Carbon;
use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\Company;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use App\Roipal\Eloquent\UserChecklist;
use App\Roipal\Eloquent\CompanyProfile;
use App\Roipal\Eloquent\PaymentProfile;
use App\Exceptions\PasswordMissMatchException;

class CompanyRepository
{
    public function store(array $data)
    {
        return DB::transaction(function () use ($data) {
            $company = new Company();
            $company->business_name = array_get($data, 'legal_name');
            $company->name = array_get($data, 'name');
            $company->vertical = '';
            $company->terms_accepted = array_get($data, 'terms_accepted');
            $company->rfc = array_get($data, 'rfc');

            $user = new User();
            $user->password = bcrypt(array_get($data, 'password'));
            $user->company_root = 1;
            $user->lang = App::getLocale();
            $user->name = array_get($data, 'name');
            $user->email = array_get($data, 'email');
            $user->classification = 'company';

            $company->save();
            $company->owner()->save($user);

            $checklist = new UserChecklist();
            $checklist->step = 'activation';
            $checklist->completed = false;
            $checklist->completed_at = Carbon::now();

            $user->checklist()->save($checklist);

            return $company;
        });
    }

    public function show(array $meta, Company $company)
    {
        $companyUsers = User::whereCompanyId($company->id)
            ->get();

        return $companyUsers;
    }

    public function update(array $data, Company $company)
    {
        return DB::transaction(function () use ($data, $company) {
            $user = $company->owner;
            $userProfile = array_get($data, 'user_profile');

            if ($userProfile) {
                $user->update(array_only($userProfile, ['name', 'avatar']));

                if (array_get($userProfile, 'password')) {
                    if (Hash::check(array_get($userProfile, 'password'), $user->password)) {   
                        $user->password = Hash::make(array_get($userProfile, 'new_password'));
                        $user->save();
                    } else {
                        throw new PasswordMissMatchException();
                    }
                }
            }

            if (array_get($data, 'profile')) {
                $latitude = array_get($data, 'profile.position.latitude');
                $longitude = array_get($data, 'profile.position.longitude');
                $avatar = array_get($data, 'profile.photo_bio_url');

                array_forget($data, 'profile.position');
                $position = sprintf('%f %f', $latitude, $longitude);

                if ($company->profile) {
                    $profile = $company->profile;
                    $profile->position = $position;
                    $profile->update(array_pull($data, 'profile'));
                } else {
                    $profile = new CompanyProfile(array_pull($data, 'profile'));
                    $profile->position = $position;
                    $company->profile()->save($profile);
                }

                if ($avatar) {
                    $user->avatar = $avatar;
                    $user->save();
                }

                if (!$user->checklist()->where('step', '=', 'profile')->first()) {
                    $checklist = new UserChecklist();
                    $checklist->step = 'profile';
                    $checklist->completed = true;
                    $checklist->completed_at = Carbon::now();

                    $user->checklist()->save($checklist);
                }
            }

            if (array_get($data, 'contact')) {
                $contact = array_get($data, 'contact');

                $companyContact = [
                    'contact_name' => $contact['name'],
                    'contact_job_title' => $contact['job_title'],
                    'contact_phone' => $contact['phone']
                ];

                if ($company->profile) {
                    $profile = $company->profile;
                    $profile->update($companyContact);
                } else {
                    $profile = new CompanyProfile($companyContact);
                    $company->profile()->save($profile);
                }
            }

            if (array_get($data, 'payment')) {
                if ($company->payment) {
                    $payment = $company->payment;
                    $payment->update(array_pull($data, 'payment'));
                } else {
                    $payment = new PaymentProfile(array_pull($data, 'payment'));

                    $company->payment()->save($payment);
                }

                if (!$user->checklist()->where('step', '=', 'payment')->first()) {
                    $checklist = new UserChecklist();
                    $checklist->step = 'payment';
                    $checklist->completed = true;
                    $checklist->completed_at = Carbon::now();

                    $user->checklist()->save($checklist);
                }
            }

            return $company; 
        });
    }
}
