<?php
namespace App\Roipal\Repositories;

use Illuminate\Support\Facades\DB;
use App\Roipal\Eloquent\Assestment;
use Illuminate\Support\Facades\Log;
use App\Roipal\Scopes\AssesmentScope;
use App\Roipal\Eloquent\ExecutiveAssestment;

class AssestmentRepository
{

    public function getAssestmentByType($type, array $meta = [])
    {
        $language = array_get($meta, 'lang') ?? \App::getLocale();

        if ($language) {
            return Assestment::where('name', '=', $type)
                            ->where('lang', $language)
                            ->firstOrFail();
        }

        return Assestment::where('name', '=', $type)->firstOrFail();
    }

    public function show(array $meta)
    {
        $limit = array_get($meta, 'limit', 25);
        $search = array_get($meta, 'search');
        $field = array_get($meta, 'field');

        $type = ['ROIPAL', 'DISC'];

        $assessments = 
            Assestment::whereName('DISC')
                ->orWhere('name', 'ROIPAL')
                ->newQuery();

        if ($search) {
            if ($field) {
                $assessments->where($field, $search);
            } else {
                $assessments->SearchAssessment($search);
            }
        }

        return $assessments->simplePaginate($limit);
    }

    public function update(array $meta, Assestment $assessment)
    {
        $getAssessment = array_except($meta, ['questions']);
        $questions = array_get($meta, 'questions');

        return DB::transaction(function() use ($getAssessment, $questions, $assessment){

            if ($getAssessment) {
                $assessment->update($getAssessment);
            }

            $getAnswers = [];

            if ($questions) {
                foreach ($questions as $question) {
                    $number = $question['number'];
                    $options = $question['options'];  
        
                    $assessmentQuestion = 
                                $assessment
                                    ->questions()
                                    ->where('number', $number)
                                    ->first();
        
                    $assessmentQuestion->fill($question);
                    $assessmentQuestion->save();

                    $answerPoll = 
                        $assessment
                            ->poll()
                            ->where('number', $number)
                            ->get();     

                    foreach ($answerPoll as $poll) {
                        foreach ($poll->meaning as $key => $answer) {
                            $getAnswers['meaning'][$key] = $options[$key];
                        }

                        $poll->meaning = $getAnswers['meaning'];
                        $poll->number = $number;
                        $poll->save();
                    }
                }
            }

            return $assessment;
        });   
    }
}