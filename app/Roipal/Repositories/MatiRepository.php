<?php
namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\User;
use Illuminate\Support\Facades\DB;
use App\Roipal\Eloquent\UsersVerifyMati;
use App\Exceptions\UserMissMatchException;

class MatiRepository
{
    public function store($request)
    {

        $dataUserVerification = [];
        
        $dataUserFillable = [];

        $dataStepVerification = [];

        $dataStepVerificationFails = [];

        $metadata = $request['metadata'];

        $uuid = array_get($metadata, 'uuid');

        foreach ($request['steps'] as $value) {
            $dataStepVerificationFails ['error_liveness'] = $value['error'];
            $dataStepVerification ['liveness'] = $value['id'];

        }

        foreach ($request['documents'] as $value) {
            foreach ($value['steps'] as  $key => $item) {
               $dataStepVerification[$item['id']] = $item['id'];

               $getDataUserVerification = array_get($item, 'data');

               if ($key === 0 && $getDataUserVerification) {
                   $dataUserVerification [] = $getDataUserVerification;
               }

               if ($key === 2) {
                $dataStepVerificationFails ['error_alteration_detection'] = $item['error'];
               }

            }
        }

        foreach ($dataUserVerification as $value) {
            foreach ($value as $key => $item) {
                 $dataUserFillable [$key] = $item['value'];
            }

        }

        $errorLiveness = array_get($dataStepVerificationFails, 'error_liveness');

        $errorAlterationDetection = array_get($dataStepVerificationFails, 'error_alteration_detection');

        if (!$errorLiveness && !$errorAlterationDetection) {

            $uuidUserRebuild = $this->rebuildUuid($uuid);

            $user = User::Where('uuid', '=', $uuidUserRebuild)->first();

            if ($user) {
                return DB::transaction(function () use ($user,$dataUserFillable,$dataStepVerification){
                    $mati = new UsersVerifyMati($dataUserFillable);
                    $mati->user_id = $user->id;
                    $mati->liveness = array_get($dataStepVerification, 'liveness');
                    $mati->document_reading = array_get($dataStepVerification, 'document-reading');
                    $mati->alteration_detection = array_get($dataStepVerification, 'alteration-detection');
                    $mati->save();

                    return $mati;

                });
            }

            throw new UserMissMatchException();
        }
    }

    public function rebuildUuid($uuidV4)
    {
        $digitOneWithEight = substr($uuidV4, 0 , 8);
        $digitTwoWithFour = substr($uuidV4, 8 , 4);
        $digitThreeWithFour = substr($uuidV4, 12, 4);
        $digitFourWithFour = substr($uuidV4,16, 4);
        $digitFiveWithTwelve = substr($uuidV4,20, 12);

        return  $digitOneWithEight . "-" . 
                $digitTwoWithFour . "-" . 
                $digitThreeWithFour . "-" .
                $digitFourWithFour . "-" . 
                $digitFiveWithTwelve;
    }
}