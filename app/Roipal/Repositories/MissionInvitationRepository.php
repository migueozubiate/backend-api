<?php
namespace App\Roipal\Repositories;

use Carbon\Carbon;
use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Location;
use App\Roipal\Eloquent\Executive;
use Illuminate\Support\Facades\DB;
use App\Roipal\Scopes\InvitationScope;
use App\Roipal\Constants\MissionStatus;
use App\Roipal\Eloquent\MissionActivity;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Eloquent\ExecutiveMission;
use App\Roipal\Eloquent\ExecutiveProfile;
use App\Roipal\Eloquent\MissionInvitation;


class MissionInvitationRepository
{
    public function index(array $meta, InvitationScope $scope)
    {
        $limit = array_get($meta, 'limit', 100);
        $relationships = array_get($meta, 'relationships', []);
        $search = array_get($meta, 'search');
        $field = array_get($meta, 'field');
        $invitation = MissionInvitation::query();
        $scope->build($invitation, $meta);

        if ($search) {
            if ($field) {
                $invitation->where($field, 'LIKE', "%$search%");
            }

            if ($relationships) {
                $available = [
                    'mission',
                    'location',
                    'company',
                    'executive',
                ];

                $arrayRelations = [];
                foreach ($available as $key) {
                    if (in_array($key, $relationships)) {
                        $arrayRelations[$key] = $key;
                    }
                }

                $this->queryByRelationship($invitation, $search, $arrayRelations);
            }
        }

        return $invitation->orderBy('updated_at', 'desc')->simplePaginate($limit);
    }

    protected function queryByRelationship($invitation, $search, array $arrayRelations)
    {
        return $invitation->orWhere(function ($query) use ($search, $arrayRelations) {
            $query->when(array_get($arrayRelations, 'mission') === 'mission',
                function ($query) use ($search) {
                    $query->orWhereHas('mission', function ($query) use ($search) {
                        $query->where('name', 'LIKE', "%$search%")
                            ->orWhere('description', 'LIKE', "%$search%")
                            ->orWhere('type', 'LIKE', "%$search%")
                            ->orWhere('profile', 'LIKE', "%$search%");
                    });
                })
                ->when(array_get($arrayRelations, 'company') === 'company',
                    function ($query) use ($search) {
                        $query->orWhereHas('company', function ($query) use ($search) {
                            $query->where('name', 'LIKE', "%$search%")
                                ->orWhere('business_name', 'LIKE', "%$search%");
                        });
                    })
                ->when(array_get($arrayRelations, 'location') === 'location',
                    function ($query) use ($search) {
                        $query->orWhereHas('location', function ($query) use ($search) {
                            $query->where('name', 'LIKE', "%$search%")
                                ->orWhere('address', 'LIKE', "%$search%");
                        });
                    })
                ->when(array_get($arrayRelations, 'executive') === 'executive',
                    function ($query) use ($search) {
                        $query->orWhereHas('executive', function ($query) use ($search) {
                            $query->where('name', 'LIKE', "%$search%")
                                ->orWhere('email', 'LIKE', "%$search%")
                                ->orWhere('classification', 'LIKE', "%$search%");
                        });
                    });
        });
    }

    public function getByExecutive(Executive $executive, array $meta = [])
    {
        $limit = array_get($meta, 'limit');

        return MissionInvitation::whereExecutiveId($executive->id)
            ->simplePaginate($limit);
    }

    public function store(array $data, Mission $mission, User $user)
    {
        return DB::transaction(function () use ($data, $mission, $user) {
            $invitations = array_pull($data, 'invitations', []);
            $response = [];
            $executiveTokens = [];

            foreach ($invitations as $invitation) {
                $location = MissionLocation::where(['uuid' => array_pull($invitation, 'location_uuid')])->first();
                $executives = array_pull($invitation, 'executives', []);

                foreach ($executives as $executive) {
                    $executive = Executive::where(['uuid' => $executive])->first();

                    $executiveTokens[] = $executive->fcm_token;

                    $invitation = new MissionInvitation();
                    $invitation->executive_id = $executive->id;
                    $invitation->mission_id = $mission->id;
                    $invitation->mission_location_id = $location->id;
                    $invitation->company_id = $mission->company_id;
                    $invitation->user_id = $user->id;
                    $invitation->save();

                    array_push($response, $invitation);
                }
            }

            return $response;
        });
    }

    public function storeInvitation(MissionLocation $location, ExecutiveProfile $profile)
    {
        $response = DB::transaction(function () use ($location, $profile) {

            $invitation = new MissionInvitation();
            $invitation->executive_id = $profile->user_id;
            $invitation->mission_id = $location->mission->id;
            $invitation->mission_location_id = $location->id;
            $invitation->company_id = $location->mission->company_id;
            $invitation->user_id = $location->user_id;

            $invitation->save();

            return $invitation;

        });

        return $response;
    }

    public function status(array $data, MissionInvitation $invitation)
    {
        return DB::transaction(function () use ($data, $invitation) {
            $accepted = array_get($data, 'status');

            $invitation->fill($data);
            $invitation->status = $accepted ? $accepted : MissionStatus::CANCELED;
            $invitation->response_at = Carbon::now();
            $invitation->save();

            $mission = $invitation->mission;
            $executive = $invitation->executive;

            if ($invitation->status == MissionStatus::STARTED) {
                $executiveMission = new ExecutiveMission();
                $executiveMission->mission_id = $mission->id;
                $executiveMission->company_id = $mission->company_id;
                $executiveMission->executive_id = $executive->id;
                $executiveMission->mission_location_id = $invitation->mission_location_id;
                $executiveMission->status = $mission->status;
                $executiveMission->accepted_at = Carbon::now();
                $executiveMission->save();

                $mission->increment('accepted');
                $invitation->location->increment('accepted');
            } else {
                $mission->increment('rejected');
                $invitation->location->increment('rejected');
            }

            $executive->profile->decrement('invitations_received');

            return $invitation;
        });

    }

    public function canceledInvitation(MissionInvitation $invitation)
    {
        $executiveProfile = $invitation->executive->profile;

        $mission = $invitation->mission;

        $invitation->status = MissionStatus::CANCELED;
        $invitation->response_at = Carbon::now();
        
        DB::transaction(function () use ($mission, $invitation, $executiveProfile){
            $invitation->save();
            $mission->increment('rejected');
            $invitation->location->increment('rejected');
            $executiveProfile->decrement('invitations_received');
        });
    }

    public function acceptanceInvitation(MissionInvitation $invitation)
    {
        $mission = $invitation->mission;
        $executive = $invitation->executive;
        $activities = $mission->type_sale_activities;

        foreach ($activities as $activity) {
            $getActivity = explode(": ", $activity);
            $typeSaleActivity[] = [
                'title' => $getActivity[0],
                'description' => $getActivity[1]
            ];
        }

        $invitation->status = MissionStatus::STARTED;
        $invitation->response_at = Carbon::now();

        return DB::transaction(function () use ($mission, $executive, $invitation, $typeSaleActivity){

            $invitation->save();

            foreach ($typeSaleActivity as $activity) {
                $missionActivity = new MissionActivity($activity);
                $missionActivity->company_id = $mission->company->id;
                $missionActivity->executive_id = $executive->id;
                $mission->activities()->save($missionActivity);
            }

            $executiveMission = new ExecutiveMission();
            $executiveMission->mission_id = $mission->id;
            $executiveMission->company_id = $mission->company_id;
            $executiveMission->executive_id = $executive->id;
            $executiveMission->mission_location_id = $invitation->mission_location_id;
            $executiveMission->status = $mission->status;
            $executiveMission->accepted_at = Carbon::now();
            $executiveMission->save();

            $mission->increment('accepted');
            $invitation->location->increment('accepted');
            $executive->profile->decrement('invitations_received');

            return $mission;
        });
    }

    public function getMissionLocationByUuid(array $data, Mission $mission,$oldExecutives)
    {
        $executives = array_get($data, 'executives_requested');

        $subtractionExecutives = $executives - $oldExecutives;

        $sumExecutives = $mission->executives_requested + $subtractionExecutives;

        $location = MissionLocation::where('uuid', array_get($data, 'uuid'))->first();
        $location->executives_requested = $executives;
        $location->invitations_sent = $subtractionExecutives;
        $location->save(); 

        $mission->executives_requested = $sumExecutives;
        $mission->invitations_sent = $sumExecutives;
        $mission->save();

        return $location;
    }
}
