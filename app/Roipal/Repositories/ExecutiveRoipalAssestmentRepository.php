<?php
namespace App\Roipal\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Roipal\Eloquent\Executive;
use Illuminate\Support\Facades\DB;
use App\Roipal\Eloquent\Assestment;
use App\Roipal\Eloquent\UserChecklist;
use App\Exceptions\AssessmentExistException;
use App\Roipal\Eloquent\ExecutiveAssestment;
use App\Roipal\Eloquent\ExecutiveAssestmentAnswer;
use App\Roipal\Contracts\ExecutiveAssessmentRepositoryInterface;

class ExecutiveRoipalAssestmentRepository implements ExecutiveAssessmentRepositoryInterface
{
    public function store(Assestment $roipal, Executive $executive, array $data)
    {
        $assessment = 
                    $executive
                        ->assestments()
                        ->whereName('ROIPAL')
                        ->first();

        if ($assessment) {
            throw new AssessmentExistException($assessment->name);
        }

        return DB::transaction(function () use ($roipal, $data, $executive) {
            $poll = $roipal->poll;

            $assessment = new ExecutiveAssestment($roipal->toArray());
            $executive->assestments()->save($assessment);

            $answers = array_get($data, 'answers');

            $assessmentQuestions = $roipal->questions;
            $assessmentAnswers = [];

            foreach ($answers as $values) {
                $answer = new ExecutiveAssestmentAnswer($values);
                $assessment_question = $assessmentQuestions->where('number', $values['number'])->first();
                $eval =
                $poll->where('number', (int) $answer->number)
                    ->where('answer', Str::upper($values['answer']))
                    ->first();

                $answer->question = $assessment_question->question;
                $answer->options = $assessment_question->options;
                $answer->profile = $assessment_question->profile;
                $answer->group = $assessment_question->group;
                $answer->points = $eval ? $eval->points : null;

                $executiveAnswers[] = $answer;
            }

            $resultGroup =
            $assessment
                ->answers()
                ->select('group', \DB::raw('SUM(points) as points'))
                ->groupBy('group')
                ->get()
                ->map(function ($item) {
                    $item->points = (int) $item->points;

                    return $item;
                })->toArray();

            $evaluationGroup = [];

            foreach ($resultGroup as $res) {
                $assestmentEvaluation = new ExecutiveAssestmentsEvaluation($res);
                $assestmentEvaluation->group = array_get($res, 'group');
                $assestmentEvaluation->points = array_get($res, 'points');
                $assestmentEvaluation->user_id = $executive->id;

                $evaluationGroup[] = $assestmentEvaluation;
            }

            $result =
            $assessment
                ->answers()
                ->sum('points');

            $assessment->points = $result;

            $executive->assestments()->save($assessment);
            $assessment->answers()->saveMany($executiveAnswers);
            $assessment->evaluations()->saveMany($evaluationGroup);

            if (!$executive->checklist()->where('step', '=', 'roipal-assessment')->first()) {
                $checklist = new UserChecklist();
                $checklist->step = 'roipal-assessment';
                $checklist->completed = true;
                $checklist->completed_at = Carbon::now();

                $executive->checklist()->save($checklist);
            }

            return $assessment;

        });

    }

}
