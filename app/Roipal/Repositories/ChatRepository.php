<?php
namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\Message;

class ChatRepository
{
    public function getHistoryConversation(array $request)
    {      
        $message = Message::whereRoomUuid(array_get($request,'room_uuid'))
                ->latest()
                ->get();
               
        return $message;    
    }

}