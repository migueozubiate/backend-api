<?php

namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\PhysicalLimitation;

class PhysicalLimitationRepository
{
    public function index()
    {
        $limitations = PhysicalLimitation::all();

        return $limitations;
    }
}