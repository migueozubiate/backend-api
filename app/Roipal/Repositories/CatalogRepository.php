<?php
namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\Catalog;

class CatalogRepository
{
  public function getByType($type, $language = 'es')
  {
    return
      Catalog::whereType($type)
        ->whereLanguage($language)
        ->get();
  }
}