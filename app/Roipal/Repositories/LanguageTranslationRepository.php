<?php
namespace App\Roipal\Repositories;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Waavi\Translation\Models\Language;
use Waavi\Translation\Models\Translation;

class LanguageTranslationRepository
{
   
    public function index($limit)
    {      
        $trasnlation = Translation::simplePaginate($limit);

        return $trasnlation;
    }

    public function store(array $data,Language $language)
    {
        return DB::transaction(function () use ($data,$language){
           
            $trasnlation = new Translation($data);
            $trasnlation->locale = $language->locale;
            $trasnlation->save();

            return $trasnlation;

        });
    } 

    public function update(array $data,Translation $translation)
    {
        return DB::transaction(function () use ($data,$translation){
            $translation->fill($data);
            $translation->save();

            return $translation;

        });
    }

    public function getByNamespaceAndGroup(array $data)
    {
        $localeApp = App::getLocale();

        return  Translation::whereNamespace(array_get($data, 'namespace'))
                        ->whereLocale($localeApp)
                        ->whereGroup(array_get($data, 'group'))
                        ->get();
    }
}