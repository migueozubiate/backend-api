<?php

namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Location;
use Illuminate\Support\Facades\DB;
use App\Roipal\Scopes\LocationScope;
use App\Roipal\Eloquent\MissionLocation;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class LocationRepository
{
    public function show(array $meta, LocationScope $scope)
    {
        $limit         = array_get($meta, 'limit', 25);
        $relationships = array_get($meta, 'relationships', []);
        $search        = array_get($meta, 'search');
        $field         = array_get($meta, 'field');

        $location = Location::with($relationships);
        $scope->build($location->newQuery(), $meta);

        if ($search) {
            if ($field) {
                $location->where($field, 'LIKE', "%$search%");
            } else {
                $location->searchLocation($search);
            }

            if ($relationships) {
                $available = ['company'];

                foreach ($available as $key) {
                    if (in_array($key, $relationships)) {
                        $method = "queryBy{$key}Relationship";

                        $this->{$method}($location, $search);
                    }
                }
            }
        }

        return $location->orderBy('updated_at', 'desc')->simplePaginate($limit);
    }

    protected function queryByCompanyRelationship($location, $search)
    {
        return $location->orWhere(function ($query) use ($search) {

            $query->orWhereHas('company', function ($query) use ($search) {
                $query->where('name', 'LIKE', "%$search%")
                    ->orWhere('business_name', 'LIKE', "%$search%");
            });

        });

    }

    public function store(array $data = [], Company $company)
    {
        $positions             = array_get($data, 'position');
        $locations             = new Location($data);
        $locations->company_id = $company->id;
        $locations->position   = new Point($positions['latitude'], $positions['longitude']);

        return DB::transaction(function () use ($locations) {
            $locations->save();

            return $locations;
        });
    }

    public function getLocationById(Location $location)
    {
        $getLocation = Location::whereUuid($location->uuid)
            ->get();

        return $getLocation;
    }

    public function update(array $request, Company $company, Location $location)
    {
        return DB::transaction(function () use ($request, $company, $location) {
            $positions = array_get($request, 'position');

            $location->name     = array_get($request, 'name');
            $location->address  = array_get($request, 'address');
            $location->position = new Point(array_get($positions, 'latitude'), array_get($positions, 'longitude'));
            $location->save();

            return $location;
        });
    }

    public function storeByMission(array $meta, Company $company, Mission $mission)
    {
        $locations = array_get($meta, 'locations');

        return DB::transaction(function () use ($locations, $company, $mission) {

            $missionLocations = [];

            foreach ($locations as $value) {
                if (array_get($value, 'uuid')) {
                    $uuid = array_get($value, 'uuid');
        
                    $found = Location::whereUuid($uuid)->first();
                    $location = $found;
                } else {
                    $latitude = array_get($value, 'latitude');
                    $longitude = array_get($value, 'longitude');
        
                    $location = new Location($value);
                    $location->position = new Point($latitude, $longitude);
                    $company->location()->save($location);
                }
    
                $missionLocation = new MissionLocation($location->toArray() + $value);
                $missionLocation->mission_id = $mission->id;
                $missionLocation->user_id = $company->user->id;
                $missionLocation->executives_requested = 0;
                $location->missionLocation()->save($missionLocation);

                $missionLocations[] = $missionLocation;
            }

            return $missionLocations;
        });
    }

    public function getLocationsThatNotInMission(array $meta, Company $company, Mission $mission)
    {
        $limit = array_get($meta, 'limit', 25);
        $search = array_get($meta, 'search');
        $field = array_get($meta, 'field');

        $missionLocations = [];

        foreach ($mission->locations as $value) {
          $missionLocations[] = $value->location_id;
        }

        $locations = $company->location()->whereNotIn('id', $missionLocations);

        if ($search) {
            if ($field) {
                $locations->where($field, 'LIKE', "%$search%");
            } else {
                $locations->searchLocation($search);
            }
        }

        return $locations->latest()->simplePaginate($limit);
    }
}
