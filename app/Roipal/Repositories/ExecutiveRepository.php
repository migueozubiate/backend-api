<?php
namespace App\Roipal\Repositories;

use Carbon\Carbon;
use Faker\Provider\Uuid;
use App\Roipal\Eloquent\Executive;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Hash;
use App\Roipal\Eloquent\UserChecklist;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Constants\ExecutiveStatus;
use App\Roipal\Eloquent\ExecutiveProfile;
use App\Roipal\Eloquent\PhysicalLimitation;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Exceptions\PasswordMissMatchException;
use App\Roipal\Eloquent\ExecutiveDocumentation;
use App\Roipal\Eloquent\ExecutivePaymentProfile;
use App\Roipal\Eloquent\ExecutivePhysicalLimitation;

class ExecutiveRepository 
{
    /**
     * Genera un nuevo usuario de tipo Vendedor sin persistencia.
     *
     * @param array $data name, email, password
     *
     * @return Executive
     */
    public function store(array $data)
    {
        $data['password'] = bcrypt($data['password']);

        $executive = new Executive(array_only($data, ['name', 'email', 'password']));
        $executive->lang = App::getLocale();
        $executive->uuid = Uuid::uuid(); //v3
        $executive->classification = 'executive';

        $profile = new ExecutiveProfile([
            'bio' => '',
            'website' => '',
            'address' => '',
            'position' => '0 0',
        ]);

        $checklist = new UserChecklist();
        $checklist->step = 'activation';
        $checklist->completed = false;
        $checklist->completed_at = Carbon::now();

        DB::transaction(function () use ($executive, $profile, $checklist) {
            $executive->save();
            $executive->profile()->save($profile);
            $executive->checklist()->save($checklist);
        });

        return $executive;
    }

    /**
     * @param array $data
     * @param Executive $executive
     *
     * @return Executive
     */
    public function update(array $data, Executive $executive)
    {
        if (array_get($data, 'password')) {
            $passwordUser = $executive->password;
            
            if (Hash::check(array_get($data, 'password'), $passwordUser)) {   
                $executive->password = Hash::make(array_get($data, 'password_new'));
                
            } else {
               throw new PasswordMissMatchException();
            }
        }

        if (isset($data['name'])) {
            $executive->name = $data['name'];
        }

        $profile = $executive->profile ?? new ExecutiveProfile();

        $profile->fill($data);

        if (isset($data['position'])) {
            $profile->position = new Point(
                $data['position']['latitude'],
                $data['position']['longitude']
            );
        }

        $limitationsToBeRegistered = array_get($data, 'limitations');
        $executiveLimitation = [];

        if (!empty($limitationsToBeRegistered) and is_array($limitationsToBeRegistered)) {
            foreach ($limitationsToBeRegistered as $uuidLimitation) {
                $physicalLimitation = PhysicalLimitation::whereUuid($uuidLimitation)->first();

                $limitations = new ExecutivePhysicalLimitation($physicalLimitation->toArray());
                $limitations->physical_limitation_id = $physicalLimitation->id;

                $executiveLimitation[] = $limitations;
            }
        }

        $executivePayment = array_get($data, 'payment');
        $getExecutiveDocumentacion = array_get($data, 'documents');
       
        DB::transaction(function () use ($executive, $profile, $executiveLimitation, $executivePayment, $getExecutiveDocumentacion) {
        
            $executive->save();
            $executive->profile()->save($profile);

            if ($executiveLimitation) {
                $executive->limitations()->delete();
                $executive->limitations()->saveMany($executiveLimitation);
            }

            if ($executivePayment) {
                if ($executive->payment) {
                    $payment = $executive->payment;
                    $payment->update($executivePayment);

                } else {
                    $payment = new ExecutivePaymentProfile($executivePayment);
                    $profile->update(['registered_payment' => 1]);
                    $executive->payment()->save($payment);
                }
            }

            if ($getExecutiveDocumentacion) {
                $documentsProfile = [];

                foreach ($getExecutiveDocumentacion as $documents) {
                    foreach ($documents as $key => $value) {
                        $documentsProfile [$key] = $value;   
                    }    
                }

                $executiveDocumentacion = new ExecutiveDocumentation($documentsProfile);
                $profile->update(['verification_status' => ExecutiveStatus::VERIFICATION_STARTED]);
                $executive->documentExecutive()->save($executiveDocumentacion);         
            }

            if (!$executive->checklist()->where('step', '=', 'profile')->first()) {
                $checklist = new UserChecklist();
                $checklist->step = 'profile';
                $checklist->completed = true;
                $checklist->completed_at = Carbon::now();

                $executive->checklist()->save($checklist);
            }
        });

        $executive->refresh();

        return $executive;
    }

    public function getAvailableByRegion(MissionLocation $location, int $numProfiles, array $excludeProfiles)
    {
        $invitations = $location->invitations;

        if ($invitations) {
            foreach ($invitations as $invitation) {
                $excludeProfiles[] = $invitation->executive_id;
            }
        }

        $points = ExecutiveProfile::select(
            '*',
            DB::raw("ST_Distance_Sphere(`position`, ST_GeomFromText('{$location->position->toWKT()}')) as distance"))
            ->orderByDistanceSphere('position', $location->position, 'asc')
            ->whereNotIn('user_id', $excludeProfiles);

        return $points
            ->whereHas('executive', function ($query) {
                $query->where('classification', '!=', 'company');
            })
            ->limit($numProfiles)
            ->get();

    }

    public function index()
    {

        $executive = Executive::where('classification', '=', 'executive')
            ->get();

        return $executive->reverse();
    }
}
