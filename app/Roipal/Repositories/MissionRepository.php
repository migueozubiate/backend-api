<?php
namespace App\Roipal\Repositories;

use Carbon\Carbon;
use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Location;
use App\Roipal\Eloquent\Executive;
use Illuminate\Support\Facades\DB;
use App\Roipal\Scopes\MissionScope;
use App\Roipal\Constants\MissionStatus;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Constants\ExecutiveStatus;
use App\Roipal\Eloquent\ExecutiveMission;
use Waavi\Translation\Models\Translation;
use App\Roipal\Eloquent\MissionSuggestion;
use App\Exceptions\MissionQualifyException;
use App\Roipal\Eloquent\CommercialTypeSale;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Exceptions\TypeSaleActivitiesExistException;

class MissionRepository
{
    public function paginate(array $meta, MissionScope $scope)
    {
        $limit = array_get($meta, 'limit', 25);
        $relationships = array_get($meta, 'relationships', []);
        $search = array_get($meta, 'search');
        $field = array_get($meta, 'field');

        $user = $scope->getUser();

        $missions = Mission::query();
        $scope->build($missions, $meta);
        $orderBy = 'updated_at';

        if ($user->classification == 'executive') {
            $missions = Executive::find($user->id)->missions();
            $orderBy = 'missions.updated_at';
        }

        if (isset($search)) {
            if ($field) {
                if ($field === 'status') {
                    $missions->where("missions.{$field}", $search);
                } else {
                    $missions->where("missions.{$field}", 'LIKE', "%$search%");
                }
            } else {
                $missions->searchMission($search);
            }

            if ($relationships) {
                $available = [
                    'company',
                    'locations',
                    'activities',
                    'executives',
                ];

                $arrayRelations = [];
                foreach ($available as $key) {

                    if (in_array($key, $relationships)) {
                        $arrayRelations[$key] = $key;
                    }
                }

                $this->queryByRelationship($missions, $search, $arrayRelations);
            }
        }

        return $missions->orderby($orderBy, 'desc')->simplePaginate($limit);
    }

    protected function queryByRelationship($missions, $search, array $arrayRelations)
    {
        return $missions->orwhere(function ($query) use ($search, $arrayRelations) {

            $query->when(array_get($arrayRelations, 'company') === 'company',
                function ($query) use ($search) {
                    $query->orWhereHas('company', function ($query) use ($search) {
                        $query->where('name', 'LIKE', "%$search%")
                            ->orWhere('business_name', 'LIKE', "%$search%");
                    });
                })
                ->when(array_get($arrayRelations, 'locations') === 'locations',
                    function ($query) use ($search) {
                        $query->orWhereHas('locations', function ($query) use ($search) {
                            $query->where('name', 'LIKE', "%$search%")
                                ->orWhere('address', 'LIKE', "%$search%");
                        });
                    })
                ->when(array_get($arrayRelations, 'activities') === 'activities',
                    function ($query) use ($search) {
                        $query->orWhereHas('activities', function ($query) use ($search) {
                            $query->where('title', 'LIKE', "%$search%")
                                ->orWhere('description', 'LIKE', "%$search%");
                        });
                    })
                ->when(array_get($arrayRelations, 'executives') === 'executives',
                    function ($query) use ($search) {
                        $query->orWhereHas('executives', function ($query) use ($search) {
                            $query->where('name', 'LIKE', "%$search%")
                                ->orWhere('email', 'LIKE', "%$search%")
                                ->orWhere('classification', 'LIKE', "%$search%");
                        });
                    });

        });

    }

    public function store(Company $company, User $user, array $data)
    {  
        return DB::transaction(function () use ($company, $user, $data) {
            $locations = collect(array_get($data, 'locations'));
            $typeSale = array_get($data, 'type_sale');
            $executivesRequested = $locations->sum('executives_requested');
            $started_at = array_get($data, 'date');
            $typeSaleActivities = array_get($typeSale, 'type_sale_activities');
            $getTypeSale = 
                CommercialTypeSale::whereUuid($typeSale['uuid'])
                ->first();

            $getActivityByLocale = $this->getActivitiesByCountryAndItem(array_get($data, 'country'), $getTypeSale->item_type);
            
            $activities = json_decode($getActivityByLocale, true);
            
            $mission = new Mission($data);
            $mission->user_id = $user->id;
            $mission->type_sale_id = $getTypeSale->id;
            $mission->executives_requested = $executivesRequested;
            $mission->status = MissionStatus::CREATED;
            $mission->started_at = Carbon::parse($started_at);

            if (is_array($typeSaleActivities)) {
                
                if (count($typeSaleActivities) >= 1 ) {
                    $activitiesCustom = [];

                    $getActivities = $activities['activities'];

                    foreach ($typeSaleActivities as $value) {
                        if ($value >= 1) {
                            $activitiesCustom [] =  $getActivities[$value - 1][$value];
                        }
                    }

                    $mission->fill(['type_sale_activities' => $activitiesCustom]);

                } else {
                    throw new TypeSaleActivitiesExistException();
                }

            } else {
                $mission->fill(['type_sale_activities' => $activities['activities']]);
            }

            $mission = $company->missions()->save($mission);

            $missionLocationFillable = [];

            foreach ($locations as $value) {
                $location = new Location($value);
                $suggested = new MissionSuggestion(array_get($data, 'suggested', []));

                if (isset($value['uuid'])) {
                    $found = Location::whereUuid($value['uuid'])->first();

                    $location = $found ?? $location;
                }

                if (!$location->id) {
                    $location->position = new Point($value['latitude'], $value['longitude']);
                    $location->company_id = $company->id;
                    $location->save();
                }

                $missionLocation = new MissionLocation($location->toArray() + $value); 
                $missionLocation->location_id = $location->id;
                $missionLocation->user_id = $user->id;
                $missionLocation = $mission->locations()->save($missionLocation);
                $mission->suggestion()->save($suggested);
                $missionLocation->save();
            }

            return $mission;
        });
    }

    public function status(array $request, Company $company, Mission $mission)
    {
        return DB::transaction(function () use ($request, $company, $mission) {
            $status = array_get($request, 'status');

            foreach ($mission->executives as $executive) {
                if (!MissionStatus::isAllowedStatusCanceledExecutive($executive->pivot->status)) {

                    $mission
                        ->executives()
                        ->updateExistingPivot(
                            $executive->id, ['status' => $status]
                        );
                }
            }

            $mission->status = $status;
            $mission->save();

            return $mission;
        });
    }

    public function rateMission(array $request, Mission $mission, Executive $executive)
    {
        return DB::transaction(function () use ($request, $mission, $executive) {

            $executiveMission = ExecutiveMission::where('executive_id', $executive->id)
                ->where('mission_id', $mission->id)->first();

            if ($executiveMission->rate_mission > 0) {
                throw new MissionQualifyException('ONLY_QUALIFY_ONCE');
            }

            $executiveMission->rate_mission = array_get($request, 'rate');
            $executiveMission->review = array_get($request, 'review');
            $executiveMission->save();

            $missionsThatBelong = $executiveMission::whereMissionId($mission->id);
            $count = $missionsThatBelong->count();

            $executiveThatBelong = $executiveMission::whereExecutiveId($executive->id);
            $executiveMissionCount = $executiveThatBelong->count();
            $executiveScore = $executiveThatBelong->sum('rate_mission') / $executiveMissionCount;
            $executive->profile->update(['score' => $executiveScore]);

            $mission->score_mission = $missionsThatBelong->sum('rate_mission') / $count;;
            $mission->save();

            return $mission;
        });
    }

    public function executiveMissionStatus(array $request, Executive $executive, Mission $mission)
    {
        return DB::transaction(function () use ($request, $executive, $mission) {
            $status = array_get($request, 'status');

            if ($status == MissionStatus::CANCELED) {
                $mission->decrement('accepted');
                $mission->increment('rejected');
            }

            if ($status == MissionStatus::REJECT) {
                $mission->decrement('accepted');
                $mission->increment('rejected');
            }

            $mission
                ->executives()
                ->updateExistingPivot(
                    $executive->id, ['status' => $status]
                );

            $missions = $executive
                ->missions()
                ->where('mission_id', $mission->id)
                ->first();

            return $missions;
        });
    }

    public function getActivitiesByCountryAndItem($country, $item)
    {
        switch ($country) {
            case 'MX':
                $locale = 'es';
                break;
            case 'FR':
                $locale = 'fr';
                break;

            case 'US':
                $locale = 'en';
                break;
            default:
                $locale = 'en';
                # code...
                break;
        }

        $getActivities = Translation::whereLocale($locale)
                     ->whereItem($item)
                     ->first();

        return $getActivities->text;
    }
}
