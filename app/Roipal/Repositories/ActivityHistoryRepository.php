<?php
namespace App\Roipal\Repositories;

use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use App\Roipal\Constants\ActivityStatus;
use App\Roipal\Eloquent\ActivityHistory;
use App\Roipal\Eloquent\MissionActivity;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Carbon\Carbon;

class ActivityHistoryRepository
{
    public function index(array $request, MissionActivity $activity)
    {
        $limit = array_get($request, 'limit', 25);

        $activityHistory = ActivityHistory::whereMissionActivityId($activity->id)
                            ->orderBy('updated_at', 'desc')
                            ->simplePaginate($limit);

        return $activityHistory;
    }

    public function store(array $request, MissionActivity $activity)
    {
        
        $position = array_get($request, 'position');

        return DB::transaction(function () use ($request, $activity, $position) {
            $type = array_get($request, 'type');

            $activityHistory = new ActivityHistory($request);
            $activityHistory->position = new Point(array_get($position, 'latitude'), array_get($position, 'longitude'));
            $activityHistory->images = array_get($request, 'images');
            $activityHistory = $activity->activityHistory()->save($activityHistory);

            switch ($type) {
                case 'start':
                    if ($activity->status === 0) {
                        $activity->status = ActivityStatus::STARTED;
                        $activity->started_at = Carbon::now();
                        $activity->save();
                    }
                    break;

                case 'finish':
                    if ($activity->status === 1) {
                        $activity->status = ActivityStatus::FINISHED;
                        $activity->finished_at = Carbon::now();
                        $activity->save();
                    }
                    break;

                default:
                    # code...
                    break;
            }

            return $activityHistory;
        });
    }
}