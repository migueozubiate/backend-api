<?php
namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\UserWeb;
use Illuminate\Support\Facades\DB;

class UserWebRepository
{
    public function store(array $meta)
    {
        $firstName = array_get($meta, 'first_name');
        $lastName = array_get($meta, 'last_name');

        $firstName = ucwords(strtolower($firstName));
        $lastName = ucwords(strtolower($lastName));

        $userWeb = new UserWeb(array_only($meta, ['email', 'language']));
        $userWeb->first_name = $firstName;
        $userWeb->last_name = $lastName;
        
        return DB::transaction(function () use ($userWeb) {
            $userWeb->save();

            return $userWeb;
        });
    }
}