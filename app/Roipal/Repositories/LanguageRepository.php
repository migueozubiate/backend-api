<?php
namespace App\Roipal\Repositories;

use Illuminate\Support\Facades\DB;
use Waavi\Translation\Models\Language;

class LanguageRepository
{
   
    public function index($limit)
    {      
        $language = Language::simplePaginate($limit);
             
        return $language; 
    }

    public function store(array $data)
    {
        return DB::transaction(function () use ($data){
            $language = Language::create($data);
            $language->save();

            return $language;

        });
    } 

    public function update(array $data,Language $language)
    {
        return DB::transaction(function () use ($data,$language){
            $language->name = array_get($data, 'name');
            $language->save();
            
            return $language;

        });
    } 
}