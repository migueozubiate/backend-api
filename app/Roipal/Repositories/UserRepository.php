<?php
namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\User;
use App\Roipal\Scopes\UserScope;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserRepository
{
    public function index(array $meta, UserScope $scope)
    {
        $limit = array_get($meta, 'limit', 25);

        $search = array_get($meta, 'search');

        $field = array_get($meta, 'field');

        $query = User::query();

        $users = $scope->build($query, $meta);
        
        $users = $users->where($field ?? 'name', 'LIKE', "%$search%")
                        ->orWhere($field ?? 'name', 'LIKE', "%$search%")
                        ->orWhere($field ?? 'email', 'LIKE', "%$search%")
                        ->orWhere($field ?? 'classification', 'LIKE', "%$search%")
                        ->simplePaginate($limit);

        return $users;
    }

    public function resetPassword(array $meta)
    {
        return User::where('email', array_get($meta, 'email'))
                  ->first();
    }

}