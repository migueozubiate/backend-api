<?php
namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\LineBusiness;

class LineBusinessRepository
{
    public function index(array $meta)
    {
        $limit = array_get($meta, 'limit', 25);
        $search = array_get($meta, 'search');

        $lineBusiness = LineBusiness::query();

        if ($search) {
            $lineBusiness->searchLineBusiness($search);
        }

        return $lineBusiness->simplePaginate($limit);
    }

    public function lineBusinessBySector(array $meta, $sector)
    {
        $limit = array_get($meta, 'limit', 25);
        $search = array_get($meta, 'search');

        $lineBusiness = 
                LineBusiness::whereSector($sector);
                
        if ($search) {
            $lineBusiness->searchLineBusiness($search);
        }
                    
        return $lineBusiness->simplePaginate($limit);
    }
}
