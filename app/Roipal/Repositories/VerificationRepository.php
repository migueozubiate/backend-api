<?php

namespace App\Roipal\Repositories;

use App\Notifications\EmailVerifyRequest;
use App\Notifications\PasswordResetRequest;
use App\Roipal\Eloquent\PasswordReset;
use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\verifyUser;
use Carbon\Carbon;

class VerificationRepository
{

    public function index($token)
    {

        $PasswordReset = PasswordReset::where('token',$token)
            ->first();

        return $PasswordReset;

    }

    public function store(array $request, $type)
    {

        $user = User::where('email', $request['email'])->first();

        if ($type === 'PasswordReset') {

            $verifyUser = PasswordReset::updateOrCreate(
                [
                    'email' => $user->email,
                    'token' => str_random(60),
                ]
            );

            $user->notify(
                new PasswordResetRequest($verifyUser->token)
            );
        } else if ($type === 'verifyUser') {
            $verifyUser = verifyUser::updateOrCreate(
                [
                    'email' => $user->email,
                    'token' => str_random(60),
                ]
            );

            $user->notify(
                new EmailVerifyRequest($verifyUser->token)
            );
        }

        return $verifyUser;
    }

    public function put(array $request)
    {

        $verifyUser = PasswordReset::where([
            ['token', $request['token']],
            ['email', $request['email']],
        ])->first();

        $user = User::where('email', $verifyUser->email)->first();

        $user->password = bcrypt($request['secret']);
        $user->save();

        $verifyUser->delete();

        return $user;

    }

    public function verify(array $request)
    {

        $verifyUser = verifyUser::where([
            ['token', $request['token']],
        ])->first();

        $user = User::where('email', $verifyUser->email)->first();
        $user->email_verified_at = Carbon::now();
        $user->save();

        $verifyUser->delete();

        return $user;

    }
}
