<?php
namespace App\Roipal\Repositories;

use App\Roipal\Constants\MissionStatus;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\MissionActivity;
use App\Roipal\Scopes\ActivityScope;
use Illuminate\Support\Facades\DB;

class ActivityRepository
{
    public function index(array $request, Mission $mission, Executive $executive)
    {
        $limit = array_get($request, 'limit', 100);

        $missionActivity = MissionActivity::whereMissionId($mission->id)
            ->whereExecutiveId($executive->id)
            ->simplePaginate($limit);

        return $missionActivity;
    }

    public function show(array $meta, ActivityScope $scope)
    {
        $limit = array_get($meta, 'limit', 100);
        $relationships = array_get($meta, 'relationships', []);
        $search = array_get($meta, 'search');
        $field = array_get($meta, 'field');

        $activity = MissionActivity::query();

        $scope->build($activity, $meta);

        if ($search) {
            if ($field) {
                $activity->where($field, 'LIKE', "%$search%");
            } else {
                $activity->searchActivity($search);
            }

            if ($relationships) {
                $available = [
                    'mission',
                    'executive',
                    'company',
                ];

                $arrayRelations = [];
                foreach ($available as $key) {

                    if (in_array($key, $relationships)) {
                        $arrayRelations[$key] = $key;
                    }
                }

                $this->queryByRelationship($activity, $search, $arrayRelations);
            }
        }

        return $activity->latest()->simplePaginate($limit);
    }

    public function queryByRelationship($activity, $search, array $arrayRelations)
    {
        return $activity->orWhere(function ($query) use ($search, $arrayRelations) {

            $query->when(array_get($arrayRelations, 'company') === 'company',
                function ($query) use ($search) {
                    $query->orWhereHas('company', function ($query) use ($search) {
                        $query->where('name', 'LIKE', "%$search%")
                            ->orWhere('business_name', 'LIKE', "%$search%");
                    });
                })
                ->when(array_get($arrayRelations, 'mission') === 'mission',
                    function ($query) use ($search) {
                        $query->orWhereHas('mission', function ($query) use ($search) {
                            $query->where('name', 'LIKE', "%$search%")
                                ->orWhere('description', 'LIKE', "%$search%")
                                ->orWhere('type', 'LIKE', "%$search%")
                                ->orWhere('profile', 'LIKE', "%$search%");
                        });
                    })
                ->when(array_get($arrayRelations, 'executive') === 'executive',
                    function ($query) use ($search) {
                        $query->orWhereHas('executive', function ($query) use ($search) {
                            $query->where('name', 'LIKE', "%$search%")
                                ->orWhere('email', 'LIKE', "%$search%")
                                ->orWhere('classification', 'LIKE', "%$search%");
                        });
                    });

        });

    }

    public function store(array $request, Executive $executive, Mission $mission)
    {
        return DB::transaction(function () use ($mission, $request, $executive) {
            $missionActivity               = new MissionActivity($request);
            $missionActivity->status       = MissionStatus::CREATED;
            $missionActivity->executive_id = $executive->id;
            $missionActivity->company_id   = $mission->company->id;
            $missionActivity               = $mission->activities()->save($missionActivity);

            return $missionActivity;
        });
    }
}
