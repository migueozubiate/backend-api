<?php
namespace App\Roipal\Repositories;

use Illuminate\Support\Str;
use Illuminate\Support\Carbon;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use Illuminate\Support\Facades\DB;
use App\Roipal\Eloquent\Assestment;
use App\Roipal\Eloquent\AssestmentQuestion;
use App\Roipal\Eloquent\ExecutiveAssestment;
use App\Roipal\Traits\AsessmentCompanyTrait;
use App\Roipal\Eloquent\AssestmentAnswersPoll;
use App\Roipal\Eloquent\ExecutiveAssestmentAnswer;

class CompanyAssestmentRepository
{
    use AsessmentCompanyTrait;

    public function store(Mission $mission)
    {
        $assesmentCompanyQuestions = $this->getAsessmentCompany($mission);

        $questions = array_get($assesmentCompanyQuestions, 'questions');
        shuffle($questions);

        $number = 1;
        $questionsAsessment = []; 

        foreach ($questions as $value) {
            data_set($value, 'number', $number++);
            $questionsAsessment[] = $value;
        }

        array_forget($questionsAsessment, 5);

        $assesment = new Assestment($assesmentCompanyQuestions);
        $assesment->company_id = $mission->company->id;
        $assesment->mission_id = $mission->id;
        $assesment->total_questions = count($questionsAsessment);

        DB::transaction(function () use ($questionsAsessment, $assesment){
            $assesment->save();

            foreach ($questionsAsessment as $value) {
                $asessmentQuestion = new AssestmentQuestion($value);

                $assessmentAnswer = new AssestmentAnswersPoll($value);
                $assessmentAnswer->meaning = array_get($value, 'options');
                $assessmentAnswer->points = 20;

                $assesment->questions()->save($asessmentQuestion);
                $assesment->poll()->save($assessmentAnswer);
            }
        });
    }
    
    public function getAsessment(Mission $mission)
    {
        $assesment = Assestment::whereMissionId($mission->id)
                        ->get();

        return $assesment;
    }

    public function evaluationAssessment(array $meta, Mission $mission, Executive $executive)
    {
        $answers = array_get($meta, 'answers');

        $assesment = Assestment::whereMissionId($mission->id)->first();

        return DB::transaction(function () use ($answers, $assesment, $executive, $mission){
            $executiveAssessment = new ExecutiveAssestment($assesment->toArray());
            $executiveAssessment->user_id = $executive->id;
            $executiveAssessment->mission_id = $mission->id;
            $executiveAssessment->save();

            $assessmentQuestion = $assesment->questions;
            $assessmentPoll = $assesment->poll;

            $assestmetAnswers = [];

            foreach ($answers as $values) {
                $answer = new ExecutiveAssestmentAnswer($values);
                $question = $assessmentQuestion->where('number', $values['number'])->first();
                $eval = $assessmentPoll->where('number', (int)$answer->number)
                            ->where('answer', Str::upper($values['answer']))
                            ->first();

                $answer->question = $question->question;
                $answer->options = $question->options;
                $answer->points = $eval ? $eval->points : null;

                $assestmetAnswers[] = $answer;
            }

            $executiveAssessment
                        ->answers()
                        ->saveMany($assestmetAnswers);

            $result = 
                $executiveAssessment
                        ->answers()
                        ->sum('points');

            $executiveAssessment->points = $result;
            $executiveAssessment->save();

            return $executiveAssessment;
        });
    }
}