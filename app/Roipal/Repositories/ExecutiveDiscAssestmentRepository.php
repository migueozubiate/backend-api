<?php
namespace App\Roipal\Repositories;

use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Roipal\Eloquent\Executive;
use Illuminate\Support\Facades\DB;
use App\Roipal\Eloquent\Assestment;
use App\Roipal\Eloquent\UserChecklist;
use App\Exceptions\AssessmentExistException;
use App\Roipal\Eloquent\ExecutiveAssestment;
use App\Roipal\Eloquent\ExecutiveAssestmentAnswer;
use App\Roipal\Traits\AssessmentDiscDescriptionTrait;
use App\Roipal\Contracts\ExecutiveAssessmentRepositoryInterface;

class ExecutiveDiscAssestmentRepository implements ExecutiveAssessmentRepositoryInterface
{
    use AssessmentDiscDescriptionTrait;

    public function store(Assestment $disc, Executive $executive, array $data = [])
    {
        $assessment = 
                $executive
                    ->assestments()
                    ->whereName('DISC')
                    ->first();

        if ($assessment) {
            throw new AssessmentExistException($assessment->name);
        }

        return DB::transaction(function () use ($disc, $data, $executive) {
            $poll = $disc->poll;

            $assestmet = new ExecutiveAssestment($disc->toArray());

            $executive->assestments()->save($assestmet);

            $answers = array_get($data, 'answers');

            $discQuestions = $disc->questions;
            $assestmetAnswers = [];

            foreach ($answers as $values) {
                $answer = new ExecutiveAssestmentAnswer($values);
                $question = $discQuestions->where('number', $values['number'])->first();
                $eval = $poll->where('number', (int) $answer->number)
                    ->where('answer', Str::upper($values['answer']))
                    ->first();

                $answer->question = $question->question;
                $answer->options = $question->options;
                $answer->profile = $eval ? $eval->profile : null;
                $answer->points = $eval ? $eval->points : null;

                $assestmetAnswers[] = $answer;
            }

            $assestmet->answers()->saveMany($assestmetAnswers);

            $getProfilesResult =
                $assestmet
                    ->answers()
                    ->select('profile', \DB::raw('SUM(points) as points'))
                    ->groupBy('profile')
                    ->latest('points')
                    ->limit(2)
                    ->get();

            $getProfiles = [];

            foreach ($getProfilesResult as $value) {
                $getProfiles[] = $value['profile'];
            }

            $description = $this->getDiscDecription($getProfiles);
            $result = $getProfilesResult->first();
            $item = implode("", $getProfiles);

            $assestmet->profile = $result->profile;
            $assestmet->points = $result->points;
            $assestmet->description = $description;
            $assestmet->item = "description_{$item}";

            $assestmet->save();

            if (!$executive->checklist()->where('step', '=', 'disc-assessment')->first()) {
                $checklist = new UserChecklist();
                $checklist->step = 'disc-assessment';
                $checklist->completed = true;
                $checklist->completed_at = Carbon::now();

                $executive->checklist()->save($checklist);
            }

            return $assestmet;

        });
    }
}
