<?php
namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\ExecutiveAssestment;
use Illuminate\Database\Query\orderBy;
use Illuminate\Support\Arr;

class ExecutiveAssestmentRepository
{
    protected function getQueryByType($type, array $meta = [])
    {
        return ExecutiveAssestment::where('name', '=', $type);
    }

    public function paginateByType($type, array $meta = [])
    {
        $limit = array_get($meta, 'limit');

        return $this->getQueryByType($type, $meta)->paginate($limit ?? 50);
    }

    public function getByType($type, array $meta = [])
    {
        return $this->getQueryByType($type, $meta)->get();
    }

    public function getByUuid($uuid, array $meta = [])
    {
        return ExecutiveAssestment::where('uuid', '=', $uuid)->firstOrFail();
    }

    public function getScore(Executive $executive)
    {
        $assestments = 
            $executive
                ->assestments
                ->where('name', 'DISC')
                ->first();

        return $assestments;
    }
}