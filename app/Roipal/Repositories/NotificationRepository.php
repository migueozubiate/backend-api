<?php
namespace App\Roipal\Repositories;

use App\Roipal\Eloquent\User;
use Illuminate\Support\Facades\DB;

class NotificationRepository
{
    public function store(array $data, $user)
    {
        return DB::transaction(function () use ($data, $user) {

            $fcm_token = array_get($data, 'fcm_token');
            $user->fcm_token = $fcm_token;
            $user->save();

            return $user;
        });
    }

    public function deleteToken(array $meta)
    {
        $userUuid = array_get($meta, 'uuid');
        
        $user = User::whereUuid($userUuid)->first();

        return DB::transaction(function () use ($user) {

            if ($user) {
                $user->fcm_token = null;
                $user->save();
            }
    
            return $user;
        });
    }

}
