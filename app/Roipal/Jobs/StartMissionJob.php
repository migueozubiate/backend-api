<?php

namespace App\Roipal\Jobs;

use App\Roipal\Eloquent\Mission;
use App\Roipal\Strategies\PushNotifications\MissionNotificationStrategy;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StartMissionJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    private $mission;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Mission $mission)
    {
        //
        $this->mission = $mission;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MissionNotificationStrategy $notification)
    {
        $notification->notifyToExecutivesWhenCompanyStartedMission($this->mission);
    }
}
