<?php

namespace App\Roipal\Jobs;

use App\Roipal\Eloquent\MissionInvitation;
use App\Roipal\Eloquent\MissionInvitationControl;
use App\Roipal\Repositories\ExecutiveRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ProcessInvitationCanceledJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;
    private $invitation;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MissionInvitation $invitation)
    {
        $this->invitation = $invitation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ExecutiveRepository $repository)
    {
        $values = $this->invitation;
        $invitationCanceled = new MissionInvitationControl();
        $invitationCanceled->company_id = $values->company_id;
        $invitationCanceled->mission_id = $values->mission_id;
        $invitationCanceled->user_id = $values->user_id;
        $invitationCanceled->executive_id = $values->executive_id;
        $invitationCanceled->mission_location_id = $values->mission_location_id;
        $invitationCanceled->status = $values->status;
        $invitationCanceled->reason = $values->reason;
        $invitationCanceled->response_at = $values->response_at;
        $invitationCanceled->save();
    }
}
