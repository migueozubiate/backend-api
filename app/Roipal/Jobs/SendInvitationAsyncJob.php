<?php

namespace App\Roipal\Jobs;

use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\ExecutiveProfile;
use App\Roipal\Eloquent\Location;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Repositories\MissionInvitationRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendInvitationAsyncJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries=1;
    private $location;
    private $profile;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MissionLocation $location,  ExecutiveProfile $profile)
    {
        $this->location = $location;
        $this->profile = $profile;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(MissionInvitationRepository $repository)
    {
        $invitation = $repository->storeInvitation($this->location, $this->profile);

        $this->profile->increment('invitations_received');

        InvitationFirebaseNotificationJob::dispatch($invitation)->onQueue('mission');
    }
}
