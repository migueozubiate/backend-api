<?php
namespace App\Roipal\Jobs;

use Illuminate\Bus\Queueable;
use App\Roipal\Support\Firebase;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class ProcessFirebaseNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 3;

    protected $payload;
    protected $token;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(array $payload, $token)
    {
        $this->payload = $payload;
        $this->token = $token;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Firebase $firebase)
    {
        $firebase->notification($this->payload, $this->token);
    }
}
