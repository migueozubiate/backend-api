<?php

namespace App\Roipal\Jobs;

use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Jobs\SendInvitationAsyncJob;
use App\Roipal\Repositories\ExecutiveRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateInvitationByNearProfileJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;
    private $location;
    private $numProfiles;
    private $excludeProfiles;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MissionLocation $location, int $numProfiles, array $excludeProfiles)
    {
        $this->location = $location;
        $this->numProfiles = $numProfiles;
        $this->excludeProfiles = $excludeProfiles;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(ExecutiveRepository $repository)
    {
        $profiles = $repository->getAvailableByRegion($this->location, $this->numProfiles, $this->excludeProfiles);

        foreach ($profiles as $profile) {

            SendInvitationAsyncJob::dispatch($this->location, $profile)->onQueue('mission');

        }
    }
}