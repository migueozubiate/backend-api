<?php
namespace App\Roipal\Jobs;

use Illuminate\Bus\Queueable;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Support\Firebase;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use App\Roipal\Eloquent\MissionInvitation;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Constants\NotificationAction;
use App\Roipal\Transformers\CompanyTransformer;
use App\Roipal\Transformers\MissionTransformer;
use App\Roipal\Transformers\MissionLocationTransformer;
use App\Roipal\Transformers\MissionInvitationTransformer;
use App\Roipal\Transformers\Executives\ExecutiveProfileTransformer;

class InvitationFirebaseNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 1;
    private $invitation;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(MissionInvitation $invitation)
    {
        $this->invitation = $invitation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Firebase $firebase)
    {
        $executive = $this->invitation->executive;
        $location = $this->invitation->location;

        if ($executive->fcm_token) {
            $missionName = $this->invitation->mission->name;
            $companyName = $this->invitation->mission->company->name;

            $payload = [
                'app' => 'executive',
                'title' => trans('invitations.invitation_mission', ['mission' => $missionName]),
                'body' => trans('invitations.invitation_company', ['company' => $companyName, 'mission' => $missionName]),
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::SEND_INVITATION,
                    'invitation' => Fractal::item($this->invitation, new MissionInvitationTransformer),
                    'mission' => Fractal::item($this->invitation->mission, new MissionTransformer),
                    'location' => Fractal::item($this->invitation->location, new MissionLocationTransformer),
                    'executive_profile' => Fractal::item($executive->profile, new ExecutiveProfileTransformer),
                    'company' => Fractal::including('profile')
                                    ->item($this->invitation->company, new CompanyTransformer)
                ],
            ];

            $firebase->notification($payload, $executive->fcm_token);
        }
    }

}
