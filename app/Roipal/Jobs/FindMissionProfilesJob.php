<?php

namespace App\Roipal\Jobs;

use App\Roipal\Eloquent\Mission;
use App\Roipal\Jobs\CreateInvitationByNearProfileJob;
use App\Roipal\Repositories\ExecutiveRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class FindMissionProfilesJob implements ShouldQueue
{

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 1;
    private $mission;
    private $numProfiles;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Mission $mission, array $numProfiles)
    {
        $this->mission = $mission;
        $this->numProfiles = $numProfiles;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $locations = $this->mission->locations;
   
        foreach ($locations as $key => $location) {
         
            CreateInvitationByNearProfileJob::dispatch($location, $this->numProfiles[$key], [0])->onQueue('mission');
          
        }
    }
}
