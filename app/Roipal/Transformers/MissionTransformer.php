<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\Mission;
use Illuminate\Support\Facades\Auth;
use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\ExecutiveMission;
use App\Roipal\Transformers\MissionActivityTransformer;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;
use App\Roipal\Transformers\Executives\ExecutiveMissionTransformer;
use App\Roipal\Transformers\CommercialNeeds\CommercialTypeSalesTransformer;
use App\Roipal\Transformers\MissionSuggestionTransformer;

class MissionTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'activities',
        'locations',
        'company',
        'invitations',
        'executives',
        'type_sales',
        'room_chat'
    ];

    public function transform(Mission $mission)
    {
        return [
            'uuid' => $mission->uuid,
            'name' => $mission->name,
            'description' => $mission->description,
            'video' => (bool) isset($mission->video_url),
            'video_url' => $mission->video_url,
            'thumbnail_video' => $mission->thumbnail_video,
            'commercial_need' => $mission->commercial_need,
            'type' => $mission->type,
            'profile' => $mission->profile,
            'time' => (int) $mission->time,
            'time_unit' => trans('missions.mission_time_unit'),
            'fare_by_time' => (float) $mission->fare_by_time,
            'subtotal' => (float) $mission->subtotal,
            'tax' => (float) $mission->tax,
            'status' => $mission->pivot->status ?? $mission->status,
            'total' => (float) $mission->total,
            'type_sale_activities' => $mission->type_sale_activities,
            'currency' => $mission->currency,
            'executives_requested' => (int) $mission->executives_requested,
            'invitations_sent' => (int) $mission->invitations_sent,
            'accepted' => (int) $mission->accepted,
            'rejected' => (int) $mission->rejected,
            'score_mission' => (float) $mission->score_mission,
            'started_at' => (string) $mission->started_at,
            'updated_at' => (string) $mission->updated_at,
            'created_at' => (string) $mission->created_at,
        ];
    }

    public function includeActivities(Mission $mission)
    {
        return $this->collection($mission->activities, new MissionActivityTransformer);
    }

    public function includeLocations(Mission $mission)
    {
        return $this->collection($mission->locations, new MissionLocationTransformer);
    }

    public function includeCompany(Mission $mission)
    {
        if (!$mission->company) {
            return $this->null();
        }

        return $this->item($mission->company, new CompanyTransformer);
    }

    public function includeInvitations(Mission $mission)
    {
        return $this->collection($mission->invitations, new MissionInvitationTransformer);
    }

    public function includeExecutives(Mission $mission)
    {
        return $this->collection($mission->executives, new ExecutiveTransformer);
    }

    public function includeRoomChat(Mission $mission)
    {
        $user = Auth::user();

        if ($user->classification !== 'executive') 
        {
            return $this->null();
        }

        $executiveMission = 
            ExecutiveMission::whereMissionId($mission->id)
            ->whereExecutiveId($user->id)
            ->first();

        return $this->item($executiveMission, new ExecutiveMissionTransformer);
    }

    public function includeTypeSales(Mission $mission)
    {
        if (!$mission->typeSales) {
            return $this->null();
        }

        return $this->item($mission->typeSales, new CommercialTypeSalesTransformer);
    }

}
