<?php
namespace App\Roipal\Transformers;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\MissionInvitation;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;

class MissionInvitationTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'mission',
        'location',
        'company',
        'executive'
    ];

    public function transform(MissionInvitation $invitation)
    {
        return [
            'uuid' => $invitation->uuid,
            'status' => (int)$invitation->status,
            'reason' => $invitation->reason,
            'notification' => $invitation->notification,
            'response_at' => (string)$invitation->response_at,
            'updated_at' => (string)$invitation->updated_at,
            'created_at' => (string)$invitation->created_at
        ];
    }

    public function includeMission(MissionInvitation $invitation)
    {
        return $this->item($invitation->mission, new MissionTransformer);
    }

    public function includeLocation(MissionInvitation $invitation)
    {
        return $this->item($invitation->location, new MissionLocationTransformer);
    }

    public function includeCompany(MissionInvitation $invitation)
    {
        return $this->item($invitation->company, new CompanyTransformer);
    }

    public function includeExecutive(MissionInvitation $invitation)
    {
        return $this->item($invitation->executive, new ExecutiveTransformer);
    }
}
