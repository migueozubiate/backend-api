<?php
namespace App\Roipal\Transformers;

use Waavi\Translation\Models\Language;
use League\Fractal\TransformerAbstract;

class LanguageTransformer extends TransformerAbstract
{
    
    public function transform(Language $language)
    {
        return [
            'id' => $language->id, 
            'locale' => $language->locale,
            'name' => $language->name,
            'updated_at' => (string)$language->updated_at,
            'created_at' => (string)$language->created_at,
        ];
    }
}