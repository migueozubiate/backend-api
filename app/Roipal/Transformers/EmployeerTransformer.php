<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\Employeer;
use League\Fractal\TransformerAbstract;

class EmployeerTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
    ];

    public function transform(Employeer $employeer)
    {
        return [
            'uuid' => $employeer->uuid,
            'company_id' => $employeer->company_id,
            'name' => $employeer->name,
            'email' => $employeer->email,
            'classification' => $employeer->classification,
            'updated_at' => (string)$employeer->updated_at,
            'created_at' => (string)$employeer->created_at,
        ];
    }
}