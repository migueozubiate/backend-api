<?php
namespace App\Roipal\Transformers;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\ActivityHistory;
use App\Roipal\Transformers\Geometric\PointTransformer;
use App\Roipal\Transformers\MissionActivityTransformer;

class ActivityHistoryTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'activity'
    ];

    public function transform(ActivityHistory $activityHistory)
    {
        $position = $activityHistory->position;

        return [
            'uuid' => $activityHistory->uuid,
            'type' => $activityHistory->type,
            'comment' => $activityHistory->comment,
            'address' => $activityHistory->address,
            'position' => [
                'latitude' => $position->getLat(),
                'longitude' => $position->getLng()
            ],
            'images' => $activityHistory->images,
            'updated_at' => (string)$activityHistory->updated_at,
            'created_at' => (string)$activityHistory->created_at,
        ];
    }

    public function includeActivity(ActivityHistory $activityHistory)
    {
        return $this->item($activityHistory->activities, new MissionActivityTransformer);
    }
}