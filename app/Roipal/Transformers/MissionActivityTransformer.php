<?php
namespace App\Roipal\Transformers;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\MissionActivity;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use App\Roipal\Transformers\CompanyTransformer;
use App\Roipal\Transformers\MissionTransformer;
use App\Roipal\Transformers\Geometric\PointTransformer;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;


class MissionActivityTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'mission',
        'executive',
        'company'
    ];

    public function transform(MissionActivity $activity)
    {

        $point = new PointTransformer();
        
        return [
            'uuid' => $activity->uuid,
            'title' => $activity->title,
            'description' => $activity->description,
            'status' => $activity->status,
            'started_at' => $activity->started_at,
            'finished_at' => $activity->finished_at,
            'created_at' => (string) $activity->created_at,
            'updated_at' => (string) $activity->updated_at
        ];
    }

    public function includeMission(MissionActivity $activity)
    {
        return $this->item($activity->mission, new MissionTransformer);
    }

    public function includeExecutive(MissionActivity $activity)
    {
        return $this->item($activity->executive, new ExecutiveTransformer);
    }

    public function includeCompany(MissionActivity $activity)
    {
        return $this->item($activity->company, new CompanyTransformer);
    }

}