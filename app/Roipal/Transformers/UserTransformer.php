<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'company',
        'checklist'
    ];

    public function transform(User $user)
    {
        return [
            'uuid' => $user->uuid,
            'name' => $user->name,
            'email' => $user->email,
            'classification' => $user->classification,
            'fcm_token' => $user->fcm_token,
            'avatar' => $user->avatar,
            'updated_at' => (string)$user->updated_at,
            'created_at' => (string)$user->created_at,
        ];
    }

    public function includeCompany(User $user)
    {
        return $user->company ? $this->item($user->company, new CompanyTransformer) : null;
    }

    public function includeChecklist(User $user)
    {
        return $this->collection($user->checklist, new UserChecklistTransformer);
    }
}