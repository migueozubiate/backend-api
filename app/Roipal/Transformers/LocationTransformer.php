<?php

namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\Location;
use League\Fractal\TransformerAbstract;
use App\Roipal\Transformers\CompanyTransformer;

class LocationTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'company',
    ];

    public function transform(Location $location)
    {
        $position = $location->position;

        return [
            'uuid' => $location->uuid,
            'name' => $location->name,
            'address' => $location->address,
            'latitude' => $position->getLat(),
            'longitude' => $position->getLng(),
            'updated_at' => (string)$location->updated_at,
            'created_at' => (string)$location->created_at,
        ];
    }

    public function includeCompany(Location $location)
    {
        return $this->item($location->company, new CompanyTransformer);
    }
}