<?php
namespace App\Roipal\Transformers\Companies;

use App\Roipal\Eloquent\Company;
use App\Roipal\Constants\MissionStatus;
use League\Fractal\TransformerAbstract;
use App\Roipal\Transformers\Companies\CompanyMissionsCountTransformer;

class CompanyMissionsCountTransformer extends TransformerAbstract
{
    public function transform(Company $company)
    {
        return [
            'missions_created' => $company->missions()
                                        ->whereStatus(MissionStatus::CREATED)
                                        ->count(),
            'missions_started' => $company->missions()
                                        ->whereStatus(MissionStatus::STARTED)
                                        ->count(),
            'missions_finished' => $company->missions()
                                        ->whereStatus(MissionStatus::COMPLETED)
                                        ->count(),
            'missions_canceled' => $company->missions()
                                        ->whereStatus(MissionStatus::CANCELED)
                                        ->count(),
        ];
    }
}