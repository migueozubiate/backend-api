<?php
namespace App\Roipal\Transformers\Catalogs;

use App\Roipal\Eloquent\Catalog;
use League\Fractal\TransformerAbstract;

class MissionTypeTransformer extends TransformerAbstract
{
  public function transform(Catalog $catalog)
  {
    return [
      'id' => $catalog->id,
      'language' => $catalog->language,
      'type' => $catalog->value,
      'icon' => $catalog->icon,
    ];
  }
}