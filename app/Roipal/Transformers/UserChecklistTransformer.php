<?php
namespace App\Roipal\Transformers;

use Grimzy\LaravelMysqlSpatial\Types\Point;
use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\UserChecklist;

class UserChecklistTransformer extends TransformerAbstract
{
    public function transform(UserChecklist $checklist)
    {
        return [
            'step' => $checklist->step,
            'completed' => (bool)$checklist->completed,
            'completed_at' => (string)$checklist->completed_at,
            'created_at' => (string)$checklist->created_at,
            'updated_at' => (string)$checklist->updated_at,
        ];
    }

}