<?php
namespace App\Roipal\Transformers\Assestments;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\ExecutiveAssestmentAnswer;

class ExecutiveAssestmentAnswerTransformer extends TransformerAbstract
{
    public function transform(ExecutiveAssestmentAnswer $question)
    {
        return [
            'number' => $question->number,
            'question' => $question->question,
            'options' => $question->options,
            'answer' => $question->answer,
            'profile' => $question->profile,
            'points' => $question->points,
            'created_at' => (string)$question->created_at,
            'updated_at' => (string)$question->updated_at,
        ];
    }
}