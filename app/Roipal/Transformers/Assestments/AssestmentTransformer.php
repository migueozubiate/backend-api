<?php
namespace App\Roipal\Transformers\Assestments;

use App\Roipal\Eloquent\Assestment;
use League\Fractal\TransformerAbstract;
use App\Roipal\Transformers\CompanyTransformer;
use App\Roipal\Transformers\MissionTransformer;

class AssestmentTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'company',
        'mission',
        'questions'
    ];

    public function transform(Assestment $assestment)
    {
        return [
            'uuid' => $assestment->uuid,
            'name' => $assestment->name,
            'lang' => $assestment->lang,
            'summary' => $assestment->summary,
            'total_questions' => $assestment->total_questions,
            'created_at' => (string)$assestment->created_at,
            'updated_at' => (string)$assestment->updated_at,
        ];
    }

    public function includeQuestions(Assestment $assestment)
    {
        return $this->collection($assestment->questions, new AssestmentQuestionTransformer);
    }

    public function includeCompany(Assestment $assestment)
    {
        return $this->item($assestment->company, new CompanyTransformer);
    }

    public function includeMission(Assestment $assestment)
    {
        return $this->item($assestment->mission, new MissionTransformer);
    }
}