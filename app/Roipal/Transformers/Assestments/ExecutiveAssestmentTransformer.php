<?php
namespace App\Roipal\Transformers\Assestments;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\ExecutiveAssestment;
use App\Roipal\Transformers\CompanyTransformer;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;
use App\Roipal\Transformers\Assestments\ExecutiveAssestmentAnswerTransformer;

class ExecutiveAssestmentTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'answers',
        'company',
        'executive'
    ];

    public function transform(ExecutiveAssestment $assestment)
    {
        return [
            'uuid' => $assestment->uuid,
            'name' => $assestment->name,
            'profile' => $assestment->profile,
            'points' => $assestment->points,
            'validity' => $assestment->validity,
            'description' => json_decode(trans("description_profiles.{$assestment->item}")),
            'total_questions' => $assestment->total_questions,
            'created_at' => (string)$assestment->created_at,
            'updated_at' => (string)$assestment->updated_at,
        ];
    }

    public function includeAnswers(ExecutiveAssestment $assestment)
    {
        return $this->collection($assestment->answers, new ExecutiveAssestmentAnswerTransformer);
    }

    public function includeCompany(ExecutiveAssestment $assestment)
    {
        if (is_null($assestment->company)) {
            return $this->null();
        }

        return $this->item($assestment->company, new CompanyTransformer);
    }

    public function includeExecutive(ExecutiveAssestment $assestment)
    {
        return $this->item($assestment->executive, new ExecutiveTransformer);
    }

}