<?php
namespace App\Roipal\Transformers\Assestments;

use App\Roipal\Eloquent\AssestmentQuestion;
use League\Fractal\TransformerAbstract;

class AssestmentQuestionTransformer extends TransformerAbstract
{

    public function transform(AssestmentQuestion $question)
    {
        return [
            'number' => $question->number,
            'type' => $question->type,
            'group' => $question->group,
            'question' => $question->question,
            'options' => $question->options,
            'answer' => $question->answer,
            'created_at' => (string) $question->created_at,
            'updated_at' => (string) $question->updated_at,
        ];
    }
}