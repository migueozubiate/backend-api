<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\Company;
use League\Fractal\TransformerAbstract;
use App\Roipal\Transformers\UserTransformer;
use App\Roipal\Transformers\CompanyContacTransformer;
use App\Roipal\Transformers\Companies\CompanyMissionsCountTransformer;

class CompanyTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'profile',
        'payments_profiles',
        'missions_status',
        'contact',
        'owner'
    ];

    public function transform(Company $company)
    {
        return [
            'uuid' => $company->uuid,
            'business_name' => $company->business_name,
            'name' => $company->name,
            'vertical' => $company->vertical,
            'rfc' => $company->rfc,
            'terms_accepted' => $company->terms_accepted,
            'updated_at' => (string)$company->updated_at,
            'created_at' => (string)$company->created_at,
        ];
    }

    public function includeProfile(Company $company)
    {
        if (!$company->profile) {
            return $this->null();
        }
        
        return $this->item($company->profile, new CompanyProfileTransformer);
    }

    public function includeContact(Company $company)
    {
        if (!$company->profile) {
            return $this->null();
        }
        
        return $this->item($company->profile, new CompanyContacTransformer);
    }

    public function includePaymentsProfiles(Company $company)
    {
        return $this->collection($company->payments, new PaymentProfileTransformer);
    }
    
    public function includeMissionsStatus(Company $company)
    {
        return $this->item($company, new CompanyMissionsCountTransformer);
    }

    public function includeOwner(Company $company)
    {
        return $this->item($company->owner, new UserTransformer);
    }
}