<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\PaymentProfile;
use League\Fractal\TransformerAbstract;

class PaymentProfileTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
    ];

    public function transform(PaymentProfile $paymentProfile)
    {
        if (strcasecmp ($paymentProfile->type , "american-express") == 0 ) {
            $lastNumbersCard = substr($paymentProfile->card_number, -3);
            
        } else {
            $lastNumbersCard = substr($paymentProfile->card_number, -4);
        }


        return [
            'owner' => $paymentProfile->owner,
            'clasification' => $paymentProfile->clasification,
            'card_number' => $lastNumbersCard,
            'exp' => $paymentProfile->exp,
            'type' => $paymentProfile->type,
            'ccv' => $paymentProfile->ccv,
            'updated_at' => (string)$paymentProfile->updated_at,
            'created_at' => (string)$paymentProfile->created_at,
        ];
    }
}