<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\CompanyProfile;
use League\Fractal\TransformerAbstract;

class CompanyContacTransformer extends TransformerAbstract
{
    public function transform(CompanyProfile $companyContact)
    {
        return [
            'contact_name' => $companyContact->contact_name,
            'contact_phone' => $companyContact->contact_phone,
            'contact_job_title' => $companyContact->contact_job_title,
        ];
    }
}