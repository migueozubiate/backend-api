<?php

namespace App\Roipal\Transformers;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\PhysicalLimitation;

class PhysicalLimitationsTransformer extends TransformerAbstract
{
    public function transform(PhysicalLimitation $limitations)
    {
        return [
            'uuid' => $limitations->uuid,
            'limitation' => trans("physical_limitations.{$limitations->item}"),
            'flag' => $limitations->flag,
            'created_at' => (string) $limitations->created_at,
            'updated_at' => (string) $limitations->updated_at
        ];
    }
}