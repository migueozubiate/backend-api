<?php
namespace App\Roipal\Transformers;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;
use App\Roipal\Transformers\MissionTransformer;

class MissionLocationTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'executives',
        'mission'
    ];

    public function transform(MissionLocation $location)
    {
        $position = $location->position;

        return [
            'uuid' => $location->uuid,
            'name' => $location->name,
            'address' => $location->address,
            'executives_requested' => $location->executives_requested,
            'invitations_sent' => $location->invitations_sent,
            'accepted' => (int)$location->accepted,
            'rejected' => (int)$location->rejected,
            'latitude' => $position->getLat(),
            'longitude' => $position->getLng(),
            'updated_at' => (string)$location->updated_at,
            'created_at' => (string)$location->created_at,
        ];
    }

    public function includeExecutives(MissionLocation $location)
    {
        return $this->collection($location->executives, new ExecutiveTransformer);
    }

    public function includeMission(MissionLocation $location)
    {
        return $this->item($location->mission, new MissionTransformer);
    }


}