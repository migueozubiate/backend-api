<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\MissionSuggestion;
use League\Fractal\TransformerAbstract;

class MissionSuggestionTransformer extends TransformerAbstract
{
    public function transform(MissionSuggestion $missionSuggestion)
    {
        return [
            'currency' => $missionSuggestion->currency,
        ];
    }
}