<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\LineBusiness;
use League\Fractal\TransformerAbstract;

class LineBusinessTransformer extends TransformerAbstract
{
    public function transform(LineBusiness $lineBusiness)
    {
        return [
            'uuid' => $lineBusiness->uuid,
            'code' => $lineBusiness->code,
            'line_business' => $lineBusiness->line_business,
            'sector' => $lineBusiness->sector,
            'created_at' => (string) $lineBusiness->created_at,
            'updated_at' => (string) $lineBusiness->updated_at
        ];
    }
}