<?php

namespace App\Roipal\Transformers\CommercialNeeds;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\CommercialTypeSale;

class CommercialTypeSalesTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'type_activities'
    ];

    public function transform(CommercialTypeSale $typeSales)
    {
        $transTypeSales = json_decode(trans("commercial_type_sales.{$typeSales->item_type}"));

        return [
            'uuid' => $typeSales->uuid,
            'type' => $transTypeSales->type,
            'description' => $transTypeSales->description,
            'created_at' => (string) $typeSales->created_at,
            'updated_at' => (string) $typeSales->updated_at
        ];
    }

    public function includeTypeActivities(CommercialTypeSale $typeSales)
    {
        return $this->item($typeSales, new CommercialTypeActivitiesTransformer);
    }
}