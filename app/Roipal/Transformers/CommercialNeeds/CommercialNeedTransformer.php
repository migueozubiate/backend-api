<?php

namespace App\Roipal\Transformers\CommercialNeeds;

use App\Roipal\Eloquent\CommercialNeed;
use League\Fractal\TransformerAbstract;
use App\Roipal\Transformers\CommercialNeeds\CommercialTypeSalesTransformer;

class CommercialNeedTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'type_sales'
    ];

    public function transform(CommercialNeed $commercialNeed)
    {
        return [
            'uuid' => $commercialNeed->uuid,
            'commercial_need' => trans("commercial_needs.{$commercialNeed->commercial_item}"),
            'created_at' => (string) $commercialNeed->created_at,
            'updated_at' => (string) $commercialNeed->updated_at
        ];
    }

    public function includeTypeSales(CommercialNeed $commercialNeed)
    {
        return $this->collection($commercialNeed->typeSales, new CommercialTypeSalesTransformer);
    }
}