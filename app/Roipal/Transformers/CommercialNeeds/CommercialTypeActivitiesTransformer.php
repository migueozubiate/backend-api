<?php

namespace App\Roipal\Transformers\CommercialNeeds;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\CommercialTypeSale;

class CommercialTypeActivitiesTransformer extends TransformerAbstract
{
    public function transform(CommercialTypeSale $typeSales)
    {
        $transTypeSales = json_decode(trans("commercial_type_sales.{$typeSales->item_type}"));
        
        return  $transTypeSales->activities;
    }
}