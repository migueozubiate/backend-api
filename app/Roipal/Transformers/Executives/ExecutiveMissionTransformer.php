<?php
namespace App\Roipal\Transformers\Executives;

use App\Roipal\Eloquent\Executive;
use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\ExecutiveMission;

class ExecutiveMissionTransformer extends TransformerAbstract
{
    public function transform(ExecutiveMission $executiveMission)
    {
        return [
            'room_uuid' => $executiveMission->uuid,
        ];
    }
}
