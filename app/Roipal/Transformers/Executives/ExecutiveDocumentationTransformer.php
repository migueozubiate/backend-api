<?php
namespace App\Roipal\Transformers\Executives;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\ExecutiveDocumentation;

class ExecutiveDocumentationTransformer extends TransformerAbstract
{
    public function transform(ExecutiveDocumentation $executiveDocumentation)
    {
        return [
            'national_id_back_url' => $executiveDocumentation->national_id_back_url,
            'national_id_from_url' => $executiveDocumentation->national_id_from_url,
            'proof_residency_url' => $executiveDocumentation->proof_residency_url,
            'curp_url' => $executiveDocumentation->curp_url,
            'rfc_url' => $executiveDocumentation->rfc_url,
        ];
    }
}
