<?php
namespace App\Roipal\Transformers\Executives;

use App\Roipal\Eloquent\Executive;
use League\Fractal\TransformerAbstract;
use App\Roipal\Transformers\MissionTransformer;
use App\Roipal\Transformers\UserChecklistTransformer;
use App\Roipal\Transformers\Executives\ExecutiveMissionTransformer;
use App\Roipal\Transformers\Executives\ExecutiveDocumentationTransformer;
use App\Roipal\Transformers\Executives\ExecutivePaymentProfileTransformer;
use App\Roipal\Transformers\Executives\ExecutivePhysicalLimitationTransformer;


class ExecutiveTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
        'profile',
        'address',
        'assessments',
        'missions',
        'checklist',
        'limitations',
        'payments_profiles',
        'documents_profile'
    ];

    public function transform(Executive $executive)
    {
        return [
            'uuid' => $executive->uuid,
            'name' => $executive->name,
            'email' => $executive->email,
            'classification' => $executive->classification,
            'status' => $executive->pivot->status ?? null,
            'rate_mission' => $executive->pivot->rate_mission ?? null,
            'room_uuid' => $executive->pivot->uuid ?? null,
            'created_at' => (string) $executive->created_at,
            'updated_at' => (string) $executive->updated_at,
        ];
    }

    public function includeProfile(Executive $executive)
    {
        if (is_null($executive->profile)) {
            return $this->null();
        }

        return $this->item($executive->profile, new ExecutiveProfileTransformer);
    }

    public function includeAddress(Executive $executive)
    {
        if (is_null($executive->profile)) {
            return $this->null();
        }

        return $this->item($executive->profile, new ExecutiveAddressTransformer);
    }

    public function includeAssessments(Executive $executive)
    {
        return [];
    }

    public function includeMissions(Executive $executive)
    {
        return $this->collection($executive->missions, new MissionTransformer);
    }

    public function includeChecklist(Executive $user)
    {
        return $this->collection($user->checklist, new UserChecklistTransformer);
    }

    public function includeLimitations(Executive $executive)
    {
        return $this->collection($executive->limitations, new ExecutivePhysicalLimitationTransformer);
    }

    public function includePaymentsProfiles(Executive $executive)
    {
        return $this->collection($executive->payments, new ExecutivePaymentProfileTransformer);
    }

    public function includeDocumentsProfile(Executive $executive)
    {
        return $this->item($executive->documentExecutive, new ExecutiveDocumentationTransformer);
    }
}
