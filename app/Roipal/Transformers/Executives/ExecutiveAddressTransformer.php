<?php
namespace App\Roipal\Transformers\Executives;

use App\Roipal\Eloquent\ExecutiveProfile;
use League\Fractal\TransformerAbstract;

class ExecutiveAddressTransformer extends TransformerAbstract
{
    public function transform(ExecutiveProfile $profile)
    {
        return [
            'address' => $profile->address,
            'latitude' => $profile->position->getLat(),
            'longitude' => $profile->position->getLng()
        ];

    }

}