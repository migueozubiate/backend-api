<?php
namespace App\Roipal\Transformers\Executives;

use App\Roipal\Eloquent\ExecutiveProfile;
use League\Fractal\TransformerAbstract;

class ExecutiveProfileTransformer extends TransformerAbstract
{

    protected $availableIncludes = [
        'executive',
    ];
    public function transform(ExecutiveProfile $profile)
    {
        $position = $profile->position;
        return [
            'bio' => $profile->bio,
            'phone' => $profile->phone,
            'video_bio' => $profile->video_bio,
            'video_bio_url' => $profile->video_bio_url,
            'thumbnail_video' => $profile->thumbnail_video,
            'photo_bio_url' => $profile->photo_bio_url,
            'website' => $profile->website,
            'address' => $profile->address,
            'country' => $profile->country,
            'score' => $profile->score,
            'terms_accepted' => $profile->terms_accepted,
            'registered_payment' => $profile->registered_payment,
            'invitations_received' => $profile->invitations_received,
            'position' => [
                'latitude' => $position->getLat(),
                'longitude' => $position->getLng(),
            ],
        ];
    }

    public function includeExecutive(ExecutiveProfile $profile)
    {
        return $this->item($profile->executive, new ExecutiveTransformer);
    }

}
