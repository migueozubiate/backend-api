<?php
namespace App\Roipal\Transformers\Executives;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\ExecutivePaymentProfile;

class ExecutivePaymentProfileTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
    ];

    public function transform(ExecutivePaymentProfile $executivePayment)
    {    
        switch ($executivePayment->type_payment) {
            case 'card':
                if (strcasecmp ($executivePayment->type_card , "american-express") == 0 ) {
                    $lastNumbersCard = substr($executivePayment->card_number, -3);
                    
                } else {
                    $lastNumbersCard = substr($executivePayment->card_number, -4);
                }

                return [
                    'owner' => $executivePayment->owner,
                    'type_payment' => $executivePayment->type_payment,
                    'card_number' => $lastNumbersCard,
                    'bank' => $executivePayment->bank,
                    'type_card' => $executivePayment->type_card,
                    'updated_at' => (string) $executivePayment->updated_at,
                    'created_at' => (string) $executivePayment->created_at,
                ];

                break;
            
            case 'paypal':
                return [
                    'owner' => $executivePayment->owner,
                    'type_payment' => $executivePayment->type_payment,
                    'email' => $executivePayment->email,
                    'updated_at' => (string) $executivePayment->updated_at,
                    'created_at' => (string) $executivePayment->created_at,
                ];
                break;
            
            case 'account':
                return [
                    'owner' => $executivePayment->owner,
                    'type_payment' => $executivePayment->type_payment,
                    'bank' => $executivePayment->bank,
                    'clabe' => substr($executivePayment->clabe, -4),
                    'updated_at' => (string) $executivePayment->updated_at,
                    'created_at' => (string) $executivePayment->created_at,
                ];

                break;

            default:
                return [];
                break;
        }
    }
}
