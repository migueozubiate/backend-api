<?php
namespace App\Roipal\Transformers\Executives;

use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\ExecutivePhysicalLimitation;

class ExecutivePhysicalLimitationTransformer extends TransformerAbstract
{
    public function transform(ExecutivePhysicalLimitation $executiveLimitations)
    {
        return [
            'uuid' => $executiveLimitations->physicalLimitations->uuid,
            'limitation' => $executiveLimitations->limitation,
            'created_at' => (string) $executiveLimitations->created_at,
            'updated_at' => (string) $executiveLimitations->updated_at
        ];
    }
}