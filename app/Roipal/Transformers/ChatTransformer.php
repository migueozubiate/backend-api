<?php
namespace App\Roipal\Transformers;

use Waavi\Translation\Models\Language;
use League\Fractal\TransformerAbstract;
use App\Roipal\Eloquent\Message;

class ChatTransformer extends TransformerAbstract
{
    
    public function transform(Message $message)
    {
        return [
            'id' => $message->id,
            'room_uuid' => $message->room_uuid,
            'executive_uuid' => $message->executive_uuid,
            'message' => $message->message, 
            'app' => $message->app,
            'created_at' => (string) $message->created_at,
            'updated_at' => (string) $message->updated_at
        ];
    }
}