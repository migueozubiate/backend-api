<?php
namespace App\Roipal\Transformers\Geometric;

use Grimzy\LaravelMysqlSpatial\Types\Point;
use League\Fractal\TransformerAbstract;

class PointTransformer extends TransformerAbstract
{
    public function transform(Point $point)
    {
        return [
            'latitude' => $point->getLat(),
            'longitude' => $point->getLng()
        ];
    }

}