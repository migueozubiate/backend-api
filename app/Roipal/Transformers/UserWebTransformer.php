<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\UserWeb;
use League\Fractal\TransformerAbstract;

class UserWebTransformer extends TransformerAbstract
{
    public function transform(UserWeb $userWeb)
    {
        return [
            'uuid' => $userWeb->uuid,
            'first_name' => $userWeb->first_name,
            'last_name' => $userWeb->last_name,
            'email' => $userWeb->email,
            'language' => $userWeb->language
        ];
    }
}