<?php
namespace App\Roipal\Transformers;

use App\Roipal\Eloquent\CompanyProfile;
use League\Fractal\TransformerAbstract;
use App\Roipal\Transformers\Geometric\PointTransformer;

class CompanyProfileTransformer extends TransformerAbstract
{
    protected $availableIncludes = [
    ];

    public function transform(CompanyProfile $companyProfile)
    {
        $point = new PointTransformer();

        return [
            'slogan' => $companyProfile->slogan,
            'video_bio' => $companyProfile->video_bio,
            'photo_bio_url' => $companyProfile->photo_bio_url,
            'video_bio_url' => $companyProfile->video_bio_url,
            'thumbnail_video' => $companyProfile->thumbnail_video,
            'bio' => $companyProfile->bio,
            'website' => $companyProfile->website,
            'vertical' => $companyProfile->vertical,
            'terms_accepted' => $companyProfile->terms_accepted,
            'address' => $companyProfile->address,
            'country' => $companyProfile->country,
            'position' => $point->transform($companyProfile->position ?? new Point(0, 0)),
            'updated_at' => (string)$companyProfile->updated_at,
            'created_at' => (string)$companyProfile->created_at,
        ];
    }
}