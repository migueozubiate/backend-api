<?php
namespace App\Roipal\Transformers;

use Waavi\Translation\Models\Translation;
use League\Fractal\TransformerAbstract;

class LanguageTranslationTransformer extends TransformerAbstract
{
    
    public function transform(Translation $translation)
    {
        return [
            'id' => $translation->id,
            'locale' => $translation->locale,
            'namespace' => $translation->namespace,
            'group' => $translation->group,
            'item' => $translation->item,
            'text' => json_decode($translation->text),
            'updated_at' => (string)$translation->updated_at,
            'created_at' => (string)$translation->created_at,
        ];
    }
}