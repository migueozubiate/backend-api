<?php
namespace App\Roipal\Constants;

class MissionStatus
{
    public const CREATED = 0;
    public const STARTED = 1;
    public const COMPLETED = 10;
    public const CANCELED = -1;
    public const SEARCHED = 2;
    public const COMMITTED = 3;
    public const REJECT = -10;

    public static function isAllowedStatusUpdate($status)
    {
        $allowed = [
            MissionStatus::COMPLETED,
            MissionStatus::CANCELED,
            MissionStatus::STARTED
        ];

        return !in_array($status, $allowed);
    }

    public static function isAllowedStatusUpdateExecutive($status)
    {
        $allowed = [
            MissionStatus::COMPLETED,
            MissionStatus::CANCELED,
        ];

        return in_array($status, $allowed);
    }

    public static function ratingIsAllowed($status)
    {
        $allowed = [
            MissionStatus::COMPLETED,
            MissionStatus::CANCELED,
            MissionStatus::STARTED,
            MissionStatus::CREATED
        ];


        return in_array($status, $allowed);
    }

    public static function isAllowedStatusRejectExecutive($status)
    {
        $allowed = [
            MissionStatus::CREATED,
        ];

        return in_array($status, $allowed);
    }

    public static function isAllowedStatusCanceledExecutive($status)
    {
        $allowed = [
            MissionStatus::CANCELED,
            MissionStatus::REJECT
        ];

        return in_array($status, $allowed);
    }
}