<?php
namespace App\Roipal\Constants;

class ActivityStatus
{
    public const CREATED = 0;
    public const STARTED = 1;
    public const CANCELLED = 2;
    public const FINISHED = 10;
}