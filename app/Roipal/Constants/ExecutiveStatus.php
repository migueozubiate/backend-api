<?php
namespace App\Roipal\Constants;

class ExecutiveStatus
{
    public const VERIFICATION_STARTED = 0;
    public const VERIFICATION_COMPLETED = 1;
}