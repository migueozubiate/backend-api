<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Database\Eloquent\Relations\Pivot ;

/**
 * Class ExecutiveMissionPivot
 * @package App\Roipal\Eloquent
 *
 * @property integer $id [PK]
 * @property integer $company_id [FK]
 * @property integer $user_id [FK]
 * @property integer $mission_id [FK]
 * @property integer $mission_location_id [FK]
 */
class ExecutiveMission extends Pivot
{

    public $table = 'executive_missions';

    protected $fillable = [
      'rate_mission',
      'review'
    ];

    public function executive()
    {
        return $this->belongsTo(Executive::class);
    }

    public function  mission()
    {
        return $this->belongsToMany(Mission::class);
    }

    /**
     * ExecutiveMission has one .
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function locations()
    {
        // hasOne(RelatedModel, foreignKeyOnRelatedModel = executiveMission_id, localKey = id)
        return $this->belongsToMany(Executive::class);
    }

    /**
     * TODO
     *
     * Esta relación realmente existe?
     */
    public function executives()
    {
        return $this->belogsToMany(MissionLocation::class);
    }
}