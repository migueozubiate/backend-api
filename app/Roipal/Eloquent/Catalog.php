<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $fillable = [
        'language',
        'type',
        'key',
        'value',
        'icon',
    ];
}
