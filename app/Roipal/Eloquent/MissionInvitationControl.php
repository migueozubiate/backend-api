<?php

namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class MissionInvitationControl extends Model
{
    public $table = 'missions_invitations_control';

    protected $dates = [
        'response_at',
        'created_at',
        'updated_at',
    ];

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($mision) {
            $mision->uuid = Uuid::uuid4()->toString();
        });
    }
}
