<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Mission;
use Illuminate\Database\Eloquent\Model;

class MissionSuggestion extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'profile',
        'time',
        'time_unit',
        'min_time',
        'max_time',
        'fare_by_time',
        'estimations'
    ];

    public function mission()
    {
        return $this->belongsTo(Mission::class);
    }
}