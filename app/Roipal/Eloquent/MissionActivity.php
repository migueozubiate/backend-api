<?php
namespace App\Roipal\Eloquent;

use Ramsey\Uuid\Uuid;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\Model;
use App\Roipal\Eloquent\ActivityHistory;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

/**
 * Class MissionActivity
 * @package App\Roipal\Eloquent
 *
 * @property integer $id [PK]
 * @property integer $mission_id [FK]
 * @property integer $executive_id [FK]
 * @property string $status
 *
 */
class MissionActivity extends Model
{   
    protected $table = 'mission_activities';

    protected $fillable = [
        'title',
        'description',
    ];

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($activity) {
            $activity->uuid = Uuid::uuid4()->toString();
        });
    }

    protected static $availableIncludes = [
        'mission' => 'mission',
        'executive' => 'executive',
        'company' => 'company',
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function scopeSearchActivity($query, $search)
    {
        return $query->where('title', 'LIKE', "%$search%")
                    ->orWhere('description', 'LIKE', "%$search%");
    }

    public function mission()
    {
        return $this->belongsTo(Mission::class);
    }

    public function executive()
    {
       return $this->belongsTo(Executive::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function activityHistory()
    {
        return $this->hasMany(ActivityHistory::class);
    }

}