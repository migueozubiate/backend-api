<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Model;
use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\Assestment;
use App\Roipal\Eloquent\CompanyProfile;
use App\Roipal\Eloquent\PaymentProfile;
use App\Roipal\Eloquent\MissionActivity;

class Company extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'business_name',
        'name',
        'vertical'
    ];

    protected static $availableIncludes = [
        'missions' => 'mission'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function owner()
    {
        return $this->hasOne(User::class);
    }

    public function profile()
    {
        return $this->hasOne(CompanyProfile::class);
    }

    public function payments()
    {
        return $this->hasMany(PaymentProfile::class);
    }

    public function payment()
    {
        return $this->hasOne(PaymentProfile::class);
    }

    public function missions()
    {
        return $this->hasMany(Mission::class);
    }

    public function invitations()
    {
        return $this->hasMany(MissionInvitation::class);
    }

    public function activities()
    {
        return $this->hasMany(MissionActivity::class);
    }

    public function assestment()
    {
        return $this->hasMany(Assestment::class);
    }

    public function location()
    {
        return $this->hasMany(Location::class);
    }
}