<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ExecutivePaymentProfile extends Model
{
    public $table = 'executive_payment_profiles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner',
        'type_payment',
        'card_number',
        'bank',
        'type_card',
        'email',
        'clabe'
    ];

    public function executive()
    {
        return $this->belongsTo(Executive::class);
    }
}
