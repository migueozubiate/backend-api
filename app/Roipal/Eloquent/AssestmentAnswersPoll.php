<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Assestment;
use Illuminate\Database\Eloquent\Model;

class AssestmentAnswersPoll extends Model
{
    public $table = 'assestment_answers_poll';

    public $fillable = [
        'answer',
        'number',
        'questions',
        'profile',
        'points'
    ];

    protected $casts = [
        'meaning' => 'array'
    ];

    public function assestment()
    {
        return $this->belongsTo(Assestment::class);
    }
}
