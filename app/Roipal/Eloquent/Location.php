<?php

namespace App\Roipal\Eloquent;

use Ramsey\Uuid\Uuid;
use App\Roipal\Eloquent\Model;
use App\Roipal\Eloquent\MissionLocation;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class Location extends Model
{
    use SpatialTrait;

    protected $table = 'locations';

    protected $spatialFields = ['position'];

    protected $fillable = [
        'name',
        'address',
    ];

    protected static $availableIncludes = [
        'company' => 'company'
    ];

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($location) {
            $location->uuid = Uuid::uuid4()->toString();
        });
    }

    public function scopeSearchLocation($query, $search)
    {
        return $query->whereName('LIKE', "%$search%")
            ->orWhere('name', 'LIKE', "%$search%")
            ->orWhere('address', 'LIKE', "%$search%");
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function missionLocation()
    {
        return $this->hasOne(MissionLocation::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}
