<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AssestmentQuestion
 *
 * @package App\Roipal\Eloquent
 *
 * @property integer $id PK
 * @property integer $assestment_id FK
 * @property integer $number
 * @property string $type
 * @property string $question Pregunta (NULLABLE)
 * @property string $options Opciones de la pregunta (NULLABLE)
 * @property string $answere Respuespuesta o respuestas correctas (NULLABLE)
 * @property string $clasification (NULLABLE)
 *
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class AssestmentQuestion extends Model
{
    protected $casts = [
        'options' => 'array'
    ];

    public $fillable = [
        'number',
        'type',
        'group',
        'question',
        'options',
        'answer'
    ];

    public function assestment()
    {
        return $this->belongsTo(Assestment::class);
    }
}