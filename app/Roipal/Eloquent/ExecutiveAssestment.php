<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\Company;
use Illuminate\Database\Eloquent\Model;

class ExecutiveAssestment extends Model
{
    public $table = 'executive_assestments';

    public $fillable = [
        'name',
        'total_questions'
    ];

    protected $casts = [
        'description' => 'array'
    ];

    public function answers()
    {
        return $this->hasMany(ExecutiveAssestmentAnswer::class, 'assestment_id');
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function executive()
    {
        return $this->belongsTo(Executive::class, 'user_id');
    }

    public function evaluations()
    {
        return $this->hasMany(ExecutiveAssestmentsEvaluation::class, 'assestment_id');
    }

}
