<?php

namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class verifyUser extends Model
{
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'token',
    ];
}
