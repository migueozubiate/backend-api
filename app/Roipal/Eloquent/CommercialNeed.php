<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class CommercialNeed extends Model
{
    public $table = 'commercial_needs';

    public $fillable = [
        'commercial_need',
        'commercial_item'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function scopeSearchCommercialNeed($query, $search)
    {
        return $query->where('commercial_need', 'LIKE', "%{$search}%");
    }

    public function typeSales()
    {
        return $this->hasMany(CommercialTypeSale::class);
    }
}