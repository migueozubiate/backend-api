<?php

namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    /**
     * Fields that can be mass assigned.
     *
     * @var array
     */
    protected $fillable = [
      'email', 'token'
    ];  
}
