<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Company;
use Illuminate\Database\Eloquent\Model;

class PaymentProfile extends Model
{
    public $table = 'company_payment_profiles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'owner',
        'clasification',
        'card_number',
        'exp',
        'type',
        'ccv'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }
}