<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class UserChecklist extends Model
{
    protected $table = 'user_checklist';
}
