<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Executive;
use Illuminate\Database\Eloquent\Model;

class ExecutiveDocumentation extends Model
{
    public $table = 'executive_documentation';

    public $fillable = [
        'national_id_back_url',
        'national_id_from_url',
        'proof_residency_url',
        'curp_url',
        'rfc_url',
    ];

    public function executive()
    {
        return $this->belongsTo(Executive::class);
    }


}
