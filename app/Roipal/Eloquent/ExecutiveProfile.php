<?php
namespace App\Roipal\Eloquent;

use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Eloquent\Model;


/**
 * Class ExecutiveProfile
 *
 * @package App\Roipal\Eloquent
 *
 * @property boolean $video_bio
 * @property string $video_bio_url
 * @property string $bio
 * @property string $website
 * @property boolean $terms_accepted
 * @property string $address
 * @property Point $position
 *
 */
class ExecutiveProfile extends Model
{
    use SpatialTrait;

    public $table = 'executive_profiles';

    protected $spatialFields = [
        'position'
    ];

    protected $casts = [
        'video_bio' => 'boolean',
        'terms_accepted' => 'boolean'
    ];

    protected $fillable = [
        'video_bio_url',
        'phone',
        'photo_bio_url',
        'bio',
        'website',
        'terms_accepted',
        'address',
        'position',
        'score',
        'registered_payment',
        'verification_status',
        'country',
        'thumbnail_video'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function executive()
    {
        return $this->belongsTo(Executive::class, 'user_id');
    }

}
