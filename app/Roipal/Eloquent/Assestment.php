<?php
namespace App\Roipal\Eloquent;

use Ramsey\Uuid\Uuid;
use App\Roipal\Eloquent\Company;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Assestment
 * @package App\Roipal\Eloquent
 *
 * @property integer $id PK
 * @property string $uuid PK
 * @property string $name
 * @property string $summary NULLABLE
 * @property integer $total_questions
 * @property integer $created_by FK|NULLABLE
 * @property integer $updated_by FK|NULLABLE
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Assestment extends Model
{
    protected $table = 'assestments';

    public $fillable = [
        'name',
        'summary',
        'total_questions',
        'lang'
    ];

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($assestment) {
            $assestment->uuid = Uuid::uuid4()->toString();
        });
    }

    public function scopeSearchAssessment($query, $search)
    {
        return $query->where('name', 'LIKE', "%{$search}%")
                    ->orWhere('lang', 'LIKE', "%{$search}%")
                    ->orWhere('summary', 'LIKE', "%{$search}%");
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function questions()
    {
        return $this->hasMany(AssestmentQuestion::class);
    }

    public function poll()
    {
        return $this->hasMany(AssestmentAnswersPoll::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function mission()
    {
        return $this->belongsTo(Mission::class);
    }

}
