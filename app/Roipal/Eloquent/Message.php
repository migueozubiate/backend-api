<?php

namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    public $table = 'messages';
}
