<?php

namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Location;
use Illuminate\Database\Eloquent\Model;
use App\Roipal\Eloquent\MissionInvitation;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

/**
 * Class MissionLocation
 * @package App\Roipal\Eloquent
 *
 * @property integer $id [PK]
 * @property integer $user_id [FK]
 * @property integer mission_id [FK]
 * @property string $address
 * @property Point $position
 * @property integer $executives_requested
 *
 */
class MissionLocation extends Model
{
    use SpatialTrait;

    protected $table = 'mision_locations';

    protected $spatialFields = ['position'];

    protected $fillable = [
        'name',
        'address',
        'executives_requested',
        'position'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function mission()
    {
        return $this->belongsTo(Mission::class);
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function invitations()
    {
        return $this->hasMany(MissionInvitation::class, 'mission_location_id');
    }

    public function executives()
    {
        return $this->belongsToMany(
          Executive::class,
          'executive_missions',
          'mission_location_id',
          'executive_id'
        );
    }

    public function locations()
    {
        return $this->belongsTo(Location::class);
    }
}