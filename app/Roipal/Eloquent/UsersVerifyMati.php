<?php

namespace App\Roipal\Eloquent;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class UsersVerifyMati extends Model
{
    protected $table = 'users_verify_mati';

    protected $fillable = [
        'fullName',
        'address',
        'curp'
    ];

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($mati) {
            $mati->uuid = Uuid::uuid4()->toString();
        });
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
