<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Model;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Observers\InvitationObserver;

/**
 * Class MissionInvitation
 * @package App\Roipal\Eloquent
 *
 * @property integer $id [PK]
 * @property integer $mission_id [FK]
 * @property string $status
 *
 */
class MissionInvitation extends Model
{
    public $table = 'missions_invitations';

    protected $fillable = [
        'reason', 'notification', 'status'
    ];

    protected $dates = [
        'response_at',
        'created_at',
        'updated_at'
    ];

    protected static $availableIncludes = [
        'mission' => 'mission',
        'location' => 'location',
        'company' => 'company',
        'executives' => 'executives',
    ];

    public function scopeSearchInvitation($query, $search)
    {
        return $query->where('reason', 'LIKE', "%$search%");
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function mission()
    {
        return $this->belongsTo(Mission::class);
    }

    public function location()
    {
        return $this->belongsTo(MissionLocation::class, 'mission_location_id');
    }

    public function users()
    {
        return $this->belongsTo(Users::class);
    }

    public function executive()
    {
        return $this->belongsTo(Executive::class, 'executive_id');
    }

}