<?php

namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ExecutiveAssestmentsEvaluation extends Model
{
    public $table = 'executive_assestments_evaluation';

    public $fillable = [
        'group',
        'points'
    ];

    public function assestment()
    {
        return $this->belongsTo(ExecutiveAssestment::class);
    }

    public function executive()
    {
        return $this->belongsTo(Executive::class,'id');
    }
}
