<?php

namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class PhysicalLimitation extends Model
{
    protected $table = 'physical_limitations';

    protected $fillable = [
        'limitation',
        'item'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function executivePhysicalLimitations()
    {
        return $this->hasMany(ExecutivePhysicalLimitation::class);
    }
}
