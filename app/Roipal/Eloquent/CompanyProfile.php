<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Company;
use Illuminate\Database\Eloquent\Model;
use App\Roipal\Observers\CompanyProfileObserver;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class CompanyProfile extends Model
{
    use SpatialTrait;

    protected $spatialFields = ['position'];

    public $table = 'company_profiles';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'video_bio',
        'video_bio_url',
        'photo_bio_url',
        'slogan',
        'bio',
        'website',
        'vertical',
        'country',
        'contact_name',
        'contact_phone',
        'contact_job_title',
        'terms_accepted',
        'address',
        'position',
        'thumbnail_video'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id'
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function paymentProfile()
    {
        return $this->hasMany(CompanyProfile::class, 'company_id', 'id');
    }
}