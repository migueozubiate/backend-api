<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model as EloquentModel;

class Model extends EloquentModel
{
    protected static $availableIncludes = [];

    public static function getAvilableIncludes()
    {
        return static::$availableIncludes;
    }

    public function getRelationshipsToBeLoaded(array $includes) {
        $relationships = [];
        $available = static::getAvilableIncludes();

        foreach ($includes as $include) {
            $relationship = array_get($available, $include);

            if ($relationship) {
                $relationships[] = $relationship;
            }
        }

        return $relationships;
    }
}