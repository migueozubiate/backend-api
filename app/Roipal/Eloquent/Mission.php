<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Model;
use Ramsey\Uuid\Uuid;

/**
 * Class Mission
 *
 * @package App\Roipal\Eloquent
 *
 * @property integer $id
 * @property string $uuid (auto)
 * @property integer $user_id [FK]
 * @property integer $company_id [FK]
 * @property boolean $video
 * @property string $video_url
 * @property string $type
 * @property string $profile_suggested
 * @property string $payment_offered
 * @property string $extra_offered
 * @property integer $requested_time
 * @property string $requested_units
 * @property integer $suggested_time
 * @property string $suggested_units
 * @property double $cost_by_unit
 * @property double $total
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class Mission extends Model
{
    protected $search;

    protected $table = 'missions';

    protected $fillable = [
        'name',
        'description',
        'video_url',
        'type',
        'commercial_need',
        'profile',
        'time',
        'time_unit',
        'fare_by_time',
        'subtotal',
        'tax',
        'total',
        'score_mission',
        'started_at',
        'type_sale_activities',
        'currency',
        'country',
        'thumbnail_video'
    ];

    protected $casts = [
        'type_sale_activities' => 'array',
        'currency' => 'array'
    ];

    protected static $availableIncludes = [
        'company' => 'company',
        'activities' => 'activities',
        'locations' => 'locations',
        'executives' => 'executives',
    ];

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($mision) {
            $mision->uuid = Uuid::uuid4()->toString();
        });
    }

    public function scopeSearchMission($query, $search)
    {
        return $query->where('name', 'LIKE', "%$search%")
            ->orWhere('description', 'LIKE', "%$search%")
            ->orWhere('type', 'LIKE', "%$search%")
            ->orWhere('profile', 'LIKE', "%$search%");
    }

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function locations()
    {
        return $this->hasMany(MissionLocation::class);
    }

    public function invitations()
    {
        return $this->hasMany(MissionInvitation::class);
    }

    public function suggestion()
    {
        return $this->hasOne(MissionSuggestion::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    // region - user executive / UrlRoutable

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function activities()
    {
        return $this->hasMany(MissionActivity::class);
    }

    public function executives()
    {
        return $this->belongsToMany(Executive::class, 'executive_missions', 'mission_id')->withPivot('status','uuid', 'review', 'rate_mission');
    }

    public function assesment()
    {
        return $this->hasMany(Assestment::class);
    }

    public function typeSales()
    {
        return $this->belongsTo(CommercialTypeSale::class, 'type_sale_id');
    }

    // endregion
}
