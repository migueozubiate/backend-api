<?php

namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class LineBusiness extends Model
{
    public $table = 'line_business';

    protected $fillable = [
        'code',
        'line_business',
        'sector',
        'item_line_business',
        'item_sector'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function scopeSearchLineBusiness($query, $search)
    {
        return $query->where('line_business', 'LIKE', "%$search%")
                    ->orWhere('sector', 'LIKE', "%$search%")
                    ->orWhere('code', 'LIKE', "%$search%");
    }
}
