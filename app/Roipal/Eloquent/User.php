<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\Executive;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Contracts\Auth\CanResetPassword;
use App\Notifications\ResetPasswordNotification;
use App\Notifications\EmailVerificationNotification;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @package App\Roipal\Eloquent
 *
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $password (BCRIPT)
 * @property string $uuid
 * @property string $classification
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class User extends Authenticatable implements MustVerifyEmail, CanResetPassword
{
    use HasApiTokens, Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'email', 
        'password', 
        'clasification',
        'avatar',
        'lang'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'id',
        'password',
        'remember_token',
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function mission()
    {
        return $this->hasMany(Mission::class);
    }

    public function checklist()
    {
        return $this->hasMany(UserChecklist::class, 'user_id');
    }

    public function invitation()
    {
        return $this->hasOne(MissionInvitation::class, 'id', 'user_id');
    }

    public function executive()
    {
        return $this->hasOne(Executive::class, 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    public function sendEmailVerificationNotification()
    {
         $this->notify(new EmailVerificationNotification);
    }

}
