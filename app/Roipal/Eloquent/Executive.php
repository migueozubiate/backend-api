<?php
namespace App\Roipal\Eloquent;

use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\MissionActivity;
use App\Roipal\Eloquent\ExecutiveDocumentation;

/**
 * Class Executive
 *
 * @package App\Roipal\Eloquent
 */
class Executive extends User
{

    public $table = 'users';

    // region - user executive / UrlRoutable

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    // endregion

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function profile()
    {
        return $this->hasOne(ExecutiveProfile::class, 'user_id');
    }

    public function assestments()
    {
        return $this->hasMany(ExecutiveAssestment::class, 'user_id');
    }

    public function evaluations()
    {
        return $this->hasMany(ExecutiveAssestmentsEvaluation::class, 'user_id');
    }

    public function invitations()
    {
        return $this->hasMany(MissionInvitation::class, 'executive_id');
    }

    public function missions()
    {
        return $this->belongsToMany(Mission::class, 'executive_missions', 'executive_id')->withPivot('status', 'uuid', 'review', 'rate_mission');
    }

    public function location()
    {
        return $this->belongsToMany(
            MissionLocation::class,
            'executive_missions',
            'executive_id',
            'mission_location_id'
        );
    }

    public function activities()
    {
        return $this->hasMany(MissionActivity::class);
    }

    public function payments()
    {
        return $this->hasMany(ExecutivePaymentProfile::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function limitations()
    {
        return $this->hasMany(ExecutivePhysicalLimitation::class, 'user_id');
    }

    public function payment()
    {
        return $this->hasOne(ExecutivePaymentProfile::class);
    }

    public function documentExecutive()
    {
        return $this->hasOne(ExecutiveDocumentation::class);
    }
}
