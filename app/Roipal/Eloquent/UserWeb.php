<?php

namespace App\Roipal\Eloquent;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;

class UserWeb extends Model
{
    protected $table = 'users_web';

    protected $fillable = [
        'first_name', 
        'last_name', 
        'email', 
        'language'
    ];

    protected $hidden = [
        'id'
    ];

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($usersWeb) {
            $usersWeb->uuid = Uuid::uuid4()->toString();
        });
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }
}
