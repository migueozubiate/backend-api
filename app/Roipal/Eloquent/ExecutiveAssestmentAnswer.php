<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class ExecutiveAssestmentAnswer extends Model
{
    public $table = 'executive_assestment_answers';

    public $fillable = [
        'number',
        'group',
        'questions',
        'answer',
        'profile',
        'points'
    ];

    protected $casts = [
        'options' => 'array'
    ];

    public function assestment()
    {
        return $this->belongsTo(ExecutiveAssestment::class);
    }
}