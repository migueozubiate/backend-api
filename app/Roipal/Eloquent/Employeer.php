<?php
namespace App\Roipal\Eloquent;

class Employeer extends User
{
    public $table = 'users';

    public function profile()
    {
        return $this->hasOne(ExecutiveProfile::class);
    }

    public function payment()
    {
        return $this->hasOne(PaymentProfile::class);
    }

    public function company()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

}