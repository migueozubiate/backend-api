<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

class CommercialTypeSale extends Model
{
    public $table = 'commercial_type_sales';

    public $fillable = [
        'type',
        'description',
        'item_type',
        'activities'
    ];

    protected $casts = [
        'activities' => 'array'
    ];

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function scopeSearchCommercialTypeSales($query, $search)
    {
        return $query->where('type', 'LIKE', "%{$search}%");
    }

    public function commercialNeed()
    {
        return $this->belongsTo(CommercialNeed::class, '');
    }

    public function mission()
    {
        return $this->hasOne(Mission::class);
    }
}