<?php

namespace App\Roipal\Eloquent;

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use App\Roipal\Eloquent\MissionActivity;
use Grimzy\LaravelMysqlSpatial\Eloquent\SpatialTrait;

class ActivityHistory extends Model
{
    use SpatialTrait;

    public $table = 'mission_activities_history';

    protected $spatialFields = ['position'];

    protected $fillable = [
        'type',
        'comment',
        'status',
        'address'
    ];

    protected static function boot()
    {
        parent::boot();
        self::creating(function ($activityHistory) {
            $activityHistory->uuid = Uuid::uuid4()->toString();
        });
    }

    public function getRouteKeyName()
    {
        return 'uuid';
    }

    public function activities()
    {
        return $this->belongsTo(MissionActivity::class);
    }
}
