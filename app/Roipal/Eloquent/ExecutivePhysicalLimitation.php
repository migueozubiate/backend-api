<?php
namespace App\Roipal\Eloquent;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ExecutivePhysicalLimitation
 *
 * @package App\Roipal\Eloquent
 *
 * @property integer $id PK
 * @property integer $user_id FK
 * @property string $limitation
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 */
class ExecutivePhysicalLimitation extends Model
{

    protected $table = 'executive_physical_limitations';

    protected $fillable = [
        'limitation'
    ];

    public function executive()
    {
        return $this->belongsTo(Executive::class);
    }

    public function physicalLimitations()
    {
        return $this->belongsTo(PhysicalLimitation::class, 'physical_limitation_id');
    }
}