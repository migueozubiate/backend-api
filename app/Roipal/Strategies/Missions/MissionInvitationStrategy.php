<?php

namespace App\Roipal\Strategies\Missions;

use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\MissionLocation;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Eloquent\ExecutiveAssestment;
use App\Roipal\Repositories\MissionInvitationRepository;
use App\Roipal\Transformers\Assestments\ExecutiveAssestmentTransformer;
use App\Roipal\Strategies\PushNotifications\MissionNotificationStrategy;

class MissionInvitationStrategy
{
    protected $repository;

    public function __construct(MissionInvitationRepository $repository)
    {
        $this->repository = $repository;
    }
   
    public function invitationStatus(
        Executive $executive, 
        Mission $mission, 
        ExecutiveAssestment $executiveAssessment,
        MissionNotificationStrategy $notification
        )
    {
        $assessment = 
            $executiveAssessment
                ->whereUserId($executive->id)
                ->whereMissionId($mission->id);
        
        $executiveInvitation = 
            $executive
                ->invitations()
                ->whereMissionId($mission->id)
                ->whereExecutiveId($executive->id)
                ->first();

        if ($assessment->count() >= 3 && $executiveAssessment->points < 100) {
            $this->repository->canceledInvitation($executiveInvitation);

            return [
                'message' => 'You exceeded the number of attempts allowed',
                'code' => 'NUMBER_OF_ATTEMPTS_ALLOWED'
            ];
        }

        if ($executiveAssessment->points == 100) {
            $missionInvitation = $this->repository->acceptanceInvitation($executiveInvitation);

            $notification->notifyToCompanyWhenExecutiveAcceptasAnInvitation(
                $missionInvitation,
                $executive
            );

            return [
                'message' => 'Success',
                'data' => Fractal::item($executiveAssessment, new ExecutiveAssestmentTransformer)
            ];

        } else {
            return [
                'message' => 'Assessment not approved, try again',
                'data' => Fractal::item($executiveAssessment, new ExecutiveAssestmentTransformer)
            ];
        }
    }

    public function getUuidMissionLocation(array $data)
    {
        $location = MissionLocation::where('uuid', array_get($data, 'uuid'))->first();
         
        return $location;
    } 
}