<?php
namespace App\Roipal\Strategies\Missions;

use App\Roipal\Eloquent\Mission;
use App\Roipal\Support\Firebase;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Constants\NotificationAction;
use App\Roipal\Transformers\CompanyTransformer;
use App\Roipal\Transformers\MissionTransformer;
use App\Roipal\Repositories\ExecutiveRepository;
use App\Roipal\Repositories\MissionInvitationRepository;

class MissionProfilesStrategy
{
    private $ExecutiveRepository;
    private $invitationRepository;
    private $firebase;

    public function __construct(ExecutiveRepository $ExecutiveRepository, MissionInvitationRepository $invitationRepository, Firebase $firebase)
    {

        $this->ExecutiveRepository = $ExecutiveRepository;
        $this->invitationRepository = $invitationRepository;
        $this->firebase = $firebase;
    }

    public function getAvailable(Mission $mission)
    {
        $locations = $mission->locations;

        foreach ($locations as $location) {
            $profiles = $this->ExecutiveRepository->getAvailableByRegion($location, 3, [0]);
            foreach ($profiles as $profile) {
                $invitation = $this->invitationRepository->storeInvitation($location, $profile);
                //dd($profile);
                $token = $invitation->executive->fcm_token;

                if ($token) {
                    $mName = $invitation->mission->name;
                    $cName = $invitation->mission->company->name;
                    $mDescription = $invitation->mission->description;
                    $mType = $invitation->mission->type;
                    $mProfile = $invitation->mission->profile;

                    $payload = [
                        'app' => 'executive',
                        'title' => "Invitation to a mission {$mName}",
                        'body' => "The {$cName} company invites you to
                                    participate in the mission {$mName}",
                        'sound' => 'default',
                        'data' => [
                            'action' => NotificationAction::SEND_INVITATION,
                            'mission' => Fractal::item($invitation->mission, new MissionTransformer),
                            'company' => Fractal::including('profile')
                                            ->item($invitation->company, new CompanyTransformer)
                        ],
                    ];

                    $this->firebase->notification($payload, $token);
                }
            }
        }

    }
}
