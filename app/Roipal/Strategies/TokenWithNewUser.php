<?php
namespace App\Roipal\Strategies;

use App\Roipal\Eloquent\User;
use League\OAuth2\Server\AuthorizationServer;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\Response as Psr7Response;

trait TokenWithNewUser
{
    protected function geTokenFromNewUser(User $user, string $password, $scopes = [])
    {
        $body = [
            'grant_type' => 'password',
            'client_id' => '923a69f6-c2c7-401c-802c-0fff3c67e448',
            'client_secret' => 'owQmd6i7YM9ofDS41gZ8bJN8E9tBta5ZVshom5l6',
            'username' => $user->email,
            'password' => $password,
            'scope' => $scopes,
        ];

        $authServer = app()->make(AuthorizationServer::class);

        $tokenResponse = $authServer->respondToAccessTokenRequest(ServerRequestFactory::fromGlobals(
            null,
            null,
            $body
        ), new Psr7Response);
        
        $token = $tokenResponse->getBody();
        $token = json_decode($token, true);

        return $token;
    }

}