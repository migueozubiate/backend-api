<?php
namespace App\Roipal\Strategies; 

use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\URL;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;

class StoreFileStrategy
{
    public function store(array $files)
    {
        $url = [];
        
        foreach ($files as $file) {
            if (is_file($file)) {                
                $mimeType = explode('/', $file->getClientMimeType());
                $type = $mimeType[0];

                $fileName = Uuid::uuid4()->toString().'.'.$mimeType[1];
                $filePath = "app/{$type}/{$fileName}";

                if ($type === 'image') {
                    $file = Image::make($file)->fit(320);
                    $file->save(storage_path($filePath));
                } else {
                    Storage::disk('local')
                            ->putFileAs("{$type}", $file, $fileName);
                }

                $route = 'https://media-roipal.nulldata.com/api/generic/download-files/?file=' . $filePath;

                $url[$type] = $route;
            }
        }

        return $url;
    }
}