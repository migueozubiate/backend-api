<?php
namespace App\Roipal\Strategies\PushNotifications;

use App\Roipal\Eloquent\User;
use App\Roipal\Jobs\ProcessFirebaseNotificationJob;

class NotificationTestStrategy
{
    public function sendTestNotificationExcutive(array $meta)
    {
        $title = array_get($meta, 'title');
        $body = array_get($meta, 'body');
        $token = array_get($meta, 'fcm_token');
        $data = array_get($meta, 'data');

        $payload = [
            'app' => 'executive',
                'title' => $title,
                'body' => $body,
                'sound' => 'default',
                'data' => $data
        ];

        ProcessFirebaseNotificationJob::dispatch($payload, $token)->onQueue("invitation");
    }

    public function sendTestNotificationBusiness(array $meta)
    {
        $title = array_get($meta, 'title');
        $body = array_get($meta, 'body');
        $token = array_get($meta, 'fcm_token');
        $data = array_get($meta, 'data');

        $payload = [
            'app' => 'company',
                'title' => $title,
                'body' => $body,
                'sound' => 'default',
                'data' => $data
        ];

        ProcessFirebaseNotificationJob::dispatch($payload, $token)->onQueue("invitation");
    }
}