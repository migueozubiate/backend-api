<?php
namespace App\Roipal\Strategies\PushNotifications;

use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use Illuminate\Support\Facades\App;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Constants\NotificationAction;
use App\Roipal\Transformers\CompanyTransformer;
use App\Roipal\Transformers\MissionTransformer;
use App\Roipal\Jobs\ProcessFirebaseNotificationJob;
use App\Roipal\Transformers\MissionLocationTransformer;
use App\Roipal\Transformers\MissionInvitationTransformer;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;

class MissionNotificationStrategy
{
    public function notifyToCompanyExecutivesAreBeingSearching(Mission $mission)
    {
        $user = $mission->user;

        if ($user->fcm_token) {
            $payload = [
                'app' => $userCompany->classification,
                'title' => trans("executives.executive_searching"),
                'body' => trans("executives.executive_search_mission", ['title' => $mission->title]),
                'sound' => 'default',
                'android' => [
                    'color' => '',
                    'icon' => '',
                ],
                'data' => [
                    'mission' => $mission->toArray(),
                ],
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $user->fcm_token)->onQueue('mission');
        }
    }

    public function notifyToCompanyWhenExecutiveAcceptasAnInvitation(Mission $mission, Executive $executive)
    {
        $userCompany = $mission->user;

        $lang = $userCompany->lang;

        $executiveToMission = 
                        $mission
                            ->executives()
                            ->whereExecutiveId($executive->id)
                            ->first();

        if ($userCompany->fcm_token) {
            $payload = [
                'app' => $userCompany->classification,
                'title' => trans("missions.mission_goal", [], $lang),
                'body' => trans("missions.mission_accepted", 
                                ['name' => $mission->name, 
                                'executive' => $executive->name], 
                                $lang),
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::INVITATION_ACCEPTED,
                    'mission' => Fractal::item($mission, new MissionTransformer),
                    'executive' => Fractal::including('profile')
                        ->item($executiveToMission, new ExecutiveTransformer),
                ],
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $userCompany->fcm_token)->onQueue('mission');
        }

    }

    public function notifyToCompanyWhenExecutiveCanceledAnInvitation(Mission $mission, User $user)
    {
        $userCompany = $mission->user;

        $lang = $userCompany->lang;

        $executive = $user->executive;

        if ($userCompany->fcm_token) {
            $payload = [
                'app' => $userCompany->classification,
                'title' => trans("missions.mission_missed", [], $lang),
                'body' => trans("missions.mission_rejected", 
                                ['name' => $mission->name, 
                                'executive' => $executive->name],
                                $lang),
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::INVITATION_CANCELED,
                    'mission' => Fractal::item($mission, new MissionTransformer),
                    'executive' => Fractal::including('profile')
                        ->item($executive, new ExecutiveTransformer),
                ],
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $userCompany->fcm_token)->onQueue('mission');
        }
    }

    public function notifyToExecutivesWhenCompanyStartedMission(Mission $mission)
    {
        foreach ($mission->executives as $executive) {
            $missionToExecutive = $executive->missions()->find($mission->id);

            if ($executive->fcm_token) {
                $lang = $executive->lang;

                $missionIncludeRoomChat = 
                        Fractal::item($missionToExecutive, new MissionTransformer) 
                                + ['room_chat' => [
                                    'room_uuid' => $executive->pivot->uuid
                                    ]
                                ];

                $payload = [
                    'app' => 'executive',
                    'title' => trans("missions.mission_started", [], $lang),
                    'body' => trans("missions.mission_been_started", 
                                   ['name' => $mission->name], 
                                   $lang),
                    'sound' => 'default',
                    'data' => [
                        'action' => NotificationAction::MISSION_STARTED,
                        'mission' =>  $missionIncludeRoomChat,
                        'company' => Fractal::including('profile')
                            ->item($mission->company, new CompanyTransformer),
                    ],
                ];
                
                ProcessFirebaseNotificationJob::dispatch($payload, $executive->fcm_token)->onQueue('mission');
            }
        }
    }

    public function notifyToExecutivesWhenCompanyFinishedMission(Mission $mission)
    {
        foreach ($mission->executives as $executive) {
            $missionToExecutive = $executive->missions()->find($mission->id);
    
            if ($executive->fcm_token) {
                $lang = $executive->lang;

                $missionIncludeRoomChat = 
                        Fractal::item($missionToExecutive, new MissionTransformer) 
                                    + ['room_chat' => [
                                        'room_uuid' => $executive->pivot->uuid
                                        ]
                                    ];

                $payload = [
                    'app' => 'executive',
                    'title' => trans("missions.mission_finished", [], $lang),
                    'body' => trans("missions.mission_been_finished", 
                                   ['name' => $mission->name], 
                                   $lang),
                    'sound' => 'default',
                    'data' => [
                        'action' => NotificationAction::MISSION_FINISHED,
                        'mission' => $missionIncludeRoomChat,
                        'company' => Fractal::including('profile')
                            ->item($mission->company, new CompanyTransformer),
                    ],
                ];

                ProcessFirebaseNotificationJob::dispatch($payload, $executive->fcm_token)->onQueue('mission');
            }
        }
    }

    public function notifyToExecutivesWhenCompanyCanceledMission(Mission $mission)
    {
        foreach ($mission->executives as $executive) {
            $missionToExecutive = $executive->missions()->find($mission->id);
    
            if ($executive->fcm_token) {
                $lang = $executive->lang;

                $missionIncludeRoomChat = Fractal::item($missionToExecutive, new MissionTransformer) 
                                          + ['room_chat' => [
                                                'room_uuid' => $executive->pivot->uuid
                                                ]
                                            ];

                $payload = [
                    'app' => 'executive',
                    'title' => trans("missions.mission_canceled", [], $lang),
                    'body' => trans("company.company_executive_mission_canceled", 
                                  ['company' => $mission->company->name, 
                                   'name' => $mission->name],
                                   $lang),
                    'sound' => 'default',
                    'data' => [
                        'action' => NotificationAction::MISSION_CANCELED,
                        'mission' => $missionIncludeRoomChat,
                        'company' => Fractal::including('profile')
                            ->item($mission->company, new CompanyTransformer),
                    ],
                ];
                
                ProcessFirebaseNotificationJob::dispatch($payload, $executive->fcm_token)->onQueue('mission');
            } 
        }

    }

    public function notifyWhenCanceledMission(Mission $mission, User $user, Executive $executive)
    {
        $classification = $user->classification;

        switch ($classification) {
            case 'executive':
                    $userApp = $mission->company->user;
                    $title = trans("missions.mission_canceled", [], $mission->company->user->lang);
                    $body =  trans("executives.executive_canceled", 
                                    ['executive' => $executive->name, 
                                    'name' => $mission->name],
                                    $mission->company->user->lang
                                );

                    $action = NotificationAction::MISSION_EXECUTIVE_CANCELED;
                break;

            case 'company':
                    $userApp = $executive;
                    $title = trans("missions.mission_canceled", [], $executive->lang);
                    $body = trans("company.company_executive_mission_canceled", 
                                    ['company' => $mission->company->name, 
                                    'name' => $mission->name],
                                    $executive->lang
                                );

                    $action = NotificationAction::MISSION_CANCELED;
                break;
            default:
                # code...
                break;
        }

        $executiveMission = 
                    $mission
                    ->executives()
                    ->whereExecutiveId($executive->id)
                    ->first();

        if ($userApp->fcm_token) {
            $payload = [
                'app' => $userApp->classification,
                'title' => $title,
                'body' => $body,
                'sound' => 'default',
                'data' => [
                    'action' => $action,
                    'mission' => Fractal::item($mission, new MissionTransformer),
                    'executive' => Fractal::including('profile')
                        ->item($executiveMission, new ExecutiveTransformer),
                ],
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $userApp->fcm_token)->onQueue('mission');
        }
    }

    public function notifyWhenCompletedMission(Mission $mission, User $user, Executive $executive)
    {
        $classification = $user->classification;

        switch ($classification) {
            case 'executive':
                    $userApp = $mission->company->user;
                    $title = trans("missions.mission_finished", [], $mission->company->user->lang);
                    $body =  trans("executives.executive_finished", 
                                    ['executive' => $executive->name, 
                                    'name' => $mission->name],
                                    $mission->company->user->lang
                                );
                    $action = NotificationAction::MISSION_FINISHED;
                break;

            case 'company':
                    $userApp = $executive;
                    $title = trans("missions.mission_finished", [], $executive->lang);
                    $body = trans("company.company_executive_mission_finished", 
                                    ['company' => $mission->company->name, 
                                    'name' => $mission->name],
                                    $executive->lang
                                );
                    $action = NotificationAction::MISSION_EXECUTIVE_FINISHED;
                break;
            default:
                # code...
                break;
        }

        $executiveMission = 
                    $mission
                    ->executives()
                    ->whereExecutiveId($executive->id)
                    ->first();

        if ($userApp->fcm_token) {
            $payload = [
                'app' => $userApp->classification,
                'title' => $title,
                'body' => $body,
                'sound' => 'default',
                'data' => [
                    'action' => $action,
                    'mission' => Fractal::item($mission, new MissionTransformer),
                    'executive' => Fractal::including('profile')
                        ->item($executiveMission, new ExecutiveTransformer),
                ],
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $userApp->fcm_token)->onQueue('mission');
        }
    }

    public function notifyExecutiveWhenCompanyRejected(Mission $mission, Executive $executive)
    {
        $fcmToken = $executive->fcm_token;

        $executiveMission = 
                    $mission
                    ->executives()
                    ->whereExecutiveId($executive->id)
                    ->first();

        if ($fcmToken) {

            $payload = [
                'app' => $executive->classification,
                'title' => trans('executives.executive_rejected', [], $executive->lang),
                'body' => trans('company.company_executive_rejected', 
                                ['company' => $mission->company->name,
                                'mission' => $mission->name],
                                $executive->lang
                            ),
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::EXECUTIVE_REJECTED,
                    'mission' => Fractal::item($mission, new MissionTransformer),
                    'executive' => Fractal::including('profile')
                        ->item($executiveMission, new ExecutiveTransformer),
                ],
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $fcmToken);
        }
    }
}