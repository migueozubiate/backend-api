<?php
namespace App\Roipal\Strategies\PushNotifications;

use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\MissionActivity;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Constants\NotificationAction;
use App\Roipal\Transformers\MissionTransformer;
use App\Roipal\Jobs\ProcessFirebaseNotificationJob;
use App\Roipal\Transformers\MissionActivityTransformer;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;

class ActivityNotificationStrategy
{
    public function notifyToCompanyWhenActivityStarted(MissionActivity $activity)
    {
        $user = $activity->company->user;
        $executive = $activity->executive;
        $mission = $activity->mission;

        $executiveToMission = 
                        $mission
                            ->executives()
                            ->whereExecutiveId($executive->id)
                            ->first();

        if (!$user->fcm_token) {
            $lang = $user->lang;

            $payload = [
                'app' => $user->classification,
                'title' => trans("executives.executive_activity_started", [], $lang),
                'body' => trans("executives.executive_been_activity_started", 
                                ['title' => $activity->title,
                                'executive' => $executiveToMission->name],
                                $lang),
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::ACTIVITY_STARTED,
                    'activity' => Fractal::item($activity, new MissionActivityTransformer),
                    'mission' => Fractal::item($mission, new MissionTransformer),
                    'executive' => Fractal::including('profile')
                                        ->item($executiveToMission, new ExecutiveTransformer)
                ]
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $user->fcm_token);
        }
    }

    public function notifyToCompanyWhenActivityFinished(MissionActivity $activity)
    {
        $user = $activity->company->user;
        $executive = $activity->executive;
        $mission = $activity->mission;

        $executiveToMission = 
                        $mission
                            ->executives()
                            ->whereExecutiveId($executive->id)
                            ->first();

        if ($user->fcm_token) {
            $lang = $user->lang;

            $payload = [
                'app' => $user->classification,
                'title' => trans("executives.executive_activity_finished", [], $lang),
                'body' => trans("executives.executive_been_activity_finished",
                                ['title' => $activity->title,
                                'executive' => $executiveToMission->name],
                                 $lang),
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::ACTIVITY_FINISHED,
                    'activity' => Fractal::item($activity, new MissionActivityTransformer),
                    'mission' => Fractal::item($mission, new MissionTransformer),
                    'executive' => Fractal::including('profile')
                                        ->item($executiveToMission, new ExecutiveTransformer)
                ]
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $user->fcm_token);
        }
    }
}