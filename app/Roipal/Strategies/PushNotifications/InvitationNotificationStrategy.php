<?php
namespace App\Roipal\Strategies\PushNotifications;

use App\Roipal\Eloquent\Mission;
use App\Roipal\Jobs\ProcessFirebaseNotificationJob;

class InvitationNotificationStrategy
{
    public function notifyToExecutivesWhenSendInvitations(Mission $mission)
    {
        $invitations = $mission->invitations;

        $tokens = [];

        foreach ($invitations as $key => $value) {
            $tokens[] = $value->executive->fcm_token;
        }

        if ($tokens) {
            $payload = [
                'app' => 'executive',
                'title' => trans("invitations.invitation_mission",['name' => $mission->name]),
                'body' => trans("invitations.invitation_company",['company' => $mission->company->name,'name' => $mission->name]),
                'sound' => 'default',
                'data' => [
                    'mission' => [
                        'name' => $mission->name,
                        'description' => $mission->description,
                        'type' => $mission->type,
                        'profile' => $mission->profile,
                    ],
                ]
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $tokens);
        }
    }
}
