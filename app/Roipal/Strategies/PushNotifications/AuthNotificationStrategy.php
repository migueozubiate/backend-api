<?php
namespace App\Roipal\Strategies\PushNotifications;

use App\Roipal\Eloquent\User;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Constants\NotificationAction;
use App\Roipal\Transformers\UserTransformer;
use App\Roipal\Jobs\ProcessFirebaseNotificationJob;

class AuthNotificationStrategy
{
    public function sendEmailVerifyNotification(User $user)
    {
        if($user->fcm_token) {
            $lang = $user->lang;
            
            $payload = [
                'app' => $user->classification,
                'title' => trans("mail.account_activated", [], $lang),
                'body' => trans("mail.mail_verification", 
                               ['name' => $user->name, 'email' => $user->email],
                                $lang),
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::ACCOUNT_ACTIVATED,
                    'profile' => Fractal::including('checklist')
                                        ->item($user, new UserTransformer)
                ]
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $user->fcm_token);
        }
    }
}