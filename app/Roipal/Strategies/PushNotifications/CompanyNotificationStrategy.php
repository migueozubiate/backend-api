<?php

namespace App\Roipal\Strategies\PushNotifications;

use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Constants\NotificationAction;
use App\Roipal\Transformers\MissionTransformer;
use App\Roipal\Jobs\ProcessFirebaseNotificationJob;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;

class CompanyNotificationStrategy
{
    public function notifyExecutiveWhenCompanyRejected(Mission $mission, Executive $executive)
    {
        $fcmToken = $executive->fcm_token;

        if ($fcmToken) {
            $lang = $executive->lang;

            $payload = [
                'app' => $executive->classification,
                'title' => trans('executives.executive_rejected', [], $lang),
                'body' => trans('company.company_executive_rejected', 
                                ['company' => $mission->company->name,
                                'mission' => $mission->name],
                                $lang
                                ),
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::EXECUTIVE_REJECTED,
                    'mission' => Fractal::item($mission, new MissionTransformer),
                    'executive' => Fractal::including('profile')
                        ->item($executive, new ExecutiveTransformer),
                ],
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $fcmToken);
        }
    }
}