<?php
namespace App\Roipal\Strategies\PushNotifications;

use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\ExecutiveMission;
use Xaamin\Fractal\Facades\Laravel\Fractal;
use App\Roipal\Constants\NotificationAction;
use App\Roipal\Transformers\CompanyTransformer;
use App\Roipal\Transformers\MissionTransformer;
use App\Roipal\Jobs\ProcessFirebaseNotificationJob;
use App\Roipal\Transformers\Executives\ExecutiveTransformer;

class ChatNotificationStrategy
{
    public function sendExecutiveChatNotification(array $meta)
    {
        $roomUuid = array_get($meta, 'room_uuid');
        $message = array_get($meta, 'message');

        $executiveMission = $this->getExecutiveMissionByRoomUuid($roomUuid);
            
        $company = $this->getCompanyById($executiveMission->company_id);

        $mission = $this->getMissionById($executiveMission->mission_id);

        $executive = $executiveMission->executive; 
        
        $lang = $executive->lang;

        if ($executive->fcm_token) {
            $missionIncludeRoomChat = Fractal::item($mission, new MissionTransformer) 
                                    + ['room_chat' => [
                                        'room_uuid' => $executiveMission->uuid
                                        ]
                                    ];

            $payload = [
                'app' => $executive->classification,
                'title' => trans("missions.mission_chat_notification", ['name' => $company->user->name], $lang),
                'body' => $message,
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::CHAT_MESSAGE,
                    'mission' => $missionIncludeRoomChat,
                    'company' => Fractal::including('profile')
                                ->item($company, new CompanyTransformer),
                ], 
            ];

            ProcessFirebaseNotificationJob::dispatch($payload, $executive->fcm_token);
        }
    }

    public function sendBusinessChatNotification(array $meta)
    {
        $roomUuid = array_get($meta, 'room_uuid');
        $message = array_get($meta, 'message');

        $executiveMission = $this->getExecutiveMissionByRoomUuid($roomUuid);
                
        $company = $this->getCompanyById($executiveMission->company_id);
        
        $mission = $this->getMissionById($executiveMission->mission_id);

        $getExecutive = 
                $mission
                ->executives
                ->where('id', $executiveMission->executive->id)
                ->first();

        $lang = $company->user->lang;

        if ($company->user->fcm_token) {
            $payload = [
                'app' => $company->user->classification,
                'title' => trans("missions.mission_chat_notification", ['name' => $getExecutive->name], $lang),
                'body' => $message,
                'sound' => 'default',
                'data' => [
                    'action' => NotificationAction::CHAT_MESSAGE,
                    'mission' => Fractal::item($mission, new MissionTransformer),
                    'executive' => Fractal::including('profile')
                             ->item($getExecutive, new ExecutiveTransformer),
                ],
            ];
            ProcessFirebaseNotificationJob::dispatch($payload, $company->user->fcm_token);
        }
    }

    public function getExecutiveMissionByRoomUuid($roomUuid)
    {
        return ExecutiveMission::whereUuid($roomUuid)
                         ->first();
    }

    public function getCompanyById($companyId)
    {
        return Company::whereId($companyId)
                         ->first();
    }

    public function getMissionById($missionId)
    {
        return Mission::whereId($missionId)
                   ->first();
    }


}
