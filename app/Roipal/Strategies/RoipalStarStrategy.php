<?php
namespace App\Roipal\Strategies;

use Illuminate\Support\Facades\App;
use App\Roipal\Eloquent\ExecutiveAssestment;

class RoipalStarStrategy
{
    public function drawStar(ExecutiveAssestment $data)
    {
        $img_width  = 800;
        $img_height = 800;

        $img = imagecreatetruecolor($img_width, $img_height);

        list($r, $g, $b, $o) = $this->hex2rgba('#fff');
        $white = imagecolorallocatealpha($img, $r, $g, $b, $o);

        list($r, $g, $b, $o) = $this->hex2rgba('#455a64');
        $gray = imagecolorallocatealpha($img, $r, $g, $b, $o);

        list($r, $g, $b, $o) = $this->hex2rgba('#546e7a', 25);
        $backgroundLight = imagecolorallocatealpha($img, $r, $g, $b, $o);

        list($r, $g, $b, $o) = $this->hex2rgba('#b0bec5', 25);
        $backgroundBold = imagecolorallocatealpha($img, $r, $g, $b, $o);

        list($r, $g, $b, $o) = $this->hex2rgba('#0288d1', 0);
        $blue = imagecolorallocatealpha($img, $r, $g, $b, $o);

        list($r, $g, $b, $o) = $this->hex2rgba('#b71c1c');
        $red = imagecolorallocatealpha($img, $r, $g, $b, $o);

        list($r, $g, $b, $o) = $this->hex2rgba('#fdd835');
        $yellow = imagecolorallocatealpha($img, $r, $g, $b, $o);

        list($r, $g, $b, $o) = $this->hex2rgba('#64dd17', 0);
        $green = imagecolorallocatealpha($img, $r, $g, $b, $o);

        imagefill($img, 0, 0, $white);

        // TRIANGULOS
        imagesetthickness($img, 0);
        $this->createTriangule(0, 0, 0, 200, 200, 200, $backgroundLight, $img);
        $this->createTriangule(200, 0, 400, 200, 400, 0, $backgroundLight, $img);
        $this->createTriangule(200, 200, 400, 400, 400, 200, $white, $img);
        $this->createTriangule(0, 200, 0, 400, 200, 400, $backgroundLight, $img);

        $this->createTriangule(400, 0, 400, 200, 600, 0, $backgroundLight, $img);
        $this->createTriangule(600, 0, 600, 200, 800, 0, $backgroundLight, $img);
        $this->createTriangule(400, 400, 600, 400, 600, 200, $white, $img);
        $this->createTriangule(600, 400, 800, 400, 800, 200, $backgroundLight, $img);

        $this->createTriangule(0, 400, 0, 600, 200, 400, $backgroundLight, $img);
        $this->createTriangule(0, 800, 200, 800, 200, 600, $backgroundLight, $img);
        $this->createTriangule(200, 400, 200, 600, 400, 400, $white, $img);
        $this->createTriangule(400, 600, 200, 800, 400, 800, $backgroundLight, $img);

        $this->createTriangule(400, 600, 400, 800, 600, 800, $backgroundLight, $img);
        $this->createTriangule(600, 400, 800, 600, 800, 400, $backgroundLight, $img);
        $this->createTriangule(400, 400, 400, 600, 600, 600, $white, $img);
        $this->createTriangule(600, 600, 800, 800, 800, 600, $backgroundLight, $img);

        imagesetthickness($img, 0);

        $this->createTrianguleBorder(0, 0, 0, 200, 200, 200, $gray, $img);
        $this->createTrianguleBorder(200, 200, 400, 400, 400, 200, $gray, $img);
        $this->createTrianguleBorder(200, 0, 400, 200, 400, 0, $gray, $img);
        $this->createTrianguleBorder(0, 200, 0, 400, 200, 400, $gray, $img);

        $this->createTrianguleBorder(400, 0, 400, 200, 600, 0, $gray, $img);
        $this->createTrianguleBorder(600, 0, 600, 200, 800, 0, $gray, $img);
        $this->createTrianguleBorder(400, 400, 600, 400, 600, 200, $gray, $img);
        $this->createTrianguleBorder(600, 400, 800, 400, 800, 200, $gray, $img);

        $this->createTrianguleBorder(0, 400, 0, 600, 200, 400, $gray, $img);
        $this->createTrianguleBorder(200, 400, 200, 600, 400, 400, $gray, $img);
        $this->createTrianguleBorder(0, 800, 200, 800, 200, 600, $gray, $img);
        $this->createTrianguleBorder(400, 600, 200, 800, 400, 800, $gray, $img);

        $this->createTrianguleBorder(400, 400, 400, 600, 600, 600, $gray, $img);
        $this->createTrianguleBorder(400, 600, 400, 800, 600, 800, $gray, $img);
        $this->createTrianguleBorder(600, 400, 800, 600, 800, 400, $gray, $img);
        $this->createTrianguleBorder(600, 600, 800, 800, 800, 600, $gray, $img);

        // // TRAPEZOIDES
        imagesetthickness($img, 0);

        $this->createTrapezoide(0, 0, 200, 200, 400, 200, 200, 0, $backgroundBold, $img);
        $this->createTrapezoide(0, 200, 200, 400, 400, 400, 200, 200, $blue, $img);

        $this->createTrapezoide(400, 200, 400, 400, 600, 200, 600, 0, $red, $img);
        $this->createTrapezoide(600, 200, 600, 400, 800, 200, 800, 0, $backgroundBold, $img);

        $this->createTrapezoide(0, 600, 0, 800, 200, 600, 200, 400, $backgroundBold, $img);
        $this->createTrapezoide(200, 600, 200, 800, 400, 600, 400, 400, $green, $img);

        $this->createTrapezoide(400, 400, 600, 600, 800, 600, 600, 400, $yellow, $img);
        $this->createTrapezoide(400, 600, 600, 800, 800, 800, 600, 600, $backgroundBold, $img);

        imagesetthickness($img, 0);

        $this->createTrapezoideBorder(0, 0, 200, 200, 400, 200, 200, 0, $gray, $img);
        $this->createTrapezoideBorder(0, 200, 200, 400, 400, 400, 200, 200, $gray, $img);

        $this->createTrapezoideBorder(400, 200, 400, 400, 600, 200, 600, 0, $gray, $img);
        $this->createTrapezoideBorder(600, 200, 600, 400, 800, 200, 800, 0, $gray, $img);

        $this->createTrapezoideBorder(0, 600, 0, 800, 200, 600, 200, 400, $gray, $img);
        $this->createTrapezoideBorder(200, 600, 200, 800, 400, 600, 400, 400, $gray, $img);

        $this->createTrapezoideBorder(400, 400, 600, 600, 800, 600, 600, 400, $gray, $img);
        $this->createTrapezoideBorder(400, 600, 600, 800, 800, 800, 600, 600, $gray, $img);

        imagesetthickness($img, 0);
        imageFilledRectangle($img, 330, 330, 470, 470, $white);

        //imageFilledRectangle($img, 330, 330, 470, 470, $black);

        /*----------  draw perfil score  ----------*/
        if (isset($data)) {
            $this->DrawScore($data, $img);
        }

        /**
         * Cuadro central
         */
        imagesetthickness($img, 0.5);

        imageRectangle($img, 330, 330, 470, 470, $gray);

        $path = storage_path('tmp/');
        $filePath = 'tmp/' . uniqid() . '.png';
        $filePathStorage = storage_path($filePath);

        if (!file_exists($path)) {
            @mkdir($path);
        }

        imagepng($img,$filePathStorage);

        $perfiles = $this->getDefinitionROIPALBehaviorsByLocale(App::getLocale());
        $perfil = $perfiles[$data->profile];
        
        $coordLogo = $perfil['coord-logo'];
        
        $imageLogoRoipal = base_path('public/assets/logotipo_roipal.png');
        
        $imgLogo = imagecreatefrompng($imageLogoRoipal);
        $imgNew = imagecreatefrompng($filePathStorage);

        imagecopy($imgNew, $imgLogo, $coordLogo['x'], $coordLogo['y'], 0, 0, 70, 70);

        imagepng($imgNew, $filePathStorage);
        imagedestroy($imgNew);

        return $filePath;
    }

    public function DrawScore(ExecutiveAssestment $data, $img)
    {
        list($r, $g, $b, $o) = $this->hex2rgba('#000');
        $black = imagecolorallocatealpha($img, $r, $g, $b, $o);
        $locale = App::getLocale();
        
        $perfiles = $this->getDefinitionROIPALBehaviorsByLocale($locale);

        $perfil = $perfiles[$data->profile];
        list($r, $g, $b, $o) = $this->hex2rgba($perfil['color']);
        $color = imagecolorallocatealpha($img, $r, $g, $b, $o);

        $coordName  = $perfil['coord-name'];
        

        $fontPath = base_path('resources/assets/fonts/HomBoldB_16x24_LE.gdf');
        $font = imageloadfont($fontPath);

        //imagestring($img, $font, $coordName['x'], $coordName['y'], $perfil['name'], $black);
        imagettftext($img, 20, 0, 10, 20, $black, $font, 'HOLA');
        //imagettftext($img, 20, 0, 10, 20, $black, $font, 'HOLA');
        imagesetthickness($img, 0);

    }

    public function translationDefinitionROIPALBehaviors()
    {
        return [
            'calculator' => trans('executives.executive_calculator'),
            'hunter' => trans('executives.executive_hunter'),
            'farmer' => trans('executives.executive_farmer'),
            'promotor' => trans('executives.executive_promotor')
        ];
    }

    public function getDefinitionROIPALBehaviorsByLocale($locale)
    {
        $getProfile = $this->translationDefinitionROIPALBehaviors();

        switch ($locale) {
            case 'en':
                return [
                    'C' => [
                        'name' => $getProfile['calculator'],
                        'coord-x' => 135,
                        'coord-y' => 100,
                        'coord-logo' => ['x' => 120, 'y' => 210],
                        'coord-name' => ['x' => 120, 'y' => 290],
                        'color' => '#6bb0d1',
                    ],
                    'H' => [
                        'name' => $getProfile['hunter'],
                        'coord-x' => 650,
                        'coord-y' => 200,
                        'coord-logo' => ['x' => 475, 'y' => 115],
                        'coord-name' => ['x' => 453, 'y' => 189],
                        'color' => '#7eafd1'
                    ],
                    'F' => [
                        'name' => $getProfile['farmer'],
                        'coord-x' => 50,
                        'coord-y' => 600,
                        'coord-logo' => ['x' => 265, 'y' => 520],
                        'coord-name' => ['x' => 252, 'y' => 589],
                        'color' => '#addda5'
                    ],
                    'P' => [
                        'name' => $getProfile['promotor'],
                        'coord-x' => 550,
                        'coord-y' => 700,
                        'coord-logo' => ['x' => 544, 'y' => 415],
                        'coord-name' => ['x' => 537, 'y' => 489],
                        'color' => '#f5fda2'
                    ]
                ];
                break;
                   
            case 'es':
                return [
                    'C' => [
                        'name' => $getProfile['calculator'],
                        'coord-x' => 135,
                        'coord-y' => 100,
                        'coord-logo' => ['x' => 120, 'y' => 210],
                        'coord-name' => ['x' => 120, 'y' => 290],
                        'color' => '#6bb0d1'
                    ],
                    'H' => [
                        'name' => $getProfile['hunter'],
                        'coord-x' => 650,
                        'coord-y' => 200,
                        'coord-logo' => ['x' => 475, 'y' => 115],
                        'coord-name' => ['x' => 453, 'y' => 189],
                        'color' => '#7eafd1'
                    ],
                    'F' => [
                        'name' => $getProfile['farmer'],
                        'coord-x' => 1,
                        'coord-y' => 600,
                        'coord-logo' => ['x' => 265, 'y' => 520],
                        'coord-name' => ['x' => 209, 'y' => 589],
                        'color' => '#addda5'
                    ],
                    'P' => [
                        'name' => $getProfile['promotor'],
                        'coord-x' => 550,
                        'coord-y' => 700,
                        'coord-logo' => ['x' => 544, 'y' => 415],
                        'coord-name' => ['x' => 537, 'y' => 489],
                        'color' => '#f5fda2'
                    ]
                ];
                break;

            case 'fr':
                return [
                    'C' => [
                        'name' => $getProfile['calculator'],
                        'coord-x' => 126,
                        'coord-y' => 100,
                        'coord-logo' => ['x' => 120, 'y' => 210],
                        'coord-name' => ['x' => 118, 'y' => 290],
                        'color' => '#6bb0d1'
                    ],
                    'H' => [
                        'name' => $getProfile['hunter'],
                        'coord-x' => 641,
                        'coord-y' => 200,
                        'coord-logo' => ['x' => 475, 'y' => 115],
                        'coord-name' => ['x' => 444, 'y' => 189],
                        'color' => '#7eafd1'
                    ],
                    'F' => [
                        'name' => $getProfile['farmer'],
                        'coord-x' => 5,
                        'coord-y' => 600,
                        'coord-logo' => ['x' => 265, 'y' => 520],
                        'coord-name' => ['x' => 212, 'y' => 589],
                        'color' => '#addda5'
                    ],
                    'P' => [
                        'name' => $getProfile['promotor'],
                        'coord-x' => 540,
                        'coord-y' => 700,
                        'coord-logo' => ['x' => 544, 'y' => 415],
                        'coord-name' => ['x' => 537, 'y' => 489],
                        'color' => '#f5fda2'
                    ],
                ];
                break;
            default:
                # code...
                break;
        }
    }

    public function calculatePoly(array $coord, $points)
    {

        $gap[0]['x'] = $coord[1]['x'] - $coord[2]['x'];
        $gap[0]['y'] = $coord[1]['y'] - $coord[2]['y'];
        $gap[1]['x'] = $coord[0]['x'] - $coord[3]['x'];
        $gap[1]['y'] = $coord[0]['y'] - $coord[3]['y'];

        foreach ($gap as $key => $value) {

            $coord[$key]['x'] = $coord[$key]['x'] - ($value['x'] * (1000 - $points) / 1000);
            $coord[$key]['y'] = $coord[$key]['y'] - ($value['y'] * (1000 - $points) / 1000);
        }

        return $coord;

    }

    public function hex2rgba($color, $opacity = 0)
    {

        $default = [0, 0, 0, 0];

        //Return default if no color provided
        if (empty($color)) {
            return $default;
        }

        //Sanitize $color if "#" is provided
        if ($color[0] == '#') {
            $color = substr($color, 1);
        }

        //Check if color has 6 or 3 characters and get values
        if (strlen($color) == 6) {
            $hex = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);
        } elseif (strlen($color) == 3) {
            $hex = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
        } else {
            return $default;
        }

        //Convert hexadec to rgb
        $rgb   = array_map('hexdec', $hex);
        $rgb[] = $opacity ?? 0;

        return $rgb;
    }

    public function createTriangule($x1, $y1, $x2, $y2, $x3, $y3, $color, $img)
    {
        $valuesTriangule = array(
            $x1, $y1,
            $x2, $y2,
            $x3, $y3,
        );

        imagefilledpolygon($img, $valuesTriangule, 3, $color);
    }

    public function createTrianguleBorder($x1, $y1, $x2, $y2, $x3, $y3, $color, $img)
    {
        $valuesTriangule = array(
            $x1, $y1,
            $x2, $y2,
            $x3, $y3,
        );

        imagePolygon($img, $valuesTriangule, 3, $color);
    }

    public function createTrapezoide($x1, $y1, $x2, $y2, $x3, $y3, $x4, $y4, $color, $img)
    {
        $valuesTrapezoide = array(
            $x1, $y1,
            $x2, $y2,
            $x3, $y3,
            $x4, $y4,
        );

        imagefilledpolygon($img, $valuesTrapezoide, 4, $color);
    }

    public function createTrapezoideBorder($x1, $y1, $x2, $y2, $x3, $y3, $x4, $y4, $color, $img)
    {
        $valuesTrapezoide = array(
            $x1, $y1,
            $x2, $y2,
            $x3, $y3,
            $x4, $y4,
        );

        imagepolygon($img, $valuesTrapezoide, 4, $color);
    }
}
