<?php
namespace App\Roipal\Strategies\Assessments;

use Exception;
use App\Roipal\Eloquent\Assestment;
use Illuminate\Database\Eloquent\Collection;
use App\Roipal\Strategies\Assessments\Contracts\AssessmentRepositoryInterface;

class AssessmentFactory
{
    /**
     * @return AssessmentRepositoryInterface
     */
    public function build($type)
    {
        $type = ucfirst($type);

        $class = "App\Roipal\Repositories\Executive{$type}AssestmentRepository";

        if (!class_exists($class)) {
            throw new Exception("No definition for {$type} assestment");
        }

        $instance = app($class);

        if ($instance instanceof AssessmentRepositoryInterface) {
            throw new Exception("{$type} assestment mus be implement the AssessmentRepositoryInterface");
        }

        return $instance;
    }

    public function getQuestions(Assestment $assestment, array $meta = [])
    {
        $questionsByGroup = array_get($meta, 'questions_by_group');
        $questions = $assestment->questions;

        if ($questionsByGroup) {
            $max = $questions->max('group');

            $questions = Collection::make();

            foreach (range(1, $max) as $group) {
                $found = $assestment->questions->where('group', $group)->take($questionsByGroup)->all();

                foreach ($found as $question) {
                    $questions->add($question);
                }
            }
        }

        return $questions;
    }

}