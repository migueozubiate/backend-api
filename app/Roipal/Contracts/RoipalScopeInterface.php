<?php
namespace App\Roipal\Contracts;

use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\Mission;
use Illuminate\Database\Eloquent\Builder;

interface RoipalScopeInterface
{
    public function make(array $meta = []);

    public function build(Builder $mission, array $meta = []);

    public function setUser(User $user);

    public function getUser();
}