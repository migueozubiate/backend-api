<?php
namespace App\Roipal\Contracts;

use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\Assestment;
use App\Roipal\Eloquent\ExecutiveAssestment;

interface ExecutiveAssessmentRepositoryInterface
{
    /**
     * @param Executive $executive
     * @param array $answers
     *
     * @return ExecutiveAssestment
     */
    public function store(Assestment $assestment, Executive $executive, array $answers);

}