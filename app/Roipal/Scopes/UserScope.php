<?php
namespace App\Roipal\Scopes;

use App\Roipal\Eloquent\User;
use Illuminate\Database\Eloquent\Builder;
use App\Roipal\Contracts\RoipalScopeInterface;

class UserScope implements RoipalScopeInterface
{
    protected $user;

    public function make(array $meta = [])
    {
        $params = [];

        switch ($this->user->classification) {
            case 'employeer':
            case 'company':
                $params['company_id'] = $this->user->company->id;
                break;
            default:
                break;
        }

        return $params;
    }

    public function build(Builder $users, array $meta = [])
    {
        $params = $this->make($meta);

        foreach ($params as $field => $value) {
            $users->where($field, '=', $value);
        }
        return $users;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}