<?php
namespace App\Roipal\Scopes;

use App\Roipal\Eloquent\User;
use Illuminate\Database\Eloquent\Builder;
use App\Roipal\Contracts\RoipalScopeInterface;

class ActivityScope implements RoipalScopeInterface
{
    protected $user;

    public function make(array $meta = [])
    {
        $params = [];

        switch ($this->user->classification) {
            case 'executive':
                $params['executive_id'] = $this->user->id;
                break;
            case 'employeer':
            case 'company':
                $params['company_id'] = $this->user->company->id;
                break;
            default:
                break;
        }

        return $params;
    }

    public function build(Builder $activity, array $meta = [])
    {
        $params = $this->make($meta);

        foreach ($params as $field => $value) {
            $activity->where($field, '=', $value);
        }
        return $activity;
    }

    public function setUser(User $user)
    {
        $this->user = $user;
    }

    public function getUser()
    {
        return $this->user;
    }
}