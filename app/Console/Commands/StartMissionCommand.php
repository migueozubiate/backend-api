<?php

namespace App\Console\Commands;

use App\Roipal\Constants\MissionStatus;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Jobs\StartMissionJob;
use Carbon\Carbon;
use Illuminate\Console\Command;

class StartMissionCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'mission:start';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'start currently mission by date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $today = Carbon::today();
        $missions = Mission::where('started_at', $today)->get();

        foreach ($missions as $mission) {
            $mission->status = MissionStatus::STARTED;
            $mission->save();

            StartMissionJob::dispatch($mission)->onQueue('mission');

        }
    }
}
