<?php
namespace App\Console\Commands\Invitations;

use App\Roipal\Eloquent\MissionInvitation;
use App\Roipal\Eloquent\MissionInvitationControl;
use App\Roipal\Jobs\CreateInvitationByNearProfileJob;
use Illuminate\Console\Command;

class SendMissionInvitationsToExecutivesCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invitations:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send mission invitations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $rejected = MissionInvitationControl::first();

        $invitation = MissionInvitation::where('company_id', $rejected->company_id)
            ->where('mission_id', $rejected->mission_id)
            ->where('executive_id', $rejected->executive_id)
            ->where('mission_location_id', $rejected->mission_location_id)
            ->first();

        $invitations = $invitation->mission->Invitations;

        foreach ($invitations as $value) {
            $dismiss[] = $value->executive_id;
        }

        $location = $invitation->location;
        CreateInvitationByNearProfileJob::dispatch($location, 1, $dismiss)
            ->onQueue('mission');

        $rejected->delete();

    }
}
