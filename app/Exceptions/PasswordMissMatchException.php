<?php

namespace App\Exceptions;
use Exception;

class PasswordMissMatchException extends Exception
{
    public function render()
    {
        return response()->json([
            'message' => 'The password is not correct',
            'code' => 'PASSWORD_MISSMATCH_EXCEPTION', 
        ], 400);   
    }
}