<?php

namespace App\Exceptions;
use Exception;

class UserMissMatchException extends Exception
{
    public function render()
    {
        return response()->json([
            'message' => 'User not found',
            'code' => 'USER_MISSMATCH_EXCEPTION', 
        ], 400);   
    }
}