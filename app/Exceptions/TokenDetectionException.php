<?php

namespace App\Exceptions;
use Exception;

class TokenDetectionException extends Exception
{
    protected $code;

    public function __construct($code)
    {
        $this->code = $code;
    }

    public function render()
    {
        return response()->json([
            'message' => trans('auth.token_expired'),
            'code' => $this->code
        ], 401);
    }
}