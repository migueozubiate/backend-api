<?php

namespace App\Exceptions;
use Exception;

class TypeSaleActivitiesExistException extends Exception
{
   
    public function render()
    {
        return response()->json([
            'message' => trans('missions.mission_necessary_activity'),
            'code' => "ACTIVITY_NECESSARY_EXCEPTION", 
        ], 400);
    }
}