<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Support\Facades\App;
use App\Exceptions\UserMissMatchException;
use Illuminate\Auth\AuthenticationException;
use App\Exceptions\PasswordMissMatchException;
use Illuminate\Validation\ValidationException;
use App\Exceptions\TypeSaleActivitiesExistException;
use League\OAuth2\Server\Exception\OAuthServerException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        OAuthServerException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if ($request->is('api/*')
            or $request->is('admin/*')
            or $request->wantsJson()
            or ($request->ajax() and !$request->pjax())
        ) {
            $response = null;

            switch (true) {
                case $exception instanceof TypeSaleActivitiesExistException:
                case $exception instanceof AssessmentExistException:
                case $exception instanceof MissionQualifyException:
                case $exception instanceof PasswordMissMatchException:
                case $exception instanceof UserMissMatchException:
                case $exception instanceof TokenDetectionException:

                return $exception->render();

                default:
                    # code...
                    break;
            }

            if ($exception instanceof OAuthServerException) {
                return response()->json([
                    'message' => $exception->getMessage(),
                    'code' => 401,
                    'trace' => App::environment('production') ? '' : (string)$exception
                ], 401);
            }

            if ($exception instanceof AuthenticationException) {
                return response()->json([
                    'message' => $exception->getMessage(),
                    'code' => 401
                ], 401);
            }

            if (!$exception instanceof ValidationException) {
                return response()->json([
                    'message' => $exception->getMessage(),
                    'code' => 500,
                    'trace' => App::environment('production') ? '' : (string)$exception
                ], 500);
            }
        }

        return parent::render($request, $exception);
    }

    /**
     * Convert a validation exception into a JSON response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Validation\ValidationException  $exception
     * @return \Illuminate\Http\JsonResponse
     */
    protected function invalidJson($request, ValidationException $exception)
    {
        return response()->json([
            'message' => $exception->getMessage(),
            'code' => 422,
            'errors' => $exception->errors(),
        ], $exception->status);
    }
}
