<?php

namespace App\Exceptions;
use Exception;

class AssessmentExistException extends Exception
{
    private $type;

    public function __construct($type = null)
    {
        $this->type = $type;
    }

    public function render()
    {
        return response()->json([
            'message' =>"The {$this->type} assessment has already been answered",
            'code' => 'ASSESSMENT_ALREADY_EXISTS', 
        ], 400);
    }
}