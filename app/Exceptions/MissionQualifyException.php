<?php

namespace App\Exceptions;
use Exception;

class MissionQualifyException extends Exception
{
    protected $code;

    public function __construct($code)
    {
        $this->code = $code;
    }

    public function render()
    {
        return response()->json([
            'message' => trans('missions.mission_qualify_once'),
            'code' => $this->code
        ], 400);
    }
}