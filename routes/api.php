<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::group(['prefix' => 'tools'], function ($users) {
    $users->get('website-info', 'WebsiteInfoController@index');
});

Route::group(['middleware' => ['auth:api', 'token.detector']], function ($route) {
    $route->group(['prefix' => 'users'], function ($users) {
        $users->get('{user}', 'UserController@show');
        $users->get('/', 'UserController@index');
        $users->post('/email-verification', 'UserController@sendMailVerificationAgain');
    });

    /// Assessment??
    $route->group(['prefix' => 'assestments'], function ($assestments) {
        $assestments->get('{type}', 'Assessments\AssestmentController@info');
        $assestments->get('/', 'Assessments\AssestmentController@show');
        $assestments->get('{type}/evaluations', 'Assessments\AssestmentController@index');

        $assestments->put('/{assessment}', 'Assessments\AssestmentController@update');
    });

    // Evaluations
    $route->group(['prefix' => 'evaluations'], function ($assestments) {
        $assestments->get('{uuid}', 'Assessments\ExecutiveAssestmentController@info');
    });

    $route->group(['prefix' => 'missions'], function ($missions) {
        $missions->get('/', 'Missions\MissionController@index');

        $missions->post('{mission}/invitations', 'Missions\InvitationController@store');
        $missions->post('{mission}/locations', 'Missions\InvitationController@store');

        $missions->get('suggestions', 'Missions\SuggestionController@show');

        $missions->post('{mission}/profiles', 'Missions\ProfilingController@index');

        $missions->post('{mission}/executives/{executive}/activities', 'Missions\ActivityController@store');
        $missions->put('{mission}/activities/{missionActivity}', 'Missions\ActivityController@update');

        $missions->get('{mission}/assessments', 'Assessments\CompanyAssesmentController@index');
        $missions->post('{mission}/assessments-evaluation', 'Assessments\CompanyAssesmentController@store');
    });

    $route->group(['prefix' => 'activities'], function ($activity) {
        $activity->get('/', 'Missions\ActivityController@show');
    });

    $route->group(['prefix' => 'invitations'], function ($invitation) {
        $invitation->get('{invitation}', 'Missions\InvitationController@show');
        $invitation->get('/', 'Missions\InvitationController@index');
        $invitation->put('{invitation}', 'Missions\InvitationController@status');
    });

    $route->group(['prefix' => 'executives'], function ($executives) {
        $executives->get('/', 'Executives\ExecutiveController@index');
        $executives->get('{executive}', 'Executives\ExecutiveController@show');

        $executives->get('{executive}/missions/{mission}/activities', 'Missions\ActivityController@index');
        $executives->post('{executive}/missions/{mission}/activities', 'Missions\ActivityController@store');
        $executives->post('{executive}/missions/{mission}/activities/{activity}/events', 'Missions\ActivityHistoryController@store');
        $executives->get('{executive}/missions/{mission}/activities/{activity}/events', 'Missions\ActivityHistoryController@index');

        $executives->put('{executive}', 'Executives\ExecutiveController@update');
        $executives->put('{executive}/missions/{mission}/status', 'Missions\MissionController@executiveStatus');
        $executives->get('{executive?}/missions', 'Missions\MissionController@index');
        $executives->get('{executive}/missions/{mission}', 'Executives\ExecutiveController@showMissions');

        $executives->get('/{executive?}/invitations/', 'Missions\InvitationController@index');

        $executives->get('/{executive}/roipal-star', 'RoipalStar@index');

        /// Guarda la evaluacion del vendedor.
        /// Assessment?
        $executives->post('{executive}/assestments/{type}', 'Assessments\ExecutiveAssestmentController@store');
    });

    $route->group(['prefix' => 'companies'], function ($companies) {
        $companies->put('/{company}', 'Companies\CompanyController@update');
        $companies->get('/{company}', 'Companies\CompanyController@info');
        $companies->get('/{company}/users', 'Companies\CompanyController@show');
        $companies->get('/', 'Companies\CompanyController@index');

        $companies->post('{company}/locations', 'Missions\LocationController@store');
        $companies->post('/{company}/missions/{mission}/locations', 'Missions\LocationController@storeByMission');
        $companies->get('/{company?}/locations', 'Missions\LocationController@show');
        $companies->get('/{company}/missions/{mission}/locations/{location}', 'Missions\LocationController@index');
        $companies->get('/{company}/missions/{mission}/locations', 'Missions\LocationController@getLocationsThatNotInMission');
        $companies->put('{company}/locations/{location}', 'Missions\LocationController@update');

        $companies->get('/{company?}/invitations', 'Missions\InvitationController@index');

        $companies->get('/{company?}/missions', 'Missions\MissionController@index');
        $companies->get('/{company?}/missions/{mission}', 'Missions\MissionController@show');
        $companies->post('/{company}/missions', 'Missions\MissionController@store');
        $companies->put('/{company}/missions/{mission}/status', 'Missions\MissionController@status');
        $companies->put('/missions/{mission}/executives/{executive}/rating', 'Missions\MissionController@rate');
    });

    $route->group(['prefix' => 'locations'], function ($location) {
        $location->get('/{location}', 'Missions\LocationController@info');
        $location->get('/', 'Missions\LocationController@show');
    });

    $route->group(['prefix' => 'notifications'], function ($notifications) {
        $notifications->post('/', 'NotificationController@store');
        $notifications->put('/block', 'NotificationController@block');
    });

    $route->get('catalogs/{catalog}', 'CatalogController@show');

    $route->group(['prefix' => 'languages'], function ($languages) {
        $languages->post('/', 'LanguageController@store');
        $languages->get('/', 'LanguageController@index');
        $languages->put('/{language}', 'LanguageController@update');
        $languages->get('/{locale}', 'LanguageController@info');
    });

    $route->group(['prefix' => 'translations'], function ($translation) {
        $translation->post('/{language}', 'LanguageTranslationController@store');
        $translation->get('/', 'LanguageTranslationController@index');
        $translation->put('/{translation}', 'LanguageTranslationController@update');
        $translation->get('catalogs', 'LanguageTranslationController@getByNamespaceAndGroup');
    });                           

    $route->group(['prefix' => 'files'], function ($files) {
        $files->post('/', 'FileController@store');
    });

    $route->group(['prefix' => 'limitations'], function ($limitations) {
        $limitations->get('/', 'Executives\ExecutivePhysicalLimitationController@index');
    });

    $route->group(['prefix' => 'chat'], function ($chat) {
        $chat->get('/', 'ChatController@getHistoryConversation');
    });

    $route->group(['prefix' => 'line-business'], function ($lineBusiness) {
        $lineBusiness->get('/', 'LineBusinessCatalogController@index');
        $lineBusiness->get('/{sector}', 'LineBusinessCatalogController@info');
    });

    $route->group(['prefix' => 'commercial-needs'], function ($commercialNeeds) {
        $commercialNeeds->get('/', 'CommercialNeeds\CommercialNeedController@index');
        $commercialNeeds->get('/{commercialNeed}', 'CommercialNeeds\CommercialNeedController@show');

        $commercialNeeds->get('/{commercialNeed}/type-sales', 'CommercialNeeds\CommercialTypeSaleController@index');
        $commercialNeeds->get('/{commercialNeed}/type-sales/{typeSale}', 'CommercialNeeds\CommercialTypeSaleController@getTypeSale');
    });
});

Route::post('chat-notifications', 'ChatController@notification');
Route::post('verify/user', 'MatiController@verifyUser');
Route::post('password/recovery', 'Auth\ForgotPasswordController@verifyPassword');
Route::get('generic/downloads', 'FileController@delete');
Route::get('generic/download-files', 'FileController@download');
Route::post('companies', 'Companies\CompanyController@store');
Route::post('notifications/send', 'NotificationController@sendNotificationTest');
Route::post('auth/companies', 'Companies\CompanyController@store');
Route::post('executives', 'Executives\ExecutiveController@store');
Route::post('auth/executives', 'Executives\ExecutiveController@store');
Route::post('users-web', 'UserWebController@store')->middleware('roipalWeb');
