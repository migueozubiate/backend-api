<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Illuminate\Support\Facades\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('nulldata-debugbar', function () {
    $key = Request::input('key');

    if ($key === 'ufCvoIACkboYku4B6W6zVoLpI7HqhqqvQHyXOvyB6hEUt1oxWp') {
        $renderer = Debugbar::getJavascriptRenderer();

        if (Debugbar::getStorage()) {
            $openHandlerUrl = route('debugbar.openhandler');
            $renderer->setOpenHandlerUrl($openHandlerUrl);
        }

        return '<html><head><title>Debugbar</title></head><body>' . $renderer->renderHead() . $renderer->render() . '</body></html>';
    }

    return view('welcome');
});

Auth::routes(['verify' => true]);
Auth::routes(['reset' => true]);

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/verify-email/{locale}', function ($locale) {  

    $dataMessageEmail = [
        'email_verify_title' => trans("mail.email_verify_title", [], $locale),
        'email_verify_description' => trans("mail.email_verify_description", [], $locale)
    ]; 

    return view('auth.verify_email', $dataMessageEmail); 
});

Route::get('/confirmation', 'PasswordController@index')->name('confirmation');

Route::get('/download/app', function () {
    return view('application.download-app');
});

    