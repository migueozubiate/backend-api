<?php
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Request;
use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\ExecutiveProfile;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Support\Facades\DB;

Route::get('test', function () {
    $point = new Point(19.3665903, -99.1832615);

    $points = ExecutiveProfile::select(
        '*',
        DB::raw("ST_Distance_Sphere(`position`, ST_GeomFromText('{$point->toWKT()}')) as distance"))
        ->distanceSphere('position', $point , 2 * 1000)
        ->limit(5)
        ->get();

    return $points;
});

Route::get('tests', function () {
    $data = [
        '1a' => 'No demostró lo suficiente, el valor de su producto o de su servicio.',
        '4b' => 'Aportó demasiada información y confunfio al comprador, forzándolo a buscar en otra parte.',
        '3c' => 'Vendió con base en las necesidades del cliente, pero falloó en el cierre o en obtener el compromiso del cliente.',
        '2d' => 'No logró crear un clime de confianza.'
    ];

    $sorted = Arr::sort($data, function($item, $index) {
        return $index[0];
    });

    $ideal = [];

    foreach ($sorted as $key => $value) {
        $ideal[] = Str::upper($key[1]);
    }

    $answers = [];

    $haystack = [
        'ABCD' => 100,
        'ABDC' => 75,
        'ACBD' => 75,
        'ACDB' => 75,
        'ADBC' => 50,
        'ADCB' => 50,
        'BACD' => 75,
        'BADC' => 75,
        'BCAD' => 50,
        'BCDA' => 25,
        'BDAC' => 25,
        'BDCA' => 25,
        'CBAD' => 25,
        'CBDA' => 25,
        'CABD' => 25,
        'CADB' => 25,
        'CDBA' => 0,
        'CDAB' => 0,
        'DBCA' => 0,
        'DBAC' => 0,
        'DCBA' => 0,
        'DCAB' => 0,
        'DABC' => 0,
        'DACB' => 0,
    ];

    $keys = ['A', 'B', 'C', 'D'];
    $mapping = [];

    foreach ($ideal as $value) {
        $mapping[array_shift($keys)] = $value;
    }

    foreach ($haystack as $key => $value) {
        $chars = str_split($key);
        $index = '';

        foreach ($chars as $char) {
            $index .= $mapping[$char];
        }

        $index = $index;
        $answers[$index] = $value;
    }

    $answer = 'ADCB';

    $points = $answers[$answer];

    dd($sorted, $ideal, $haystack, $answers, $mapping, $points);
});
