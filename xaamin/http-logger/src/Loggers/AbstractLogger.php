<?php
namespace Xaamin\HttpLogger\Loggers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\Guard;
use Xaamin\HttpLogger\Support\Browser;
use Illuminate\Contracts\Container\Container;
use Symfony\Component\HttpFoundation\Response;
use Xaamin\HttpLogger\Contracts\LoggerWriterInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

abstract class AbstractLogger implements LoggerWriterInterface
{
    /**
     * Get the basic request info
     *
     * @param Request $request
     * @return array
     */
    public function getRequestInfo(Request $request)
    {
        $filteredFields = config('http-logger.except');

        $method = strtoupper($request->getMethod());
        $url = str_replace(url(''), '', $request->fullUrl());
        $input = $request->except($filteredFields);
        $ip_address = $request->header('X_FORWARDED_FOR', $request->ip());

        $files = iterator_to_array($request->files);

        $parseFileNames = function (UploadedFile $file) {
            return $file->getClientOriginalName();
        };

        $files = array_map($parseFileNames, $files);

        return compact('method', 'url', 'input', 'files', 'ip_address');
    }

    /**
     * Get logged in user info
     *
     * @param Container $container
     * @return array
     */
    public function getUserInfo(Container $container = null)
    {
        $data = [];

        if (!$container) {
            return $data;
        }

        if ($container->bound(Guard::class)) {
            $auth = $container->make('auth');
            $api = $auth->guard('api');

            if (!$auth->guest() or !$api->guest()) {
                $user = $auth->id() ? $auth : $api;

                $data['user_id'] = $user->id();
                $data['email'] = $user->user()->email;
            }
        }

        return $data;
    }

    /**
     * Get browser info
     *
     * @param Container $container
     * @return array
     */
    public function getBrowserInfo(Container $container = null)
    {
        $data = [];

        if (!$container) {
            return $data;
        }

        if ($container->bound('request')) {
            $request = $container->make('request');
            $browser = new Browser($request->header('User-Agent'));

            $data['platform'] = $browser->getPlatform();
            $data['browser'] = $browser->getBrowser();

            $data['user_agent'] = substr(
                (string)$request->header('User-Agent'), 0, 500
            );
        }

        return $data;
    }

    public function getResponse(Response $response = null)
    {
        $data = [];

        if ($response) {
            $content = $response->getContent();
            $status = $response->getStatusCode();
            $type = $response->headers->get('Content-Type');

            $data = [
                'status_code' => $status,
                'response_type' => $type,
                'response_body' => $content
            ];
        }

        return $data;
    }
}