<?php
namespace Xaamin\HttpLogger\Loggers;

use Illuminate\Http\Request;
use Illuminate\Contracts\Container\Container;
use Xaamin\HttpLogger\Loggers\AbstractLogger;
use Symfony\Component\HttpFoundation\Response;

class FileLogger extends AbstractLogger
{
    private $container;

    public function __construct(Container $container)
    {
        $this->container = $container;
    }

    public function log(Request $request, Response $response = null, array $meta = [])
    {
        $data = $this->getRequestInfo($request);
        $response = $this->getResponse($response);;

        $url = array_get($data, 'url');
        $method = array_get($data, 'method');
        $bodyAsJson = json_encode(array_get($data, 'input', []));

        $files = array_get($data, 'files', []);
        $user = $this->getUserInfo($this->container);
        $browser = $this->getBrowserInfo($this->container);
        $files = implode(', ', $files);

        $requestInfo = "\n{$method} {$url}";
        $requestBodyInfo = "{$bodyAsJson} \n\tFiles: " . ($files !== '' ? : 'None');
        $userInfo = (!empty($user) ? "\n\t {$user['user_id']} - {$user['email']}." : "");
        $browserInfo = (!empty($browser) ? " using  {$browser['browser']} on platform {$browser['platform']} \n\t" : "\n\t");
        $responseInfo = $response ? "\nResponse: {$response['status_code']} - {$response['response_type']} in {$meta['response_time']} s \n\t{$response['response_body']}" : "";

        $message = $requestInfo
            . $userInfo
            . $browserInfo
            . $requestBodyInfo
            . $responseInfo;

        info($message);
    }
}