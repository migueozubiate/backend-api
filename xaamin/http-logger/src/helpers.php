<?php
use Illuminate\Http\Request;

if (!function_exists('http_log')) {
    function http_log(Request $request)
    {
        app('http-logger')->log($request);
    }
}