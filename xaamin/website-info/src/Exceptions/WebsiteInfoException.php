<?php
namespace Xaamin\WebsiteInfo\Exceptions;

use RuntimeException;

class WebsiteInfoException extends RuntimeException
{
}