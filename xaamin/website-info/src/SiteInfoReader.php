<?php
namespace Xaamin\WebsiteInfo;

use DOMDocument;
use Xaamin\WebsiteInfo\Contracts\ProcessorInterface;
use Xaamin\WebsiteInfo\Exceptions\WebsiteInfoException;

class SiteInfoReader
{
    private $processors = [];

    protected $metadata;

    public function parse($url)
    {
        $this->metadata = [];

        $html = $this->curl_get_contents($url);

        $doc = new DOMDocument();
        @$doc->loadHTML('<?xml encoding="utf-8" ?>' . $html);

        $processors = array_values($this->processors);

        foreach ($processors as $processor) {
            $this->metadata += $processor->parse($doc);
        }

        return $this->metadata;
    }

    public function register(ProcessorInterface $processor)
    {
        $this->processors[$processor->getName()] = $processor;
    }

    protected function curl_get_contents($url)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_FAILONERROR, 1);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_TIMEOUT, 30);
        curl_setopt($curl, CURLOPT_ENCODING, 'UTF-8');

        $response = curl_exec($curl);

        curl_close($curl);

        return $response;
    }

    public function using($processor)
    {
        if (isset($this->processors[$processor])) {
            return $this->processors[$processor];
        }

        throw new WebsiteInfoException("Processor {$processor} is not registered");
    }

    public function __get($key)
    {
        return $this->using($key);
    }
}