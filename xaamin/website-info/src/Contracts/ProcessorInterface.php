<?php
namespace Xaamin\WebsiteInfo\Contracts;

use DOMDocument;

interface ProcessorInterface
{
    public function getName();

    public function all();

    public function parse(DOMDocument $document);
}