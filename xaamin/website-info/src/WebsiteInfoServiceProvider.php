<?php
namespace Xaamin\WebsiteInfo;

use Xaamin\WebsiteInfo\SiteInfoReader;
use Illuminate\Support\ServiceProvider;
use Xaamin\WebsiteInfo\Processors\Html;
use Xaamin\WebsiteInfo\Processors\Twitter;
use Xaamin\WebsiteInfo\Processors\OpenGraph;

class WebsiteInfoServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function boot()
    {
    }

    public function register()
    {
        $this->app->singleton('website-info', function () {
            $reader = new SiteInfoReader();

            $reader->register(new Html());
            $reader->register(new OpenGraph());
            $reader->register(new Twitter());

            return $reader;
         });

        $this->app->singleton(SiteInfoReader::class, function ($app) {
            return $app['website-info'];
        });
    }

    public function provides()
    {
        return [
            'website-info',
            SiteInfoReader::class
        ];
    }
}