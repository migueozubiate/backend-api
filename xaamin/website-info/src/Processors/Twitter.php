<?php
namespace Xaamin\WebsiteInfo\Processors;

use DOMXPath;
use DOMDocument;

class Twitter extends AbstractProcessor
{
    protected $protocol = 'twitter';

    public function getName()
    {
        return 'twitter';
    }

    public function parse(DOMDocument $document)
    {
        $metadata = [];

        $xpath = new DOMXPath($document);

        $tags = [];

        foreach (array('name', 'property') as $name) {
            $tags = $xpath->query("//*/meta[starts-with(@{$name}, '{$this->protocol}:')]");

            if ($tags->length > 0) {
                break;
            }
        }

        foreach ($tags as $tag) {
            $nodeHasPropertyAttribute = $tag->hasAttribute('property');
            $property = trim(($nodeHasPropertyAttribute ? $tag->getAttribute('property') : $tag->getAttribute('name')));

            $property = str_replace("{$this->protocol}:", '', $property);

            if ($property) {
                $metadata[$property] = $tag->getAttribute('content');
            }
        }

        $this->metadata = $metadata;

        return [
            $this->getName() => $metadata
        ];
    }
}