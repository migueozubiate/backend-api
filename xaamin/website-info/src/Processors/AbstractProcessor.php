<?php
namespace Xaamin\WebsiteInfo\Processors;

use DOMXPath;
use DOMDocument;
use Xaamin\WebsiteInfo\Contracts\ProcessorInterface;

abstract class AbstractProcessor implements ProcessorInterface
{
    protected $metadata = [];

    public function all()
    {
        return $this->metadata;
    }
}