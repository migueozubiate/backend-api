<?php
namespace Xaamin\WebsiteInfo\Processors;

use DOMXPath;
use DOMDocument;

class OpenGraph extends AbstractProcessor
{
    protected $protocol = 'og';

    private $structured = [
        'audio',
        'image',
        'video',
        'music',
        'article',
        'book',
        'profile'
    ];

    private $arrayable = [
        'audio',
        'image',
        'video'
    ];

    protected $parseable = [];

    public function getName()
    {
        return 'opengraph';
    }

    public function parse(DOMDocument $document)
    {
        $metadata = [];

        $xpath = new DOMXPath($document);

        $tags = [];

        foreach (array('property', 'name') as $name) {
            $tags = $xpath->query("//*/meta[starts-with(@{$name}, '{$this->protocol}:')]");

            if ($tags->length > 0) {
                break;
            }
        }

        foreach ($tags as $tag) {
            $nodeHasPropertyAttribute = $tag->hasAttribute('property');
            $property = ($nodeHasPropertyAttribute ? $tag->getAttribute('property') : $tag->getAttribute('name'));
            $protocol = "{$this->protocol}:";

            $key = substr($property, strlen($protocol));

            if (strpos($key, ':') === false and !in_array($key, $this->structured)) {
                $value = $tag->getAttribute('content');

                $metadata[$key] = $value;
            } else {
                $this->parseable[] = [
                    $key => $tag->getAttribute('content')
                ];
            }
        }

        $metadata += $this->processStructuredProperties($this->parseable);

        $this->metadata = $metadata;

        return [
            $this->getName() => $metadata
        ];
    }

    protected function processStructuredProperties(array $properties)
    {
        $metadata = $this->parsePropertiesAsMetadata($properties);

        $parsed = $this->parsePropertiesToStructures($metadata);

        return $parsed;
    }

    protected function parsePropertiesAsMetadata(array $properties)
    {
        $metadata = [];

        foreach ($properties as $property) {
            foreach ($property as $key => $value) {
                if (!isset($metadata[$key])) {
                    $metadata[$key] = [];
                }

                $metadata[$key][] = $value;
            }
        }

        return $metadata;
    }

    protected function parsePropertiesToStructures(array $metadata)
    {
        $parsed = [];

        foreach ($this->arrayable as $key) {
            $parsed["{$key}s"] = [];
        }

        foreach ($metadata as $key => $value) {
            $original = $key;

            $key = $this->getMetaTagKey($original);

            if (in_array($key, $this->arrayable)) {
                $key = "{$key}s";

                $name = $this->getMetaTagName($original) ? : 'url';

                foreach ($value as $index => $content) {
                    if (!isset($parsed[$key][$index])) {
                        $parsed[$key][$index] = [];
                    }

                    $parsed[$key][$index][$name] = $content;
                }

            } else {
                if (!isset($parsed[$key])) {
                    $parsed[$key] = [];
                }

                $name = $this->getMetaTagName($original);

                $parsed[$key][$name] = $value[0];
            }
        }

        return $parsed;
    }

    protected function validateTagIfTagsNeedsParsing($key)
    {
        $needsParse = in_array($key, $this->structured);

        if (!$needsParse) {
            foreach ($this->structured as $attribute) {
                $needsParse = strpos($key, $attribute) === 0;

                if ($needsParse) {
                    break;
                }
            }
        }

        return $needsParse;
    }

    protected function addToMetadata(array $metadata, $key, $value)
    {
        $name = in_array($key, $this->structured) ? 'url' : $this->getMetaTagName($key);
        $key = $this->getMetaTagKey($key);

        $info = isset($metadata[$key]) ? $metadata[$key] : [];

        $info[$name] = $value;

        $metadata[$key] = $info;

        return $metadata;
    }

    protected function getMetaTagKey($key) {
        $parts = explode(':', $key);

        return array_shift($parts);
    }

    protected function getMetaTagName($key) {
        $parts = explode(':', $key);

        array_shift($parts);

        return implode(':', $parts);
    }

    protected function closeStructuredProperty(array $metadata, $key)
    {
        if (isset($this->parseable[$key])) {
            $metadata[$key] = $this->parseable[$key];

            unset($this->parseable[$key]);

            $this->parseable[$key] =[];
        } else {
            $metadata[$key] = [];

            $this->parseable[$key] =[];
        }

        return $metadata;
    }
}