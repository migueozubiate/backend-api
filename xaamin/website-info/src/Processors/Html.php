<?php
namespace Xaamin\WebsiteInfo\Processors;

use DOMXPath;
use DOMDocument;

class Html extends AbstractProcessor
{
    public function getName()
    {
        return 'html';
    }

    public function parse(DOMDocument $document)
    {
        $metadata = [];

        $xpath = new DOMXPath($document);

        $tags = $xpath->query("//meta");

        foreach ($tags as $tag) {
            $nodeHasPropertyAttribute = $tag->hasAttribute('property');
            $property = trim(($nodeHasPropertyAttribute ? $tag->getAttribute('property') : $tag->getAttribute('name')));

            if ($property) {
                $metadata[$property] = $tag->getAttribute('content');
            }
        }

        $this->metadata = $metadata;

        return [
            $this->getName() => $metadata
        ];
    }
}