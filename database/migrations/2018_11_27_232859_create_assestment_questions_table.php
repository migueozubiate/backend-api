<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssestmentQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assestment_questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assestment_id');
            $table->smallInteger('number');
            $table->string('type',12)->nullable();
            $table->tinyInteger('group')->unsigned()->nullable();
            $table->text('question')->nullable();
            $table->mediumText('options')->nullable();
            $table->text('answer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assestment_questions');
    }
}