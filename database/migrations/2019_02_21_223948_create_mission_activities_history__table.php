<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissionActivitiesHistoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mission_activities_history', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('mission_activity_id');
            $table->geometry('position');
            $table->string('address');
            $table->string('images')->nullable();
            $table->string('type');
            $table->mediumText('comment')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mission_activities_history');
    }
}
