<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecutiveProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executive_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->bigInteger('phone')->nullable();
            $table->boolean('video_bio')->default(false);
            $table->string('video_bio_url')->nullable();
            $table->string('thumbnail_video')->nullable();
            $table->string('photo_bio_url')->nullable();
            $table->text('bio');
            $table->text('website');
            $table->boolean('terms_accepted')->nullable();
            $table->text('address');
            $table->string('country')->nullable();
            $table->geometry('position')->spatialIndex();
            $table->float('score')->default(0);
            $table->boolean('registered_payment')->default(0);
            $table->integer('invitations_received')->default(0);
            $table->integer('verification_status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('executive_profiles');
    }
}
