<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecutivePaymentProfiles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executive_payment_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('executive_id');
            $table->string('owner')->nullable();
            $table->string('type_payment', 20)->nullable();
            $table->string('card_number', 30)->nullable();
            $table->string('bank', 50)->nullable();
            $table->string('type_card', 20)->nullable();
            $table->string('email')->nullable();
            $table->string('clabe', 20)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('executive_payment_profiles');
    }
}
