<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionInvitationControlTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missions_invitations_control', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('company_id');
            $table->integer('mission_id');
            $table->integer('user_id')->nullable();
            $table->integer('executive_id')->nullable();
            $table->integer('mission_location_id')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->string('reason')->nullable();
            $table->tinyInteger('notification')->default(0);
            $table->dateTime('response_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missions_invitations_control');
    }
}
