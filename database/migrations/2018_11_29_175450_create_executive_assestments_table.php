<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecutiveAssestmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executive_assestments', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('user_id');
            $table->integer('mission_id')->nullable();
            $table->string('name', 255);
            $table->longText('summary')->nullable();
            $table->smallInteger('total_questions');
            $table->char('profile', 1)->nullable();
            $table->smallInteger('points')->nullable();
            $table->text('description')->nullable();
            $table->string('item', 150)->nullable();
            $table->string('validity', 15)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('executive_assestments');
    }
}
