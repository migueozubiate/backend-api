<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMissionLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mision_locations', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('user_id')->index();
            $table->integer('mission_id')->index();
            $table->integer('location_id')->index();
            $table->string('name');
            $table->text('address');
            $table->geometry('position');
            $table->mediumInteger('executives_requested')->default(0);
            $table->mediumInteger('invitations_sent')->default(0);
            $table->smallInteger('accepted')->default(0);
            $table->smallInteger('rejected')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mision_locations');
    }

    
}
