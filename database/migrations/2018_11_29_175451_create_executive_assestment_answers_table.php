<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecutiveAssestmentAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executive_assestment_answers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assestment_id');
            $table->tinyInteger('number');
            $table->tinyInteger('group')->nullable();
            $table->text('question');
            $table->text('options');
            $table->text('answer')->nullable();
            $table->char('profile', 1)->nullable();
            $table->smallInteger('points')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('executive_assestment_answers');
    }
}
