<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssestmentAnswersPollTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assestment_answers_poll', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('assestment_id');
            $table->smallInteger('number')->nullable();
            $table->char('answer', 4)->index();
            $table->text('meaning');
            $table->char('profile', 1)->nullable();
            $table->smallInteger('points')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assestment_answers_poll');
    }
}
