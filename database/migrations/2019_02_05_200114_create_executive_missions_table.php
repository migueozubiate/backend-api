<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecutiveMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executive_missions', function (Blueprint $table) {

            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('mission_id')->unsigned();
            $table->integer('company_id')->unsigned();
            $table->integer('executive_id')->unsigned();
            $table->dateTime('accepted_at')->nullable();
            $table->integer('mission_location_id')->unsigned();
            $table->float('rate_mission')->default(0);
            $table->string('review')->default(' ');
            $table->tinyInteger('status')->nullable();
            $table->timestamps();

            $table->unique([
                'mission_id','company_id','executive_id'
            ]);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('executive_missions');
    }
}
