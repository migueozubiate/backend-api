<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExecutiveDocumentationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executive_documentation', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('executive_id');
            $table->string('national_id_back_url')->nullable();
            $table->string('national_id_from_url')->nullable();
            $table->string('proof_residency_url')->nullable();
            $table->string('curp_url')->nullable();
            $table->string('rfc_url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('executive_documentation');
    }
}
