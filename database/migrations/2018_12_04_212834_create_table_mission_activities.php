<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMissionActivities extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mission_activities', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->uuid('uuid')->index();
            $table->integer('mission_id');
            $table->integer('executive_id');
            $table->integer('company_id');
            $table->string('title');
            $table->longText('description')->nullable();
            $table->tinyInteger('status')->default(0);
            $table->dateTime('started_at')->nullable()->index();
            $table->dateTime('finished_at')->nullable()->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mission_activities');
    }
}
