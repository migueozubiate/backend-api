<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyPaymentProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_payment_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('owner')->nullable();
            $table->string('clasification', 50)->nullable();
            $table->string('card_number', 30);
            $table->string('exp');
            $table->string('type', 20)->nullable();
            $table->string('ccv');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_payment_profiles');
    }
}
