<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompanyProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id');
            $table->string('slogan')->nullable();
            $table->boolean('video_bio')->default(false);
            $table->string('video_bio_url')->nullable();
            $table->string('thumbnail_video')->nullable();
            $table->string('photo_bio_url')->nullable();
            $table->longText('bio');
            $table->text('website');
            $table->string('vertical');
            $table->boolean('terms_accepted')->nullable();
            $table->text('address');
            $table->string('country');
            $table->geometry('position');
            $table->string('contact_name')->nullable();
            $table->string('contact_phone', 50)->nullable();
            $table->string('contact_job_title')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('company_profiles');
    }
}
