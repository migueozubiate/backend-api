<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('missions', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->uuid('uuid')->index();

            $table->integer('user_id')->unsigned()->index();
            $table->integer('company_id')->unsigned()->index();
            $table->integer('type_sale_id')->unsigned()->index();

            $table->string('name');
            $table->text('description');
            $table->boolean('video')->default(false);
            $table->string('video_url')->nullable();
            $table->string('thumbnail_video')->nullable();
            $table->string('commercial_need');
            $table->string('type')->index();
            $table->string('profile')->index();
            $table->mediumInteger('time');
            $table->string('time_unit');
            $table->decimal('fare_by_time', 18, 4);
            $table->decimal('subtotal', 18, 4);
            $table->decimal('tax', 18, 4)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->decimal('total', 18, 4);
            $table->longText('type_sale_activities');
            $table->text('currency');
            $table->string('country', 3);
            $table->smallInteger('executives_requested')->default(0);
            $table->smallInteger('invitations_sent')->default(0);
            $table->smallInteger('accepted')->default(0);
            $table->smallInteger('rejected')->default(0);
            $table->float('score_mission')->default(0);
            $table->date('started_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('missions');
    }
}
