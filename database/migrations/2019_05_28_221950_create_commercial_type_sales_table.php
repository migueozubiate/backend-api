<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommercialTypeSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commercial_type_sales', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('commercial_need_id');
            $table->string('type');
            $table->mediumText('description');
            $table->text('activities');
            $table->string('item_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commercial_type_sales');
    }
}
