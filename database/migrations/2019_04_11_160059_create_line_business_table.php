<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLineBusinessTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('line_business', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('uuid');
            $table->integer('code');
            $table->string('line_business');
            $table->string('sector');
            $table->string('item_line_business', 150)->nullable();
            $table->string('item_sector', 150)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('line_business');
    }
}
