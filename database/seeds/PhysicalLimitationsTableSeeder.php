<?php

use Ramsey\Uuid\Uuid;
use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\PhysicalLimitation;

class PhysicalLimitationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $limitations = [
            [
                'limitation' => "I don't have any limitation",
                'flag' => 1,
                'item' => 'not_physical_limitation'
            ],
            [
                'limitation' => 'Visual Limitations',
                'flag' => 0,
                'item' => 'physical_visual'
            ],
            [
                'limitation' => 'Mobility Limitations',
                'flag' => 0,
                'item' => 'physical_mobility'
            ],
            [
                'limitation' => 'Verbal Limitations',
                'flag' => 0,
                'item' => 'physical_verbal'
            ],
        ];

        foreach ($limitations as $value) {
            $physicalLimitation = new PhysicalLimitation($value);
            $physicalLimitation->uuid = Uuid::uuid4()->toString();
            $physicalLimitation->save();
        }
    }
}
