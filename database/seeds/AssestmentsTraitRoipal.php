<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Str;

trait AssestmentsTraitRoipal
{

    protected function getROIPALAssetment()
    {
        return [
            [
                'uuid' => '08030441-53b9-4649-9a40-6cc490f2baaa',
                'name' => 'ROIPAL',
                'lang' => 'es',
                'summary' => <<< SUMMEOL
Instrucciones para responder el cuestionario del Centro de Habilidades de Ventas:

Nuestras reacciones y deciciones frente a una situación, a una pregunta u objeción harán nuestro éxito o nuestro fracaso en la venta. Favor de leer cuidadosamente, a continuaciòn, las instucciones reales que ocurren durante el proceso de venta, frente a las cuales tendrá 4 opciones a priorizar. El número uno será su mejor opciòn de acción, el nùmero dos su segunda opción, y así sucesivamente, el cuarto siendo lo que no hay que hacer.
SUMMEOL
            ],
            [
                'uuid' => '4bb3a9f2-2eb4-47eb-90c3-5867f277b1a2',
                'name' => 'ROIPAL',
                'lang' => 'en',
                'summary' => <<< SUMMEOL
Instructions for answering the Sales Skill Center assessment:

Our reactions and decisions regarding a situation, a question or an objection will make our success or failure in the sale. Please read carefully, below, the real instuctions that occur during the sales process, against which you will have 4 options to prioritize. The number one will be your best option of action, the number two your second option, and so on, the fourth being the right thing to do.
SUMMEOL
            ],
            [
                'uuid' => 'e660be14-267c-4473-afe0-465166c77c39',
                'name' => 'ROIPAL',
                'lang' => 'fr',
                'summary' => <<< SUMMEOL
Instructions pour répondre au questionnaire sur les Centres de Compétences en Vente:

Nos réactions et décisions concernant une situation, une question ou une objection feront notre succès ou notre échec dans la vente. Veuillez lire attentivement, ci-dessous, les instructions réelles qui se produisent au cours du processus de vente, contre lesquelles vous aurez 4 options à prioriser. Le numéro un sera votre meilleure option d'action, le numéro deux votre deuxième option, et ainsi de suite, le quatrième étant la bonne chose à faire.
SUMMEOL
            ]
        ];
    }

    protected function getROIPALQuestions($language)
    {
        switch ($language) {
            case 'es':
                $questions = $this->getROIPALQuestionsFull();
                break;

            case 'en':
                $questions = $this->getROIPALQuestionsInEnglish();
                break;

            case 'fr':
                $questions = $this->getROIPALQuestionsInFrench();
                break;
            
            default:
                # code...
                break;
        }

        foreach($questions as $index => $value) {
            $options = $value['options'];

            $questions[$index]['options'] = [];

            foreach ($options as $key => $option) {
                $questions[$index]['options'][$key[1]] = $option;
            }
        }

        return $questions;
    }

    protected function getROIPALAnswers($language)
    {
        $original = $this->getROIPALQuestions($language);

        switch ($language) {
            case 'es':
                $questions = $this->getROIPALQuestionsFull();
                break;

            case 'en':
                $questions = $this->getROIPALQuestionsInEnglish();
                break;

            case 'fr':
                $questions = $this->getROIPALQuestionsInFrench();
                break;
            
            default:
                # code...
                break;
        }

        $answers = [];

        foreach($questions as $number => $question) {
            $permutations = $this->getAnswersPollForRoipal($question['options']);

            $answers[$number] = [];

            foreach($permutations as $index => $points) {
                $meaning = [];

                $values = str_split($index);

                foreach ($values as $value) {
                    $meaning[$value] = $original[$number]['options'][$value];
                }

                $answers[$number][] = [
                    "meaning" => $meaning,
                    "result" => " {$points}"
                ];
            }
        }

        return $answers;
    }

    protected function getAnswersPollForRoipal($options)
    {
        $sorted = Arr::sort($options, function($item, $index) {
            return $index[0];
        });

        $ideal = [];

        foreach ($sorted as $key => $value) {
            $ideal[] = Str::upper($key[1]);
        }

        $answers = [];

        $haystack = [
            'ABCD' => 100,
            'ABDC' => 75,
            'ACBD' => 75,
            'ACDB' => 75,
            'ADBC' => 50,
            'ADCB' => 50,
            'BACD' => 75,
            'BADC' => 75,
            'BCAD' => 50,
            'BCDA' => 25,
            'BDAC' => 25,
            'BDCA' => 25,
            'CBAD' => 25,
            'CBDA' => 25,
            'CABD' => 25,
            'CADB' => 25,
            'CDBA' => 0,
            'CDAB' => 0,
            'DBCA' => 0,
            'DBAC' => 0,
            'DCBA' => 0,
            'DCAB' => 0,
            'DABC' => 0,
            'DACB' => 0,
        ];

        $keys = ['A', 'B', 'C', 'D'];
        $mapping = [];

        foreach ($ideal as $value) {
            $mapping[array_shift($keys)] = $value;
        }

        foreach ($haystack as $key => $value) {
            $chars = str_split($key);
            $index = '';

            foreach ($chars as $char) {
                $index .= $mapping[$char];
            }

            $index = $index;
            $answers[$index] = $value;
        }

        return $answers;
    }

    protected function getROIPALQuestionsFull()
    {
        return [

            /// -- clasificacion 4

            [
                'group' => 4,
                'type' => 'order',
                'question' => 'Ha terminado su presentación y le dicen que vuelva en una semana. Cuando regresa, encuentra que su cliente potencial ha decidido comprar a un competidor.',
                'options' => [
                    '1A' => 'No demostró lo suficiente, el valor de su producto o de su servicio.',
                    '4B' => 'Aportó demasiada información y confundió al comprador, forzándolo a buscar en otra parte.',
                    '3C' => 'Vendió con base en las necesidades del cliente, pero falló en el cierre o en obtener el compromiso del cliente.',
                    '2D' => 'No logró crear un clime de confianza.'
                ]
            ],
            [
                'group' => 4,
                'type' => 'order',
                'question' => 'Han pasado 45 minutos con un cliente y le dice: “Se nos acabó el tiempo”. Usted debería',
                'options' => [
                    '3A' => 'Insistir para cerrar la venta.',
                    '2B' => 'Pedir una cita posterior para regresar y terminar su presentación.',
                    '4C' => 'Pedir el cliente cinco minutos más.',
                    '1D' => 'Preguntar al cliente que información adicional necesita.'
                ]
            ],
            [
                'group' => 4,
                'type' => 'order',
                'question' => 'Llega a la oficina del cliente y lo reciben diciendo: “Enséñeme qué trae”. Usted debería',
                'options' => [
                    '4A' => 'Mostrarle lo que va a ofrecer.',
                    '3B' => 'Preguntarle qué está usando actualmente.',
                    '2C' => 'Averiguar si tiene una necesidad real.',
                    '1D' => 'Decirle quién es y por qué esta ahí'
                ]
            ],
            [
                'group' => 4,
                'type' => 'order',
                'question' => 'El cliente dice. "El precio está bien, pero tengo muchas dudas acerca de la durabilidad del producto". Debería:',
                'options' => [
                    '4A' => 'Demostrar concretamente al cliente lo duradero que es su producto.',
                    '1B' => 'Buscar el origen de estas dudas y preguntar directamente: "¿Qué es lo que le preocupa acerca de la durabilidad?"',
                    '2C' => 'Hacer que el cliente hable con otros clientes que hayan tenido buena experiencia con la durabilidad.',
                    '3D' => 'Preguntar. "¿Qué función juega la durabilidad del producto en su decisión?'
                ]
            ],

            // -- clasificacion 3

            [
                'group' => 3,
                'type' => 'order',
                'question' => 'Después de una conversación preliminar, es momento de empezar la presentación de ventas. Usted debería:',
                'options' => [
                    '4A' => 'Empezar mostrando las características y beneficios de su producto o servicio.',
                    '1B' => 'Empezar a efecturar preguntas para hacer un análisis de necesidades.',
                    '3C' => 'Comenzar diciendole al cliente cómo sus productos pueden resolverle problemas.',
                    '2D' => 'Efectuar preguntas que llevan la conversación a sus propósitos de venta.'
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => 'Durante la entrevista, usted descubre que su interlocutor es nuevo en el puesto y que no tiene muy claro las necesidades pasadas ni las futuras. Usted debería:',
                'options' => [
                    '1A' => 'Preguntar "¿Quién más participara en la toma de decisión?".',
                    '4B' => 'Preguntar "¿En cuánto tiempo cree que se tomará la decisión?".',
                    '3C' => 'Hacer preguntas para descubrir si se ha reunido con la competencia o con otros vendedores de productos o servicios similares.',
                    '2D' => 'Ofrecer colaborar con él y ayudarlo a adquirir conocimientos de modo más rápido.'
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => 'El cliente expresa que necesita hacer alguna investigación sobre usted y la empresa que representa. Usted debería:',
                'options' => [
                    '3A' => 'Darle una lista de clientes a los que puede pedir referencias.',
                    '2B' => 'Ofrecerle ayuda para buscar la información',
                    '1C' => 'Preguntarle específicamente qué le gustaría investigar.',
                    '4D' => 'Ofrecerle la llamada posterios por parte de clientes satisfechos.'
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => 'Un cliente le dice que quiere hablar con usted, expresando: "Creo que tiene exactamente lo que busco". Basándose en este comentario, usted debería:',
                'options' => [
                    '2A' => 'Averiguar qué hace el cliente que éste es el producto que está buscando',
                    '3B' => 'Descubrir cuánto sabe acerca de su producto o servicio y dónde obtuvo este información.',
                    '4C' => 'Concertar una entrevista para efectuar una demostración de inmediato.',
                    '1D' => 'Concertar una entrevista para calificar mejor al cliente'
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => 'Durante la entrevista, usted se da cuenta que hay otras personas que participan en la toma de decisión de las compras. Usted debería:',
                'options' => [
                    '1A' => 'Preguntar qué papel van a jugar esas personas.',
                    '4B' => 'Preguntar a su cliente que influencia tienen sobre las otras personas.',
                    '3C' => 'Intentar ver a todos los demás.',
                    '2D' => 'Presentar su producto y concertar una nueva entrevista para reunirse con los demás.'
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => 'Durante la entrevista, usted se da cuenta que el cliente ha comprado siempre a la empresa X y que está satisfecho con la calidad del producto y el servicio recibido. Usted deberìa:',
                'options' => [
                    '1A' => 'Preguntarle que le gusta más de X.',
                    '4B' => 'Decirle lo superior que es su producto con respecto al de ellos.',
                    '2C' => 'Preguntarle qué cambios haría en su relación con X.',
                    '3D' => 'Explicarle los problemas potenciales que existen cuando se compra un solo proveedor.'
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => 'Empezando su presentación el cliente dice: "Voy a escucharlo, pero ahora no estoy dispuesto a comprar nada,... no importa lo que me ofrezca". Usted debería:',
                'options' => [
                    '1A' => 'Decirle al cliente que usted no está allí para vender nada, sino para descubrir sus necesidades y saber si las puede satisfacer.',
                    '4B' => 'Seguir con su presentación con la esperanza de que cuando el cliente necesite el producto o esté preparado para comprar, sabrá de su existencia.',
                    '3C' => 'Decirle. "Hoy no voy a pedirle que compre nada".',
                    '2D' => 'Intentar descubrir por qué el cliente no está interesando en comprar nada.'
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => 'Un cliente potencial le dice que sólo dispone de 15 minutos para hacer su presentación. Usted necesita por lo menos 30 minutos. ¿Qué debería hacer?',
                'options' => [
                    '3A' => 'Preguntar cuándo podría volver.',
                    '1B' => 'Continuar con la calidicación del cliente potencial para descubrir si es necesario hacer una presentación.',
                    '2C' => 'Emplear ese tiempo para crear buena comunicación con el cliente potencial y olvidarse de hacer la presentación en ese momento.',
                    '4D' => 'Negarse a hacer la presentación argumentando que hacerla más corta sería contraproducente para su producto y para el cliente.'
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => 'Calificar un cliente potencial es necesario para:',
                'options' => [
                    '3A' => 'Determinar qué clientes le llevarán más o menos tiempo.',
                    '1B' => 'Decidir que producto o servicio recomendará',
                    '2C' => 'Comprender los motivos de compra del cliente.',
                    '4D' => 'Anticiparse a las objeciones que un cliente puede presentar.'
                ]
            ],

            /// -- clasificacion 7

            [
                'group' => 7,
                'type' => 'order',
                'question' => 'De todas las personas que participan en una relación inicial de ventas el más importante es:',
                'options' => [
                    '3A' => 'El usuario de su producto o servicio.',
                    '2B' => 'La persona que paga o quién toma la decisión.',
                    '1C' => 'El contacto interno.',
                    '4D' => 'El vendedor.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Al poco de iniciar su presentación, el cliente dice, "me gusta lo que veo, ¿cuánto cuesta?". Usted debería:',
                'options' => [
                    '4A' => 'Decirle el precio.',
                    '3B' => 'Preguntarle al cliente cuál es el precio que él espera',
                    '2C' => 'Preguntarle al cliente con qué presupuesto se esta manejando.',
                    '1D' => 'Decirle al cliente que usted hablará de precios una vez que le haya explicado los beneficios del producto.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Un cliente hace comentarios negativos acerca de su competencia, un proveedor cuyo producto está usando y con el cual está teniendo problemas. Usted debería:',
                'options' => [
                    '2A' => 'Estar de acuerdo con la experiencia del cliente y decirle por qué su producto es mejor.',
                    '3B' => 'Estar de acuerdo con la experiencia del cliente y decirle por qué usted también ha recibido los mismos comentarios de otras personas.',
                    '1C' => 'Decir: "entiendo que problemas como éste pueden ocurrir. Dejeme mostrarle cómo funciona nuestro producto"',
                    '4D' => 'Señalar problemas adicionales del producto de su competidor que él todavía no ha experimentado.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Durante una entrevista "cara a cara", preguntar al cliente que instalaciones tiene es un ejemplo de:',
                'options' => [
                    '1A' => 'Estar poco preparado para una entrevista de ventas.',
                    '3B' => 'Intentar capta información que le permita ofrecer una solución.',
                    '2C' => 'Parte del proceso de juntar información',
                    '4D' => 'Una buena manera de hacer que el cliente hable de sus problemas potenciales.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Usted llega temprano a una cita y le informan que no podrá reunirse con el cliente a la hora que habían acordado. Usted debería:',
                'options' => [
                    '1A' => 'Avisar a su próxima cita en su agenda, que estará demorado.',
                    '4B' => 'Ensayar mentamente su presentación.',
                    '3C' => 'Ponerse a conversar con la recepcionista.',
                    '2D' => 'Observar a su alrededor para obtener inforación clave.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Cuando un cliente dice: "Normalmente no tomamos decisiones en esta época del año", usted debería:',
                'options' => [
                    '1A' => 'Preguntar: "¿En qué epoca del año se toman las decisiones?".',
                    '2B' => 'Preguntar: "¿Qué factores influyen en la toma de decisiones?".',
                    '4C' => 'Determinar la época adecuada para establecer otra cita.',
                    '3D' => 'Hacer preguntas para reunir información adicional.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Usted ha identificado la principal necesidad del cliente con respecto a su producto. Lo invitan a volver y hacer una presentación a un grupo de personas. Usted debería:',
                'options' => [
                    '1A' => 'Determinar quiénes estarán presentres en la próxima reunión.',
                    '3B' => 'Pedir comentarios acerca de las expectativas de los otros miembros del comité badados en lo que usted ha presentado hasta ahora.',
                    '4C' => 'Repetir su presentación al grupo',
                    '2D' => 'Efectuar preguntas adicionales que le permitirán adecuar su presentación de acuerdo con las necesidades más importantes del cliente.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'La competencia obliga a los vendedores a:',
                'options' => [
                    '4A' => 'Conocer mejor a sus competidores.',
                    '3B' => 'Conocer al cliente y el mercado mejor.',
                    '2C' => 'Crear un valor más alto.',
                    '1D' => 'Conocer mejor sus producto, la competencia, y la necesidad de mejoras sus habilidades como vendedor.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Cuando mayor es la competencia que tiene un producto, mayor será la necesidad de que el vendedor:',
                'options' => [
                    '3A' => 'Aumente su conocimiento del producto.',
                    '4B' => 'Trabaje más.',
                    '1C' => 'Domine las técnicas de venta.',
                    '2D' => 'Se mantenga en contacto con los clientes.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Los clientes que dicen que tienen problemas de recursos económicos:',
                'options' => [
                    '3A' => 'Pueden estar usando como excusa para no comprarle.',
                    '2B' => 'Le están pidiendo una solución para financiar la compra.',
                    '1C' => 'Le están dando información sobre los términos bajo los que comprán.',
                    '4D' => 'Le están pidiendo que usted financie la compra.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Los clientes potenciales pueden tener miedo de:',
                'options' => [
                    '1A' => 'Tomar una decisión equivocada.',
                    '2B' => 'Hablar abiertamente y plantear sus vedaderas objeciones.',
                    '3C' => 'Decir que sí.',
                    '4D' => 'Tener un producto que los haga quedar mal.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Mantener registros precisos de ventas:',
                'options' => [
                    '2A' => 'Ayudar al vendedor a vender mejor.',
                    '1B' => 'Provee de una base para hecar seguimiento y reiterar ventas.',
                    '4C' => 'Es el valor para la gerencia.',
                    '3D' => 'Es necesario un mercado tan competitivo como el actual.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Los apoyos a la venta deberían usarse para:',
                'options' => [
                    '1A' => 'Establecer credibilidad.',
                    '3B' => 'Establecer información.',
                    '2C' => 'Presentar información',
                    '4D' => 'Crear una imagen favorable.'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Mientras busca nuevos negocios usted se encuentra con un cliente que le dice: "En el pasado hemos sido clientes de su empresa, pero los hemos dejado como proveedores". Usted debería:',
                'options' => [
                    '4A' => 'Preguntar: "¿Cuánto tiempo hace que ocurrió?".',
                    '1B' => 'Preguntar: "¿Cuál fue la razón para tomar esa decisón?".',
                    '2C' => 'Preguntar: "¿Está usted dispuesto a hablar sobre posibles caminos para reconstruir nuestra relación?".',
                    '2D' => 'Preguntar: "¿Si pudiéramos resolver los problemas asociados con el pasado, trabajaría con nosotros otra vez?".'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Entrando en la oficina de su posible cliente, inmediatamente le preguntan sobre le precio de su producto o servicio. Su respuesta sería:',
                'options' => [
                    '2A' => 'Preguntar al cliente qué nivel, columan o cantidad compraría.',
                    '3B' => 'Evitar cualquier discusión del precio hasta que haya creado un valor.',
                    '4C' => 'Preguntar al cliente cuándo querría que le fuese entregado.',
                    '1D' => 'Preguntar al cliente cómo es de importante el precio en su decisión.'
                ]
            ],

            /// -- clasificacion 5

            [
                'group' => 5,
                'type' => 'order',
                'question' => 'Durante la presentación, el cliente hace una afirmación sobre su producto que es claramente incorrecta. Debería:',
                'options' => [
                    '4A' => 'Decir: me gustaría aclarar las cosas.',
                    '2B' => 'Preguntar: "¿Donde obtuvo esa información?".',
                    '1C' => 'Afirmar que lo que él ha dicho es una confusión o malentendido bastante usual con respecto a su producto, y luego hacer la aclaración correspondiente.',
                    '3D' => 'Afirmar que su producto es tan sofisticado que pueden producirse ciertos malentendidos, y luego aclarar qué puede haber sucedido.'
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => 'Durante la presentación, el cliente continuamente le pregunra cuál es su "mejor precio". Usted debería:',
                'options' => [
                    '4A' => 'Preguntar: "¿Cuál debería ser nuestro precio?".',
                    '1B' => 'Decir: "Sólo podré darle el mejor precio cuando sepa su nuestro producto o servicio satisface sus necesidades".',
                    '3C' => 'Presentar su "mejor precio".',
                    '2D' => 'Solicitar información específica sobre las necesidades del cliente de manera de poder darle información precisa sobre el precio.'
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => 'Al final de su presentación, el cliente dice que no está seguro que lo que usted dice acerca de la efectividad de su producto sea cierto. Usted debería:',
                'options' => [
                    '4A' => 'Asegurarle que usted nunca miente.',
                    '2B' => 'Presentar cartas de referencia de usuarios satisfechos.',
                    '1C' => 'Hacer preguntas para descubrir más acerca de sus preocupaciones.',
                    '3D' => 'Continuar presentando los principales beneficios.'
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => 'Usted le da su precio al cliente y éste dice: “¡Su precio es demasiado alto!”. ¿Usted qué hace?',
                'options' => [
                    '1A' => 'Pide al cliente que le explique lo que quiere decir.',
                    '4B' => 'Ofrece descuentos.',
                    '2C' => 'Compara su valor con el de la competencia.',
                    '3D' => 'Pregunta: “¿Qué precio deberíamos tener para poder cerrar el trato?”.'
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => 'El bajar los precios hace que los clientes potenciales:',
                'options' => [
                    '3A' => 'Sospechen del vendedor.',
                    '2B' => 'Cuestionen la calidad del producto.',
                    '4C' => 'Consideren que el producto es obsoleto.',
                    '1D' => 'Se pregunten por qué al principio el precio era más alto.'
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => 'Usted ha efectuado su presentación a un comprador autorizado y ha recibido el apoyo de los expertos del área técnica. A pesar de esto, su comprador es desautorizado por un superior jerárquico que desea comprarle a otro proveedor. Usted debería:',
                'options' => [
                    '2A' => 'Intentar concertar una reunión con su cliente original y su superior.',
                    '3B' => 'Intentar ver directamente al superior jerárquico.',
                    '1C' => 'Intentar arreglar una reunión con el personal técnico, el cliente y el superior jerárquico.',
                    '4D' => 'Tratar de que el personal técnico avale la superioridad de su producto.'
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => 'Durante la presentación el cliente dice: “Estoy totalmente contento con mi actual proveedor”. Usted debería:',
                'options' => [
                    '4A' => 'Decirle al cliente que su producto es mucho mejor.',
                    '3B' => 'Señalar los beneficios de tener más de un proveedor.',
                    '1C' => 'Preguntar al cliente con qué está más contento.',
                    '2D' => 'Preguntar qué cambios realizaría a su actual proveedor si pudiera.'
                ]
            ],

            /// -- clasificacion 2

            [
                'group' => 2,
                'type' => 'order',
                'question' => 'Durante la presentación, Ud. descubre que el cliente está usando un producto de la competencia que usted sabe que tiene problemas de diseño. Usted debería:',
                'options' => [
                    '2A' => 'Preguntar al cliente qué le gusta del producto de la competencia.',
                    '1B' => 'Preguntar al cliente qué mejoras haría si pudiera cambiar el producto que está usando.',
                    '3C' => 'Hacer preguntas que reflejen los síntomas de los problemas de diseño.',
                    '4D' => 'Recalcar la superioridad de diseño de su producto.'
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => 'Al principio en su reunión con el cliente éste se muestra poco interesado y no dice nada. Es una situación incómoda para los dos. Usted debería:',
                'options' => [
                    '4A' => 'Empezar la presentación.',
                    '1B' => 'Hacer preguntas para interesar al cliente en la presentación.',
                    '3C' => 'Preguntar al cliente si tiene alguna preocupación.',
                    '2D' => 'Asegurarse de que ha informado adecuadamente al cliente acerca de quien es usted, a quién representa y por qué esta ahí .'
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => 'Cuando un cliente potencial pregunta algo que usted no sabe, usted debería:',
                'options' => [
                    '1A' => 'Admitir que no conoce la respuesta y decir que va a averiguarla.',
                    '2B' => 'Llamar a alguien que conozca la respuesta.',
                    '3C' => 'Revisar su documentación para encontrar la respuesta.',
                    '4D' => 'Preguntar al cliente qué importancia tiene esa respuesta para el proceso de compra.'
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => 'La primera impresión es importante en el proceso de ventas porque:',
                'options' => [
                    '1A' => 'El clima de confianza precede cualquier decisión de compra.',
                    '2B' => 'La gente compra a gente que le agrada.',
                    '4C' => 'Una primera impresión negativa hará que el cliente potencial no siga prestando atención a su presentación.',
                    '3D' => 'Las primeras impresiones son duraderas.'
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => 'Su cliente dice: “¿Cuándo podría ser entregado?”. Usted debería:',
                'options' => [
                    '2A' => 'Revisar y comprobar que se puede arreglar la entrega.',
                    '3B' => 'Continuar con la venta para darle más valor.',
                    '1C' => 'Preguntar: “¿Cuándo le gustaría que fuera?”.',
                    '4D' => 'Preguntar: “¿Por qué es tan importante para usted?”.'
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => 'Ha presentado su servicio y su cliente dice: “Quiero pensarlo”. Usted debería:',
                'options' => [
                    '2A' => 'Preguntar: “¿Qué es lo que quiere pensar?”.',
                    '3B' => 'Preguntar: “¿Cuánto tiempo necesita?”.',
                    '1C' => 'Decir: “Puedo entenderlo. ¿Qué quiere que le aclare?”.',
                    '4D' => 'Decir: “No importa cuánto tiempo lo piense, no hay mejor servicio a este precio en ningún sitio”.'
                ]
            ],

            /// -- clasificacion: 1

            [
                'group' => 1,
                'type' => 'order',
                'question' => 'Al acercarse a su cliente usted descubre que no es la persona que pueda tomar ninguna decisión formal. Usted debería:',
                'options' => [
                    '1A' => 'Continuar la venta con la intención de desarrollar un contacto interno.',
                    '2B' => 'Hacer su presentación para crear una necesidad y crear conciencia sobre su producto.',
                    '3C' => 'Averiguar quién puede tomar decisiones y concertar una entrevista con esa persona.',
                    '4D' => 'Hacer que el comprador invite a quien toma las decisiones a participar de la reunión que se está desarrollando.'
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => 'La primera vez que usted entrevista a un cliente potencial o trata con una empresa, usted debería:',
                'options' => [
                    '2A' => 'Hacer una serie de preguntas para conocer mejor a la organización.',
                    '1B' => 'Emplear tiempo para desarrollar confianza y buena comunicación con el cliente.',
                    '4C' => 'Emplear tiempo hablando sobre la posición de su compañía en el mercado.',
                    '3D' => 'Usar cartas de referencia para probar su credibilidad.'
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => 'Al intentar contactar telefónicamente al cliente, su secretaria le dice: “El Sr. Pérez no recibe llamadas telefónicas de vendedores”. Usted debería:',
                'options' => [
                    '4A' => 'Enviar una nota personal con material descriptivo e intentar que la información le llegue al cliente.',
                    '3B' => 'Intentar reunir información relativa a las actividades, personas conocidas e intereses del cliente.',
                    '2C' => 'Intentar crear un clima de confianza con la secretaria.',
                    '1D' => 'Preguntar a la secretaria cómo hace el señor Pérez para reunir información sobre nuevos productos o servicios.'
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => 'La secretaria del cliente le dice: “El señor Pérez no está interesado en nuevos productos en este momento”. Su reacción debería ser:',
                'options' => [
                    '3A' => 'Descubrir cuál es la agenda del cliente para la búsqueda de nuevos productos.',
                    '1B' => 'Descubrir por qué no está buscando nuevos productos.',
                    '2C' => 'Determinar si alguna otra persona de la compañía busca nuevos productos.',
                    '4D' => 'Hacer preguntas para ver si hay algún problema que se pueda resolver con sus productos.'
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => 'Usted envía material informativo a un cliente. Cuando llama para concertar una cita, el tópico inicial de su conversación debería ser:',
                'options' => [
                    '1A' => 'Confirmar si el cliente recibió y leyó la información.',
                    '3B' => 'Concertar la entrevista.',
                    '2C' => 'Determinar el nivel de interés que el cliente tiene en entrevistarlo.',
                    '4D' => 'Proporcionar al cliente la suficiente información para que decida entrevistarlo.'
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => 'Cuando los clientes potenciales se niegan a recibir sus llamados, usted debería:',
                'options' => [
                    '3A' => 'Continuar llamando hasta que lo atienda.',
                    '4B' => 'Dejar más mensajes.',
                    '1C' => 'Continuar trabajando con la persona que lo atienda hasta encontrar el momento adecuado para abordar a su cliente potencial.',
                    '2D' => 'Encontrar otra persona que pueda ayudarlo a contactarse con el cliente potencial.'
                ]
            ],

            /// -- clasificacion: 6

            [
                'group' => 6,
                'type' => 'order',
                'question' => 'Inmediatamente después de realizar una venta, usted debería:',
                'options' => [
                    '4A' => 'Solicitar referencias.',
                    '3B' => 'Intentar vender productos o servicios adicionales.',
                    '1C' => 'Felicitar al cliente y reafirmarlo en su decisión.',
                    '2D' => 'Comentarle al cliente como va a hacer el seguimiento.'
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'Usted y su cliente han estado suficiente tiempo hablando y el cliente dice: “Todo me parece bien”. Usted debería:',
                'options' => [
                    '4A' => 'Preguntar qué es lo que le parece mejor.',
                    '1B' => 'Preguntar qué tiene que hacer para comenzar el proceso.',
                    '3C' => 'Completar el pedido e irse.',
                    '2D' => 'Resumir los beneficios y cerrar la venta.'
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'A usted le queda poco tiempo, ha intentado cerrar la venta y el cliente dice: “Quiero pensarlo”. Usted debería:',
                'options' => [
                    '1A' => 'Preguntar al cliente qué es lo que desea pensar.',
                    '3B' => 'Resumir los principales beneficios y hacer al cierre dando al cliente razones para tomar una decisión ahora.',
                    '4C' => 'Intentar hacer el cierre creando un sentido de urgencia.',
                    '2D' => 'Preguntar al cliente cuánto tiempo necesita para pensar.'
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'Ha hecho su presentación y le ha pedido al cliente que compre. La respuesta es: “Quiero seguir mirando”. ¿Qué deberías hacer?',
                'options' => [
                    '2A' => 'Preguntarle qué más quiere ver y cuándo lo hará.',
                    '4B' => 'Resumir y cerrar otra vez.',
                    '3C' => 'Hablar sobre sus ventajas competitivas en el mercado.',
                    '1D' => 'Intentar descubrir cuáles son las verdaderas razones para que se produzca la demora.'
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'Usted le ha presentado al cliente todos los datos sobre su producto. Sin embargo, éste continua posponiendo la toma de la decisión. Usted debería:',
                'options' => [
                    '2A' => 'Tratar una por una las objeciones y luego cerrar la venta.',
                    '3B' => 'Seguir adelante e intentar cerrar la venta.',
                    '4C' => 'Parar su presentación y ofrecerle volver en otro momento.',
                    '1D' => 'Preguntar al cliente qué es lo que le hace vacilar en tomar la decisión.'
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'Su cliente dice: “Me gusta su servicio y me parece bueno; sin embargo, creo que debería chequearlo más”. Usted debería:',
                'options' => [
                    '2A' => 'Preguntar: “¿Qué quiere chequear?” Luego proceder a cerrar la venta.',
                    '4B' => 'Preguntar”: ¿Por qué quiere hacer eso?”.',
                    '1C' => 'Ofrecer ayuda al cliente revisando lo que él quiere aprender.',
                    '3D' => 'Decir: “Hay algunas cosas que siempre son lo mismo, no importa cuántas veces las revise”.'
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'Usted le pregunta al cliente: “¿Quién más participa en la decisión de compra?”. Él afirma que tomará la decisión solo. Luego, cuando lo incita a comprar, el cliente insiste en chequearlo con otras dos personas. Usted debería:',
                'options' => [
                    '4A' => 'Preguntar al cliente por qué inicialmente dijo que era capaz de tomar la decisión él solo.',
                    '2B' => 'Realizar la presentación a los otros que van a decidir.',
                    '3C' => 'Dejar que su cliente se reúna él sólo con las otras personas para presentarles su historia.',
                    '1D' => 'Preguntar sobre las otras personas de manera que pueda formular una nueva estrategia.'
                ]
            ],
        ];
    }

    protected function getROIPALQuestionsInEnglish()
    {
        return [
            [
                'group' => 4,
                'type' => 'order',
                'question' => "A salesman completes his presentation and is told to come back in a week. Upon his return, his potential client told him that he decided to do business with one of his competitors. What was his mistake?",
                'options' => [
                    '1A' => "He has not sufficiently demonstrated the value of his product or service",
                    '4B' => "He provided too much information causing confusion to the buyer who after, preferred to look elsewhere",
                    '3C' => "He has certainly sold according to the needs of the client but failed to close the sale because he did not reach a compromise with him.",
                    '2D' => "He couldn’t create an environment of trust with the customer."
                ]
            ],
            [
                'group' => 4,
                'type' => 'order',
                'question' => 'It has been 45 minutes that you are with a customer and at one point he tells you that "we have no time". You should:',
                'options' => [
                    '3A' => "Insist to close the sale.",
                    '2B' => "Request another appointment to return and finish your presentation.",
                    '4C' => "Ask the customer to give you 5 minutes more.",
                    '1D' => "Ask the customer if he would need additional information."
                ]
            ],
            [
                'group' => 4,
                'type' => 'order',
                'question' => "You arrive at the costumer's office and he receives you by saying: 'Let us see what you offer us'. You should:",
                'options' => [
                    '4A' => " show him what you have to offer.",
                    '3B' => "Ask him what he uses actually.",
                    '2C' => "Find out what are his real needs.",
                    '1D' => "Tell him who you are and why you are there."
                ]
            ],
            [
                'group' => 4,
                'type' => 'order',
                'question' => 'The customer tells you: "the price is right but I doubt the durability of your product" You should:',
                'options' => [
                    '4A' => "Show him concretely that your product is sustainable.",
                    '1B' => 'Find the source of his doubts and ask him directly: "What are you worried about the durability of the product"',
                    '2C' => "Ensuring that the customer is close to other clients who have had good experience with product sustainability.",
                    '3D' => 'Ask him: "What function does the sustainability of the product play in its decision-making?"'
                ]
            ],

            // -- clasificacion 3

            [
                'group' => 3,
                'type' => 'order',
                'question' => "After a preliminary discussion, the time to start presenting your sale, you should:",
                'options' => [
                    '4A' => "Starting to present the features and benefits of your product or service.",
                    '1B' => "Ask the customer about his need to make an analyze.",
                    '3C' => "Start telling the customer how your products can solve their problems.",
                    '2D' => "Making a series of questions that brings the conversation to your sales goal."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "During the interview, you realize that the person you are talking is newly assigned to the post and that the person don’t know clearly about the past or future necessities of his enterprise. You should:",
                'options' => [
                    '1A' => 'Ask him, "Who else is involved in this decision-making outside of him?"',
                    '4B' => 'Ask him, "How long does he think he will’ make this decision?',
                    '3C' => "Ask him to know if he has previously met with your competitors or with other sellers who offering products or services similar to yours.",
                    '2D' => "Offer him your collaboration to help him to have more knowledge faster."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "The customer tells you his need to investigate you and the enterprise you represent. You should:",
                'options' => [
                    '3A' => "Give him a list of your customers in which he can ask for your professional references.",
                    '2B' => "Offer him your help in finding information",
                    '1C' => "Ask him specifically what he would like to investigate.",
                    '4D' => "Offer a later call from satisfied customers."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "A client tells you he wants to talk to you because he believes you have exactly what he needs. Based on this comment, you should:",
                'options' => [
                    '2A' => "Find out why this customer believes your product is the product they need.",
                    '3B' => "Find out what he knows about the product or service in question and where he has held this information.",
                    '4C' => "Arrange an interview to give him a demonstration of your products right now.",
                    '1D' => "Arrange an interview to better qualify the client."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "During the interview, you realize there are other people involved in the decision-making process. You should:",
                'options' => [
                    '1A' => "Ask what the different roles of each person will be present.",
                    '4B' => "Ask your client what influence he has on his people.",
                    '3C' => "Try to see everyone else.",
                    '2D' => "Present your product and arrange a new interview to get together with everyone else."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "During the interview, you realize that the customer has always bought from enterprise X and he feel satisfy with the quality of the product and the service received, you should:",
                'options' => [
                    '1A' => "Ask him what he likes most about X.",
                    '4B' => "Tell him that your product is much better than others.",
                    '2C' => "Ask him what changes he would make in his relationship with X.",
                    '3D' => "Explain the potential problems that exist when buying from one buyer."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "At the beginning of your presentation, the customer tells you that he will listen to you, but that at the moment he is not able to buy your product... no matter what you have to offer him. You should:",
                'options' => [
                    '1A' => "Tell your customer that you did not come to sell but to find out about their different needs and how you can meet them.",
                    '4B' => "Continue your presentation with the hope that when the customer is ready and will need the product, he will remember your existence.",
                    '3C' => 'Tell him: "Today I will not ask you to buy me anything"',
                    '2D' => "Try to find out why the customer is not interested in buying the product."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "A potential customer tells you he only has 15 minutes for you to make your presentation or you need at least 30 minutes, what should you do?",
                'options' => [
                    '3A' => "Ask him when can you come back?.",
                    '1B' => "Continue to qualify the potential customer to find out if it is necessary to make your presentation.",
                    '2C' => "Use this time to create good communication with the potential customer and forget to make the presentation in the moment.",
                    '4D' => "Refuse to make the presentation arguing making it shorter will be counterproductive for your product and for it as a customer."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "Qualify a potential customer is necessary for:",
                'options' => [
                    '3A' => "Determine the number of times you will take the client.",
                    '1B' => "Decide on the product and service you recommend.",
                    '2C' => "Understand what the customer's buying motives are.",
                    '4D' => "Anticipate the different objections that could present the customer."
                ]
            ],

            /// -- clasificacion 7

            [
                'group' => 7,
                'type' => 'order',
                'question' => "Of all the people who participate in a start of the most important sales relationship is:",
                'options' => [
                    '3A' => "The user of your product or service.",
                    '2B' => "The person who pays and who makes the decision.",
                    '1C' => "Hhe intern contact.",
                    '4D' => "The saller."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Shortly after starting your presentation, the customer tells you that he likes what you are presenting and what the price is. You should:",
                'options' => [
                    '4A' => "Tell him the price.",
                    '3B' => "Ask the customer what price he hopes to get.",
                    '2C' => "Ask the client what his budget is.",
                    '1D' => "Ask the customer that you will tell him prices once you explain the benefits of the product."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "A customer makes negative comments about the skill using one of their products with which they have problems. You should:",
                'options' => [
                    '2A' => "Agree with the customer and their experience and tell them why your product is the best.",
                    '3B' => "Agree with the client and their experience and tell them why you received the same comments from other people.",
                    '1C' => 'Tell him: "That kind of problems can happen. Let me show you how our product works.',
                    '4D' => "Report additional problems with your competitor's product that the customer has not yet experienced."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "During a face-to-face interview, ask the customer what kind of facilities they maintain, is an example of:",
                'options' => [
                    '1A' => "Being unprepared for a sales glimpse.",
                    '3B' => "Try to capture the information that will allow you to offer a solution.",
                    '2C' => "Part of the process of assembling information.",
                    '4D' => "A good way to get the customer to talk about their potential problems."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "You arrive at an appointment on time and are informed that you can’t meet with the client as agreed, you should:",
                'options' => [
                    '1A' => "Notify at your next appointment, you will arrive late.",
                    '4B' => "Learning mentally your presentation.",
                    '3C' => "Have a conversation with the receptionist.",
                    '2D' => "Observe your surroundings for the purpose of obtaining key information."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'When a customer says, "Normally we do not make a decision at this time of the year. You should:"',
                'options' => [
                    '1A' => 'Ask: "At what time of the year do they make decisions?"',
                    '2B' => 'Ask: "What are the facts that can influence decision-making?"',
                    '4C' => "Determine the appropriate time to make another appointment.",
                    '3D' => "Ask questions to gather additional information."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "You have been able to identify the customer's main need in relation to the product, and he invites you to make a presentation to a group of people: You should:",
                'options' => [
                    '1A' => "To determine who will be present at the next meeting.",
                    '3B' => "Ask him to comment on the expectations of other committee members based on what you have presented.",
                    '4C' => "Repeat your presentation to the group.",
                    '2D' => "Ask additional questions that will allow you to tailor your presentation to the customer's most important needs."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Competition forces sellers:",
                'options' => [
                    '4A' => "To know better the skill concurrence.",
                    '3B' => "To Know the customer and the market.",
                    '2C' => "To Create added value.",
                    '1D' => "To Know her product better, the skill and the need to improve your abilities as a salesman."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "More superior is the product of the competition, more is the need of the seller to:",
                'options' => [
                    '3A' => "Deepen your knowledge on the product.",
                    '4B' => "work a lot more.",
                    '1C' => "Master sales techniques.",
                    '2D' => "Maintain a relationship with clients."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Customers who say they have economic resources problems:",
                'options' => [
                    '3A' => "Can pretend that to not buy your products.",
                    '2B' => "Ask you to offer them a solution to finance the purchase.",
                    '1C' => "Give you information on the conditions of purchase.",
                    '4D' => "Ask for your financial assistance in the purchase."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Potential customers may be afraid of:",
                'options' => [
                    '1A' => "To err in decision-making.",
                    '2B' => "Speak openly and expose their true objections.",
                    '3C' => "To say yes.",
                    '4D' => "Get a product that could be bad."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Maintain a precise record of sales:",
                'options' => [
                    '2A' => "Help the seller to sell better.",
                    '1B' => "Provides a tracking database to reiterate sales.",
                    '4C' => "Is added value in terms of management.",
                    '3D' => "In currently necessary in such a competitive market."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Supports for sale should be used for:",
                'options' => [
                    '1A' => "Increase interest.",
                    '3B' => "Establish credibility.",
                    '2C' => "Present information.",
                    '4D' => "Create a favorable image."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "While you are looking for a new customer, you meet a customer who lets you know that he has been your customer in the past but left you as a supplier. You should:",
                'options' => [
                    '4A' => 'Ask him, "Since when did this happen?"',
                    '1B' => 'Ask him: "What was the reason for this decision?',
                    '3C' => 'Ask Him: "Would you be willing to talk about different existential possibilities to rebuild our relationship?',
                    '2D' => 'Ask him, "If we could solve the problems associated with the past, would you work with us once again?"'
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => 'Entering in the office of your "next" customer, he immediately asks you what the prices of your products or services are. Your best answer would be:',
                'options' => [
                    '2A' => "Ask the customer what the level or quantity he would buy.",
                    '3B' => "Avoid price discussion without creating value.",
                    '4C' => "Ask the customer when he wants the delivery to be done.",
                    '1D' => "Ask the client how important the price is in making a decision."
                ]
            ],

            /// -- clasificacion 5

            [
                'group' => 5,
                'type' => 'order',
                'question' => "During the presentation, the customer clearly states an incorrect statement about your product. You should:",
                'options' => [
                    '4A' => 'Tell him: "I would like to clarify things"',
                    '2B' => 'Ask him, "Where did you get this information?"',
                    '1C' => "Affirm that what he has said is a fairly usual confusion or misunderstanding regarding his product, and then make the corresponding clarification.",
                    '3D' => 'Affirm that your product is so sophisticated that some misunderstandings can occur after clarifying why it is this misunderstanding.'               ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => 'During the presentation, the customer continues to ask you what is "your best price". You should:',
                'options' => [
                    '4A' => 'Ask him, "What should be our price?"',
                    '1B' => 'Tell him: "You can give him your best price only knowing that your products or services can satisfies his needs"',
                    '3C' => 'Present your "best price".',
                    '2D' => "Ask for specific information about the customer's needs to give him precise information about the product."
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => "At the end of your presentation, the customer says, he is not sure what you say about the effectiveness of your product is true. You should:",
                'options' => [
                    '4A' => "Say him you never lie.",
                    '2B' => "Submit letters of reference from satisfied users.",
                    '1C' => "Ask more questions to learn more about her concerns.",
                    '3D' => "Continue to present the main benefits of your products."
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => "You offer your price to the customer and he tells you your cost is too high, what would you do?",
                'options' => [
                    '1A' => "Ask the client to explain what he means.",
                    '4B' => "Offer him discounts.",
                    '2C' => "Make a comparison of your prices with the competition’s prices.",
                    '3D' => 'Ask him, "What price should we have to close this sale with you?'
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => "A drop-in price, makes that potential customers:",
                'options' => [
                    '3A' => "Suspect the seller.",
                    '2B' => "Ask about the quality of the product.",
                    '4C' => "Consider that the product is obsolete.",
                    '1D' => "Wonder why the price was high initially."
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => 'You made your presentation to an authorized buyer and you received support from the technical department experts. Even so, your buyer has been "unauthorized" by a supervisor who wants to buy from another supplier. You should:',
                'options' => [
                    '2A' => "Try to have a meeting with your client her supervisor.",
                    '3B' => "Try to see the supervisor directly.",
                    '1C' => "Try to have a meeting with the technical staff, the client and the line manager.",
                    '4D' => "Ensure that technical personnel are superior to your product."
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => 'During the presentation the client tells you: "I am satisfied with my current supplier. You should:"',
                'options' => [
                    '4A' => "Tell the customer your product is much better.",
                    '3B' => "Report the benefits of having more suppliers.",
                    '1C' => "Ask the customer who satisfies him the most.",
                    '2D' => "Ask him what he would changes with his current supplier?."
                ]
            ],
  
            /// -- clasificacion 2

            [
                'group' => 2,
                'type' => 'order',
                'question' => "During the presentation, you discover that the customer is using a competitor's product that you know has design problems. You should:",
                'options' => [
                    '2A' => "Ask the customer what he likes about the product of the competition.",
                    '1B' => "Ask the customer what he would improve if he could, in the product he is currently using.",
                    '3C' => "Ask questions that reflect the different defects present in your competitor's product.",
                    '4D' => "Highlight the quality of your product."
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => "At the beginning of the meeting with a client, the one not shows interest and does not say anything, an embarrassing situation. You should:",
                'options' => [
                    '4A' => "Start the presentation.",
                    '1B' => "Ask the questions to attract the customer to the presentation.",
                    '3C' => "Ask the customer if he is concerned about anything.",
                    '2D' => "Ensure that the client is properly informed about your identity, the company you represent and the why you are there."
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => "When a potential customer asks a question that you do not have an answer to, you should:",
                'options' => [
                    '1A' => "Admit that you do not know the answer but that you will investigate.",
                    '2B' => "Call someone who knows the answer.",
                    '3C' => "Review your documents to find an answer.",
                    '4D' => "Ask the customer how important this answer is in the sales process."
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => "The first impression is important in the sales process why:",
                'options' => [
                    '1A' => "The climate of trust precedes any purchase decision.",
                    '2B' => "People buy with people they like.",
                    '4C' => "A negative first impression will prevent the potential customer from paying attention to your presentation.",
                    '3D' => "First impressions are durable."
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => 'Your client says, "When can it be delivered?" You should:',
                'options' => [
                    '2A' => "Review and verify that delivery can be made.",
                    '3B' => "Continue the sale to give it more value.",
                    '1C' => 'Ask, "When would you like it to be done?"',
                    '4D' => 'Ask: "Why it’s so important to you?"'
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => 'You have presented your product or service and your customer says, "I want to think about it". You should:',
                'options' => [
                    '2A' => 'Ask: "What do you want to think about?"',
                    '3B' => 'Ask: "How long do you need?"',
                    '1C' => 'Tell Him: "I can understand, what do you want to clarify?"',
                    '4D' => 'Tell Him: "No matter how long you think, there is no better service at this price anywhere."'
                ]
            ],

            /// -- clasificacion: 1

            [
                'group' => 1,
                'type' => 'order',
                'question' => "By getting closer to your client, you discover he’s not the person who can make a formal decision. You should:",
                'options' => [
                    '1A' => "Continue the sale with the intention of developing an internal contact.",
                    '2B' => "Make your presentation to create any need and publicize your product.",
                    '3C' => "Determine who can make decisions and arrange an interview with this person.",
                    '4D' => "Ask the buyer to invite the decision maker to participate in your meeting."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => "The first time you interview a potential client or do business with a company, you should:",
                'options' => [
                    '2A' => "Ask a series of questions to get to know the organization better.",
                    '1B' => "Use your time to develop trust and good communication with the customer.",
                    '4C' => "Take the time to talk about your company's position in the market.",
                    '3D' => "Use reference letters to prove your credibility."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => 'When you try to call the customer, his secretary tells you, Mr. Pérez don’t receive phone calls from suppliers. "You should:',
                'options' => [
                    '4A' => "Send a personal note with descriptive material and try to convey the information to the customer.",
                    '3B' => "Try to collect information about her activities, the known people and the interests of the client.",
                    '2C' => "Try to create a trust climate with the secretary.",
                    '1D' => "Ask the secretary how Mr. Pérez collects information on new products or services."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => "The customer's secretary tells you, 'Mr. Pérez is not interested in new products at the moment.' Your reaction should be:",
                'options' => [
                    '3A' => "Find out what is the customer's agenda to finding new products.",
                    '1B' => "Find out why he’s not looking for new products.",
                    '2C' => "Determine if someone else in the company is looking for new products.",
                    '4D' => "Ask questions to know if your product can resolve any problem in the company."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => "You send information material to a client. When you call to make an appointment, the initial topic of your conversation should be:",
                'options' => [
                    '1A' => "Confirm if the customer has received and read the information.",
                    '3B' => "Organize a meeting.",
                    '2C' => "Determine the interest client's level in the meeting time.",
                    '4D' => "Provide the client enough information to decide by herself to have an interview with you."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => "When a potential customer refuse to receive your calls, you must:",
                'options' => [
                    '3A' => "Continue calling until someone receives your call phone.",
                    '4B' => "Leave more messages.",
                    '1C' => "Continue working with the person who received you until you find the right time to contact your potential client.",
                    '2D' => "Find another person who can help you contact the potential client."
                ]
            ],

            /// -- clasificacion: 6

            [
                'group' => 6,
                'type' => 'order',
                'question' => "Immediately after making a sale, you should::",
                'options' => [
                    '4A' => "Ask for references.",
                    '3B' => "Try to sale an additional products or services.",
                    '1C' => "Congratulate the customer and reaffirm him in her decision making.",
                    '2D' => "Tell the client you will follow up him and how you think make it."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'You and your customer have been talking for a long time and the client tells you: "Everything looks fine". You should:',
                'options' => [
                    '4A' => "Ask him what seems best to him.",
                    '1B' => "Ask him what he needs to do to start the process.",
                    '3C' => "Take the order and leave.",
                    '2D' => "Summarize the benefits and close the sale."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'You only have a little time and you tried to close the sale and the customer replied: "I want to think about it". You should:',
                'options' => [
                    '1A' => "Ask the customer what they would like to think about.",
                    '3B' => "Summarize the main benefits and close the sale giving the customer different reasons to make decision now.",
                    '4C' => "Try to close the sale creating an emergency.",
                    '2D' => "Ask the customer how much time he will take to think."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'You make your presentation and ask the customer to buy your product and it responds to you: "I want to continue to search". What should you do?',
                'options' => [
                    '2A' => "Ask Him what else he wants to see and when he will.",
                    '4B' => "Summarize your presentation and close.",
                    '3C' => "Talk about your competitive advantages in the market.",
                    '1D' => "Try to find out what are the real reasons who cause her hesitation in her decision-making."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => "You presented all the information to your customer about your product. However, he still undecided on the purchase of your product. You should:",
                'options' => [
                    '2A' => "Treat one by one his objections and then close the sale.",
                    '3B' => "Continue forward and try to close the sale.",
                    '4C' => "Stop your presentation and suggest that you come back later.",
                    '1D' => "Ask the client what makes him hesitate in her decision-making."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => "Your client tells you that he likes your service and that it seems very good to him; However, he thinks he should check it better. You should:",
                'options' => [
                    '2A' => 'Ask him, "what would he want to check?" And proceed to close the sale.',
                    '4B' => "Why do you want to do this?",
                    '1C' => "Offer help to the client by reviewing what he wants to learn.",
                    '3D' => "Tell him that some things will always be the same regardless of the number of times he wants to revisit them."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => 'You ask the customer: "Who else participate in the decision making on the purchase of your product?" He tells you that he will make this decision alone. Then when you offer to buy, the one insists to check the sale with two other people. You should:',
                'options' => [
                    '4A' => "Ask the customer why at the beginning he said he would be the only one to decide on the purchase.",
                    '2B' => "Make the presentation to others who will decide too.",
                    '3C' => "Let your client alone meet other people for them present his story.",
                    '1D' => "Inform yourself about the other two people in order to formulate a new sales strategy."
                ]
            ],
        ];
    }

    protected function getROIPALQuestionsInFrench()
    {
        return [

            /// -- clasificacion 4

            [
                'group' => 4,
                'type' => 'order',
                'question' => "Vous avez terminé votre présentation et vous dit de revenir dans une semaine. Lors de cette nouvelle réunion, votre client potentiel vous informe qu’il a décidé d’acheter à concurrent. Quelle a été votre erreur ?",
                'options' => [
                    '1A' => "Vous n’avez pas suffisamment démontré la valeur de votre produit ou service.",
                    '4B' => "Vous avez fourni trop d’information et confondu l’acheteur, ce qui l’a poussé à chercher ailleurs.",
                    '3C' => "Vous avez vendu en fonction des besoins du client mais vous n’avez pas réussi à conclure ou obtenir l’engagement client. ",
                    '2D' => "Vous n’avez pas su créer un climat de confiance. "
                ]
            ],
            [
                'group' => 4,
                'type' => 'order',
                'question' => "Cela fait 45 minutes que vous êtes avec un client et il vous dit “nous n’avons plus de temps”. Que devriez-vous faire?:",
                'options' => [
                    '3A' => "Insister pour conclure la vente.",
                    '2B' => "Demander un autre rendez-vous pour revenir et terminer votre présentation.",
                    '4C' => "Demander au client de vous accorder 5 minutes de plus",
                    '1D' => "Demander au client s’il a besoin d’information supplémentaire"
                ]
            ],
            [
                'group' => 4,
                'type' => 'order',
                'question' => "Vous arrivez au bureau du client et il vous reçoit en vous disant: “Montrez-moi ce que vous avez”. Que devriez-vous faire? .",
                'options' => [
                    '4A' => "Lui montrer ce que vous avez à lui offrir.",
                    '3B' => "Lui demander ce qu’il utilise actuellement.",
                    '2C' => "Découvrir s’il a un besoin réel.",
                    '1D' => "Lui dire qui vous êtes et pourquoi vous êtes là."
                ]
            ],
            [
                'group' => 4,
                'type' => 'order',
                'question' => "Le client vous dit: “le prix es juste mais je doute de la durabilité de votre produit” Vous devriez:",
                'options' => [
                    '4A' => "Démontrer concrètement au client que votre produit est durable.",
                    '1B' => "Chercher l’origine de ses doutes et lui demander directement : “qu’est ce qui vous préoccupe exactement par rapport à la durabilité ?”",
                    '2C' => "Faire en sorte que le client connaisse d’autres clients qui ont eu une bonne expérience concernant la durabilité du produit.",
                    '3D' => "Lui demander : “Quelle fonction joue la durabilité du produit dans votre prise de décision ?” "
                ]
            ],

            // -- clasificacion 3

            [
                'group' => 3,
                'type' => 'order',
                'question' => "Après une discussion préliminaire, c’est le moment de commencer votre présentation de vente. Vous devriez :",
                'options' => [
                    '4A' => "Commencer par présenter les caractéristiques et bénéfices de votre produit ou service.",
                    '1B' => "Commencer à poser des questions au client dans le but faire une analyse de ses besoins.",
                    '3C' => "Commencer par dire au client comment vos produits peuvent résoudre ses problèmes.",
                    '2D' => "Effectuer une série de questions qui amène la conversation à vos objectifs de vente."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "Durant la réunion, vous vous rendez compte que votre interlocuteur est nouveau dans ce poste et qu’il ne sait pas exactement quels sont les besoins passés ou futurs. Vous devriez :",
                'options' => [
                    '1A' => "Lui demander : “Qui d’autre participera dans cette prise de décision?”",
                    '4B' => "Lui demander : “Combien de temps estimez-vous pour prendre la décision?”",
                    '3C' => "Poser des questions pour découvrir s’il s’est aussi réuni avec des concurrents ou avec vendeurs offrant des produits ou services similaires aux vôtres",
                    '2D' => "Lui proposer de collaborer avec lui et de l’aider à acquérir de l’information plus rapidement."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "Le client vous fait savoir son besoin de faire des recherches sur vous et l’entreprise quevous représentez. Vous devriez :",
                'options' => [
                    '3A' => "Lui fournir une liste de clients à qui il pourra demander des références.",
                    '2B' => "Lui offrir votre aide pour chercher l’information.",
                    '1C' => "Lui demander spécifiquement ce qu’il aimerait savoir.",
                    '4D' => "Lui offrir un appel ultérieur de la part de clients satisfaits."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "Un client vous dit qu’il veut discuter avec vous parce qu’il croit que “vous avez exactement ce qu’il recherche”. En vous basant sur ce commentaire, vous devriez :",
                'options' => [
                    '2A' => "Chercher pourquoi ce client pense que votre produit est ce dont il a besoin.",
                    '3B' => "Découvrir ce qu’il sait sur votre produit ou le service en question et où il a obtenu ces informations.",
                    '4C' => "Organiser une réunion pour lui faire une démonstration immédiatement.",
                    '1D' => "Organisez une réunion pour mieux qualifier le client"
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "Durant la réunion, vous vous rendez compte que d’autres personnes participent à la prise de décision concernant achats.  Vous devriez ",
                'options' => [
                    '1A' => "Demander quel rôle vont jouer ces personnes.",
                    '4B' => "Demander à votre client quelle influence il a sur ces personnes",
                    '3C' => "Tenter de rencontrer tout le monde.",
                    '2D' => "Présenter votre produit et organiser une nouvelle réunion pour vous connaître les autres"
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "Durant la réunion, vous vous rendez compte que le client a toujours acheté à l’entreprise X et qu’il est satisfait de la qualité du produit et du service reçu. Vous devriez :",
                'options' => [
                    '1A' => "Lui demander ce qu’il aime le plus de X",
                    '4B' => "Lui dire que votre produit est supérieur à celui des autres.",
                    '2C' => "Lui demander quels changements souhaiterait-il dans sa relation avec X.",
                    '3D' => "Lui expliquer les problèmes potentiels qui existent lorsqu’on achète à un seul fournisseur."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "Au début de votre présentation, le client vous dit “je vais vous écouter mais pour l’instant je ne compte rien acheter, … peut importe ce que vous offrez.” Vous devriez: ",
                'options' => [
                    '1A' => "Dire à votre client que vous n’êtes pas venu pour vendre mais pour connaître ses différents besoins et savoir si vous pouvez les satisfaire.",
                    '4B' => "Continuer votre présentation en espérant que le client se souviendra de vous lorsqu’il sera prêt et aura besoin du produit.",
                    '3C' => "Lui dire : “Aujourd’hui je ne vais pas vous demander d'acheter quoi que ce soit”",
                    '2D' => "Essayez de savoir pourquoi le client n'est pas intéressé à acheter quoi que ce soit."
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "Un client potentiel vous dit qu’il ne dispose que de 15 minutes pour vous écouter or vous avez besoin d’au moins de 30 minutes pour votre présentation. Que devriez-vous faire ?",
                'options' => [
                    '3A' => "Lui demander quand pourriez-vous revenir",
                    '1B' => "Continuer de qualifier le prospect pour découvrir s’il est nécessaire de faire une présentation. ",
                    '2C' => "Utiliser ce temps pour créer une bonne communication avec le client potentiel et laisser de côté la présentation pour le moment. ",
                    '4D' => "Refuser de faire la présentation en faisant valoir que la raccourcir serait contre-productif autant pour votre produit que pour le client"
                ]
            ],
            [
                'group' => 3,
                'type' => 'order',
                'question' => "Qualifier un client potentiel est nécessaire pour :",
                'options' => [
                    '3A' => "Déterminer quels clients vont prendront plus ou moins de temps",
                    '1B' => "Décider quel produit ou service que vous lui recommanderez.",
                    '2C' => "Comprendre quels sont les motifs d’achat du client.",
                    '4D' => "Anticiper les différentes objections que le client pourrait présenter"
                ]
            ],

            /// -- clasificacion 7

            [
                'group' => 7,
                'type' => 'order',
                'question' => "De toutes les personnes qui participent à une relation initiale de ventes, la plus importante est:",
                'options' => [
                    '3A' => "L’utilisateur de votre produit ou service.",
                    '2B' => "La personne qui paie ou qui prend la décision.",
                    '1C' => "Le contact interne.",
                    '4D' => "Le vendeur."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Peu de temps après avoir commencé votre présentation, le client vous dit “j’aime ce que vous proposez, quel est le prix? Vous devriez :",
                'options' => [
                    '4A' => "Lui dire le prix.",
                    '3B' => "Demandez au client quel est le prix qu’il attend.",
                    '2C' => "Demander au client quel est son budget.",
                    '1D' => "Dire au client que vous lui parlerez des prix une fois que vous ayez expliqué les bénéfices du produit. "
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Un client fait des commentaires négatifs sur la concurrence, un fournisseur dont il utilise le produit et avec lequel il a des problèmes. Vous devriez :",
                'options' => [
                    '2A' => "Être d’accord avec l’expérience du client et lui dire pourquoi votre produit est mieux.",
                    '3B' => "Être d’accord avec l’expérience du client et lui dire pourquoi vous avez reçu les mêmes commentaires de la part d'autres personnes. ",
                    '1C' => "Dire : “Je comprends que ce genre de problèmes peuvent arriver. Laissez-moi, vous montrer comment fonctionne notre produit”.",
                    '4D' => "Signaler d’autres problèmes du produit de votre concurrent que le client n’a pas encore rencontré."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Durant une réunion en “face à face”, demander au client quel type d’installations il a est un exemple :",
                'options' => [
                    '1A' => "Du fait d’être peu préparé pour une réunion de vente.",
                    '3B' => "De tenter de capter de l’information qui vous permettra de lui offrir une solution.",
                    '2C' => "Du processus de réunion d’information.",
                    '4D' => "D’une bonne manière d’inciter le client à parler de ses problèmes potentiels."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Vous arrivez tôt à un rendez-vous et on vous informe que vous ne pourrez vous réunir avec le client à l’heure convenue. Vous devriez",
                'options' => [
                    '1A' => "Informer à votre rendez-vous suivant que vous arriverez en retard.",
                    '4B' => "Répéter mentalement votre présentation.",
                    '3C' => "Discuter avec la réceptionniste.",
                    '2D' => "Observer autour de vous afin d’obtenir des informations clés."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Quand un cliente dit: “Normalement nous ne prenons pas de décision à cette époque de l’année. Vous devriez:",
                'options' => [
                    '1A' => "Demander: “À quelle époque de l’année prenez-vous ce genre de décision?”",
                    '2B' => "Demander: “Quels facteurs sont importants pour la prise de décision?”",
                    '4C' => "Déterminer l’époque adéquate pour organiser un nouveau rendez-vous",
                    '3D' => "Poser des questions permettant de réunir des informations additionnelles."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Vous avez pu identifier le besoin principal du client par rapport à votre produit, et il vous invite à revenir faire la présentation à un groupe de personnes: Vous devriez",
                'options' => [
                    '1A' => "Déterminer qui sera présent lors de la prochaine réunion.",
                    '3B' => "Demander quelles sont les attentes des autres membres du comité par rapport à que vous avez presenté jusqu’a maintenant.",
                    '4C' => "Répéter exactement votre présentation au groupe.",
                    '2D' => "Poser des questions additionnelles qui vous permettront d’adaptez votre présentation aux besoins les plus importants du client."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "La concurrence oblige les vendeurs à",
                'options' => [
                    '4A' => "Connaître mieux les acteurs de la concurrence.",
                    '3B' => "Connaître mieux le client et le marché.",
                    '2C' => "Créer une valeur ajoutée",
                    '1D' => "Connaître mieux leur produit, la concurrence et le besoin d’améliorer ses compétences en tant que vendeur. "
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Plus le produit a de la concurrence, plus le vendeur a besoin :",
                'options' => [
                    '3A' => "D’approfondir ses connaissances sur le produit.",
                    '4B' => "De travailler beaucoup plus.",
                    '1C' => "De dominer les techniques de vente.",
                    '2D' => "De garder le contact avec les clients."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Les clients qui disent qu’ils ont des problèmes de budget:",
                'options' => [
                    '3A' => "Peuvent l’utiliser comme une excuse pour ne pas acheter chez vous.",
                    '2B' => "Vous demandent de leur proposer une solution pour financer l’achat.",
                    '1C' => "Vous donnent de l’information sur les conditions selon lesquelles ils achèteront.",
                    '4D' => "Vous demandent de financer l’achat"
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Les clients potentiels peuvent avoir peur de:",
                'options' => [
                    '1A' => "Prendre une mauvaise décision.",
                    '2B' => "Parler ouvertement y exposer leurs vraies objections.",
                    '3C' => "Dire oui.",
                    '4D' => "Avoir un produit qui pourrait leur donner une mauvaise image."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Maintenir un contrôle précis des ventes:",
                'options' => [
                    '2A' => "Aide le vendeur à mieux vendre.",
                    '1B' => "Fournit une base de suivi afin de réitérer les ventes.",
                    '4C' => "Est de grande valeur pour la gestion",
                    '3D' => "Est nécessaire dans un marché si compétitif."
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Les aides à la vente devraient être utilisés pour:",
                'options' => [
                    '1A' => "Susciter l’intérêt",
                    '3B' => "Donner de la crédibilité",
                    '2C' => "Présenter l’information",
                    '4D' => "Créer une image favorable"
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "Lors de votre prospection, vous rencontrez un client qui vous fait savoir qu’il a été client de votre entreprise dans le passé mais qu’il vous a laissé comme fournisseur. Vous devriez :",
                'options' => [
                    '4A' => "Lui demander : “Il y a combien de temps?”",
                    '1B' => "Lui demander : “Quelle a été la raison qui a poussé cette décision ?",
                    '2C' => "Lui demander : “Seriez-vous prêt à discuter des différents chemins pour reconstruire notre relation ?”",
                    '2D' => "Lui demander : “Si nous pourrions résoudre les problèmes du passé, travailleriez-vous avec nous à nouveau?”"
                ]
            ],
            [
                'group' => 7,
                'type' => 'order',
                'question' => "En entrant dans le bureau d’un client potentiel, il vous demande immédiatement quels sont les prix de vos produits ou services. Votre meilleure réponse serait de:",
                'options' => [
                    '2A' => "Demander au client à quel niveau, volume ou quantité il achèterait.",
                    '3B' => "Éviter toute discussion sur le prix avant d’avoir créé de la valeur.",
                    '4C' => "Demander au client quand il souhaiterait que la livraison soit faite.",
                    '1D' => "Demander au client quelle est l’importance du prix dans sa prise de décision."
                ]
            ],

            /// -- clasificacion 5

            [
                'group' => 5,
                'type' => 'order',
                'question' => "Durant la présentation, le client énonce une affirmation clairement incorrecte sur votre produit. Vous devriez:",
                'options' => [
                    '4A' => "Lui dire : “J’aimerais clarifier les choses”.",
                    '2B' => "Lui demander : “Où avez-vous obtenu cette information ?”",
                    '1C' => "Affirmer que ce qu’il a dit est une confusion ou malentendu fréquent par rapport au produit puis clarifier les choses.",
                    '3D' => "Affirmer que votre produit est si sophistiqué que certains malentendus peuvent survenir, puis expliquer pourquoi."
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => "Durant la présentation, le client continue de vous demander quel est votre “meilleur prix”. Vous devriez :",
                'options' => [
                    '4A' => "Lui demander : “Quel devrait être notre prix ?”",
                    '1B' => "Lui dire : “Je pourrais vous donner le meilleur prix seulement lorsque vous saurez que notre produit/service satisfait vos besoins”",
                    '3C' => "Présenter votre “meilleur prix”.",
                    '2D' => "Demander des informations spécifiques sur les besoins du client afin de pouvoir lui donner des informations précises sur le prix."
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => "A la fin de votre présentation, le client dit qu’il n’est pas sûr que ce vous dites sur l’efficacité de votre produit soit vrai. Vous devriez:",
                'options' => [
                    '4A' => "Lui assurer que vous ne mentez jamais.",
                    '2B' => "Présenter des lettres de recommandation d'utilisateurs satisfaits.",
                    '1C' => "Posez des questions pour en savoir plus sur ses préoccupations.",
                    '3D' => "Continuer à présenter les principaux avantages de votre produits"
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => "Vous offrez votre prix au client et il vous dit “C’est trop cher!”. Que faites-vous ?",
                'options' => [
                    '1A' => "Vous demandez au client qu’il vous explique ce qu’il entend par là.",
                    '4B' => "Vous lui offrez une remise sur le prix.",
                    '2C' => "Vous comparez sa valeur avec celle de la concurrence.",
                    '3D' => "Vous demandez : “Quels prix devrions-nous vous proposer pour pouvoir faire affaire avec vous ?” "
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => "Baisser les prix peut provoquer que les clients potentiels :",
                'options' => [
                    '3A' => "Doutent du vendeur.",
                    '2B' => "Doutent de la qualité du produit.",
                    '4C' => "Considèrent que le produit est obsolète.",
                    '1D' => "Se demandent pourquoi le prix était plus élevé au départ."
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => "Vous avez fait votre présentation à un acheteur autorisé et vous avez reçu le soutien des experts du département technique. Malgré tout, l’achat n’est finalement pas autorisé par un supérieur hiérarchique qui veut traiter avec un autre fournisseur. Vous devriez:",
                'options' => [
                    '2A' => "Essayer d’organiser une réunion avec votre client originel et son supérieur.",
                    '3B' => "Tenter de rencontrer directement le supérieur hiérarchique",
                    '1C' => "Essayer d’avoir une réunion avec le personnel technique, le client et le supérieur hiérarchique.",
                    '4D' => "Faire en sorte que le personnel technique confirme la supériorité de votre produit."
                ]
            ],
            [
                'group' => 5,
                'type' => 'order',
                'question' => "Durant la présentation le client vous dit : “ Je suis satisfait avec mon fournisseur actuel.” Vous devriez :",
                'options' => [
                    '4A' => "Dire au client que votre produit est beaucoup mieux",
                    '3B' => "Signaler les bénéfices d’avoir plusieurs fournisseurs.",
                    '1C' => "Demander au client ce qui le satisfait le plus.",
                    '2D' => "Lui demander quels changements réaliserait-il avec son actuel fournisseur, s’il pouvait."
                ]
            ],

            /// -- clasificacion 2

            [
                'group' => 2,
                'type' => 'order',
                'question' => "Durant la présentation, vous découvrez que le client utilise un produit de la concurrence que vous savez quelque peu défectueux. Vous devriez :",
                'options' => [
                    '2A' => "Demander au client ce qu’il aime du produit concurrent.",
                    '1B' => "Demander au client : “Si vous pourriez modifier le produit, qu’améliorerez-vous?”.",
                    '3C' => "Poser des questions qui révèlent les symptômes et défauts présents dans le produit de votre concurrent.",
                    '4D' => "Insister sur la qualité de votre produit."
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => "Au début de la réunion avec un client, celui se montre peu intéressé et ne dit rien, Une situation assez gênante pour vous deux. Que faites-vous :",
                'options' => [
                    '4A' => "Vous commencez la présentation.",
                    '1B' => "Vous posez des questions pour susciter l’intérêt du client.",
                    '3C' => "Vous demandez au client s’il est préoccupé par quelque chose.",
                    '2D' => "Vous vous assurez que le client sache correctement qui vous êtes, qui vous représentez et pourquoi vous êtes là."
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => "Lorsqu’un client potentiel pose une question à laquelle vous n’avez pas la réponse, vous devriez:",
                'options' => [
                    '1A' => "Admettre que vous ne connaissez pas la réponse mais que vous allez vous renseigner.",
                    '2B' => "Appeler quelqu’un qui connait la réponse",
                    '3C' => "Réviser vos documents pour trouver la réponse.",
                    '4D' => "Demander au client si cette réponse est importante dans son processus d’achat."
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => "La premiere impression est importante dans le processus de vente car :",
                'options' => [
                    '1A' => "Le climat de confiance précède toute décision d'achat",
                    '2B' => "Les gens achètent aux gens qu'ils apprécient.",
                    '4C' => "Une première impression négative pourrait empêcher le client potentiel de prêter attention à votre présentation.",
                    '3D' => "Les premières impressions sont durables."
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => "Votre client dit : “Quand pourrais-je être livré ?” Vous devriez:",
                'options' => [
                    '2A' => "Réviser et vérifiez que la livraison puisse être faite.",
                    '3B' => "Continuer la vente pour lui donner encore plus de valeur.",
                    '1C' => "Demander : “Quand souhaiteriez-vous que cela soit fait?”",
                    '4D' => "Demander : “Pourquoi est-ce si important pour vous ?”"
                ]
            ],
            [
                'group' => 2,
                'type' => 'order',
                'question' => "Vous avez présenté votre produit ou service et votre client dit : “Je veux y réfléchir”. Vous devriez :",
                'options' => [
                    '2A' => "Demander : “A quoi souhaitez-vous réfléchir?”",
                    '3B' => "Demander : “Combien de temps avez-vous besoin pour y réfléchir ?”",
                    '1C' => "Lui dire : “Je comprends. Souhaitez-vous plus de précisions?”",
                    '4D' => "Lui dire : “Peu importe combien de temps vous y penserez, vous ne trouverez nulle part de meilleur service à ce prix.”"
                ]
            ],

            /// -- clasificacion: 1

            [
                'group' => 1,
                'type' => 'order',
                'question' => "En vous rapprochant de votre client, vous découvrez que ce n'est pas la personne qui peut prendre la décision formelle. Vous devriez :",
                'options' => [
                    '1A' => "Continuer la vente avec l'intention de développer un contact interne.",
                    '2B' => "Faire votre présentation pour créer un besoin et faire connaître votre produit.",
                    '3C' => "Chercher qui peut prendre la décision et organiser une réunion avec cette personne",
                    '4D' => "Demandez à l'acheteur d'inviter le décideur à participer à votre réunion en cours."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => "La première fois que vous vous réunissez avec un client potentiel ou que vous avez affaire à une entreprise, vous devriez :",
                'options' => [
                    '2A' => "Poser une série de questions pour mieux connaître l'organisation.",
                    '1B' => "Profiter pour développer la confiance et une bonne communication avec le client.",
                    '4C' => "Profiter pour à parler de la position de son entreprise sur le marché.",
                    '3D' => "Utiliser des lettres de recommandation pour prouver votre crédibilité."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => "Lorsque vous essayez de contacter le client par téléphone, sa secrétaire vous dit “Monsieur Martin ne reçoit pas d’appels téléphoniques de vendeurs.” Vous devriez :",
                'options' => [
                    '4A' => "Envoyez une note personnelle avec du matériel descriptif et faire en sorte que le client puisse le voir.",
                    '3B' => "Essayez de vous renseigner sur les activités, les personnes connues et les intérêts du client.",
                    '2C' => "Essayez de créer un climat de confiance avec la secrétaire.",
                    '1D' => "Demandez à la secrétaire comment Monsieur Mation s’informe sur les nouveaux produits ou services."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => "La secrétaire du client vous dit que “Monsieur Martin n'est pas intéressé par de nouveaux produits pour le moment.” Votre réaction devrait être de :",
                'options' => [
                    '3A' => "Découvrir quel est l’agenda du client pour la recherche de nouveaux produits.",
                    '1B' => "Découvrir pourquoi il ne cherche pas de nouveaux produits.",
                    '2C' => "Déterminer si quelqu'un d'autre dans l'entreprise se charge de chercher de nouveaux produits.",
                    '4D' => "Posez des questions pour savoir s’il existe un problème pouvant être résolu avec vos produits."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => "Vous envoyez du matériel d'information à un client. Lorsque vous appelez pour prendre rendez-vous, le sujet initial de votre conversation devrait être :",
                'options' => [
                    '1A' => "Confirmer si le client a reçu et lu les informations.",
                    '3B' => "Organiser la réunion.",
                    '2C' => "Déterminer le niveau d'intérêt du client pour le rendez-vous.",
                    '4D' => "Fournir au client suffisamment d'information pour qu’il décide lui-même de se réunir avec vous."
                ]
            ],
            [
                'group' => 1,
                'type' => 'order',
                'question' => "Lorsque des clients potentiels refusent de recevoir vos appels, vous devez :",
                'options' => [
                    '3A' => "Continuer d’appeler jusqu'à ce que quelqu’un vous écoute",
                    '4B' => "Laisser plus de messages",
                    '1C' => "Continuer à travailler avec la personne qui vous a reçu jusqu'à ce que vous puissiez trouver le moment adéquat pour contacter votre client potentiel.",
                    '2D' => "Trouver une autre personne qui puisse vous aider à contacter le client potentiel"
                ]
            ],

            /// -- clasificacion: 6

            [
                'group' => 6,
                'type' => 'order',
                'question' => "Immédiatement après avoir réalisé une vente, vous devriez:",
                'options' => [
                    '4A' => "Demander des références.",
                    '3B' => "Essayer de lui vendre des produits ou services supplémentaires",
                    '1C' => "Féliciter le client et réaffirmer sa décision",
                    '2D' => "Dire au client comment vous lui donnerez du suivi."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => "Vous et votre client discutez depuis longtemps et le client vous dit: “Tout me semble bien”. Vous devriez",
                'options' => [
                    '4A' => "Lui demander ce qui lui semble le mieux.",
                    '1B' => "Lui demander ce que vous devez faire pour démarrer le processus.",
                    '3C' => "Prendre la commande et partir.",
                    '2D' => "Résumer les avantages et conclure la vente"
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => "Il vous reste peu de temps, vous avez essayé de conclure la vente et le client vous réponds: “Je veux y réfléchir”. Vous devriez:",
                'options' => [
                    '1A' => "Demander au client à quoi il souhaite réfléchir.",
                    '3B' => "Résumez les principaux avantages et bénéfices et conclure la vente en donnant au client les raisons de prendre une décision maintenant.",
                    '4C' => "Essayer de conclure la vente en créant un sentiment d’urgence.",
                    '2D' => "Demander au client combien de temps il aura besoin pour y réfléchir."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => "Vous avez fait votre présentation et vous demandez au client d’acheter votre produit. Celui-ci vous réponds: “Je veux continuer à chercher”. Que devriez-vous faire?",
                'options' => [
                    '2A' => "Lui demander ce qu'il veut voir d'autre et quand il le fera.",
                    '4B' => "Lui résumer votre présentation et conclure à nouveau",
                    '3C' => "Parler de vos avantages compétitifs sur le marché.",
                    '1D' => "Essayer de découvrir quelles sont les véritables raisons qui pourraient provoquer son hésitation ou un temps de prise de décision si long."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => "Vous avez présenté au client toutes les informations concernant votre produit. Cependant, il reste indécis et continue de repousser la prise de décision. Vous devriez",
                'options' => [
                    '2A' => "Traiter une à une ses objections puis conclure la vente",
                    '3B' => "Continuer de l’avant et tenter de conclure la vente",
                    '4C' => "Arrêter votre présentation et lui proposer de revenir un plus tard",
                    '1D' => "Demander au client ce qui le fait hésiter dans sa prise de décision"
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => "Votre client vous dit “J’aime votre service, il me semble très bien; cependant, je pense que je devrais vérifier plus. Vous devriez",
                'options' => [
                    '2A' => "Lui demander: “Que souhaitez-vous vérifier?” et ensuite conclure la vente.",
                    '4B' => "Lui demander: “Pourquoi voulez-vous faire cela?”",
                    '1C' => "Proposer de l’aide au client en révisant ce qu’il souhaite savoir.",
                    '3D' => "Dire : “Certaines choses restent toujours les mêmes, peu importe combien de fois vous les vérifiez”."
                ]
            ],
            [
                'group' => 6,
                'type' => 'order',
                'question' => "Vous demandez au client: “Qui d'autre participe à la prise de décision d’achat ?” Il affirme prendre seul cette décision. Ensuite, lorsque vous l’encouragez d’acheter, le client insiste pour le vérifier avec deux autres personnes. Vous devriez alors:",
                'options' => [
                    '4A' => "Demander au client pourquoi au début il a dit qu’il serait la seule personne capable de prendre la décision.",
                    '2B' => "Faire la présentation aux autres personnes qui participeront à la prise de décision.",
                    '3C' => "Laisser votre client se réunir seul avec les autres personnes pour leur présenter votre histoire.",
                    '1D' => "Vous renseigner sur les deux autres personnes afin de formuler une nouvelle stratégie de vente."
                ]
            ],
        ];
    }
}