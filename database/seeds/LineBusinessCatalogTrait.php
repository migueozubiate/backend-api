<?php

trait LineBusinessCatalogTrait
{
    protected function getLineBusinessCatalog()
    {
        return [
            [
                'code' => 461121,	
                'line_business' => 'Comercio de carnes rojas',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461121',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 461122,	
                'line_business' => 'Comercio de carne de aves',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461122',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 461123,	
                'line_business' => 'Comercio de pescados y mariscos',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461123',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 461130,	
                'line_business' => 'Comercio de frutas y verduras frescas',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461130',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 461140,	
                'line_business' => 'Comercio de semillas y granos alimenticios, especias y chiles secos',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461140',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 461150,	
                'line_business' => 'Comercio de leche, otros productos lácteos y embutidos',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461150',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 461160,	
                'line_business' => 'Comercio de dulces y materias primas para repostería',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461160',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 461190,	
                'line_business' => 'Comercio de otros alimentos',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461190',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 461213,	
                'line_business' => 'Comercio de bebidas no alcohólicas y hielo',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461213',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 461220,	
                'line_business' => 'Comercio de cigarros, puros y tabaco',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_461220',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 462112,	
                'line_business' => 'Comercio en minisupers',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_462112',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 463111,	
                'line_business' => 'Comercio de telas',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_463111',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 463211,	
                'line_business' => 'Comercio de ropa, excepto de bebé y lencería',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_463211',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 463212,	
                'line_business' => 'Comercio de ropa de bebé',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_463212',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 463213,	
                'line_business' => 'Comercio de lencería',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_463213',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 463217,	
                'line_business' => 'Comercio de pañales desechables',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_463217',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 463218,	
                'line_business' => 'Comercio de sombreros',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_463218',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 463310,	
                'line_business' => 'Comercio de calzado',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_463310',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 464111,	
                'line_business' => 'Farmacias sin minisúper',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_464111',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 464113,	
                'line_business' => 'Comercio de productos naturistas, medicamentos homeopáticos y de complementos alimenticios',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_464113',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 464121,	
                'line_business' => 'Comercio de lentes',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_464121',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 464122,	
                'line_business' => 'Comercio de artículos ortopédicos',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_464122',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465111,	
                'line_business' => 'Comercio de artículos de perfumería y cosméticos',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465111',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465112,	
                'line_business' => 'Comercio de artículos de joyería y relojes',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465112',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465212,	
                'line_business' => 'Comercio de juguetes',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465212',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465213,	
                'line_business' => 'Comercio de bicicletas',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465213',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465214,	
                'line_business' => 'Comercio de equipo y material fotográfico',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465214',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465215,	
                'line_business' => 'Comercio de artículos y aparatos deportivos',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465215',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465216,	
                'line_business' => 'Comercio de instrumentos musicales',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465216',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465312,	
                'line_business' => 'Comercio de libros',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465312',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465313,	
                'line_business' => 'Comercio de revistas y periódicos',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465313',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465913,	
                'line_business' => 'Comercio de artículos religiosos',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465913',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 465919,	
                'line_business' => 'Comercio de otros artículos de uso personal',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_465919',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 466111,	
                'line_business' => 'Comercio de muebles para el hogar',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_466111',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 466112,	
                'line_business' => 'Comercio de electrodomésticos menores y aparatos de línea blanca',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_466112',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 466114,	
                'line_business' => 'Comercio de cristalería, loza y utensilios de cocina',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_466114',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 466211,	
                'line_business' => 'Comercio de mobiliario, equipo y accesorios de cómputo',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_466211',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 466212,	
                'line_business' => 'Comercio de teléfonos y otros aparatos de comunicación',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_466212',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 466312,	
                'line_business' => 'Comercio de plantas y flores naturales',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_466312',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 466313,	
                'line_business' => 'Comercio de antigüedades y obras de arte',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_466313',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 466319,	
                'line_business' => 'Comercio de otros artículos para la decoración de interiores',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_466319',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 468211,	
                'line_business' => 'Comercio de partes y refacciones nuevas para automóviles, camionetas y camiones',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_468211',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 468213,	
                'line_business' => 'Comercio de llantas y cámaras para automóviles, camionetas y camiones',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_468213',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 468311,	
                'line_business' => 'Comercio de motocicletas',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_468311',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 468319,	
                'line_business' => 'Comercio de otros vehículos de motor',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_468319',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 469110,	
                'line_business' => 'Comercio exclusivamente a través de Internet, y catálogos impresos, televisión y similares',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_469110',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 464112,	
                'line_business' => 'Farmacias con minisúper',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_464112',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 468420,	
                'line_business' => 'Comercio al por menor de aceites y grasas lubricantes, aditivos y similares para vehículos de motor',
                'sector' => 'Comercio',
                'item_line_business' => 'line_business_468420',
                'item_sector' => 'comerce'
            ],
            [
                'code' => 488511,	
                'line_business' => 'Servicios de agencias aduanales',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_488511',
                'item_sector' => 'service'
            ],
            [
                'code' => 511111,	
                'line_business' => 'Edición de periódicos',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_511111',
                'item_sector' => 'service'
            ],
            [
                'code' => 511121,	
                'line_business' => 'Edición de revistas y otras publicaciones periódicas',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_511121',
                'item_sector' => 'service'
            ],
            [
                'code' => 511131,	
                'line_business' => 'Edición de libros',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_511131',
                'item_sector' => 'service'
            ],
            [
                'code' => 511141,	
                'line_business' => 'Edición de directorios y de listas de correo',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_511141',
                'item_sector' => 'service'
            ],
            [
                'code' => 522220,	
                'line_business' => 'Fondos y fideicomisos financieros',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_522220',
                'item_sector' => 'service'
            ],
            [
                'code' => 511210,	
                'line_business' => 'Edición de software y edición de software integrada con la reproducción',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_511210',
                'item_sector' => 'service'
            ],
            [
                'code' => 512120,	
                'line_business' => 'Distribución de películas y de otros materiales audiovisuales',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_512120',
                'item_sector' => 'service'
            ],
            [
                'code' => 518210,	
                'line_business' => 'Procesamiento electrónico de información, hospedaje y otros servicios relacionados',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_518210',
                'item_sector' => 'service'
            ],
            [
                'code' => 522452,	
                'line_business' => 'Casas de empeño',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_522452',
                'item_sector' => 'service'
            ],
            [
                'code' => 524110,	
                'line_business' => 'Compañías de seguros',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_524110',
                'item_sector' => 'service'
            ],
            [
                'code' => 524130,	
                'line_business' => 'Compañías afianzadoras',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_524130',
                'item_sector' => 'service'
            ],
            [
                'code' => 524210,	
                'line_business' => 'Agentes, ajustadores y gestores de seguros y fianzas',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_524210',
                'item_sector' => 'service'
            ],
            [
                'code' => 524220,	
                'line_business' => 'Administración de cajas de pensión y de seguros independientes',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_524220',
                'item_sector' => 'service'
            ],
            [
                'code' => 531111,	
                'line_business' => 'Alquiler sin intermediación de viviendas amuebladas',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_531111',
                'item_sector' => 'service'
            ],
            [
                'code' => 531112,	
                'line_business' => 'Alquiler sin intermediación de viviendas no amuebladas',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_531112',
                'item_sector' => 'service'
            ],
            [
                'code' => 531113,	
                'line_business' => 'Alquiler sin intermediación de salones para fiestas y convenciones',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_531113',
                'item_sector' => 'service'
            ],
            [
                'code' => 531114,	
                'line_business' => 'Alquiler sin intermediación de oficinas y locales comerciales',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_531114',
                'item_sector' => 'service'
            ],
            [
                'code' => 531119,	
                'line_business' => 'Alquiler sin intermediación de otros bienes raíces',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_531119',
                'item_sector' => 'service'
            ],
            [
                'code' => 531210,	
                'line_business' => 'Inmobiliarias y corredores de bienes raíces',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_531210',
                'item_sector' => 'service'
            ],
            [
                'code' => 531311,	
                'line_business' => 'Servicios de administración de bienes raíces',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_531311',
                'item_sector' => 'service'
            ],
            [
                'code' => 532220,	
                'line_business' => 'Alquiler de prendas de vestir',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_532220',
                'item_sector' => 'service'
            ],
            [
                'code' => 532291,	
                'line_business' => 'Alquiler de mesas, sillas, vajillas y similares',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_532291',
                'item_sector' => 'service'
            ],
            [
                'code' => 532292,	
                'line_business' => 'Alquiler de instrumentos musicales',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_532292',
                'item_sector' => 'service'
            ],
            [
                'code' => 541110,	
                'line_business' => 'Bufetes jurídicos',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_541110',
                'item_sector' => 'service'
            ],
            [
                'code' => 541190,	
                'line_business' => 'Servicios de apoyo para efectuar trámites legales',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_541190',
                'item_sector' => 'service'
            ],
            [
                'code' => 541310,	
                'line_business' => 'Servicios de arquitectura',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_541310',
                'item_sector' => 'service'
            ],
            [
                'code' => 541330,	
                'line_business' => 'Servicios de ingeniería',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_541330',
                'item_sector' => 'service'
            ],
            [
                'code' => 541350,	
                'line_business' => 'Servicios de inspección de edificios',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_541350',
                'item_sector' => 'service'
            ],
            [
                'code' => 541610,	
                'line_business' => 'Servicios de consultoría en administración',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_541610',
                'item_sector' => 'service'
            ],
            [
                'code' => 561421,	
                'line_business' => 'Servicios de casetas telefónicas',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_561421',
                'item_sector' => 'service'
            ],
            [
                'code' => 561422,	
                'line_business' => 'Servicios de recepción de llamadas telefónicas y promoción por teléfono',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_561422',
                'item_sector' => 'service'
            ],
            [
                'code' => 561431,	
                'line_business' => 'Servicios de fotocopiado, fax y afines',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_561431',
                'item_sector' => 'service'
            ],
            [
                'code' => 561610,	
                'line_business' => 'Servicios de investigación y de protección y custodia, excepto mediante monitoreo',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_561610',
                'item_sector' => 'service'
            ],
            [
                'code' => 561620,	
                'line_business' => 'Servicios de protección y custodia mediante el monitoreo de sistemas de seguridad',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_561620',
                'item_sector' => 'service'
            ],
            [
                'code' => 621391,	
                'line_business' => 'Consultorios de nutriólogos y dietistas del sector privado',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_621391',
                'item_sector' => 'service'
            ],
            [
                'code' => 621398,	
                'line_business' => 'Otros consultorios del sector privado para el cuidado de la salud',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_621398',
                'item_sector' => 'service'
            ],
            [
                'code' => 711131,	
                'line_business' => 'Cantantes y grupos musicales del sector privado',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_711131',
                'item_sector' => 'service'
            ],
            [
                'code' => 713943,	
                'line_business' => 'Centros de acondicionamiento físico del sector privado',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_713943',
                'item_sector' => 'service'
            ],
            [
                'code' => 811211,	
                'line_business' => 'Reparación y mantenimiento de equipo electrónico de uso doméstico',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_811211',
                'item_sector' => 'service'
            ],
            [
                'code' => 811410,	
                'line_business' => 'Reparación y mantenimiento de aparatos eléctricos para el hogar y personales',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_811410',
                'item_sector' => 'service'
            ],
            [
                'code' => 811491,	
                'line_business' => 'Cerrajerías',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_811491',
                'item_sector' => 'service'
            ],
            [
                'code' => 485311,	
                'line_business' => 'Transporte de pasajeros en taxis de sitio',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_485311',
                'item_sector' => 'service'
            ],
            [
                'code' => 522110,	
                'line_business' => 'Banca múltiple',
                'sector' => 'Servicio',
                'item_line_business' => 'line_business_522110',
                'item_sector' => 'service'
            ],
            [
                'code' => 311520,	
                'line_business' => 'Elaboración de helados y paletas',
                'sector' => 'Industrial',
                'item_line_business' => 'line_business_311520',
                'item_sector' => 'industrial'            ],
            [
                'code' => 311993,	
                'line_business' => 'Elaboración de alimentos frescos para consumo inmediato',
                'sector' => 'Industrial',
                'item_line_business' => 'line_business_311993',
                'item_sector' => 'industrial'            ],
        ];
    }
}

