<?php

use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\ExecutiveProfile;
use App\Roipal\Eloquent\User;
use App\Roipal\Eloquent\UserChecklist;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clasification = ['executive', 'company', 'admin'];
        $users         = [
            [
                'uuid'       => 'c3a2c6b8-af91-4b05-81e4-5ecb82bcaa0e',
                'company_id' => 1,
                'name'       => 'Rocket',
                'email'      => 'rocket@nulldata.com',
                'password'   => Hash::make('XaaminRocks'),
                'classification' => 'company',
            ],
            [
                'uuid'           => 'f4f9df8b-c77b-3354-b77e-acfe5f988cd1',
                'name'           => 'Seller NullData',
                'email'          => 'seller@nulldata.com',
                'password'       => bcrypt('123456789'),
                'classification' => 'executive',
            ],
            [
                'uuid'           => 'e26c2c37-dcc7-4075-92b1-e13c0870bc6b',
                'name'           => 'Admin',
                'email'          => 'admin@roipal.com',
                'password'       => bcrypt('12345678'),
                'classification' => 'admin',
            ],
        ];

        foreach ($users as $values) {
            $user                    = new User($values);
            $user->uuid              = $values['uuid'];
            $user->classification    = array_pop($clasification);
            $user->email_verified_at = Carbon::now();
            $user->save();

            if ($user->classification == 'executive') {
                $executive = Executive::find($user->id);

                $profile = new ExecutiveProfile([
                    'bio'      => '',
                    'website'  => '',
                    'address'  => '',
                    'position' => new Point(19.366292, -99.180270),
                    'country' => 'México'
                ]);

                $executive->profile()->save($profile);

            }

            $checklist               = new UserChecklist();
            $checklist->step         = 'activation';
            $checklist->completed    = true;
            $checklist->completed_at = Carbon::now();

            $user->checklist()->save($checklist);
        }
    }
}
