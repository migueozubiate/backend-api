<?php

use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\CompanyProfile;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class CompaniesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create('es_PE');
        $companies = [
            [
                'uuid' => 'f69f7225-07a3-4caa-88cf-a5f89e84a352',
                'business_name' => 'first Company',
                'name' => 'Company',
                'vertical' => '',
                'rfc' => 'MELM8305283Y9',
                'terms_accepted' => true
            ],
        ];

        foreach ($companies as $company) {
            $company = new Company($company);
            $company->save();

            $profile = new CompanyProfile([
                'company_id' => 1,
                'slogan' => $faker->text(5),
                'video_bio_url' => $faker->url,
                'photo_bio_url' => $faker->url,
                'bio' => $faker->text(50),
                'website' => $faker->url,
                'vertical' => 'vertical',
                'terms_accepted' => 1,
                'address' => $faker->address,
                'position' => new Point(-99.179361, 19.362722),
                'country' => 'Mexico'
            ]);

            $company->profile()->save($profile);
        }
    }
}
