<?php

use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Eloquent\ExecutiveMission;

class ExecutiveMissionTableSeeder extends Seeder
{
    public function run()
    {
        $executiveMisionSeed = [
            'review' => 'review'
        ];

        $mission = Mission::find(1); 
        $company = Company::find(1);
        $executive = Executive::find(1);
        $missionLocation = MissionLocation::find(1);

        $executiveMision = new ExecutiveMission($executiveMisionSeed);
        $executiveMision->mission_id = $mission->id;
        $executiveMision->company_id = $company->id;
        $executiveMision->executive_id = $executive->id;
        $executiveMision->mission_location_id = $missionLocation->id;
        $executiveMision->save();
    }
}
