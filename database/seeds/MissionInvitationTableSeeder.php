<?php


use App\Roipal\Eloquent\User;
use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\MissionLocation;
use App\Roipal\Eloquent\MissionInvitation;

class MissionInvitationTableSeeder extends Seeder
{
    public function run()
    {
        $missionInvitationSeed = [
            'status' => 1,
            'reason' => 'Prueba',
        ];


        $executive = Executive::find(2);
        $user = User::find(1);
        $mission = Mission::find(1); 
        $company = Company::find(1);
        $missionLocation = MissionLocation::find(1);

        
        $missionInvitation = new MissionInvitation($missionInvitationSeed);
        $missionInvitation->user_id = $user->id;
        $missionInvitation->executive_id = $executive->id;
        $missionInvitation->mission_id = $mission->id;
        $missionInvitation->company_id = $company->id;
        $missionInvitation = $missionLocation->invitations()->save($missionInvitation);
        $missionInvitation->uuid = '1041822e-4529-3741-b18f-496991357e78';
        $missionInvitation->save();
    }
}
