<?php

trait DiscAnswersPollValuesTrait
{
    protected function getAnswersDisc($language)
    {
        switch ($language) {
            case 'es':
                return $this->getAnswersInSpanish();
                break;

            case 'en':
                return $this->getAnswersInEnglish();
                break;

            case 'fr':
                return $this->getAnswersInFrench();
                break;
            
            default:
                # code...
                break;
        }
    }

    private function getAnswersInSpanish()
    {
        return [
            [
                [
                    "meaning" => [
                        "B" => "Seguro de sí mismo/a",
                        "D" => "Firme",
                        "C" => "Considerado/a",
                        "A" => "Exacto/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirador",
                        "D" => "Tenaz",
                        "C" => "Moderado/a",
                        "B" => "Reflexivo/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Encantador",
                        "A" => "Agresivo/a",
                        "D" => "Generoso/a",
                        "B" => "Prudente",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Juguetón",
                        "A" => "Determinado/a",
                        "B" => "Buen vecino",
                        "C" => "Metódico/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Alentador/a",
                        "A" => "Firme",
                        "D" => "Sabe escuchar",
                        "C" => "Disciplinado/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Divertido/a",
                        "A" => "Audaz",
                        "D" => "Simpático/a",
                        "C" => "De voz suave",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Entusiasta",
                        "B" => "Competitivo/a",
                        "D" => "Ecuánime",
                        "A" => "Cuidadoso/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Con aplomo (Seductor/a)",
                        "B" => "Pionero",
                        "D" => "ESafe",
                        "A" => "Controlado/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Expresivo/a",
                        "C" => "Atrevido/a",
                        "A" => "Compasivo/a",
                        "B" => "Correcto/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "A" => "Acomodadizo/a",
                        "B" => "Con carácter",
                        "C" => "Moderado/a",
                        "D" => "Conservador",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Amistoso/a",
                        "A" => "Franco/a",
                        "C" => "Diplomático/a",
                        "D" => "Resignado/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Influyente",
                        "D" => "Dinámico/a",
                        "C" => "Imparcial",
                        "A" => "Calmado/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "B" => "Impaciente",
                        "C" => "Cooperativo",
                        "A" => "Constante",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Extrovertido/a",
                        "C" => "Persuasivo/a",
                        "B" => "ESafe",
                        "A" => "Delicate",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Simpático/a",
                        "A" => "Atractivo/a",
                        "B" => "Colaborador/a",
                        "D" => "Obstinado/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Abierto/a",
                        "A" => "Seguro/a",
                        "B" => "Comprensivo/a",
                        "D" => "Tolerante/a",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Optimista",
                        "B" => "Emprendedor/a",
                        "D" => "Servicial",
                        "A" => "Respetuoso/a",
                    ],
                    "result" => "P10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Seguro de sí mismo/a",
                        "D" => "Firme",
                        "A" => "Exacto/a",
                        "C" => "Considerado/a",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirador",
                        "D" => "Tenaz",
                        "B" => "Reflexivo/a",
                        "C" => "Moderado/a",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Encantador",
                        "A" => "Agresivo/a",
                        "B" => "Prudente",
                        "D" => "Generoso/a",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Juguetón",
                        "A" => "Determinado/a",
                        "C" => "Metódico/a",
                        "B" => "Buen vecino",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Alentador/a",
                        "A" => "Firme",
                        "C" => "Disciplinado/a",
                        "D" => "Sabe escuchar",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Divertido/a",
                        "A" => "Audaz",
                        "C" => "De voz suave",
                        "D" => "Simpático/a",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Entusiasta",
                        "B" => "Competitivo/a",
                        "A" => "Cuidadoso/a",
                        "D" => "Ecuánime",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Con aplomo (Seductor/a)",
                        "B" => "Pionero",
                        "A" => "Controlado/a",
                        "D" => "ESafe",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Expresivo/a",
                        "C" => "Atrevido/a",
                        "B" => "Correcto/a",
                        "A" => "Compasivo/a",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "A" => "Acomodadizo/a",
                        "B" => "Con carácter",
                        "D" => "Conservador",
                        "C" => "Moderado/a",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Amistoso/a",
                        "A" => "Franco/a",
                        "D" => "Resignado/a",
                        "C" => "Diplomático/a",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Influyente",
                        "D" => "Dinámico/a",
                        "A" => "Calmado/a",
                        "C" => "Imparcial",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "B" => "Impaciente",
                        "A" => "Constante",
                        "C" => "Cooperativo",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Extrovertido/a",
                        "C" => "Persuasivo/a",
                        "A" => "Delicateo",
                        "B" => "ESafe",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Simpático/a",
                        "A" => "Atractivo/a",
                        "D" => "Obstinado/a",
                        "B" => "Colaborador/a",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Abierto/a",
                        "A" => "Seguro/a",
                        "D" => "Tolerante/a",
                        "B" => "Comprensivo/a",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Optimista",
                        "B" => "Emprendedor/a",
                        "A" => "Respetuoso/a",
                        "D" => "Servicial",
                    ],
                    "result" => "P9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Seguro de sí mismo/a",
                        "C" => "Considerado/a",
                        "D" => "Firme",
                        "A" => "Exacto/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirador",
                        "C" => "Moderado/a",
                        "D" => "Tenaz",
                        "B" => "Reflexivo/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Encantador",
                        "D" => "Generoso/a",
                        "A" => "Agresivo/a",
                        "B" => "Prudente",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Juguetón",
                        "B" => "Buen vecino",
                        "A" => "Determinado/a",
                        "C" => "Metódico/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Alentador/a",
                        "D" => "Sabe escuchar",
                        "A" => "Firme",
                        "C" => "Disciplinado/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Divertido/a",
                        "D" => "Simpático/a",
                        "A" => "Audaz",
                        "C" => "De voz suave",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Entusiasta",
                        "D" => "Ecuánime",
                        "B" => "Competitivo/a",
                        "A" => "Cuidadoso/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Con aplomo (Seductor/a)",
                        "D" => "ESafe",
                        "B" => "Pionero",
                        "A" => "Controlado/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Expresivo/a",
                        "A" => "Compasivo/a",
                        "C" => "Atrevido/a",
                        "B" => "Correcto/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "A" => "Acomodadizo/a",
                        "C" => "Moderado/a",
                        "B" => "Con carácter",
                        "D" => "Conservador",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Amistoso/a",
                        "C" => "Diplomático/a",
                        "A" => "Franco/a",
                        "D" => "Resignado/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Influyente",
                        "C" => "Imparcial",
                        "D" => "Dinámico/a",
                        "A" => "Calmado/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "C" => "Cooperativo",
                        "B" => "Impaciente",
                        "A" => "Constante",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Extrovertido/a",
                        "B" => "ESafe",
                        "C" => "Persuasivo/a",
                        "A" => "Delicateo",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Simpático/a",
                        "B" => "Colaborador/a",
                        "A" => "Atractivo/a",
                        "D" => "Obstinado/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Abierto/a",
                        "B" => "Comprensivo/a",
                        "A" => "Seguro/a",
                        "D" => "Tolerante/a",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Optimista",
                        "D" => "Servicial",
                        "B" => "Emprendedor/a",
                        "A" => "Respetuoso/a",
                    ],
                    "result" => "P7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Seguro de sí mismo/a",
                        "C" => "Considerado/a",
                        "A" => "Exacto/a",
                        "D" => "Firme",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirador",
                        "C" => "Moderado/a",
                        "B" => "Reflexivo/a",
                        "D" => "Tenaz",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Encantador",
                        "D" => "Generoso/a",
                        "B" => "Prudente",
                        "A" => "Agresivo/a",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Juguetón",
                        "B" => "Buen vecino",
                        "C" => "Metódico/a",
                        "A" => "Determinado/a",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Alentador/a",
                        "D" => "Sabe escuchar",
                        "C" => "Disciplinado/a",
                        "A" => "Firme",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Divertido/a",
                        "D" => "Simpático/a",
                        "C" => "De voz suave",
                        "A" => "Audaz",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Entusiasta",
                        "D" => "Ecuánime",
                        "A" => "Cuidadoso/a",
                        "B" => "Competitivo/a",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Con aplomo (Seductor/a)",
                        "D" => "ESafe",
                        "A" => "Controlado/a",
                        "B" => "Pionero",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Expresivo/a",
                        "A" => "Compasivo/a",
                        "B" => "Correcto/a",
                        "C" => "Atrevido/a",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "A" => "Acomodadizo/a",
                        "C" => "Moderado/a",
                        "D" => "Conservador",
                        "B" => "Con carácter",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Amistoso/a",
                        "C" => "Diplomático/a",
                        "D" => "Resignado/a",
                        "A" => "Franco/a",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Influyente",
                        "C" => "Imparcial",
                        "A" => "Calmado/a",
                        "D" => "Dinámico/a",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "C" => "Cooperativo",
                        "A" => "Constante",
                        "B" => "Impaciente",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Extrovertido/a",
                        "B" => "ESafe",
                        "A" => "Delicateo",
                        "C" => "Persuasivo/a",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Simpático/a",
                        "B" => "Colaborador/a",
                        "D" => "Obstinado/a",
                        "A" => "Atractivo/a",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Abierto/a",
                        "B" => "Comprensivo/a",
                        "D" => "Tolerante/a",
                        "A" => "Seguro/a",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Optimista",
                        "D" => "Servicial",
                        "A" => "Respetuoso/a",
                        "B" => "Emprendedor/a",
                    ],
                    "result" => "P5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Seguro de sí mismo/a",
                        "A" => "Exacto/a",
                        "C" => "Considerado/a",
                        "D" => "Firme",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirador",
                        "B" => "Reflexivo/a",
                        "C" => "Moderado/a",
                        "D" => "Tenaz",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Encantador",
                        "B" => "Prudente",
                        "D" => "Generoso/a",
                        "A" => "Agresivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Juguetón",
                        "C" => "Metódico/a",
                        "B" => "Buen vecino",
                        "A" => "Determinado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Alentador/a",
                        "C" => "Disciplinado/a",
                        "D" => "Sabe escuchar",
                        "A" => "Firme",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Divertido/a",
                        "C" => "De voz suave",
                        "D" => "Simpático/a",
                        "A" => "Audaz",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Entusiasta",
                        "A" => "Cuidadoso/a",
                        "D" => "Ecuánime",
                        "B" => "Competitivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Con aplomo (Seductor/a)",
                        "A" => "Controlado/a",
                        "D" => "ESafe",
                        "B" => "Pionero",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Expresivo/a",
                        "B" => "Correcto/a",
                        "A" => "Compasivo/a",
                        "C" => "Atrevido/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Acomodadizo/a",
                        "D" => "Conservador",
                        "C" => "Moderado/a",
                        "B" => "Con carácter",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Amistoso/a",
                        "D" => "Resignado/a",
                        "C" => "Diplomático/a",
                        "A" => "Franco/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Influyente",
                        "A" => "Calmado/a",
                        "C" => "Imparcial",
                        "D" => "Dinámico/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "A" => "Constante",
                        "C" => "Cooperativo",
                        "B" => "Impaciente",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Extrovertido/a",
                        "A" => "Delicateo",
                        "B" => "ESafe",
                        "C" => "Persuasivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Simpático/a",
                        "D" => "Obstinado/a",
                        "B" => "Colaborador/a",
                        "A" => "Atractivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Abierto/a",
                        "D" => "Tolerante/a",
                        "B" => "Comprensivo/a",
                        "A" => "Seguro/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Optimista",
                        "A" => "Respetuoso/a",
                        "D" => "Servicial",
                        "B" => "Emprendedor/a",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Seguro de sí mismo/a",
                        "A" => "Exacto/a",
                        "D" => "Firme",
                        "C" => "Considerado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirador",
                        "B" => "Reflexivo/a",
                        "D" => "Tenaz",
                        "C" => "Moderado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Encantador",
                        "B" => "Prudente",
                        "A" => "Agresivo/a",
                        "D" => "Generoso/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Juguetón",
                        "C" => "Metódico/a",
                        "A" => "Determinado/a",
                        "B" => "Buen vecino",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Alentador/a",
                        "C" => "Disciplinado/a",
                        "A" => "Firme",
                        "D" => "Sabe escuchar",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Divertido/a",
                        "C" => "De voz suave",
                        "A" => "Audaz",
                        "D" => "Simpático/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Entusiasta",
                        "A" => "Cuidadoso/a",
                        "B" => "Competitivo/a",
                        "D" => "Ecuánime",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Con aplomo (Seductor/a)",
                        "A" => "Controlado/a",
                        "B" => "Pionero",
                        "D" => "ESafe",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Expresivo/a",
                        "B" => "Correcto/a",
                        "C" => "Atrevido/a",
                        "A" => "Compasivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Acomodadizo/a",
                        "D" => "Conservador",
                        "B" => "Con carácter",
                        "C" => "Moderado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Amistoso/a",
                        "D" => "Resignado/a",
                        "A" => "Franco/a",
                        "C" => "Diplomático/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Influyente",
                        "A" => "Calmado/a",
                        "D" => "Dinámico/a",
                        "C" => "Imparcial",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "A" => "Constante",
                        "B" => "Impaciente",
                        "C" => "Cooperativo",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Extrovertido/a",
                        "A" => "Delicateo",
                        "C" => "Persuasivo/a",
                        "B" => "ESafe",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Simpático/a",
                        "D" => "Obstinado/a",
                        "A" => "Atractivo/a",
                        "B" => "Colaborador/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Abierto/a",
                        "D" => "Tolerante/a",
                        "A" => "Seguro/a",
                        "B" => "Comprensivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Optimista",
                        "A" => "Respetuoso/a",
                        "B" => "Emprendedor/a",
                        "D" => "Servicial",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Firme",
                        "B" => "Seguro de sí mismo/a",
                        "C" => "Considerado/a",
                        "A" => "Exacto/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "D" => "Tenaz",
                        "A" => "Inspirador",
                        "C" => "Moderado/a",
                        "B" => "Reflexivo/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Agresivo/a",
                        "C" => "Encantador",
                        "D" => "Generoso/a",
                        "B" => "Prudente",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Determinado/a",
                        "D" => "Juguetón",
                        "B" => "Buen vecino",
                        "C" => "Metódico/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Firme",
                        "B" => "Alentador/a",
                        "D" => "Sabe escuchar",
                        "C" => "Disciplinado/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Audaz",
                        "B" => "Divertido/a",
                        "D" => "Simpático/a",
                        "C" => "De voz suave",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Competitivo/a",
                        "C" => "Entusiasta",
                        "D" => "Ecuánime",
                        "A" => "Cuidadoso/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Pionero",
                        "C" => "Con aplomo (Seductor/a)",
                        "D" => "ESafe",
                        "A" => "Controlado/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "C" => "Atrevido/a",
                        "D" => "Expresivo/a",
                        "A" => "Compasivo/a",
                        "B" => "Correcto/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Con carácter",
                        "A" => "Acomodadizo/a",
                        "C" => "Moderado/a",
                        "D" => "Conservador",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Franco/a",
                        "B" => "Amistoso/a",
                        "C" => "Diplomático/a",
                        "D" => "Resignado/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "D" => "Dinámico/a",
                        "B" => "Influyente",
                        "C" => "Imparcial",
                        "A" => "Calmado/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Impaciente",
                        "D" => "Sociable",
                        "C" => "Cooperativo",
                        "A" => "Constante",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasivo/a",
                        "D" => "Extrovertido/a",
                        "B" => "ESafe",
                        "A" => "Delicateo",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Atractivo/a",
                        "C" => "Simpático/a",
                        "B" => "Colaborador/a",
                        "D" => "Obstinado/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Seguro/a",
                        "C" => "Abierto/a",
                        "B" => "Comprensivo/a",
                        "D" => "Tolerante/a",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Emprendedor/a",
                        "C" => "Optimista",
                        "D" => "Servicial",
                        "A" => "Respetuoso/a",
                    ],
                    "result" => "H10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Firme",
                        "B" => "Seguro de sí mismo/a",
                        "A" => "Exacto/a",
                        "C" => "Considerado/a",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "D" => "Tenaz",
                        "A" => "Inspirador",
                        "B" => "Reflexivo/a",
                        "C" => "Moderado/a",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Agresivo/a",
                        "C" => "Encantador",
                        "B" => "Prudente",
                        "D" => "Generoso/a",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Determinado/a",
                        "D" => "Juguetón",
                        "C" => "Metódico/a",
                        "B" => "Buen vecino",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Firme",
                        "B" => "Alentador/a",
                        "C" => "Disciplinado/a",
                        "D" => "Sabe escuchar",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Audaz",
                        "B" => "Divertido/a",
                        "C" => "De voz suave",
                        "D" => "Simpático/a",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Competitivo/a",
                        "C" => "Entusiasta",
                        "A" => "Cuidadoso/a",
                        "D" => "Ecuánime",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Pionero",
                        "C" => "Con aplomo (Seductor/a)",
                        "A" => "Controlado/a",
                        "D" => "ESafe",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "C" => "Atrevido/a",
                        "D" => "Expresivo/a",
                        "B" => "Correcto/a",
                        "A" => "Compasivo/a",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Con carácter",
                        "A" => "Acomodadizo/a",
                        "D" => "Conservador",
                        "C" => "Moderado/a",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Franco/a",
                        "B" => "Amistoso/a",
                        "D" => "Resignado/a",
                        "C" => "Diplomático/a",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "D" => "Dinámico/a",
                        "B" => "Influyente",
                        "A" => "Calmado/a",
                        "C" => "Imparcial",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Impaciente",
                        "D" => "Sociable",
                        "A" => "Constante",
                        "C" => "Cooperativo",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasivo/a",
                        "D" => "Extrovertido/a",
                        "A" => "Delicateo",
                        "B" => "ESafe",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Atractivo/a",
                        "C" => "Simpático/a",
                        "D" => "Obstinado/a",
                        "B" => "Colaborador/a",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Seguro/a",
                        "C" => "Abierto/a",
                        "D" => "Tolerante/a",
                        "B" => "Comprensivo/a",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Emprendedor/a",
                        "C" => "Optimista",
                        "A" => "Respetuoso/a",
                        "D" => "Servicial",
                    ],
                    "result" => "H9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Firme",
                        "A" => "Exacto/a",
                        "C" => "Considerado/a",
                        "B" => "Seguro de sí mismo/a",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "D" => "Tenaz",
                        "B" => "Reflexivo/a",
                        "C" => "Moderado/a",
                        "A" => "Inspirador",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Agresivo/a",
                        "B" => "Prudente",
                        "D" => "Generoso/a",
                        "C" => "Encantador",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Determinado/a",
                        "C" => "Metódico/a",
                        "B" => "Buen vecino",
                        "D" => "Juguetón",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Firme",
                        "C" => "Disciplinado/a",
                        "D" => "Sabe escuchar",
                        "B" => "Alentador/a",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Audaz",
                        "C" => "De voz suave",
                        "D" => "Simpático/a",
                        "B" => "Divertido/a",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Competitivo/a",
                        "A" => "Cuidadoso/a",
                        "D" => "Ecuánime",
                        "C" => "Entusiasta",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Pionero",
                        "A" => "Controlado/a",
                        "D" => "ESafe",
                        "C" => "Con aplomo (Seductor/a)",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "C" => "Atrevido/a",
                        "B" => "Correcto/a",
                        "A" => "Compasivo/a",
                        "D" => "Expresivo/a",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Con carácter",
                        "D" => "Conservador",
                        "C" => "Moderado/a",
                        "A" => "Acomodadizo/a",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Franco/a",
                        "D" => "Resignado/a",
                        "C" => "Diplomático/a",
                        "B" => "Amistoso/a",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "D" => "Dinámico/a",
                        "A" => "Calmado/a",
                        "C" => "Imparcial",
                        "B" => "Influyente",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Impaciente",
                        "A" => "Constante",
                        "C" => "Cooperativo",
                        "D" => "Sociable",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasivo/a",
                        "A" => "Delicateo",
                        "B" => "ESafe",
                        "D" => "Extrovertido/a",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Atractivo/a",
                        "D" => "Obstinado/a",
                        "B" => "Colaborador/a",
                        "C" => "Simpático/a",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Seguro/a",
                        "D" => "Tolerante/a",
                        "B" => "Comprensivo/a",
                        "C" => "Abierto/a",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Emprendedor/a",
                        "A" => "Respetuoso/a",
                        "D" => "Servicial",
                        "C" => "Optimista",
                    ],
                    "result" => "H7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Firme",
                        "A" => "Exacto/a",
                        "B" => "Seguro de sí mismo/a",
                        "C" => "Considerado/a",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "D" => "Tenaz",
                        "B" => "Reflexivo/a",
                        "A" => "Inspirador",
                        "C" => "Moderado/a",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Agresivo/a",
                        "B" => "Prudente",
                        "C" => "Encantador",
                        "D" => "Generoso/a",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Determinado/a",
                        "C" => "Metódico/a",
                        "D" => "Juguetón",
                        "B" => "Buen vecino",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Firme",
                        "C" => "Disciplinado/a",
                        "B" => "Alentador/a",
                        "D" => "Sabe escuchar",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Audaz",
                        "C" => "De voz suave",
                        "B" => "Divertido/a",
                        "D" => "Simpático/a",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Competitivo/a",
                        "A" => "Cuidadoso/a",
                        "C" => "Entusiasta",
                        "D" => "Ecuánime",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Pionero",
                        "A" => "Controlado/a",
                        "C" => "Con aplomo (Seductor/a)",
                        "D" => "ESafe",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "C" => "Atrevido/a",
                        "B" => "Correcto/a",
                        "D" => "Expresivo/a",
                        "A" => "Compasivo/a",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Con carácter",
                        "D" => "Conservador",
                        "A" => "Acomodadizo/a",
                        "C" => "Moderado/a",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Franco/a",
                        "D" => "Resignado/a",
                        "B" => "Amistoso/a",
                        "C" => "Diplomático/a",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "D" => "Dinámico/a",
                        "A" => "Calmado/a",
                        "B" => "Influyente",
                        "C" => "Imparcial",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Impaciente",
                        "A" => "Constante",
                        "D" => "Sociable",
                        "C" => "Cooperativo",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasivo/a",
                        "A" => "Delicateo",
                        "D" => "Extrovertido/a",
                        "B" => "ESafe",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Atractivo/a",
                        "D" => "Obstinado/a",
                        "C" => "Simpático/a",
                        "B" => "Colaborador/a",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Seguro/a",
                        "D" => "Tolerante/a",
                        "C" => "Abierto/a",
                        "B" => "Comprensivo/a",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Emprendedor/a",
                        "A" => "Respetuoso/a",
                        "C" => "Optimista",
                        "D" => "Servicial",
                    ],
                    "result" => "H5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Firme",
                        "C" => "Considerado/a",
                        "B" => "Seguro de sí mismo/a",
                        "A" => "Exacto/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tenaz",
                        "C" => "Moderado/a",
                        "A" => "Inspirador",
                        "B" => "Reflexivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Agresivo/a",
                        "D" => "Generoso/a",
                        "C" => "Encantador",
                        "B" => "Prudente",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Determinado/a",
                        "B" => "Buen vecino",
                        "D" => "Juguetón",
                        "C" => "Metódico/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Firme",
                        "D" => "Sabe escuchar",
                        "B" => "Alentador/a",
                        "C" => "Disciplinado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Audaz",
                        "D" => "Simpático/a",
                        "B" => "Divertido/a",
                        "C" => "De voz suave",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Competitivo/a",
                        "D" => "Ecuánime",
                        "C" => "Entusiasta",
                        "A" => "Cuidadoso/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Pionero",
                        "D" => "ESafe",
                        "C" => "Con aplomo (Seductor/a)",
                        "A" => "Controlado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Atrevido/a",
                        "A" => "Compasivo/a",
                        "D" => "Expresivo/a",
                        "B" => "Correcto/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Con carácter",
                        "C" => "Moderado/a",
                        "A" => "Acomodadizo/a",
                        "D" => "Conservador",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Franco/a",
                        "C" => "Diplomático/a",
                        "B" => "Amistoso/a",
                        "D" => "Resignado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Dinámico/a",
                        "C" => "Imparcial",
                        "B" => "Influyente",
                        "A" => "Calmado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Impaciente",
                        "C" => "Cooperativo",
                        "D" => "Sociable",
                        "A" => "Constante",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasivo/a",
                        "B" => "ESafe",
                        "D" => "Extrovertido/a",
                        "A" => "Delicateo",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Atractivo/a",
                        "B" => "Colaborador/a",
                        "C" => "Simpático/a",
                        "D" => "Obstinado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Seguro/a",
                        "B" => "Comprensivo/a",
                        "C" => "Abierto/a",
                        "D" => "Tolerante/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Emprendedor/a",
                        "D" => "Servicial",
                        "C" => "Optimista",
                        "A" => "Respetuoso/a",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Firme",
                        "C" => "Considerado/a",
                        "A" => "Exacto/a",
                        "B" => "Seguro de sí mismo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tenaz",
                        "C" => "Moderado/a",
                        "B" => "Reflexivo/a",
                        "A" => "Inspirador",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Agresivo/a",
                        "D" => "Generoso/a",
                        "B" => "Prudente",
                        "C" => "Encantador",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Determinado/a",
                        "B" => "Buen vecino",
                        "C" => "Metódico/a",
                        "D" => "Juguetón",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Firme",
                        "D" => "Sabe escuchar",
                        "C" => "Disciplinado/a",
                        "B" => "Alentador/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Audaz",
                        "D" => "Simpático/a",
                        "C" => "De voz suave",
                        "B" => "Divertido/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Competitivo/a",
                        "D" => "Ecuánime",
                        "A" => "Cuidadoso/a",
                        "C" => "Entusiasta",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Pionero",
                        "D" => "ESafe",
                        "A" => "Controlado/a",
                        "C" => "Con aplomo (Seductor/a)",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Atrevido/a",
                        "A" => "Compasivo/a",
                        "B" => "Correcto/a",
                        "D" => "Expresivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Con carácter",
                        "C" => "Moderado/a",
                        "D" => "Conservador",
                        "A" => "Acomodadizo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Franco/a",
                        "C" => "Diplomático/a",
                        "D" => "Resignado/a",
                        "B" => "Amistoso/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Dinámico/a",
                        "C" => "Imparcial",
                        "A" => "Calmado/a",
                        "B" => "Influyente",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Impaciente",
                        "C" => "Cooperativo",
                        "A" => "Constante",
                        "D" => "Sociable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasivo/a",
                        "B" => "ESafe",
                        "A" => "Delicateo",
                        "D" => "Extrovertido/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Atractivo/a",
                        "B" => "Colaborador/a",
                        "D" => "Obstinado/a",
                        "C" => "Simpático/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Seguro/a",
                        "B" => "Comprensivo/a",
                        "D" => "Tolerante/a",
                        "C" => "Abierto/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Emprendedor/a",
                        "D" => "Servicial",
                        "A" => "Respetuoso/a",
                        "C" => "Optimista",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Considerado/a",
                        "B" => "Seguro de sí mismo/a",
                        "D" => "Firme",
                        "A" => "Exacto/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "A" => "Inspirador",
                        "D" => "Tenaz",
                        "B" => "Reflexivo/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Generoso/a",
                        "C" => "Encantador",
                        "A" => "Agresivo/a",
                        "B" => "Prudente",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Buen vecino",
                        "D" => "Juguetón",
                        "A" => "Determinado/a",
                        "C" => "Metódico/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Sabe escuchar",
                        "B" => "Alentador/a",
                        "A" => "Firme",
                        "C" => "Disciplinado/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Simpático/a",
                        "B" => "Divertido/a",
                        "A" => "Audaz",
                        "C" => "De voz suave",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Ecuánime",
                        "C" => "Entusiasta",
                        "B" => "Competitivo/a",
                        "A" => "Cuidadoso/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "ESafe",
                        "C" => "Con aplomo (Seductor/a)",
                        "B" => "Pionero",
                        "A" => "Controlado/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "A" => "Compasivo/a",
                        "D" => "Expresivo/a",
                        "C" => "Atrevido/a",
                        "B" => "Correcto/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "A" => "Acomodadizo/a",
                        "B" => "Con carácter",
                        "D" => "Conservador",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomático/a",
                        "B" => "Amistoso/a",
                        "A" => "Franco/a",
                        "D" => "Resignado/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Imparcial",
                        "B" => "Influyente",
                        "D" => "Dinámico/a",
                        "A" => "Calmado/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperativo",
                        "D" => "Sociable",
                        "B" => "Impaciente",
                        "A" => "Constante",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "ESafe",
                        "D" => "Extrovertido/a",
                        "C" => "Persuasivo/a",
                        "A" => "Delicateo",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Colaborador/a",
                        "C" => "Simpático/a",
                        "A" => "Atractivo/a",
                        "D" => "Obstinado/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Comprensivo/a",
                        "C" => "Abierto/a",
                        "A" => "Seguro/a",
                        "D" => "Tolerante/a",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Servicial",
                        "C" => "Optimista",
                        "B" => "Emprendedor/a",
                        "A" => "Respetuoso/a",
                    ],
                    "result" => "F10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Considerado/a",
                        "B" => "Seguro de sí mismo/a",
                        "A" => "Exacto/a",
                        "D" => "Firme",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "A" => "Inspirador",
                        "B" => "Reflexivo/a",
                        "D" => "Tenaz",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Generoso/a",
                        "C" => "Encantador",
                        "B" => "Prudente",
                        "A" => "Agresivo/a",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Buen vecino",
                        "D" => "Juguetón",
                        "C" => "Metódico/a",
                        "A" => "Determinado/a",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Sabe escuchar",
                        "B" => "Alentador/a",
                        "C" => "Disciplinado/a",
                        "A" => "Firme",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Simpático/a",
                        "B" => "Divertido/a",
                        "C" => "De voz suave",
                        "A" => "Audaz",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Ecuánime",
                        "C" => "Entusiasta",
                        "A" => "Cuidadoso/a",
                        "B" => "Competitivo/a",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "ESafe",
                        "C" => "Con aplomo (Seductor/a)",
                        "A" => "Controlado/a",
                        "B" => "Pionero",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "A" => "Compasivo/a",
                        "D" => "Expresivo/a",
                        "B" => "Correcto/a",
                        "C" => "Atrevido/a",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "A" => "Acomodadizo/a",
                        "D" => "Conservador",
                        "B" => "Con carácter",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomático/a",
                        "B" => "Amistoso/a",
                        "D" => "Resignado/a",
                        "A" => "Franco/a",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Imparcial",
                        "B" => "Influyente",
                        "A" => "Calmado/a",
                        "D" => "Dinámico/a",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperativo",
                        "D" => "Sociable",
                        "A" => "Constante",
                        "B" => "Impaciente",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "ESafe",
                        "D" => "Extrovertido/a",
                        "A" => "Delicateo",
                        "C" => "Persuasivo/a",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Colaborador/a",
                        "C" => "Simpático/a",
                        "D" => "Obstinado/a",
                        "A" => "Atractivo/a",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Comprensivo/a",
                        "C" => "Abierto/a",
                        "D" => "Tolerante/a",
                        "A" => "Seguro/a",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Servicial",
                        "C" => "Optimista",
                        "A" => "Respetuoso/a",
                        "B" => "Emprendedor/a",
                    ],
                    "result" => "F9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Considerado/a",
                        "A" => "Exacto/a",
                        "B" => "Seguro de sí mismo/a",
                        "D" => "Firme",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "B" => "Reflexivo/a",
                        "A" => "Inspirador",
                        "D" => "Tenaz",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Generoso/a",
                        "B" => "Prudente",
                        "C" => "Encantador",
                        "A" => "Agresivo/a",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Buen vecino",
                        "C" => "Metódico/a",
                        "D" => "Juguetón",
                        "A" => "Determinado/a",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Sabe escuchar",
                        "C" => "Disciplinado/a",
                        "B" => "Alentador/a",
                        "A" => "Firme",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Simpático/a",
                        "C" => "De voz suave",
                        "B" => "Divertido/a",
                        "A" => "Audaz",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Ecuánime",
                        "A" => "Cuidadoso/a",
                        "C" => "Entusiasta",
                        "B" => "Competitivo/a",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "ESafe",
                        "A" => "Controlado/a",
                        "C" => "Con aplomo (Seductor/a)",
                        "B" => "Pionero",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "A" => "Compasivo/a",
                        "B" => "Correcto/a",
                        "D" => "Expresivo/a",
                        "C" => "Atrevido/a",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "D" => "Conservador",
                        "A" => "Acomodadizo/a",
                        "B" => "Con carácter",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomático/a",
                        "D" => "Resignado/a",
                        "B" => "Amistoso/a",
                        "A" => "Franco/a",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Imparcial",
                        "A" => "Calmado/a",
                        "B" => "Influyente",
                        "D" => "Dinámico/a",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperativo",
                        "A" => "Constante",
                        "D" => "Sociable",
                        "B" => "Impaciente",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "ESafe",
                        "A" => "Delicateo",
                        "D" => "Extrovertido/a",
                        "C" => "Persuasivo/a",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Colaborador/a",
                        "D" => "Obstinado/a",
                        "C" => "Simpático/a",
                        "A" => "Atractivo/a",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Comprensivo/a",
                        "D" => "Tolerante/a",
                        "C" => "Abierto/a",
                        "A" => "Seguro/a",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Servicial",
                        "A" => "Respetuoso/a",
                        "C" => "Optimista",
                        "B" => "Emprendedor/a",
                    ],
                    "result" => "F7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Considerado/a",
                        "A" => "Exacto/a",
                        "D" => "Firme",
                        "B" => "Seguro de sí mismo/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "B" => "Reflexivo/a",
                        "D" => "Tenaz",
                        "A" => "Inspirador",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Generoso/a",
                        "B" => "Prudente",
                        "A" => "Agresivo/a",
                        "C" => "Encantador",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Buen vecino",
                        "C" => "Metódico/a",
                        "A" => "Determinado/a",
                        "D" => "Juguetón",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Sabe escuchar",
                        "C" => "Disciplinado/a",
                        "A" => "Firme",
                        "B" => "Alentador/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Simpático/a",
                        "C" => "De voz suave",
                        "A" => "Audaz",
                        "B" => "Divertido/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Ecuánime",
                        "A" => "Cuidadoso/a",
                        "B" => "Competitivo/a",
                        "C" => "Entusiasta",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "ESafe",
                        "A" => "Controlado/a",
                        "B" => "Pionero",
                        "C" => "Con aplomo (Seductor/a)",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "A" => "Compasivo/a",
                        "B" => "Correcto/a",
                        "C" => "Atrevido/a",
                        "D" => "Expresivo/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "D" => "Conservador",
                        "B" => "Con carácter",
                        "A" => "Acomodadizo/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomático/a",
                        "D" => "Resignado/a",
                        "A" => "Franco/a",
                        "B" => "Amistoso/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Imparcial",
                        "A" => "Calmado/a",
                        "B" => "Influyente",
                        "D" => "Dinámico/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperativo",
                        "A" => "Constante",
                        "B" => "Impaciente",
                        "D" => "Sociable",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "ESafe",
                        "A" => "Delicateo",
                        "C" => "Persuasivo/a",
                        "D" => "Extrovertido/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Colaborador/a",
                        "D" => "Obstinado/a",
                        "A" => "Atractivo/a",
                        "C" => "Simpático/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Comprensivo/a",
                        "D" => "Tolerante/a",
                        "A" => "Seguro/a",
                        "C" => "Abierto/a",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Servicial",
                        "A" => "Respetuoso/a",
                        "B" => "Emprendedor/a",
                        "C" => "Optimista",
                    ],
                    "result" => "F5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Considerado/a",
                        "D" => "Firme",
                        "A" => "Exacto/a",
                        "B" => "Seguro de sí mismo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "D" => "Tenaz",
                        "B" => "Reflexivo/a",
                        "A" => "Inspirador",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Generoso/a",
                        "A" => "Agresivo/a",
                        "B" => "Prudente",
                        "C" => "Encantador",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Buen vecino",
                        "A" => "Determinado/a",
                        "C" => "Metódico/a",
                        "D" => "Juguetón",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sabe escuchar",
                        "A" => "Firme",
                        "C" => "Disciplinado/a",
                        "B" => "Alentador/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Simpático/a",
                        "A" => "Audaz",
                        "C" => "De voz suave",
                        "B" => "Divertido/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Ecuánime",
                        "B" => "Competitivo/a",
                        "A" => "Cuidadoso/a",
                        "C" => "Entusiasta",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "ESafe",
                        "B" => "Pionero",
                        "A" => "Controlado/a",
                        "C" => "Con aplomo (Seductor/a)",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Compasivo/a",
                        "C" => "Atrevido/a",
                        "B" => "Correcto/a",
                        "D" => "Expresivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "B" => "Con carácter",
                        "D" => "Conservador",
                        "A" => "Acomodadizo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomático/a",
                        "A" => "Franco/a",
                        "D" => "Resignado/a",
                        "B" => "Amistoso/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Imparcial",
                        "D" => "Dinámico/a",
                        "A" => "Calmado/a",
                        "B" => "Influyente",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperativo",
                        "B" => "Impaciente",
                        "A" => "Constante",
                        "D" => "Sociable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "ESafe",
                        "C" => "Persuasivo/a",
                        "A" => "Delicateo",
                        "D" => "Extrovertido/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Colaborador/a",
                        "A" => "Atractivo/a",
                        "D" => "Obstinado/a",
                        "C" => "Simpático/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Comprensivo/a",
                        "A" => "Seguro/a",
                        "D" => "Tolerante/a",
                        "C" => "Abierto/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Servicial",
                        "B" => "Emprendedor/a",
                        "A" => "Respetuoso/a",
                        "C" => "Optimista",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Considerado/a",
                        "D" => "Firme",
                        "B" => "Seguro de sí mismo/a",
                        "A" => "Exacto/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "D" => "Tenaz",
                        "A" => "Inspirador",
                        "B" => "Reflexivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Generoso/a",
                        "A" => "Agresivo/a",
                        "C" => "Encantador",
                        "B" => "Prudente",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Buen vecino",
                        "A" => "Determinado/a",
                        "D" => "Juguetón",
                        "C" => "Metódico/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sabe escuchar",
                        "A" => "Firme",
                        "B" => "Alentador/a",
                        "C" => "Disciplinado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Simpático/a",
                        "A" => "Audaz",
                        "B" => "Divertido/a",
                        "C" => "De voz suave",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Ecuánime",
                        "B" => "Competitivo/a",
                        "C" => "Entusiasta",
                        "A" => "Cuidadoso/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "ESafe",
                        "B" => "Pionero",
                        "C" => "Con aplomo (Seductor/a)",
                        "A" => "Controlado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Compasivo/a",
                        "C" => "Atrevido/a",
                        "D" => "Expresivo/a",
                        "B" => "Correcto/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Moderado/a",
                        "B" => "Con carácter",
                        "A" => "Acomodadizo/a",
                        "D" => "Conservador",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomático/a",
                        "A" => "Franco/a",
                        "B" => "Amistoso/a",
                        "D" => "Conservador",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Imparcial",
                        "D" => "Dinámico/a",
                        "B" => "Influyente",
                        "A" => "Calmado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperativo",
                        "B" => "Impaciente",
                        "D" => "Sociable",
                        "A" => "Constante",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "ESafe",
                        "C" => "Persuasivo/a",
                        "D" => "Extrovertido/a",
                        "A" => "Delicateo",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Colaborador/a",
                        "A" => "Atractivo/a",
                        "C" => "Simpático/a",
                        "D" => "Obstinado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Comprensivo/a",
                        "A" => "Seguro/a",
                        "C" => "Abierto/a",
                        "D" => "Tolerante/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Servicial",
                        "B" => "Emprendedor/a",
                        "C" => "Optimista",
                        "A" => "Respetuoso/a",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exacto/a",
                        "D" => "Firme",
                        "C" => "Considerado/a",
                        "B" => "Seguro de sí mismo/a",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "B" => "Reflexivo/a",
                        "D" => "Tenaz",
                        "C" => "Moderado/a",
                        "A" => "Inspirador",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "B" => "Prudente",
                        "A" => "Agresivo/a",
                        "D" => "Generoso/a",
                        "C" => "Encantador",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "C" => "Metódico/a",
                        "A" => "Determinado/a",
                        "B" => "Buen vecino",
                        "D" => "Juguetón",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplinado/a",
                        "A" => "Firme",
                        "D" => "Sabe escuchar",
                        "B" => "Alentador/a",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "C" => "De voz suave",
                        "A" => "Audaz",
                        "D" => "Simpático/a",
                        "B" => "Divertido/a",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Cuidadoso/a",
                        "B" => "Competitivo/a",
                        "D" => "Ecuánime",
                        "C" => "Entusiasta",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Controlado/a",
                        "B" => "Pionero",
                        "D" => "ESafe",
                        "C" => "Con aplomo (Seductor/a)",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "B" => "Correcto/a",
                        "C" => "Atrevido/a",
                        "A" => "Compasivo/a",
                        "D" => "Expresivo/a",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Conservador",
                        "B" => "Con carácter",
                        "C" => "Moderado/a",
                        "A" => "Acomodadizo/a",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Resignado/a",
                        "A" => "Franco/a",
                        "C" => "Diplomático/a",
                        "B" => "Amistoso/a",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Calmado/a",
                        "D" => "Dinámico/a",
                        "C" => "Imparcial",
                        "B" => "Influyente",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Constante",
                        "B" => "Impaciente",
                        "C" => "Cooperativo",
                        "D" => "Sociable",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Delicateo",
                        "C" => "Persuasivo/a",
                        "B" => "ESafe",
                        "D" => "Extrovertido/a",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Obstinado/a",
                        "A" => "Atractivo/a",
                        "B" => "Colaborador/a",
                        "C" => "Simpático/a",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerante/a",
                        "A" => "Seguro/a",
                        "B" => "Comprensivo/a",
                        "C" => "Abierto/a",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Respetuoso/a",
                        "B" => "Emprendedor/a",
                        "D" => "Servicial",
                        "C" => "Optimista",
                    ],
                    "result" => "C10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exacto/a",
                        "D" => "Firme",
                        "B" => "Seguro de sí mismo/a",
                        "C" => "Considerado/a",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "B" => "Reflexivo/a",
                        "D" => "Tenaz",
                        "A" => "Inspirador",
                        "C" => "Moderado/a",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "B" => "Prudente",
                        "A" => "Agresivo/a",
                        "C" => "Encantador",
                        "D" => "Generoso/a",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "C" => "Metódico/a",
                        "A" => "Determinado/a",
                        "D" => "Juguetón",
                        "B" => "Buen vecino",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplinado/a",
                        "A" => "Firme",
                        "B" => "Alentador/a",
                        "D" => "Sabe escuchar",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "C" => "De voz suave",
                        "A" => "Audaz",
                        "B" => "Divertido/a",
                        "D" => "Simpático/a",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Cuidadoso/a",
                        "B" => "Competitivo/a",
                        "C" => "Entusiasta",
                        "D" => "Ecuánime",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Controlado/a",
                        "B" => "Pionero",
                        "C" => "Con aplomo (Seductor/a)",
                        "D" => "ESafe",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "B" => "Correcto/a",
                        "C" => "Atrevido/a",
                        "D" => "Expresivo/a",
                        "A" => "Compasivo/a",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Conservador",
                        "B" => "Con carácter",
                        "A" => "Acomodadizo/a",
                        "C" => "Moderado/a",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Resignado/a",
                        "A" => "Franco/a",
                        "B" => "Amistoso/a",
                        "C" => "Diplomático/a",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Calmado/a",
                        "D" => "Dinámico/a",
                        "B" => "Influyente",
                        "C" => "Imparcial",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Constante",
                        "B" => "Impaciente",
                        "D" => "Sociable",
                        "C" => "Cooperativo",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Delicateo",
                        "C" => "Persuasivo/a",
                        "D" => "Extrovertido/a",
                        "B" => "ESafe",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Obstinado/a",
                        "A" => "Atractivo/a",
                        "C" => "Simpático/a",
                        "B" => "Colaborador/a",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerante/a",
                        "A" => "Seguro/a",
                        "C" => "Abierto/a",
                        "B" => "Comprensivo/a",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Respetuoso/a",
                        "B" => "Emprendedor/a",
                        "C" => "Optimista",
                        "D" => "Servicial",
                    ],
                    "result" => "C9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exacto/a",
                        "C" => "Considerado/a",
                        "D" => "Firme",
                        "B" => "Seguro de sí mismo/a",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "B" => "Reflexivo/a",
                        "C" => "Moderado/a",
                        "D" => "Tenaz",
                        "A" => "Inspirador",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "B" => "Prudente",
                        "D" => "Generoso/a",
                        "A" => "Agresivo/a",
                        "C" => "Encantador",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "C" => "Metódico/a",
                        "B" => "Buen vecino",
                        "A" => "Determinado/a",
                        "D" => "Juguetón",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplinado/a",
                        "D" => "Sabe escuchar",
                        "A" => "Firme",
                        "B" => "Alentador/a",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "C" => "De voz suave",
                        "D" => "Simpático/a",
                        "A" => "Audaz",
                        "B" => "Divertido/a",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Cuidadoso/a",
                        "D" => "Ecuánime",
                        "B" => "Competitivo/a",
                        "C" => "Entusiasta",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Controlado/a",
                        "D" => "ESafe",
                        "B" => "Pionero",
                        "C" => "Con aplomo (Seductor/a)",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "B" => "Correcto/a",
                        "A" => "Compasivo/a",
                        "C" => "Atrevido/a",
                        "D" => "Expresivo/a",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Conservador",
                        "C" => "Moderado/a",
                        "B" => "Con carácter",
                        "A" => "Acomodadizo/a",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Resignado/a",
                        "C" => "Diplomático/a",
                        "A" => "Franco/a",
                        "B" => "Amistoso/a",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Calmado/a",
                        "C" => "Imparcial",
                        "D" => "Dinámico/a",
                        "B" => "Influyente",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Constante",
                        "C" => "Cooperativo",
                        "B" => "Impaciente",
                        "D" => "Sociable",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Delicateo",
                        "B" => "ESafe",
                        "C" => "Persuasivo/a",
                        "D" => "Extrovertido/a",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Obstinado/a",
                        "B" => "Colaborador/a",
                        "A" => "Atractivo/a",
                        "C" => "Simpático/a",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerante/a",
                        "B" => "Comprensivo/a",
                        "A" => "Seguro/a",
                        "C" => "Abierto/a",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Respetuoso/a",
                        "D" => "Servicial",
                        "B" => "Emprendedor/a",
                        "C" => "Optimista",
                    ],
                    "result" => "C7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exacto/a",
                        "C" => "Considerado/a",
                        "B" => "Seguro de sí mismo/a",
                        "D" => "Firme",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "B" => "Reflexivo/a",
                        "C" => "Moderado/a",
                        "A" => "Inspirador",
                        "D" => "Tenaz",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "B" => "Prudente",
                        "D" => "Generoso/a",
                        "C" => "Encantador",
                        "A" => "Agresivo/a",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "C" => "Metódico/a",
                        "B" => "Buen vecino",
                        "D" => "Juguetón",
                        "A" => "Determinado/a",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplinado/a",
                        "D" => "Sabe escuchar",
                        "B" => "Alentador/a",
                        "A" => "Firme",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "C" => "De voz suave",
                        "D" => "Simpático/a",
                        "B" => "Divertido/a",
                        "A" => "Audaz",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Cuidadoso/a",
                        "D" => "Ecuánime",
                        "C" => "Entusiasta",
                        "B" => "Competitivo/a",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Controlado/a",
                        "D" => "ESafe",
                        "C" => "Con aplomo (Seductor/a)",
                        "B" => "Pionero",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "B" => "Correcto/a",
                        "A" => "Compasivo/a",
                        "D" => "Expresivo/a",
                        "C" => "Atrevido/a",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Conservador",
                        "C" => "Moderado/a",
                        "A" => "Acomodadizo/a",
                        "B" => "Con carácter",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Resignado/a",
                        "C" => "Diplomático/a",
                        "B" => "Amistoso/a",
                        "A" => "Franco/a",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Calmado/a",
                        "C" => "Imparcial",
                        "B" => "Influyente",
                        "D" => "Dinámico/a",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Constante",
                        "C" => "Cooperativo",
                        "D" => "Sociable",
                        "B" => "Impaciente",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Delicateo",
                        "B" => "ESafe",
                        "D" => "Extrovertido/a",
                        "C" => "Persuasivo/a",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Obstinado/a",
                        "B" => "Colaborador/a",
                        "C" => "Simpático/a",
                        "A" => "Atractivo/a",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerante/a",
                        "B" => "Comprensivo/a",
                        "C" => "Abierto/a",
                        "A" => "Seguro/a",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Respetuoso/a",
                        "D" => "Servicial",
                        "C" => "Optimista",
                        "B" => "Emprendedor/a",
                    ],
                    "result" => "C5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exacto/a",
                        "B" => "Seguro de sí mismo/a",
                        "D" => "Firme",
                        "C" => "Considerado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Reflexivo/a",
                        "A" => "Inspirador",
                        "D" => "Tenaz",
                        "C" => "Moderado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Prudente",
                        "C" => "Encantador",
                        "A" => "Agresivo/a",
                        "D" => "Generoso/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Metódico/a",
                        "D" => "Juguetón",
                        "A" => "Determinado/a",
                        "B" => "Buen vecino",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplinado/a",
                        "B" => "Alentador/a",
                        "A" => "Firme",
                        "D" => "Sabe escuchar",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "De voz suave",
                        "B" => "Divertido/a",
                        "A" => "Audaz",
                        "D" => "Simpático/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Cuidadoso/a",
                        "C" => "Entusiasta",
                        "B" => "Competitivo/a",
                        "D" => "Ecuánime",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Controlado/a",
                        "C" => "Con aplomo (Seductor/a)",
                        "B" => "Pionero",
                        "D" => "ESafe",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Correcto/a",
                        "D" => "Expresivo/a",
                        "C" => "Atrevido/a",
                        "A" => "Compasivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Conservador",
                        "A" => "Acomodadizo/a",
                        "B" => "Con carácter",
                        "C" => "Moderado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Resignado/a",
                        "B" => "Amistoso/a",
                        "A" => "Franco/a",
                        "C" => "Diplomático/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Calmado/a",
                        "B" => "Influyente",
                        "D" => "Dinámico/a",
                        "C" => "Imparcial",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Constante",
                        "D" => "Sociable",
                        "B" => "Impaciente",
                        "C" => "Cooperativo",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Delicateo",
                        "D" => "Extrovertido/a",
                        "C" => "Persuasivo/a",
                        "B" => "ESafe",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Obstinado/a",
                        "C" => "Simpático/a",
                        "A" => "Atractivo/a",
                        "B" => "Colaborador/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerante/a",
                        "C" => "Abierto/a",
                        "A" => "Seguro/a",
                        "B" => "Comprensivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Respetuoso/a",
                        "C" => "Optimista",
                        "B" => "Emprendedor/a",
                        "D" => "Servicial",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exacto/a",
                        "B" => "Seguro de sí mismo/a",
                        "C" => "Considerado/a",
                        "D" => "Firme",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Reflexivo/a",
                        "A" => "Inspirador",
                        "C" => "Moderado/a",
                        "D" => "Tenaz",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Prudente",
                        "C" => "Encantador",
                        "D" => "Generoso/a",
                        "A" => "Agresivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Metódico/a",
                        "D" => "Juguetón",
                        "B" => "Buen vecino",
                        "A" => "Determinado/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplinado/a",
                        "B" => "Alentador/a",
                        "D" => "Sabe escuchar",
                        "A" => "Firme",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "De voz suave",
                        "B" => "Divertido/a",
                        "D" => "Simpático/a",
                        "A" => "Audaz",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Cuidadoso/a",
                        "C" => "Entusiasta",
                        "D" => "Ecuánime",
                        "B" => "Competitivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Controlado/a",
                        "C" => "Con aplomo (Seductor/a)",
                        "D" => "ESafe",
                        "B" => "Pionero",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Correcto/a",
                        "D" => "Expresivo/a",
                        "A" => "Compasivo/a",
                        "C" => "Atrevido/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Conservador",
                        "A" => "Acomodadizo/a",
                        "C" => "Moderado/a",
                        "B" => "Con carácter",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Resignado/a",
                        "B" => "Amistoso/a",
                        "C" => "Diplomático/a",
                        "A" => "Franco/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Calmado/a",
                        "B" => "Influyente",
                        "C" => "Imparcial",
                        "D" => "Dinámico/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Constante",
                        "D" => "Sociable",
                        "C" => "Cooperativo",
                        "B" => "Impaciente",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Delicateo",
                        "D" => "Extrovertido/a",
                        "B" => "ESafe",
                        "C" => "Persuasivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Obstinado/a",
                        "C" => "Simpático/a",
                        "B" => "Colaborador/a",
                        "A" => "Atractivo/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerante/a",
                        "C" => "Abierto/a",
                        "B" => "Comprensivo/a",
                        "A" => "Seguro/a",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Respetuoso/a",
                        "C" => "Optimista",
                        "D" => "Servicial",
                        "B" => "Emprendedor/a",
                    ],
                    "result" => "indefinido"
                ],
            ]
        ];
    }

    private function getAnswersInEnglish()
    {
        return [
            [
                [
                    "meaning" => [
                        "B" => "Confident",
                        "D" => "Decisive",
                        "C" => "Thoughtfull",
                        "A" => "Precise",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "A" => "Inspiring",
                        "D" => "Strong-willed",
                        "C" => "Controlled",
                        "B" => "Self-analyzing",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Charming",
                        "A" => "Combative",
                        "D" => "Magnanimous",
                        "B" => "Cautious",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Playful",
                        "A" => "Determined",
                        "B" => "Good-natured",
                        "C" => "Meticulous",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Pleasant",
                        "A" => "Self-assertive",
                        "D" => "Good Listener",
                        "C" => "Disciplined",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Entertaining",
                        "A" => "Impactful",
                        "D" => "Sensitive",
                        "C" => "Soft Spoken",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Enthusiastic",
                        "B" => "Competitive",
                        "D" => "Even-tempered",
                        "A" => "Attentive",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Poised",
                        "B" => "Pioneering",
                        "D" => "Safe",
                        "A" => "Controlled",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Expressive",
                        "C" => "Daring",
                        "A" => "Compassionate",
                        "B" => "Correct",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "A" => "Adaptable",
                        "B" => "Forceful",
                        "C" => "Restrained",
                        "D" => "Talkative",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Friendly",
                        "A" => "Outspoken",
                        "C" => "Diplomatic",
                        "D" => "Accepting",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Influential",
                        "D" => "Vigorous",
                        "C" => "Neutral",
                        "A" => "Calm",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Convivial",
                        "B" => "Impatient",
                        "C" => "Cooperative",
                        "A" => "Constant",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverted",
                        "C" => "Persuasive",
                        "B" => "Stable",
                        "A" => "Delicate",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathetic",
                        "A" => "Attractive",
                        "B" => "Collaborator",
                        "D" => "Stubborn",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Open Mind",
                        "A" => "Self-confident",
                        "B" => "Comprehensive",
                        "D" => "Tolerant",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Optimistic",
                        "B" => "Entrepreneur",
                        "D" => "Helpful",
                        "A" => "Respectful",
                    ],
                    "result" => "P10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confident",
                        "D" => "Decisive",
                        "A" => "Precise",
                        "C" => "Thoughtfull",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "A" => "Inspiring",
                        "D" => "Strong-willed",
                        "B" => "Self-analyzing",
                        "C" => "Controlled",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Charming",
                        "A" => "Combative",
                        "B" => "Cautious",
                        "D" => "Magnanimous",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Playful",
                        "A" => "Determined",
                        "C" => "Meticulous",
                        "B" => "Good-natured",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Pleasant",
                        "A" => "Self-assertive",
                        "C" => "Disciplined",
                        "D" => "Good Listener",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Entertaining",
                        "A" => "Impactful",
                        "C" => "Soft Spoken",
                        "D" => "Sensitive",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Enthusiastic",
                        "B" => "Competitive",
                        "A" => "Attentive",
                        "D" => "Even-tempered",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Poised",
                        "B" => "Pioneering",
                        "A" => "Controlled",
                        "D" => "Safe",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Expressive",
                        "C" => "Daring",
                        "B" => "Correct",
                        "A" => "Compassionate",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "A" => "Adaptable",
                        "B" => "Forceful",
                        "D" => "Talkative",
                        "C" => "Restrained",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Friendly",
                        "A" => "Outspoken",
                        "D" => "Accepting",
                        "C" => "Diplomatic",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Influential",
                        "D" => "Vigorous",
                        "A" => "Calm",
                        "C" => "Neutral",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Convivial",
                        "B" => "Impatient",
                        "A" => "Constant",
                        "C" => "Cooperative",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverted",
                        "C" => "Persuasive",
                        "A" => "With a lot of tact",
                        "B" => "Stable",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathetic",
                        "A" => "Attractive",
                        "D" => "Stubborn",
                        "B" => "Collaborator",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Open Mind",
                        "A" => "Self-confident",
                        "D" => "Tolerant",
                        "B" => "Comprehensive",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Optimistic",
                        "B" => "Entrepreneur",
                        "A" => "Respectful",
                        "D" => "Helpful",
                    ],
                    "result" => "P9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confident",
                        "C" => "Thoughtfull",
                        "D" => "Decisive",
                        "A" => "Precise",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "A" => "Inspiring",
                        "C" => "Controlled",
                        "D" => "Strong-willed",
                        "B" => "Self-analyzing",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Charming",
                        "D" => "Magnanimous",
                        "A" => "Combative",
                        "B" => "Cautious",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Playful",
                        "B" => "Good-natured",
                        "A" => "Determined",
                        "C" => "Meticulous",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Pleasant",
                        "D" => "Good Listener",
                        "A" => "Self-assertive",
                        "C" => "Disciplined",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Entertaining",
                        "D" => "Sensitive",
                        "A" => "Impactful",
                        "C" => "Soft Spoken",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Enthusiastic",
                        "D" => "Even-tempered",
                        "B" => "Competitive",
                        "A" => "Attentive",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Poised",
                        "D" => "Safe",
                        "B" => "Pioneering",
                        "A" => "Controlled",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Expressive",
                        "A" => "Compassionate",
                        "C" => "Daring",
                        "B" => "Correct",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "A" => "Adaptable",
                        "C" => "Restrained",
                        "B" => "Forceful",
                        "D" => "Talkative",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Friendly",
                        "C" => "Diplomatic",
                        "A" => "Outspoken",
                        "D" => "Accepting",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Influential",
                        "C" => "Neutral",
                        "D" => "Vigorous",
                        "A" => "Calm",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Convivial",
                        "C" => "Cooperative",
                        "B" => "Impatient",
                        "A" => "Constant",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverted",
                        "B" => "Stable",
                        "C" => "Persuasive",
                        "A" => "With a lot of tact",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathetic",
                        "B" => "Collaborator",
                        "A" => "Attractive",
                        "D" => "Stubborn",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Open Mind",
                        "B" => "Comprehensive",
                        "A" => "Self-confident",
                        "D" => "Tolerant",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Optimistic",
                        "D" => "Helpful",
                        "B" => "Entrepreneur",
                        "A" => "Respectful",
                    ],
                    "result" => "P7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confident",
                        "C" => "Thoughtfull",
                        "A" => "Precise",
                        "D" => "Decisive",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "A" => "Inspiring",
                        "C" => "Controlled",
                        "B" => "Self-analyzing",
                        "D" => "Strong-willed",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Charming",
                        "D" => "Magnanimous",
                        "B" => "Cautious",
                        "A" => "Combative",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Playful",
                        "B" => "Good-natured",
                        "C" => "Meticulous",
                        "A" => "Determined",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Pleasant",
                        "D" => "Good Listener",
                        "C" => "Disciplined",
                        "A" => "Self-assertive",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Entertaining",
                        "D" => "Sensitive",
                        "C" => "Soft Spoken",
                        "A" => "Impactful",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Enthusiastic",
                        "D" => "Even-tempered",
                        "A" => "Attentive",
                        "B" => "Competitive",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Poised",
                        "D" => "Safe",
                        "A" => "Controlled",
                        "B" => "Pioneering",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Expressive",
                        "A" => "Compassionate",
                        "B" => "Correct",
                        "C" => "Daring",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "A" => "Adaptable",
                        "C" => "Restrained",
                        "D" => "Talkative",
                        "B" => "Forceful",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Friendly",
                        "C" => "Diplomatic",
                        "D" => "Accepting",
                        "A" => "Outspoken",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Influential",
                        "C" => "Neutral",
                        "A" => "Calm",
                        "D" => "Vigorous",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Convivial",
                        "C" => "Cooperative",
                        "A" => "Constant",
                        "B" => "Impatient",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverted",
                        "B" => "Stable",
                        "A" => "With a lot of tact",
                        "C" => "Persuasive",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathetic",
                        "B" => "Collaborator",
                        "D" => "Stubborn",
                        "A" => "Attractive",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Open Mind",
                        "B" => "Comprehensive",
                        "D" => "Tolerant",
                        "A" => "Self-confident",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Optimistic",
                        "D" => "Helpful",
                        "A" => "Respectful",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "P5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confident",
                        "A" => "Precise",
                        "C" => "Thoughtfull",
                        "D" => "Decisive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Inspiring",
                        "B" => "Self-analyzing",
                        "C" => "Controlled",
                        "D" => "Strong-willed",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Charming",
                        "B" => "Cautious",
                        "D" => "Magnanimous",
                        "A" => "Combative",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Playful",
                        "C" => "Meticulous",
                        "B" => "Good-natured",
                        "A" => "Determined",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Pleasant",
                        "C" => "Disciplined",
                        "D" => "Good Listener",
                        "A" => "Self-assertive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Entertaining",
                        "C" => "Soft Spoken",
                        "D" => "Sensitive",
                        "A" => "Impactful",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Enthusiastic",
                        "A" => "Attentive",
                        "D" => "Even-tempered",
                        "B" => "Competitive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Poised",
                        "A" => "Controlled",
                        "D" => "Safe",
                        "B" => "Pioneering",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Expressive",
                        "B" => "Correct",
                        "A" => "Compassionate",
                        "C" => "Daring",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Adaptable",
                        "D" => "Talkative",
                        "C" => "Restrained",
                        "B" => "Forceful",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Friendly",
                        "D" => "Accepting",
                        "C" => "Diplomatic",
                        "A" => "Outspoken",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Influential",
                        "A" => "Calm",
                        "C" => "Neutral",
                        "D" => "Vigorous",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Convivial",
                        "A" => "Constant",
                        "C" => "Cooperative",
                        "B" => "Impatient",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverted",
                        "A" => "With a lot of tact",
                        "B" => "Stable",
                        "C" => "Persuasive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathetic",
                        "D" => "Stubborn",
                        "B" => "Collaborator",
                        "A" => "Attractive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Open Mind",
                        "D" => "Tolerant",
                        "B" => "Comprehensive",
                        "A" => "Self-confident",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Optimistic",
                        "A" => "Respectful",
                        "D" => "Helpful",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confident",
                        "A" => "Precise",
                        "D" => "Decisive",
                        "C" => "Thoughtfull",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Inspiring",
                        "B" => "Self-analyzing",
                        "D" => "Strong-willed",
                        "C" => "Controlled",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Charming",
                        "B" => "Cautious",
                        "A" => "Combative",
                        "D" => "Magnanimous",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Playful",
                        "C" => "Meticulous",
                        "A" => "Determined",
                        "B" => "Good-natured",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Pleasant",
                        "C" => "Disciplined",
                        "A" => "Self-assertive",
                        "D" => "Good Listener",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Entertaining",
                        "C" => "Soft Spoken",
                        "A" => "Impactful",
                        "D" => "Sensitive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Enthusiastic",
                        "A" => "Attentive",
                        "B" => "Competitive",
                        "D" => "Even-tempered",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Poised",
                        "A" => "Controlled",
                        "B" => "Pioneering",
                        "D" => "Safe",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Expressive",
                        "B" => "Correct",
                        "C" => "Daring",
                        "A" => "Compassionate",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Adaptable",
                        "D" => "Talkative",
                        "B" => "Forceful",
                        "C" => "Restrained",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Friendly",
                        "D" => "Accepting",
                        "A" => "Outspoken",
                        "C" => "Diplomatic",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Influential",
                        "A" => "Calm",
                        "D" => "Vigorous",
                        "C" => "Neutral",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Convivial",
                        "A" => "Constant",
                        "B" => "Impatient",
                        "C" => "Cooperative",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverted",
                        "A" => "With a lot of tact",
                        "C" => "Persuasive",
                        "B" => "Stable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathetic",
                        "D" => "Stubborn",
                        "A" => "Attractive",
                        "B" => "Collaborator",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Open Mind",
                        "D" => "Tolerant",
                        "A" => "Self-confident",
                        "B" => "Comprehensive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Optimistic",
                        "A" => "Respectful",
                        "B" => "Entrepreneur",
                        "D" => "Helpful",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Decisive",
                        "B" => "Confident",
                        "C" => "Thoughtfull",
                        "A" => "Precise",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "D" => "Strong-willed",
                        "A" => "Inspiring",
                        "C" => "Controlled",
                        "B" => "Self-analyzing",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Combative",
                        "C" => "Charming",
                        "D" => "Magnanimous",
                        "B" => "Cautious",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Determined",
                        "D" => "Playful",
                        "B" => "Good-natured",
                        "C" => "Meticulous",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Self-assertive",
                        "B" => "Pleasant",
                        "D" => "Good Listener",
                        "C" => "Disciplined",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Impactful",
                        "B" => "Entertaining",
                        "D" => "Sensitive",
                        "C" => "Soft Spoken",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Competitive",
                        "C" => "Enthusiastic",
                        "D" => "Even-tempered",
                        "A" => "Attentive",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Pioneering",
                        "C" => "Poised",
                        "D" => "Safe",
                        "A" => "Controlled",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "C" => "Daring",
                        "D" => "Expressive",
                        "A" => "Compassionate",
                        "B" => "Correct",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Forceful",
                        "A" => "Adaptable",
                        "C" => "Restrained",
                        "D" => "Talkative",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Outspoken",
                        "B" => "Friendly",
                        "C" => "Diplomatic",
                        "D" => "Accepting",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "D" => "Vigorous",
                        "B" => "Influential",
                        "C" => "Neutral",
                        "A" => "Calm",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "D" => "Convivial",
                        "C" => "Cooperative",
                        "A" => "Constant",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasive",
                        "D" => "Extroverted",
                        "B" => "Stable",
                        "A" => "With a lot of tact",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Attractive",
                        "C" => "Sympathetic",
                        "B" => "Collaborator",
                        "D" => "Stubborn",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Self-confident",
                        "C" => "Open Mind",
                        "B" => "Comprehensive",
                        "D" => "Tolerant",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "C" => "Optimistic",
                        "D" => "Helpful",
                        "A" => "Respectful",
                    ],
                    "result" => "H10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Decisive",
                        "B" => "Confident",
                        "A" => "Precise",
                        "C" => "Thoughtfull",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "D" => "Strong-willed",
                        "A" => "Inspiring",
                        "B" => "Self-analyzing",
                        "C" => "Controlled",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Combative",
                        "C" => "Charming",
                        "B" => "Cautious",
                        "D" => "Magnanimous",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Determined",
                        "D" => "Playful",
                        "C" => "Meticulous",
                        "B" => "Good-natured",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Self-assertive",
                        "B" => "Pleasant",
                        "C" => "Disciplined",
                        "D" => "Good Listener",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Impactful",
                        "B" => "Entertaining",
                        "C" => "Soft Spoken",
                        "D" => "Sensitive",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Competitive",
                        "C" => "Enthusiastic",
                        "A" => "Attentive",
                        "D" => "Even-tempered",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Pioneering",
                        "C" => "Poised",
                        "A" => "Controlled",
                        "D" => "Safe",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "C" => "Daring",
                        "D" => "Expressive",
                        "B" => "Correct",
                        "A" => "Compassionate",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Forceful",
                        "A" => "Adaptable",
                        "D" => "Talkative",
                        "C" => "Restrained",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Outspoken",
                        "B" => "Friendly",
                        "D" => "Accepting",
                        "C" => "Diplomatic",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "D" => "Vigorous",
                        "B" => "Influential",
                        "A" => "Calm",
                        "C" => "Neutral",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "D" => "Convivial",
                        "A" => "Constant",
                        "C" => "Cooperative",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasive",
                        "D" => "Extroverted",
                        "A" => "With a lot of tact",
                        "B" => "Stable",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Attractive",
                        "C" => "Sympathetic",
                        "D" => "Stubborn",
                        "B" => "Collaborator",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Self-confident",
                        "C" => "Open Mind",
                        "D" => "Tolerant",
                        "B" => "Comprehensive",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "C" => "Optimistic",
                        "A" => "Respectful",
                        "D" => "Helpful",
                    ],
                    "result" => "H9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Decisive",
                        "A" => "Precise",
                        "C" => "Thoughtfull",
                        "B" => "Confident",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "D" => "Strong-willed",
                        "B" => "Self-analyzing",
                        "C" => "Controlled",
                        "A" => "Inspiring",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Combative",
                        "B" => "Cautious",
                        "D" => "Magnanimous",
                        "C" => "Charming",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Determined",
                        "C" => "Meticulous",
                        "B" => "Good-natured",
                        "D" => "Playful",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Self-assertive",
                        "C" => "Disciplined",
                        "D" => "Good Listener",
                        "B" => "Pleasant",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Impactful",
                        "C" => "Soft Spoken",
                        "D" => "Sensitive",
                        "B" => "Entertaining",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Competitive",
                        "A" => "Attentive",
                        "D" => "Even-tempered",
                        "C" => "Enthusiastic",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Pioneering",
                        "A" => "Controlled",
                        "D" => "Safe",
                        "C" => "Poised",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "C" => "Daring",
                        "B" => "Correct",
                        "A" => "Compassionate",
                        "D" => "Expressive",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Forceful",
                        "D" => "Talkative",
                        "C" => "Restrained",
                        "A" => "Adaptable",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Outspoken",
                        "D" => "Accepting",
                        "C" => "Diplomatic",
                        "B" => "Friendly",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "D" => "Vigorous",
                        "A" => "Calm",
                        "C" => "Neutral",
                        "B" => "Influential",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "A" => "Constant",
                        "C" => "Cooperative",
                        "D" => "Convivial",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasive",
                        "A" => "With a lot of tact",
                        "B" => "Stable",
                        "D" => "Extroverted",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Attractive",
                        "D" => "Stubborn",
                        "B" => "Collaborator",
                        "C" => "Sympathetic",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Self-confident",
                        "D" => "Tolerant",
                        "B" => "Comprehensive",
                        "C" => "Open Mind",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "A" => "Respectful",
                        "D" => "Helpful",
                        "C" => "Optimistic",
                    ],
                    "result" => "H7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Decisive",
                        "A" => "Precise",
                        "B" => "Confident",
                        "C" => "Thoughtfull",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "D" => "Strong-willed",
                        "B" => "Self-analyzing",
                        "A" => "Inspiring",
                        "C" => "Controlled",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Combative",
                        "B" => "Cautious",
                        "C" => "Charming",
                        "D" => "Magnanimous",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Determined",
                        "C" => "Meticulous",
                        "D" => "Playful",
                        "B" => "Good-natured",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Self-assertive",
                        "C" => "Disciplined",
                        "B" => "Pleasant",
                        "D" => "Good Listener",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Impactful",
                        "C" => "Soft Spoken",
                        "B" => "Entertaining",
                        "D" => "Sensitive",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Competitive",
                        "A" => "Attentive",
                        "C" => "Enthusiastic",
                        "D" => "Even-tempered",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Pioneering",
                        "A" => "Controlled",
                        "C" => "Poised",
                        "D" => "Safe",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "C" => "Daring",
                        "B" => "Correct",
                        "D" => "Expressive",
                        "A" => "Compassionate",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Forceful",
                        "D" => "Talkative",
                        "A" => "Adaptable",
                        "C" => "Restrained",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Outspoken",
                        "D" => "Accepting",
                        "B" => "Friendly",
                        "C" => "Diplomatic",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "D" => "Vigorous",
                        "A" => "Calm",
                        "B" => "Influential",
                        "C" => "Neutral",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "A" => "Constant",
                        "D" => "Convivial",
                        "C" => "Cooperative",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasive",
                        "A" => "With a lot of tact",
                        "D" => "Extroverted",
                        "B" => "Stable",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Attractive",
                        "D" => "Stubborn",
                        "C" => "Sympathetic",
                        "B" => "Collaborator",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Self-confident",
                        "D" => "Tolerant",
                        "C" => "Open Mind",
                        "B" => "Comprehensive",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "A" => "Respectful",
                        "C" => "Optimistic",
                        "D" => "Helpful",
                    ],
                    "result" => "H5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Decisive",
                        "C" => "Thoughtfull",
                        "B" => "Confident",
                        "A" => "Precise",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Strong-willed",
                        "C" => "Controlled",
                        "A" => "Inspiring",
                        "B" => "Self-analyzing",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Combative",
                        "D" => "Magnanimous",
                        "C" => "Charming",
                        "B" => "Cautious",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Determined",
                        "B" => "Good-natured",
                        "D" => "Playful",
                        "C" => "Meticulous",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Self-assertive",
                        "D" => "Good Listener",
                        "B" => "Pleasant",
                        "C" => "Disciplined",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Impactful",
                        "D" => "Sensitive",
                        "B" => "Entertaining",
                        "C" => "Soft Spoken",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Competitive",
                        "D" => "Even-tempered",
                        "C" => "Enthusiastic",
                        "A" => "Attentive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Pioneering",
                        "D" => "Safe",
                        "C" => "Poised",
                        "A" => "Controlled",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Daring",
                        "A" => "Compassionate",
                        "D" => "Expressive",
                        "B" => "Correct",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Forceful",
                        "C" => "Restrained",
                        "A" => "Adaptable",
                        "D" => "Talkative",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Outspoken",
                        "C" => "Diplomatic",
                        "B" => "Friendly",
                        "D" => "Accepting",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Vigorous",
                        "C" => "Neutral",
                        "B" => "Influential",
                        "A" => "Calm",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "C" => "Cooperative",
                        "D" => "Convivial",
                        "A" => "Constant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasive",
                        "B" => "Stable",
                        "D" => "Extroverted",
                        "A" => "With a lot of tact",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Attractive",
                        "B" => "Collaborator",
                        "C" => "Sympathetic",
                        "D" => "Stubborn",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Self-confident",
                        "B" => "Comprehensive",
                        "C" => "Open Mind",
                        "D" => "Tolerant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "D" => "Helpful",
                        "C" => "Optimistic",
                        "A" => "Respectful",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Decisive",
                        "C" => "Thoughtfull",
                        "A" => "Precise",
                        "B" => "Confident",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Strong-willed",
                        "C" => "Controlled",
                        "B" => "Self-analyzing",
                        "A" => "Inspiring",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Combative",
                        "D" => "Magnanimous",
                        "B" => "Cautious",
                        "C" => "Charming",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Determined",
                        "B" => "Good-natured",
                        "C" => "Meticulous",
                        "D" => "Playful",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Self-assertive",
                        "D" => "Good Listener",
                        "C" => "Disciplined",
                        "B" => "Pleasant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Impactful",
                        "D" => "Sensitive",
                        "C" => "Soft Spoken",
                        "B" => "Entertaining",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Competitive",
                        "D" => "Even-tempered",
                        "A" => "Attentive",
                        "C" => "Enthusiastic",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Pioneering",
                        "D" => "Safe",
                        "A" => "Controlled",
                        "C" => "Poised",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Daring",
                        "A" => "Compassionate",
                        "B" => "Correct",
                        "D" => "Expressive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Forceful",
                        "C" => "Restrained",
                        "D" => "Talkative",
                        "A" => "Adaptable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Outspoken",
                        "C" => "Diplomatic",
                        "D" => "Accepting",
                        "B" => "Friendly",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Vigorous",
                        "C" => "Neutral",
                        "A" => "Calm",
                        "B" => "Influential",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "C" => "Cooperative",
                        "A" => "Constant",
                        "D" => "Convivial",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasive",
                        "B" => "Stable",
                        "A" => "With a lot of tact",
                        "D" => "Extroverted",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Attractive",
                        "B" => "Collaborator",
                        "D" => "Stubborn",
                        "C" => "Sympathetic",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Self-confident",
                        "B" => "Comprehensive",
                        "D" => "Tolerant",
                        "C" => "Open Mind",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "D" => "Helpful",
                        "A" => "Respectful",
                        "C" => "Optimistic",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Thoughtfull",
                        "B" => "Confident",
                        "D" => "Decisive",
                        "A" => "Precise",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Controlled",
                        "A" => "Inspiring",
                        "D" => "Strong-willed",
                        "B" => "Self-analyzing",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Magnanimous",
                        "C" => "Charming",
                        "A" => "Combative",
                        "B" => "Cautious",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Good-natured",
                        "D" => "Playful",
                        "A" => "Determined",
                        "C" => "Meticulous",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Good Listener",
                        "B" => "Pleasant",
                        "A" => "Self-assertive",
                        "C" => "Disciplined",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Sensitive",
                        "B" => "Entertaining",
                        "A" => "Impactful",
                        "C" => "Soft Spoken",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Even-tempered",
                        "C" => "Enthusiastic",
                        "B" => "Competitive",
                        "A" => "Attentive",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Safe",
                        "C" => "Poised",
                        "B" => "Pioneering",
                        "A" => "Controlled",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "A" => "Compassionate",
                        "D" => "Expressive",
                        "C" => "Daring",
                        "B" => "Correct",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Restrained",
                        "A" => "Adaptable",
                        "B" => "Forceful",
                        "D" => "Talkative",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomatic",
                        "B" => "Friendly",
                        "A" => "Outspoken",
                        "D" => "Accepting",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Neutral",
                        "B" => "Influential",
                        "D" => "Vigorous",
                        "A" => "Calm",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperative",
                        "D" => "Convivial",
                        "B" => "Impatient",
                        "A" => "Constant",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "D" => "Extroverted",
                        "C" => "Persuasive",
                        "A" => "With a lot of tact",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborator",
                        "C" => "Sympathetic",
                        "A" => "Attractive",
                        "D" => "Stubborn",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Comprehensive",
                        "C" => "Open Mind",
                        "A" => "Self-confident",
                        "D" => "Tolerant",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Helpful",
                        "C" => "Optimistic",
                        "B" => "Entrepreneur",
                        "A" => "Respectful",
                    ],
                    "result" => "F10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Thoughtfull",
                        "B" => "Confident",
                        "A" => "Precise",
                        "D" => "Decisive",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Controlled",
                        "A" => "Inspiring",
                        "B" => "Self-analyzing",
                        "D" => "Strong-willed",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Magnanimous",
                        "C" => "Charming",
                        "B" => "Cautious",
                        "A" => "Combative",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Good-natured",
                        "D" => "Playful",
                        "C" => "Meticulous",
                        "A" => "Determined",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Good Listener",
                        "B" => "Pleasant",
                        "C" => "Disciplined",
                        "A" => "Self-assertive",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Sensitive",
                        "B" => "Entertaining",
                        "C" => "Soft Spoken",
                        "A" => "Impactful",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Even-tempered",
                        "C" => "Enthusiastic",
                        "A" => "Attentive",
                        "B" => "Competitive",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Safe",
                        "C" => "Poised",
                        "A" => "Controlled",
                        "B" => "Pioneering",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "A" => "Compassionate",
                        "D" => "Expressive",
                        "B" => "Correct",
                        "C" => "Daring",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Restrained",
                        "A" => "Adaptable",
                        "D" => "Talkative",
                        "B" => "Forceful",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomatic",
                        "B" => "Friendly",
                        "D" => "Accepting",
                        "A" => "Outspoken",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Neutral",
                        "B" => "Influential",
                        "A" => "Calm",
                        "D" => "Vigorous",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperative",
                        "D" => "Convivial",
                        "A" => "Constant",
                        "B" => "Impatient",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "D" => "Extroverted",
                        "A" => "With a lot of tact",
                        "C" => "Persuasive",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborator",
                        "C" => "Sympathetic",
                        "D" => "Stubborn",
                        "A" => "Attractive",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Comprehensive",
                        "C" => "Open Mind",
                        "D" => "Tolerant",
                        "A" => "Self-confident",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Helpful",
                        "C" => "Optimistic",
                        "A" => "Respectful",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "F9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Thoughtfull",
                        "A" => "Precise",
                        "B" => "Confident",
                        "D" => "Decisive",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Controlled",
                        "B" => "Self-analyzing",
                        "A" => "Inspiring",
                        "D" => "Strong-willed",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Magnanimous",
                        "B" => "Cautious",
                        "C" => "Charming",
                        "A" => "Combative",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Good-natured",
                        "C" => "Meticulous",
                        "D" => "Playful",
                        "A" => "Determined",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Good Listener",
                        "C" => "Disciplined",
                        "B" => "Pleasant",
                        "A" => "Self-assertive",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Sensitive",
                        "C" => "Soft Spoken",
                        "B" => "Entertaining",
                        "A" => "Impactful",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Even-tempered",
                        "A" => "Attentive",
                        "C" => "Enthusiastic",
                        "B" => "Competitive",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Safe",
                        "A" => "Controlled",
                        "C" => "Poised",
                        "B" => "Pioneering",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "A" => "Compassionate",
                        "B" => "Correct",
                        "D" => "Expressive",
                        "C" => "Daring",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Restrained",
                        "D" => "Talkative",
                        "A" => "Adaptable",
                        "B" => "Forceful",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomatic",
                        "D" => "Accepting",
                        "B" => "Friendly",
                        "A" => "Outspoken",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Neutral",
                        "A" => "Calm",
                        "B" => "Influential",
                        "D" => "Vigorous",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperative",
                        "A" => "Constant",
                        "D" => "Convivial",
                        "B" => "Impatient",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "A" => "With a lot of tact",
                        "D" => "Extroverted",
                        "C" => "Persuasive",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborator",
                        "D" => "Stubborn",
                        "C" => "Sympathetic",
                        "A" => "Attractive",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Comprehensive",
                        "D" => "Tolerant",
                        "C" => "Open Mind",
                        "A" => "Self-confident",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Helpful",
                        "A" => "Respectful",
                        "C" => "Optimistic",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "F7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Thoughtfull",
                        "A" => "Precise",
                        "D" => "Decisive",
                        "B" => "Confident",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Controlled",
                        "B" => "Self-analyzing",
                        "D" => "Strong-willed",
                        "A" => "Inspiring",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Magnanimous",
                        "B" => "Cautious",
                        "A" => "Combative",
                        "C" => "Charming",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Good-natured",
                        "C" => "Meticulous",
                        "A" => "Determined",
                        "D" => "Playful",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Good Listener",
                        "C" => "Disciplined",
                        "A" => "Self-assertive",
                        "B" => "Pleasant",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Sensitive",
                        "C" => "Soft Spoken",
                        "A" => "Impactful",
                        "B" => "Entertaining",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Even-tempered",
                        "A" => "Attentive",
                        "B" => "Competitive",
                        "C" => "Enthusiastic",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Safe",
                        "A" => "Controlled",
                        "B" => "Pioneering",
                        "C" => "Poised",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "A" => "Compassionate",
                        "B" => "Correct",
                        "C" => "Daring",
                        "D" => "Expressive",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Restrained",
                        "D" => "Talkative",
                        "B" => "Forceful",
                        "A" => "Adaptable",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomatic",
                        "D" => "Accepting",
                        "A" => "Outspoken",
                        "B" => "Friendly",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Neutral",
                        "A" => "Calm",
                        "B" => "Influential",
                        "D" => "Vigorous",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperative",
                        "A" => "Constant",
                        "B" => "Impatient",
                        "D" => "Convivial",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "A" => "With a lot of tact",
                        "C" => "Persuasive",
                        "D" => "Extroverted",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborator",
                        "D" => "Stubborn",
                        "A" => "Attractive",
                        "C" => "Sympathetic",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Comprehensive",
                        "D" => "Tolerant",
                        "A" => "Self-confident",
                        "C" => "Open Mind",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Helpful",
                        "A" => "Respectful",
                        "B" => "Entrepreneur",
                        "C" => "Optimistic",
                    ],
                    "result" => "F5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Thoughtfull",
                        "D" => "Decisive",
                        "A" => "Precise",
                        "B" => "Confident",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Controlled",
                        "D" => "Strong-willed",
                        "B" => "Self-analyzing",
                        "A" => "Inspiring",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Magnanimous",
                        "A" => "Combative",
                        "B" => "Cautious",
                        "C" => "Charming",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Good-natured",
                        "A" => "Determined",
                        "C" => "Meticulous",
                        "D" => "Playful",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Good Listener",
                        "A" => "Self-assertive",
                        "C" => "Disciplined",
                        "B" => "Pleasant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sensitive",
                        "A" => "Impactful",
                        "C" => "Soft Spoken",
                        "B" => "Entertaining",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Even-tempered",
                        "B" => "Competitive",
                        "A" => "Attentive",
                        "C" => "Enthusiastic",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Safe",
                        "B" => "Pioneering",
                        "A" => "Controlled",
                        "C" => "Poised",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Compassionate",
                        "C" => "Daring",
                        "B" => "Correct",
                        "D" => "Expressive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Restrained",
                        "B" => "Forceful",
                        "D" => "Talkative",
                        "A" => "Adaptable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomatic",
                        "A" => "Outspoken",
                        "D" => "Accepting",
                        "B" => "Friendly",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Neutral",
                        "D" => "Vigorous",
                        "A" => "Calm",
                        "B" => "Influential",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperative",
                        "B" => "Impatient",
                        "A" => "Constant",
                        "D" => "Convivial",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "C" => "Persuasive",
                        "A" => "With a lot of tact",
                        "D" => "Extroverted",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborator",
                        "A" => "Attractive",
                        "D" => "Stubborn",
                        "C" => "Sympathetic",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Comprehensive",
                        "A" => "Self-confident",
                        "D" => "Tolerant",
                        "C" => "Open Mind",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Helpful",
                        "B" => "Entrepreneur",
                        "A" => "Respectful",
                        "C" => "Optimistic",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Thoughtfull",
                        "D" => "Decisive",
                        "B" => "Confident",
                        "A" => "Precise",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Controlled",
                        "D" => "Strong-willed",
                        "A" => "Inspiring",
                        "B" => "Self-analyzing",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Magnanimous",
                        "A" => "Combative",
                        "C" => "Charming",
                        "B" => "Cautious",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Good-natured",
                        "A" => "Determined",
                        "D" => "Playful",
                        "C" => "Meticulous",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Good Listener",
                        "A" => "Self-assertive",
                        "B" => "Pleasant",
                        "C" => "Disciplined",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sensitive",
                        "A" => "Impactful",
                        "B" => "Entertaining",
                        "C" => "Soft Spoken",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Even-tempered",
                        "B" => "Competitive",
                        "C" => "Enthusiastic",
                        "A" => "Attentive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Safe",
                        "B" => "Pioneering",
                        "C" => "Poised",
                        "A" => "Controlled",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Compassionate",
                        "C" => "Daring",
                        "D" => "Expressive",
                        "B" => "Correct",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Restrained",
                        "B" => "Forceful",
                        "A" => "Adaptable",
                        "D" => "Talkative",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomatic",
                        "A" => "Outspoken",
                        "B" => "Friendly",
                        "D" => "Accepting",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Neutral",
                        "D" => "Vigorous",
                        "B" => "Influential",
                        "A" => "Calm",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Cooperative",
                        "B" => "Impatient",
                        "D" => "Convivial",
                        "A" => "Constant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "C" => "Persuasive",
                        "D" => "Extroverted",
                        "A" => "With a lot of tact",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborator",
                        "A" => "Attractive",
                        "C" => "Sympathetic",
                        "D" => "Stubborn",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Comprehensive",
                        "A" => "Self-confident",
                        "C" => "Open Mind",
                        "D" => "Tolerant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Helpful",
                        "B" => "Entrepreneur",
                        "C" => "Optimistic",
                        "A" => "Respectful",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Precise",
                        "D" => "Decisive",
                        "C" => "Thoughtfull",
                        "B" => "Confident",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "B" => "Self-analyzing",
                        "D" => "Strong-willed",
                        "C" => "Controlled",
                        "A" => "Inspiring",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "B" => "Cautious",
                        "A" => "Combative",
                        "D" => "Magnanimous",
                        "C" => "Charming",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "C" => "Meticulous",
                        "A" => "Determined",
                        "B" => "Good-natured",
                        "D" => "Playful",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplined",
                        "A" => "Self-assertive",
                        "D" => "Good Listener",
                        "B" => "Pleasant",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "C" => "Soft Spoken",
                        "A" => "Impactful",
                        "D" => "Sensitive",
                        "B" => "Entertaining",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Attentive",
                        "B" => "Competitive",
                        "D" => "Even-tempered",
                        "C" => "Enthusiastic",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Controlled",
                        "B" => "Pioneering",
                        "D" => "Safe",
                        "C" => "Poised",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "C" => "Daring",
                        "A" => "Compassionate",
                        "D" => "Expressive",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Talkative",
                        "B" => "Forceful",
                        "C" => "Restrained",
                        "A" => "Adaptable",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Accepting",
                        "A" => "Outspoken",
                        "C" => "Diplomatic",
                        "B" => "Friendly",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Calm",
                        "D" => "Vigorous",
                        "C" => "Neutral",
                        "B" => "Influential",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "B" => "Impatient",
                        "C" => "Cooperative",
                        "D" => "Convivial",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "With a lot of tact",
                        "C" => "Persuasive",
                        "B" => "Stable",
                        "D" => "Extroverted",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Stubborn",
                        "A" => "Attractive",
                        "B" => "Collaborator",
                        "C" => "Sympathetic",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerant",
                        "A" => "Self-confident",
                        "B" => "Comprehensive",
                        "C" => "Open Mind",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Respectful",
                        "B" => "Entrepreneur",
                        "D" => "Helpful",
                        "C" => "Optimistic",
                    ],
                    "result" => "C10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Precise",
                        "D" => "Decisive",
                        "B" => "Confident",
                        "C" => "Thoughtfull",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "B" => "Self-analyzing",
                        "D" => "Strong-willed",
                        "A" => "Inspiring",
                        "C" => "Controlled",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "B" => "Cautious",
                        "A" => "Combative",
                        "C" => "Charming",
                        "D" => "Magnanimous",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "C" => "Meticulous",
                        "A" => "Determined",
                        "D" => "Playful",
                        "B" => "Good-natured",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplined",
                        "A" => "Self-assertive",
                        "B" => "Pleasant",
                        "D" => "Good Listener",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "C" => "Soft Spoken",
                        "A" => "Impactful",
                        "B" => "Entertaining",
                        "D" => "Sensitive",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Attentive",
                        "B" => "Competitive",
                        "C" => "Enthusiastic",
                        "D" => "Even-tempered",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Controlled",
                        "B" => "Pioneering",
                        "C" => "Poised",
                        "D" => "Safe",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "C" => "Daring",
                        "D" => "Expressive",
                        "A" => "Compassionate",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Talkative",
                        "B" => "Forceful",
                        "A" => "Adaptable",
                        "C" => "Restrained",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Accepting",
                        "A" => "Outspoken",
                        "B" => "Friendly",
                        "C" => "Diplomatic",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Calm",
                        "D" => "Vigorous",
                        "B" => "Influential",
                        "C" => "Neutral",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "B" => "Impatient",
                        "D" => "Convivial",
                        "C" => "Cooperative",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "With a lot of tact",
                        "C" => "Persuasive",
                        "D" => "Extroverted",
                        "B" => "Stable",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Stubborn",
                        "A" => "Attractive",
                        "C" => "Sympathetic",
                        "B" => "Collaborator",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerant",
                        "A" => "Self-confident",
                        "C" => "Open Mind",
                        "B" => "Comprehensive",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Respectful",
                        "B" => "Entrepreneur",
                        "C" => "Optimistic",
                        "D" => "Helpful",
                    ],
                    "result" => "C9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Precise",
                        "C" => "Thoughtfull",
                        "D" => "Decisive",
                        "B" => "Confident",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "B" => "Self-analyzing",
                        "C" => "Controlled",
                        "D" => "Strong-willed",
                        "A" => "Inspiring",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "B" => "Cautious",
                        "D" => "Magnanimous",
                        "A" => "Combative",
                        "C" => "Charming",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "C" => "Meticulous",
                        "B" => "Good-natured",
                        "A" => "Determined",
                        "D" => "Playful",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplined",
                        "D" => "Good Listener",
                        "A" => "Self-assertive",
                        "B" => "Pleasant",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "C" => "Soft Spoken",
                        "D" => "Sensitive",
                        "A" => "Impactful",
                        "B" => "Entertaining",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Attentive",
                        "D" => "Even-tempered",
                        "B" => "Competitive",
                        "C" => "Enthusiastic",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Controlled",
                        "D" => "Safe",
                        "B" => "Pioneering",
                        "C" => "Poised",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "A" => "Compassionate",
                        "C" => "Daring",
                        "D" => "Expressive",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Talkative",
                        "C" => "Restrained",
                        "B" => "Forceful",
                        "A" => "Adaptable",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Accepting",
                        "C" => "Diplomatic",
                        "A" => "Outspoken",
                        "B" => "Friendly",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Calm",
                        "C" => "Neutral",
                        "D" => "Vigorous",
                        "B" => "Influential",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "C" => "Cooperative",
                        "B" => "Impatient",
                        "D" => "Convivial",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "With a lot of tact",
                        "B" => "Stable",
                        "C" => "Persuasive",
                        "D" => "Extroverted",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Stubborn",
                        "B" => "Collaborator",
                        "A" => "Attractive",
                        "C" => "Sympathetic",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerant",
                        "B" => "Comprehensive",
                        "A" => "Self-confident",
                        "C" => "Open Mind",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Respectful",
                        "D" => "Helpful",
                        "B" => "Entrepreneur",
                        "C" => "Optimistic",
                    ],
                    "result" => "C7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Precise",
                        "C" => "Thoughtfull",
                        "B" => "Confident",
                        "D" => "Decisive",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "B" => "Self-analyzing",
                        "C" => "Controlled",
                        "A" => "Inspiring",
                        "D" => "Strong-willed",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "B" => "Cautious",
                        "D" => "Magnanimous",
                        "C" => "Charming",
                        "A" => "Combative",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "C" => "Meticulous",
                        "B" => "Good-natured",
                        "D" => "Playful",
                        "A" => "Determined",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplined",
                        "D" => "Good Listener",
                        "B" => "Pleasant",
                        "A" => "Self-assertive",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "C" => "Soft Spoken",
                        "D" => "Sensitive",
                        "B" => "Entertaining",
                        "A" => "Impactful",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Attentive",
                        "D" => "Even-tempered",
                        "C" => "Enthusiastic",
                        "B" => "Competitive",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Controlled",
                        "D" => "Safe",
                        "C" => "Poised",
                        "B" => "Pioneering",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "A" => "Compassionate",
                        "D" => "Expressive",
                        "C" => "Daring",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Talkative",
                        "C" => "Restrained",
                        "A" => "Adaptable",
                        "B" => "Forceful",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Accepting",
                        "C" => "Diplomatic",
                        "B" => "Friendly",
                        "A" => "Outspoken",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Calm",
                        "C" => "Neutral",
                        "B" => "Influential",
                        "D" => "Vigorous",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "C" => "Cooperative",
                        "D" => "Convivial",
                        "B" => "Impatient",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "With a lot of tact",
                        "B" => "Stable",
                        "D" => "Extroverted",
                        "C" => "Persuasive",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Stubborn",
                        "B" => "Collaborator",
                        "C" => "Sympathetic",
                        "A" => "Attractive",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerant",
                        "B" => "Comprehensive",
                        "C" => "Open Mind",
                        "A" => "Self-confident",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Respectful",
                        "D" => "Helpful",
                        "C" => "Optimistic",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "C5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Precise",
                        "B" => "Confident",
                        "D" => "Decisive",
                        "C" => "Thoughtfull",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Self-analyzing",
                        "A" => "Inspiring",
                        "D" => "Strong-willed",
                        "C" => "Controlled",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Cautious",
                        "C" => "Charming",
                        "A" => "Combative",
                        "D" => "Magnanimous",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Meticulous",
                        "D" => "Playful",
                        "A" => "Determined",
                        "B" => "Good-natured",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplined",
                        "B" => "Pleasant",
                        "A" => "Self-assertive",
                        "D" => "Good Listener",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Soft Spoken",
                        "B" => "Entertaining",
                        "A" => "Impactful",
                        "D" => "Sensitive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Attentive",
                        "C" => "Enthusiastic",
                        "B" => "Competitive",
                        "D" => "Even-tempered",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Controlled",
                        "C" => "Poised",
                        "B" => "Pioneering",
                        "D" => "Safe",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "D" => "Expressive",
                        "C" => "Daring",
                        "A" => "Compassionate",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Talkative",
                        "A" => "Adaptable",
                        "B" => "Forceful",
                        "C" => "Restrained",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Accepting",
                        "B" => "Friendly",
                        "A" => "Outspoken",
                        "C" => "Diplomatic",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Calm",
                        "B" => "Influential",
                        "D" => "Vigorous",
                        "C" => "Neutral",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "D" => "Convivial",
                        "B" => "Impatient",
                        "C" => "Cooperative",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "With a lot of tact",
                        "D" => "Extroverted",
                        "C" => "Persuasive",
                        "B" => "Stable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Stubborn",
                        "C" => "Sympathetic",
                        "A" => "Attractive",
                        "B" => "Collaborator",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerant",
                        "C" => "Open Mind",
                        "A" => "Self-confident",
                        "B" => "Comprehensive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Respectful",
                        "C" => "Optimistic",
                        "B" => "Entrepreneur",
                        "D" => "Helpful",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Precise",
                        "B" => "Confident",
                        "C" => "Thoughtfull",
                        "D" => "Decisive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Self-analyzing",
                        "A" => "Inspiring",
                        "C" => "Controlled",
                        "D" => "Strong-willed",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Cautious",
                        "C" => "Charming",
                        "D" => "Magnanimous",
                        "A" => "Combative",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Meticulous",
                        "D" => "Playful",
                        "B" => "Good-natured",
                        "A" => "Determined",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Disciplined",
                        "B" => "Pleasant",
                        "D" => "Good Listener",
                        "A" => "Self-assertive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Soft Spoken",
                        "B" => "Entertaining",
                        "D" => "Sensitive",
                        "A" => "Impactful",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Attentive",
                        "C" => "Enthusiastic",
                        "D" => "Even-tempered",
                        "B" => "Competitive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Controlled",
                        "C" => "Poised",
                        "D" => "Safe",
                        "B" => "Pioneering",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "D" => "Expressive",
                        "A" => "Compassionate",
                        "C" => "Daring",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Talkative",
                        "A" => "Adaptable",
                        "C" => "Restrained",
                        "B" => "Forceful",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Accepting",
                        "B" => "Friendly",
                        "C" => "Diplomatic",
                        "A" => "Outspoken",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Calm",
                        "B" => "Influential",
                        "C" => "Neutral",
                        "D" => "Vigorous",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "D" => "Convivial",
                        "C" => "Cooperative",
                        "B" => "Impatient",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "With a lot of tact",
                        "D" => "Extroverted",
                        "B" => "Stable",
                        "C" => "Persuasive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Stubborn",
                        "C" => "Sympathetic",
                        "B" => "Collaborator",
                        "A" => "Attractive",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tolerant",
                        "C" => "Open Mind",
                        "B" => "Comprehensive",
                        "A" => "Self-confident",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Respectful",
                        "C" => "Optimistic",
                        "D" => "Helpful",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "indefinido"
                ],
            ]
        ];
    }

    private function getAnswersInFrench()
    {
        return [
            [
                [
                    "meaning" => [
                        "B" => "Confiant",
                        "D" => "Décidé",
                        "C" => "Aimable",
                        "A" => "Exact",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirant",
                        "D" => "Déterminé",
                        "C" => "Raisonnable",
                        "B" => "Introspectif",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Charmant",
                        "A" => "Agressif",
                        "D" => "Généreux",
                        "B" => "Prudent",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Enjoué",
                        "A" => "Obstiné",
                        "B" => "Amical",
                        "C" => "Méthodique",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Joyeux",
                        "A" => "Sûr de soi",
                        "D" => "Attentif",
                        "C" => "Discipliné",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Amusant",
                        "A" => "Audacieux",
                        "D" => "Sympathique",
                        "C" => "À la vois douce",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Enthousiaste",
                        "B" => "Compétitif",
                        "D" => "Serein",
                        "A" => "Soigneux",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Posé",
                        "B" => "Innovateur",
                        "D" => "Régulier",
                        "A" => "Maître se soi",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Expressif",
                        "C" => "Hardi",
                        "A" => "Compatissant",
                        "B" => "Correct",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "A" => "Conciliant",
                        "B" => "Energique",
                        "C" => "Modéré",
                        "D" => "Bavard",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "A" => "Franc",
                        "C" => "Diplomate",
                        "D" => "Tolérant",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "B" => "Influent",
                        "D" => "Vigoureux",
                        "C" => "Impartial",
                        "A" => "Calme",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "B" => "Impatient",
                        "C" => "Coopératif",
                        "A" => "Constant",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverti",
                        "C" => "Persuasif",
                        "B" => "Stable",
                        "A" => "Plein de tact",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathique",
                        "A" => "Attrayant",
                        "B" => "Collaborateur",
                        "D" => "Têtu",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Ouvert",
                        "A" => "Sûr de soi",
                        "B" => "Compréhensif",
                        "D" => "Tolérant",
                    ],
                    "result" => "P10"
                ],
                [
                    "meaning" => [
                        "C" => "Optimiste",
                        "B" => "Entrepreneur",
                        "D" => "Utile",
                        "A" => "Respectueuse",
                    ],
                    "result" => "P10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confiant",
                        "D" => "Décidé",
                        "A" => "Exact",
                        "C" => "Aimable",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirant",
                        "D" => "Déterminé",
                        "B" => "Introspectif",
                        "C" => "Raisonnable",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Charmant",
                        "A" => "Agressif",
                        "B" => "Prudent",
                        "D" => "Généreux",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Enjoué",
                        "A" => "Obstiné",
                        "C" => "Méthodique",
                        "B" => "Amical",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Joyeux",
                        "A" => "Sûr de soi",
                        "C" => "Discipliné",
                        "D" => "Attentif",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Amusant",
                        "A" => "Audacieux",
                        "C" => "À la vois douce",
                        "D" => "Sympathique",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Enthousiaste",
                        "B" => "Compétitif",
                        "A" => "Soigneux",
                        "D" => "Serein",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Posé",
                        "B" => "Innovateur",
                        "A" => "Maître se soi",
                        "D" => "Régulier",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Expressif",
                        "C" => "Hardi",
                        "B" => "Correct",
                        "A" => "Compatissant",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "A" => "Conciliant",
                        "B" => "Energique",
                        "D" => "Bavard",
                        "C" => "Modéré",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "A" => "Franc",
                        "D" => "Tolérant",
                        "C" => "Diplomate",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "B" => "Influent",
                        "D" => "Vigoureux",
                        "A" => "Calme",
                        "C" => "Impartial",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "B" => "Impatient",
                        "A" => "Constant",
                        "C" => "Coopératif",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverti",
                        "C" => "Persuasif",
                        "A" => "Plein de tacto",
                        "B" => "Stable",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathique",
                        "A" => "Attrayant",
                        "D" => "Têtu",
                        "B" => "Collaborateur",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Ouvert",
                        "A" => "Sûr de soi",
                        "D" => "Tolérant",
                        "B" => "Compréhensif",
                    ],
                    "result" => "P9"
                ],
                [
                    "meaning" => [
                        "C" => "Optimiste",
                        "B" => "Entrepreneur",
                        "A" => "Respectueuse",
                        "D" => "Utile",
                    ],
                    "result" => "P9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confiant",
                        "C" => "Aimable",
                        "D" => "Décidé",
                        "A" => "Exact",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirant",
                        "C" => "Raisonnable",
                        "D" => "Déterminé",
                        "B" => "Introspectif",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Charmant",
                        "D" => "Généreux",
                        "A" => "Agressif",
                        "B" => "Prudent",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Enjoué",
                        "B" => "Amical",
                        "A" => "Obstiné",
                        "C" => "Méthodique",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Joyeux",
                        "D" => "Attentif",
                        "A" => "Sûr de soi",
                        "C" => "Discipliné",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Amusant",
                        "D" => "Sympathique",
                        "A" => "Audacieux",
                        "C" => "À la vois douce",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Enthousiaste",
                        "D" => "Serein",
                        "B" => "Compétitif",
                        "A" => "Soigneux",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Posé",
                        "D" => "Régulier",
                        "B" => "Innovateur",
                        "A" => "Maître se soi",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Expressif",
                        "A" => "Compatissant",
                        "C" => "Hardi",
                        "B" => "Correct",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "A" => "Conciliant",
                        "C" => "Modéré",
                        "B" => "Energique",
                        "D" => "Bavard",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "C" => "Diplomate",
                        "A" => "Franc",
                        "D" => "Tolérant",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "B" => "Influent",
                        "C" => "Impartial",
                        "D" => "Vigoureux",
                        "A" => "Calme",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "C" => "Coopératif",
                        "B" => "Impatient",
                        "A" => "Constant",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverti",
                        "B" => "Stable",
                        "C" => "Persuasif",
                        "A" => "Plein de tacto",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathique",
                        "B" => "Collaborateur",
                        "A" => "Attrayant",
                        "D" => "Têtu",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Ouvert",
                        "B" => "Compréhensif",
                        "A" => "Sûr de soi",
                        "D" => "Tolérant",
                    ],
                    "result" => "P7"
                ],
                [
                    "meaning" => [
                        "C" => "Optimiste",
                        "D" => "Utile",
                        "B" => "Entrepreneur",
                        "A" => "Respectueuse",
                    ],
                    "result" => "P7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confiant",
                        "C" => "Aimable",
                        "A" => "Exact",
                        "D" => "Décidé",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirant",
                        "C" => "Raisonnable",
                        "B" => "Introspectif",
                        "D" => "Déterminé",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Charmant",
                        "D" => "Généreux",
                        "B" => "Prudent",
                        "A" => "Agressif",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Enjoué",
                        "B" => "Amical",
                        "C" => "Méthodique",
                        "A" => "Obstiné",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Joyeux",
                        "D" => "Attentif",
                        "C" => "Discipliné",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Amusant",
                        "D" => "Sympathique",
                        "C" => "À la vois douce",
                        "A" => "Audacieux",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Enthousiaste",
                        "D" => "Serein",
                        "A" => "Soigneux",
                        "B" => "Compétitif",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Posé",
                        "D" => "Régulier",
                        "A" => "Maître se soi",
                        "B" => "Innovateur",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Expressif",
                        "A" => "Compatissant",
                        "B" => "Correct",
                        "C" => "Hardi",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "A" => "Conciliant",
                        "C" => "Modéré",
                        "D" => "Bavard",
                        "B" => "Energique",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "C" => "Diplomate",
                        "D" => "Tolérant",
                        "A" => "Franc",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "B" => "Influent",
                        "C" => "Impartial",
                        "A" => "Calme",
                        "D" => "Vigoureux",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "C" => "Coopératif",
                        "A" => "Constant",
                        "B" => "Impatient",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverti",
                        "B" => "Stable",
                        "A" => "Plein de tacto",
                        "C" => "Persuasif",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathique",
                        "B" => "Collaborateur",
                        "D" => "Têtu",
                        "A" => "Attrayant",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Ouvert",
                        "B" => "Compréhensif",
                        "D" => "Tolérant",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "P5"
                ],
                [
                    "meaning" => [
                        "C" => "Optimiste",
                        "D" => "Utile",
                        "A" => "Respectueuse",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "P5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confiant",
                        "A" => "Exact",
                        "C" => "Aimable",
                        "D" => "Décidé",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirant",
                        "B" => "Introspectif",
                        "C" => "Raisonnable",
                        "D" => "Déterminé",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Charmant",
                        "B" => "Prudent",
                        "D" => "Généreux",
                        "A" => "Agressif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Enjoué",
                        "C" => "Méthodique",
                        "B" => "Amical",
                        "A" => "Obstiné",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Joyeux",
                        "C" => "Discipliné",
                        "D" => "Attentif",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Amusant",
                        "C" => "À la vois douce",
                        "D" => "Sympathique",
                        "A" => "Audacieux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Enthousiaste",
                        "A" => "Soigneux",
                        "D" => "Serein",
                        "B" => "Compétitif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Posé",
                        "A" => "Maître se soi",
                        "D" => "Régulier",
                        "B" => "Innovateur",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Expressif",
                        "B" => "Correct",
                        "A" => "Compatissant",
                        "C" => "Hardi",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Conciliant",
                        "D" => "Bavard",
                        "C" => "Modéré",
                        "B" => "Energique",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "D" => "Tolérant",
                        "C" => "Diplomate",
                        "A" => "Franc",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Influent",
                        "A" => "Calme",
                        "C" => "Impartial",
                        "D" => "Vigoureux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "A" => "Constant",
                        "C" => "Coopératif",
                        "B" => "Impatient",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverti",
                        "A" => "Plein de tacto",
                        "B" => "Stable",
                        "C" => "Persuasif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathique",
                        "D" => "Têtu",
                        "B" => "Collaborateur",
                        "A" => "Attrayant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Ouvert",
                        "D" => "Tolérant",
                        "B" => "Compréhensif",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Optimiste",
                        "A" => "Respectueuse",
                        "D" => "Utile",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "B" => "Confiant",
                        "A" => "Exact",
                        "D" => "Décidé",
                        "C" => "Aimable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Inspirant",
                        "B" => "Introspectif",
                        "D" => "Déterminé",
                        "C" => "Raisonnable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Charmant",
                        "B" => "Prudent",
                        "A" => "Agressif",
                        "D" => "Généreux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Enjoué",
                        "C" => "Méthodique",
                        "A" => "Obstiné",
                        "B" => "Amical",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Joyeux",
                        "C" => "Discipliné",
                        "A" => "Sûr de soi",
                        "D" => "Attentif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Amusant",
                        "C" => "À la vois douce",
                        "A" => "Audacieux",
                        "D" => "Sympathique",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Enthousiaste",
                        "A" => "Soigneux",
                        "B" => "Compétitif",
                        "D" => "Serein",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Posé",
                        "A" => "Maître se soi",
                        "B" => "Innovateur",
                        "D" => "Régulier",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Expressif",
                        "B" => "Correct",
                        "C" => "Hardi",
                        "A" => "Compatissant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Conciliant",
                        "D" => "Bavard",
                        "B" => "Energique",
                        "C" => "Modéré",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "D" => "Tolérant",
                        "A" => "Franc",
                        "C" => "Diplomate",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Influent",
                        "A" => "Calme",
                        "D" => "Vigoureux",
                        "C" => "Impartial",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sociable",
                        "A" => "Constant",
                        "B" => "Impatient",
                        "C" => "Coopératif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Extroverti",
                        "A" => "Plein de tacto",
                        "C" => "Persuasif",
                        "B" => "Stable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Sympathique",
                        "D" => "Têtu",
                        "A" => "Attrayant",
                        "B" => "Collaborateur",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Ouvert",
                        "D" => "Tolérant",
                        "A" => "Sûr de soi",
                        "B" => "Compréhensif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Optimiste",
                        "A" => "Respectueuse",
                        "B" => "Entrepreneur",
                        "D" => "Utile",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Décidé",
                        "B" => "Confiant",
                        "C" => "Aimable",
                        "A" => "Exact",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "D" => "Déterminé",
                        "A" => "Inspirant",
                        "C" => "Raisonnable",
                        "B" => "Introspectif",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Agressif",
                        "C" => "Charmant",
                        "D" => "Généreux",
                        "B" => "Prudent",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Obstiné",
                        "D" => "Enjoué",
                        "B" => "Amical",
                        "C" => "Méthodique",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "B" => "Joyeux",
                        "D" => "Attentif",
                        "C" => "Discipliné",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Audacieux",
                        "B" => "Amusant",
                        "D" => "Sympathique",
                        "C" => "À la vois douce",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Compétitif",
                        "C" => "Enthousiaste",
                        "D" => "Serein",
                        "A" => "Soigneux",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Innovateur",
                        "C" => "Posé",
                        "D" => "Régulier",
                        "A" => "Maître se soi",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "C" => "Hardi",
                        "D" => "Expressif",
                        "A" => "Compatissant",
                        "B" => "Correct",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Energique",
                        "A" => "Conciliant",
                        "C" => "Modéré",
                        "D" => "Bavard",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Franc",
                        "B" => "Amical",
                        "C" => "Diplomate",
                        "D" => "Tolérant",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "D" => "Vigoureux",
                        "B" => "Influent",
                        "C" => "Impartial",
                        "A" => "Calme",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "D" => "Sociable",
                        "C" => "Coopératif",
                        "A" => "Constant",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasif",
                        "D" => "Extroverti",
                        "B" => "Stable",
                        "A" => "Plein de tacto",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Attrayant",
                        "C" => "Sympathique",
                        "B" => "Collaborateur",
                        "D" => "Têtu",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "C" => "Ouvert",
                        "B" => "Compréhensif",
                        "D" => "Tolérant",
                    ],
                    "result" => "H10"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "C" => "Optimiste",
                        "D" => "Utile",
                        "A" => "Respectueuse",
                    ],
                    "result" => "H10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Décidé",
                        "B" => "Confiant",
                        "A" => "Exact",
                        "C" => "Aimable",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "D" => "Déterminé",
                        "A" => "Inspirant",
                        "B" => "Introspectif",
                        "C" => "Raisonnable",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Agressif",
                        "C" => "Charmant",
                        "B" => "Prudent",
                        "D" => "Généreux",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Obstiné",
                        "D" => "Enjoué",
                        "C" => "Méthodique",
                        "B" => "Amical",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "B" => "Joyeux",
                        "C" => "Discipliné",
                        "D" => "Attentif",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Audacieux",
                        "B" => "Amusant",
                        "C" => "À la vois douce",
                        "D" => "Sympathique",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Compétitif",
                        "C" => "Enthousiaste",
                        "A" => "Soigneux",
                        "D" => "Serein",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Innovateur",
                        "C" => "Posé",
                        "A" => "Maître se soi",
                        "D" => "Régulier",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "C" => "Hardi",
                        "D" => "Expressif",
                        "B" => "Correct",
                        "A" => "Compatissant",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Energique",
                        "A" => "Conciliant",
                        "D" => "Bavard",
                        "C" => "Modéré",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Franc",
                        "B" => "Amical",
                        "D" => "Tolérant",
                        "C" => "Diplomate",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "D" => "Vigoureux",
                        "B" => "Influent",
                        "A" => "Calme",
                        "C" => "Impartial",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "D" => "Sociable",
                        "A" => "Constant",
                        "C" => "Coopératif",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasif",
                        "D" => "Extroverti",
                        "A" => "Plein de tacto",
                        "B" => "Stable",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Attrayant",
                        "C" => "Sympathique",
                        "D" => "Têtu",
                        "B" => "Collaborateur",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "C" => "Ouvert",
                        "D" => "Tolérant",
                        "B" => "Compréhensif",
                    ],
                    "result" => "H9"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "C" => "Optimiste",
                        "A" => "Respectueuse",
                        "D" => "Utile",
                    ],
                    "result" => "H9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Décidé",
                        "A" => "Exact",
                        "C" => "Aimable",
                        "B" => "Confiant",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "D" => "Déterminé",
                        "B" => "Introspectif",
                        "C" => "Raisonnable",
                        "A" => "Inspirant",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Agressif",
                        "B" => "Prudent",
                        "D" => "Généreux",
                        "C" => "Charmant",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Obstiné",
                        "C" => "Méthodique",
                        "B" => "Amical",
                        "D" => "Enjoué",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "C" => "Discipliné",
                        "D" => "Attentif",
                        "B" => "Joyeux",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Audacieux",
                        "C" => "À la vois douce",
                        "D" => "Sympathique",
                        "B" => "Amusant",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Compétitif",
                        "A" => "Soigneux",
                        "D" => "Serein",
                        "C" => "Enthousiaste",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Innovateur",
                        "A" => "Maître se soi",
                        "D" => "Régulier",
                        "C" => "Posé",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "C" => "Hardi",
                        "B" => "Correct",
                        "A" => "Compatissant",
                        "D" => "Expressif",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Energique",
                        "D" => "Bavard",
                        "C" => "Modéré",
                        "A" => "Conciliant",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Franc",
                        "D" => "Tolérant",
                        "C" => "Diplomate",
                        "B" => "Amical",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "D" => "Vigoureux",
                        "A" => "Calme",
                        "C" => "Impartial",
                        "B" => "Influent",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "A" => "Constant",
                        "C" => "Coopératif",
                        "D" => "Sociable",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasif",
                        "A" => "Plein de tacto",
                        "B" => "Stable",
                        "D" => "Extroverti",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Attrayant",
                        "D" => "Têtu",
                        "B" => "Collaborateur",
                        "C" => "Sympathique",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "D" => "Tolérant",
                        "B" => "Compréhensif",
                        "C" => "Ouvert",
                    ],
                    "result" => "H7"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "A" => "Respectueuse",
                        "D" => "Utile",
                        "C" => "Optimiste",
                    ],
                    "result" => "H7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Décidé",
                        "A" => "Exact",
                        "B" => "Confiant",
                        "C" => "Aimable",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "D" => "Déterminé",
                        "B" => "Introspectif",
                        "A" => "Inspirant",
                        "C" => "Raisonnable",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Agressif",
                        "B" => "Prudent",
                        "C" => "Charmant",
                        "D" => "Généreux",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Obstiné",
                        "C" => "Méthodique",
                        "D" => "Enjoué",
                        "B" => "Amical",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "C" => "Discipliné",
                        "B" => "Joyeux",
                        "D" => "Attentif",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Audacieux",
                        "C" => "À la vois douce",
                        "B" => "Amusant",
                        "D" => "Sympathique",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Compétitif",
                        "A" => "Soigneux",
                        "C" => "Enthousiaste",
                        "D" => "Serein",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Innovateur",
                        "A" => "Maître se soi",
                        "C" => "Posé",
                        "D" => "Régulier",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "C" => "Hardi",
                        "B" => "Correct",
                        "D" => "Expressif",
                        "A" => "Compatissant",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Energique",
                        "D" => "Bavard",
                        "A" => "Conciliant",
                        "C" => "Modéré",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Franc",
                        "D" => "Tolérant",
                        "B" => "Amical",
                        "C" => "Diplomate",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "D" => "Vigoureux",
                        "A" => "Calme",
                        "B" => "Influent",
                        "C" => "Impartial",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "A" => "Constant",
                        "D" => "Sociable",
                        "C" => "Coopératif",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasif",
                        "A" => "Plein de tacto",
                        "D" => "Extroverti",
                        "B" => "Stable",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Attrayant",
                        "D" => "Têtu",
                        "C" => "Sympathique",
                        "B" => "Collaborateur",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "D" => "Tolérant",
                        "C" => "Ouvert",
                        "B" => "Compréhensif",
                    ],
                    "result" => "H5"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "A" => "Respectueuse",
                        "C" => "Optimiste",
                        "D" => "Utile",
                    ],
                    "result" => "H5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Décidé",
                        "C" => "Aimable",
                        "B" => "Confiant",
                        "A" => "Exact",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Déterminé",
                        "C" => "Raisonnable",
                        "A" => "Inspirant",
                        "B" => "Introspectif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Agressif",
                        "D" => "Généreux",
                        "C" => "Charmant",
                        "B" => "Prudent",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Obstiné",
                        "B" => "Amical",
                        "D" => "Enjoué",
                        "C" => "Méthodique",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "D" => "Attentif",
                        "B" => "Joyeux",
                        "C" => "Discipliné",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Audacieux",
                        "D" => "Sympathique",
                        "B" => "Amusant",
                        "C" => "À la vois douce",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Compétitif",
                        "D" => "Serein",
                        "C" => "Enthousiaste",
                        "A" => "Soigneux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Innovateur",
                        "D" => "Régulier",
                        "C" => "Posé",
                        "A" => "Maître se soi",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Hardi",
                        "A" => "Compatissant",
                        "D" => "Expressif",
                        "B" => "Correct",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Energique",
                        "C" => "Modéré",
                        "A" => "Conciliant",
                        "D" => "Bavard",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Franc",
                        "C" => "Diplomate",
                        "B" => "Amical",
                        "D" => "Tolérant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Vigoureux",
                        "C" => "Impartial",
                        "B" => "Influent",
                        "A" => "Calme",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "C" => "Coopératif",
                        "D" => "Sociable",
                        "A" => "Constant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasif",
                        "B" => "Stable",
                        "D" => "Extroverti",
                        "A" => "Plein de tacto",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Attrayant",
                        "B" => "Collaborateur",
                        "C" => "Sympathique",
                        "D" => "Têtu",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "B" => "Compréhensif",
                        "C" => "Ouvert",
                        "D" => "Tolérant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "D" => "Utile",
                        "C" => "Optimiste",
                        "A" => "Respectueuse",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "D" => "Décidé",
                        "C" => "Aimable",
                        "A" => "Exact",
                        "B" => "Confiant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Déterminé",
                        "C" => "Raisonnable",
                        "B" => "Introspectif",
                        "A" => "Inspirant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Agressif",
                        "D" => "Généreux",
                        "B" => "Prudent",
                        "C" => "Charmant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Obstiné",
                        "B" => "Amical",
                        "C" => "Méthodique",
                        "D" => "Enjoué",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "D" => "Attentif",
                        "C" => "Discipliné",
                        "B" => "Joyeux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Audacieux",
                        "D" => "Sympathique",
                        "C" => "À la vois douce",
                        "B" => "Amusant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Compétitif",
                        "D" => "Serein",
                        "A" => "Soigneux",
                        "C" => "Enthousiaste",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Innovateur",
                        "D" => "Régulier",
                        "A" => "Maître se soi",
                        "C" => "Posé",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Hardi",
                        "A" => "Compatissant",
                        "B" => "Correct",
                        "D" => "Expressif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Energique",
                        "C" => "Modéré",
                        "D" => "Bavard",
                        "A" => "Conciliant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Franc",
                        "C" => "Diplomate",
                        "D" => "Tolérant",
                        "B" => "Amical",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Vigoureux",
                        "C" => "Impartial",
                        "A" => "Calme",
                        "B" => "Influent",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Impatient",
                        "C" => "Coopératif",
                        "A" => "Constant",
                        "D" => "Sociable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Persuasif",
                        "B" => "Stable",
                        "A" => "Plein de tacto",
                        "D" => "Extroverti",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Attrayant",
                        "B" => "Collaborateur",
                        "D" => "Têtu",
                        "C" => "Sympathique",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Sûr de soi",
                        "B" => "Compréhensif",
                        "D" => "Tolérant",
                        "C" => "Ouvert",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Entrepreneur",
                        "D" => "Utile",
                        "A" => "Respectueuse",
                        "C" => "Optimiste",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Aimable",
                        "B" => "Confiant",
                        "D" => "Décidé",
                        "A" => "Exact",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Raisonnable",
                        "A" => "Inspirant",
                        "D" => "Déterminé",
                        "B" => "Introspectif",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Généreux",
                        "C" => "Charmant",
                        "A" => "Agressif",
                        "B" => "Prudent",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "D" => "Enjoué",
                        "A" => "Obstiné",
                        "C" => "Méthodique",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Attentif",
                        "B" => "Joyeux",
                        "A" => "Sûr de soi",
                        "C" => "Discipliné",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Sympathique",
                        "B" => "Amusant",
                        "A" => "Audacieux",
                        "C" => "À la vois douce",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Serein",
                        "C" => "Enthousiaste",
                        "B" => "Compétitif",
                        "A" => "Soigneux",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Régulier",
                        "C" => "Posé",
                        "B" => "Innovateur",
                        "A" => "Maître se soi",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "A" => "Compatissant",
                        "D" => "Expressif",
                        "C" => "Hardi",
                        "B" => "Correct",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Modéré",
                        "A" => "Conciliant",
                        "B" => "Energique",
                        "D" => "Bavard",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomate",
                        "B" => "Amical",
                        "A" => "Franc",
                        "D" => "Tolérant",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Impartial",
                        "B" => "Influent",
                        "D" => "Vigoureux",
                        "A" => "Calme",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "C" => "Coopératif",
                        "D" => "Sociable",
                        "B" => "Impatient",
                        "A" => "Constant",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "D" => "Extroverti",
                        "C" => "Persuasif",
                        "A" => "Plein de tacto",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborateur",
                        "C" => "Sympathique",
                        "A" => "Attrayant",
                        "D" => "Têtu",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "B" => "Compréhensif",
                        "C" => "Ouvert",
                        "A" => "Sûr de soi",
                        "D" => "Tolérant",
                    ],
                    "result" => "F10"
                ],
                [
                    "meaning" => [
                        "D" => "Utile",
                        "C" => "Optimiste",
                        "B" => "Entrepreneur",
                        "A" => "Respectueuse",
                    ],
                    "result" => "F10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Aimable",
                        "B" => "Confiant",
                        "A" => "Exact",
                        "D" => "Décidé",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Raisonnable",
                        "A" => "Inspirant",
                        "B" => "Introspectif",
                        "D" => "Déterminé",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Généreux",
                        "C" => "Charmant",
                        "B" => "Prudent",
                        "A" => "Agressif",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "D" => "Enjoué",
                        "C" => "Méthodique",
                        "A" => "Obstiné",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Attentif",
                        "B" => "Joyeux",
                        "C" => "Discipliné",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Sympathique",
                        "B" => "Amusant",
                        "C" => "À la vois douce",
                        "A" => "Audacieux",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Serein",
                        "C" => "Enthousiaste",
                        "A" => "Soigneux",
                        "B" => "Compétitif",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Régulier",
                        "C" => "Posé",
                        "A" => "Maître se soi",
                        "B" => "Innovateur",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "A" => "Compatissant",
                        "D" => "Expressif",
                        "B" => "Correct",
                        "C" => "Hardi",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Modéré",
                        "A" => "Conciliant",
                        "D" => "Bavard",
                        "B" => "Energique",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomate",
                        "B" => "Amical",
                        "D" => "Tolérant",
                        "A" => "Franc",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Impartial",
                        "B" => "Influent",
                        "A" => "Calme",
                        "D" => "Vigoureux",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "C" => "Coopératif",
                        "D" => "Sociable",
                        "A" => "Constant",
                        "B" => "Impatient",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "D" => "Extroverti",
                        "A" => "Plein de tacto",
                        "C" => "Persuasif",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborateur",
                        "C" => "Sympathique",
                        "D" => "Têtu",
                        "A" => "Attrayant",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "B" => "Compréhensif",
                        "C" => "Ouvert",
                        "D" => "Tolérant",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "F9"
                ],
                [
                    "meaning" => [
                        "D" => "Utile",
                        "C" => "Optimiste",
                        "A" => "Respectueuse",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "F9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Aimable",
                        "A" => "Exact",
                        "B" => "Confiant",
                        "D" => "Décidé",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Raisonnable",
                        "B" => "Introspectif",
                        "A" => "Inspirant",
                        "D" => "Déterminé",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Généreux",
                        "B" => "Prudent",
                        "C" => "Charmant",
                        "A" => "Agressif",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "C" => "Méthodique",
                        "D" => "Enjoué",
                        "A" => "Obstiné",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Attentif",
                        "C" => "Discipliné",
                        "B" => "Joyeux",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Sympathique",
                        "C" => "À la vois douce",
                        "B" => "Amusant",
                        "A" => "Audacieux",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Serein",
                        "A" => "Soigneux",
                        "C" => "Enthousiaste",
                        "B" => "Compétitif",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Régulier",
                        "A" => "Maître se soi",
                        "C" => "Posé",
                        "B" => "Innovateur",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "A" => "Compatissant",
                        "B" => "Correct",
                        "D" => "Expressif",
                        "C" => "Hardi",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Modéré",
                        "D" => "Bavard",
                        "A" => "Conciliant",
                        "B" => "Energique",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomate",
                        "D" => "Tolérant",
                        "B" => "Amical",
                        "A" => "Franc",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Impartial",
                        "A" => "Calme",
                        "B" => "Influent",
                        "D" => "Vigoureux",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "C" => "Coopératif",
                        "A" => "Constant",
                        "D" => "Sociable",
                        "B" => "Impatient",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "A" => "Plein de tacto",
                        "D" => "Extroverti",
                        "C" => "Persuasif",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborateur",
                        "D" => "Têtu",
                        "C" => "Sympathique",
                        "A" => "Attrayant",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "B" => "Compréhensif",
                        "D" => "Tolérant",
                        "C" => "Ouvert",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "F7"
                ],
                [
                    "meaning" => [
                        "D" => "Utile",
                        "A" => "Respectueuse",
                        "C" => "Optimiste",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "F7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Aimable",
                        "A" => "Exact",
                        "D" => "Décidé",
                        "B" => "Confiant",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Raisonnable",
                        "B" => "Introspectif",
                        "D" => "Déterminé",
                        "A" => "Inspirant",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Généreux",
                        "B" => "Prudent",
                        "A" => "Agressif",
                        "C" => "Charmant",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "C" => "Méthodique",
                        "A" => "Obstiné",
                        "D" => "Enjoué",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Attentif",
                        "C" => "Discipliné",
                        "A" => "Sûr de soi",
                        "B" => "Joyeux",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Sympathique",
                        "C" => "À la vois douce",
                        "A" => "Audacieux",
                        "B" => "Amusant",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Serein",
                        "A" => "Soigneux",
                        "B" => "Compétitif",
                        "C" => "Enthousiaste",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Régulier",
                        "A" => "Maître se soi",
                        "B" => "Innovateur",
                        "C" => "Posé",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "A" => "Compatissant",
                        "B" => "Correct",
                        "C" => "Hardi",
                        "D" => "Expressif",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Modéré",
                        "D" => "Bavard",
                        "B" => "Energique",
                        "A" => "Conciliant",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomate",
                        "D" => "Tolérant",
                        "A" => "Franc",
                        "B" => "Amical",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Impartial",
                        "A" => "Calme",
                        "B" => "Influent",
                        "D" => "Vigoureux",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "C" => "Coopératif",
                        "A" => "Constant",
                        "B" => "Impatient",
                        "D" => "Sociable",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "A" => "Plein de tacto",
                        "C" => "Persuasif",
                        "D" => "Extroverti",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborateur",
                        "D" => "Têtu",
                        "A" => "Attrayant",
                        "C" => "Sympathique",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "B" => "Compréhensif",
                        "D" => "Tolérant",
                        "A" => "Sûr de soi",
                        "C" => "Ouvert",
                    ],
                    "result" => "F5"
                ],
                [
                    "meaning" => [
                        "D" => "Utile",
                        "A" => "Respectueuse",
                        "B" => "Entrepreneur",
                        "C" => "Optimiste",
                    ],
                    "result" => "F5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Aimable",
                        "D" => "Décidé",
                        "A" => "Exact",
                        "B" => "Confiant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Raisonnable",
                        "D" => "Déterminé",
                        "B" => "Introspectif",
                        "A" => "Inspirant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Généreux",
                        "A" => "Agressif",
                        "B" => "Prudent",
                        "C" => "Charmant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "A" => "Obstiné",
                        "C" => "Méthodique",
                        "D" => "Enjoué",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Attentif",
                        "A" => "Sûr de soi",
                        "C" => "Discipliné",
                        "B" => "Joyeux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sympathique",
                        "A" => "Audacieux",
                        "C" => "À la vois douce",
                        "B" => "Amusant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Serein",
                        "B" => "Compétitif",
                        "A" => "Soigneux",
                        "C" => "Enthousiaste",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Régulier",
                        "B" => "Innovateur",
                        "A" => "Maître se soi",
                        "C" => "Posé",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Compatissant",
                        "C" => "Hardi",
                        "B" => "Correct",
                        "D" => "Expressif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Modéré",
                        "B" => "Energique",
                        "D" => "Bavard",
                        "A" => "Conciliant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomate",
                        "A" => "Franc",
                        "D" => "Tolérant",
                        "B" => "Amical",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Impartial",
                        "D" => "Vigoureux",
                        "A" => "Calme",
                        "B" => "Influent",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Coopératif",
                        "B" => "Impatient",
                        "A" => "Constant",
                        "D" => "Sociable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "C" => "Persuasif",
                        "A" => "Plein de tacto",
                        "D" => "Extroverti",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborateur",
                        "A" => "Attrayant",
                        "D" => "Têtu",
                        "C" => "Sympathique",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Compréhensif",
                        "A" => "Sûr de soi",
                        "D" => "Tolérant",
                        "C" => "Ouvert",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Utile",
                        "B" => "Entrepreneur",
                        "A" => "Respectueuse",
                        "C" => "Optimiste",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "C" => "Aimable",
                        "D" => "Décidé",
                        "B" => "Confiant",
                        "A" => "Exact",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Raisonnable",
                        "D" => "Déterminé",
                        "A" => "Inspirant",
                        "B" => "Introspectif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Généreux",
                        "A" => "Agressif",
                        "C" => "Charmant",
                        "B" => "Prudent",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Amical",
                        "A" => "Obstiné",
                        "D" => "Enjoué",
                        "C" => "Méthodique",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Attentif",
                        "A" => "Sûr de soi",
                        "B" => "Joyeux",
                        "C" => "Discipliné",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Sympathique",
                        "A" => "Audacieux",
                        "B" => "Amusant",
                        "C" => "À la vois douce",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Serein",
                        "B" => "Compétitif",
                        "C" => "Enthousiaste",
                        "A" => "Soigneux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Régulier",
                        "B" => "Innovateur",
                        "C" => "Posé",
                        "A" => "Maître se soi",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Compatissant",
                        "C" => "Hardi",
                        "D" => "Expressif",
                        "B" => "Correct",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Modéré",
                        "B" => "Energique",
                        "A" => "Conciliant",
                        "D" => "Bavard",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Diplomate",
                        "A" => "Franc",
                        "B" => "Amical",
                        "D" => "Tolérant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Impartial",
                        "D" => "Vigoureux",
                        "B" => "Influent",
                        "A" => "Calme",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Coopératif",
                        "B" => "Impatient",
                        "D" => "Sociable",
                        "A" => "Constant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Stable",
                        "C" => "Persuasif",
                        "D" => "Extroverti",
                        "A" => "Plein de tacto",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Collaborateur",
                        "A" => "Attrayant",
                        "C" => "Sympathique",
                        "D" => "Têtu",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Compréhensif",
                        "A" => "Sûr de soi",
                        "C" => "Ouvert",
                        "D" => "Tolérant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Utile",
                        "B" => "Entrepreneur",
                        "C" => "Optimiste",
                        "A" => "Respectueuse",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exact",
                        "D" => "Décidé",
                        "C" => "Aimable",
                        "B" => "Confiant",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "B" => "Introspectif",
                        "D" => "Déterminé",
                        "C" => "Raisonnable",
                        "A" => "Inspirant",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "B" => "Prudent",
                        "A" => "Agressif",
                        "D" => "Généreux",
                        "C" => "Charmant",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "C" => "Méthodique",
                        "A" => "Obstinév",
                        "B" => "Amical",
                        "D" => "Enjoué",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "C" => "Discipliné",
                        "A" => "Sûr de soi",
                        "D" => "Attentif",
                        "B" => "Joyeux",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "C" => "À la vois douce",
                        "A" => "Audacieux",
                        "D" => "Sympathique",
                        "B" => "Amusant",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Soigneux",
                        "B" => "Compétitif",
                        "D" => "Serein",
                        "C" => "Enthousiaste",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Maître se soi",
                        "B" => "Innovateur",
                        "D" => "Régulier",
                        "C" => "Posé",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "C" => "Hardi",
                        "A" => "Compatissant",
                        "D" => "Expressif",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Bavard",
                        "B" => "Energique",
                        "C" => "Modéré",
                        "A" => "Conciliant",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "A" => "Franc",
                        "C" => "Diplomate",
                        "B" => "Amical",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Calme",
                        "D" => "Vigoureux",
                        "C" => "Impartial",
                        "B" => "Influent",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "B" => "Impatient",
                        "C" => "Coopératif",
                        "D" => "Sociable",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Plein de tacto",
                        "C" => "Persuasif",
                        "B" => "Stable",
                        "D" => "Extroverti",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Têtu",
                        "A" => "Attrayant",
                        "B" => "Collaborateur",
                        "C" => "Sympathique",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "A" => "Sûr de soi",
                        "B" => "Compréhensif",
                        "C" => "Ouvert",
                    ],
                    "result" => "C10"
                ],
                [
                    "meaning" => [
                        "A" => "Respectueuse",
                        "B" => "Entrepreneur",
                        "D" => "Utile",
                        "C" => "Optimiste",
                    ],
                    "result" => "C10"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exact",
                        "D" => "Décidé",
                        "B" => "Confiant",
                        "C" => "Aimable",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "B" => "Introspectif",
                        "D" => "Déterminé",
                        "A" => "Inspirant",
                        "C" => "Raisonnable",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "B" => "Prudent",
                        "A" => "Agressif",
                        "C" => "Charmant",
                        "D" => "Généreux",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "C" => "Méthodique",
                        "A" => "Obstiné",
                        "D" => "Enjoué",
                        "B" => "Amical",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "C" => "Discipliné",
                        "A" => "Sûr de soi",
                        "B" => "Joyeux",
                        "D" => "Attentif",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "C" => "À la vois douce",
                        "A" => "Audacieux",
                        "B" => "Amusant",
                        "D" => "Sympathique",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Soigneux",
                        "B" => "Compétitif",
                        "C" => "Enthousiaste",
                        "D" => "Serein",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Maître se soi",
                        "B" => "Innovateur",
                        "C" => "Posé",
                        "D" => "Régulier",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "C" => "Hardi",
                        "D" => "Expressif",
                        "A" => "Compatissant",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Bavard",
                        "B" => "Energique",
                        "A" => "Conciliant",
                        "C" => "Modéré",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "A" => "Franc",
                        "B" => "Amical",
                        "C" => "Diplomate",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Calme",
                        "D" => "Vigoureux",
                        "B" => "Influent",
                        "C" => "Impartial",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "B" => "Impatient",
                        "D" => "Sociable",
                        "C" => "Coopératif",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Plein de tacto",
                        "C" => "Persuasif",
                        "D" => "Extroverti",
                        "B" => "Stable",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Têtu",
                        "A" => "Attrayant",
                        "C" => "Sympathique",
                        "B" => "Collaborateur",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "A" => "Sûr de soi",
                        "C" => "Ouvert",
                        "B" => "Compréhensif",
                    ],
                    "result" => "C9"
                ],
                [
                    "meaning" => [
                        "A" => "Respectueuse",
                        "B" => "Entrepreneur",
                        "C" => "Optimiste",
                        "D" => "Utile",
                    ],
                    "result" => "C9"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exact",
                        "C" => "Aimable",
                        "D" => "Décidé",
                        "B" => "Confiant",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "B" => "Introspectif",
                        "C" => "Raisonnable",
                        "D" => "Déterminé",
                        "A" => "Inspirant",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "B" => "Prudent",
                        "D" => "Généreux",
                        "A" => "Agressif",
                        "C" => "Charmant",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "C" => "Méthodique",
                        "B" => "Amical",
                        "A" => "Obstiné",
                        "D" => "Enjoué",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "C" => "Discipliné",
                        "D" => "Attentif",
                        "A" => "Sûr de soi",
                        "B" => "Joyeux",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "C" => "À la vois douce",
                        "D" => "Sympathique",
                        "A" => "Audacieux",
                        "B" => "Amusant",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Soigneux",
                        "D" => "Serein",
                        "B" => "Compétitif",
                        "C" => "Enthousiaste",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Maître se soi",
                        "D" => "Régulier",
                        "B" => "Innovateur",
                        "C" => "Posé",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "A" => "Compatissant",
                        "C" => "Hardi",
                        "D" => "Expressif",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Bavard",
                        "C" => "Modéré",
                        "B" => "Energique",
                        "A" => "Conciliant",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "C" => "Diplomate",
                        "A" => "Franc",
                        "B" => "Amical",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Calme",
                        "C" => "Impartial",
                        "D" => "Vigoureux",
                        "B" => "Influent",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "C" => "Coopératif",
                        "B" => "Impatient",
                        "D" => "Sociable",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Plein de tacto",
                        "B" => "Stable",
                        "C" => "Persuasif",
                        "D" => "Extroverti",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Têtu",
                        "B" => "Collaborateur",
                        "A" => "Attrayant",
                        "C" => "Sympathique",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "B" => "Compréhensif",
                        "A" => "Sûr de soi",
                        "C" => "Ouvert",
                    ],
                    "result" => "C7"
                ],
                [
                    "meaning" => [
                        "A" => "Respectueuse",
                        "D" => "Utile",
                        "B" => "Entrepreneur",
                        "C" => "Optimiste",
                    ],
                    "result" => "C7"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exact",
                        "C" => "Aimable",
                        "B" => "Confiant",
                        "D" => "Décidé",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "B" => "Introspectif",
                        "C" => "Raisonnable",
                        "A" => "Inspirant",
                        "D" => "Déterminé",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "B" => "Prudent",
                        "D" => "Généreux",
                        "C" => "Charmant",
                        "A" => "Agressif",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "C" => "Méthodique",
                        "B" => "Amical",
                        "D" => "Enjoué",
                        "A" => "Obstiné",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "C" => "Discipliné",
                        "D" => "Attentif",
                        "B" => "Joyeux",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "C" => "À la vois douce",
                        "D" => "Sympathique",
                        "B" => "Amusant",
                        "A" => "Audacieux",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Soigneux",
                        "D" => "Serein",
                        "C" => "Enthousiaste",
                        "B" => "Compétitif",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Maître se soi",
                        "D" => "Régulier",
                        "C" => "Posé",
                        "B" => "Innovateur",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "A" => "Compatissant",
                        "D" => "Expressif",
                        "C" => "Hardi",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Bavard",
                        "C" => "Modéré",
                        "A" => "Conciliant",
                        "B" => "Energique",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "C" => "Diplomate",
                        "B" => "Amical",
                        "A" => "Franc",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Calme",
                        "C" => "Impartial",
                        "B" => "Influent",
                        "D" => "Vigoureux",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "C" => "Coopératif",
                        "D" => "Sociable",
                        "B" => "Impatient",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Plein de tacto",
                        "B" => "Stable",
                        "D" => "Extroverti",
                        "C" => "Persuasif",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Têtu",
                        "B" => "Collaborateur",
                        "C" => "Sympathique",
                        "A" => "Attrayant",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "B" => "Compréhensif",
                        "C" => "Ouvert",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "C5"
                ],
                [
                    "meaning" => [
                        "A" => "Respectueuse",
                        "D" => "Utile",
                        "C" => "Optimiste",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "C5"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exact",
                        "B" => "Confiant",
                        "D" => "Décidé",
                        "C" => "Aimable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Introspectif",
                        "A" => "Inspirant",
                        "D" => "Déterminé",
                        "C" => "Raisonnable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Prudent",
                        "C" => "Charmant",
                        "A" => "Agressif",
                        "D" => "Généreux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Méthodique",
                        "D" => "Enjoué",
                        "A" => "Obstiné",
                        "B" => "Amical",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Discipliné",
                        "B" => "Joyeux",
                        "A" => "Sûr de soi",
                        "D" => "Attentif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "À la vois douce",
                        "B" => "Amusant",
                        "A" => "Audacieux",
                        "D" => "Sympathique",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Soigneux",
                        "C" => "Enthousiaste",
                        "B" => "Compétitif",
                        "D" => "Serein",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Maître se soi",
                        "C" => "Posé",
                        "B" => "Innovateur",
                        "D" => "Régulier",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "D" => "Expressif",
                        "C" => "Hardi",
                        "A" => "Compatissant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Bavard",
                        "A" => "Conciliant",
                        "B" => "Energique",
                        "C" => "Modéré",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "B" => "Amical",
                        "A" => "Franc",
                        "C" => "Diplomate",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Calme",
                        "B" => "Influent",
                        "D" => "Vigoureux",
                        "C" => "Impartial",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "D" => "Sociable",
                        "B" => "Impatient",
                        "C" => "Coopératif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Plein de tacto",
                        "D" => "Extroverti",
                        "C" => "Persuasif",
                        "B" => "Stable",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Têtu",
                        "C" => "Sympathique",
                        "A" => "Attrayant",
                        "B" => "Collaborateur",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "C" => "Ouvert",
                        "A" => "Sûr de soi",
                        "B" => "Compréhensif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Respectueuse",
                        "C" => "Optimiste",
                        "B" => "Entrepreneur",
                        "D" => "Utile",
                    ],
                    "result" => "indefinido"
                ],
            ],
            [
                [
                    "meaning" => [
                        "A" => "Exact",
                        "B" => "Confiant",
                        "C" => "Aimable",
                        "D" => "Décidé",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Introspectif",
                        "A" => "Inspirant",
                        "C" => "Raisonnable",
                        "D" => "Déterminé",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Prudent",
                        "C" => "Charmant",
                        "D" => "Généreux",
                        "A" => "Agressif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Méthodique",
                        "D" => "Enjoué",
                        "B" => "Amical",
                        "A" => "Obstiné",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "Discipliné",
                        "B" => "Joyeux",
                        "D" => "Attentif",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "C" => "À la vois douce",
                        "B" => "Amusant",
                        "D" => "Sympathique",
                        "A" => "Audacieux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Soigneux",
                        "C" => "Enthousiaste",
                        "D" => "Serein",
                        "B" => "Compétitif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Maître se soi",
                        "C" => "Posé",
                        "D" => "Régulier",
                        "B" => "Innovateur",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "B" => "Correct",
                        "D" => "Expressif",
                        "A" => "Compatissant",
                        "C" => "Hardi",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Bavard",
                        "A" => "Conciliant",
                        "C" => "Modéré",
                        "B" => "Energique",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "B" => "Amical",
                        "C" => "Diplomate",
                        "A" => "Franc",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Calme",
                        "B" => "Influent",
                        "C" => "Impartial",
                        "D" => "Vigoureux",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Constant",
                        "D" => "Sociable",
                        "C" => "Coopératif",
                        "B" => "Impatient",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Plein de tacto",
                        "D" => "Extroverti",
                        "B" => "Stable",
                        "C" => "Persuasif",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Têtu",
                        "C" => "Sympathique",
                        "B" => "Collaborateur",
                        "A" => "Attrayant",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "D" => "Tolérant",
                        "C" => "Ouvert",
                        "B" => "Compréhensif",
                        "A" => "Sûr de soi",
                    ],
                    "result" => "indefinido"
                ],
                [
                    "meaning" => [
                        "A" => "Respectueuse",
                        "C" => "Optimiste",
                        "D" => "Utile",
                        "B" => "Entrepreneur",
                    ],
                    "result" => "indefinido"
                ],
            ]
        ];
    }
}