<?php

use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\CommercialTypeSale;

class CommercialTypeSalesSeeder extends Seeder
{
    use CommercialTypeSalesTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CommercialTypeSale::truncate();

        $typeSales = $this->getTypeSales();

        foreach ($typeSales as $type) {
            $commercialType = new CommercialTypeSale($type);
            $commercialType->commercial_need_id = $type['commercial_need_id'];
            $commercialType->save();
        }
    }
}
