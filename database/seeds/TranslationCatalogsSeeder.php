<?php

use Illuminate\Database\Seeder;
use Waavi\Translation\Models\Translation;

class TranslationCatalogsSeeder extends Seeder
{
    use TranslationCatalogsTrait;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $getCatalogs = $this->getCatalogs();

        foreach ($getCatalogs as $value) {
            $translation = new Translation($value);
            $translation->save();
        }  
    }
}
