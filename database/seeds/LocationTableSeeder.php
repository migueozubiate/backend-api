<?php

use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Location;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class LocationTableSeeder extends Seeder
{
    public function run()
    {
        $locationSeed = [
            'name' => 'NullData',
            'address' => 'Insurgentes Sur 1524, Crédito Constructor, 03940 Ciudad de México, CDMX',
            'position' => new Point(19.3665903,-99.1832615)
        ];

        $company = Company::find(1);

        $location = new Location($locationSeed);
        $location = $company->location()->save($location);
        $location->uuid = '07ed0980-d125-4e00-bed1-d97a8c0a9889';
        $location->save();
    }
}
