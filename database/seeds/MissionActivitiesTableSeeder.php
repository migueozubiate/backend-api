<?php

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\MissionActivity;

class MissionActivitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $missionActivitiesSeed = [
            'title' => 'Visita comer insurgentes',
            'description' => 'Optinal description',
            'status' => 1,
            'started_at' => Carbon::now()
        ];

        $executive = Executive::find(1);
        $mission = Mission::find(1);
        $company = Company::find(1);

        $missionActivities = new MissionActivity($missionActivitiesSeed);
        $missionActivities->executive_id = $executive->id;
        $missionActivities->company_id = $company->id;
        $missionActivities = $mission->activities()->save($missionActivities);
        $missionActivities->uuid = '982e0a5c-d2f2-39dd-b8e9-d5edbcbad750';
        $missionActivities->save();
    }
}