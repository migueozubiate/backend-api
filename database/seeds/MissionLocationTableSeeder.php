<?php

use App\Roipal\Eloquent\User;
use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\Mission;
use App\Roipal\Eloquent\Location;
use App\Roipal\Eloquent\MissionLocation;
use Grimzy\LaravelMysqlSpatial\Types\Point;

class MissionLocationTableSeeder extends Seeder
{
    public function run()
    {
        $missionLocationSeed = [
            'name' => 'NullData',
            'address' => 'Insurgentes Sur 1524, Crédito Constructor, 03940 Ciudad de México, CDMX',
            'position' => new Point(19.3665903,-99.1832615),
            'executives_requested' => 2
        ];


        $user = User::find(1);
        $mission = Mission::find(1);
        $location = Location::find(1);

        $missionLocation = new MissionLocation($missionLocationSeed);
        $missionLocation->mission_id = $mission->id;
        $missionLocation->user_id = $user->id;
        $missionLocation = $location->missionLocation()->save($missionLocation);
        $missionLocation->uuid = '3e9a3b66-33e3-334e-9a53-713cc6379a44';
        $missionLocation->save();

    }
}
