<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguageSeeder::class);
        $this->call(CompaniesTableSeeder::class);
        $this->call(CatalogsTableSeeder::class);
        $this->call(PassportClientSeeder::class);
        $this->call(AssestmentsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(MissionTableSeeder::class);
        $this->call(LocationTableSeeder::class);
        $this->call(MissionLocationTableSeeder::class);
        $this->call(MissionInvitationTableSeeder::class);
        $this->call(ExecutiveMissionTableSeeder::class);
        $this->call(MissionActivitiesTableSeeder::class);
        $this->call(PhysicalLimitationsTableSeeder::class);
        $this->call(LineBusinessCatalogSeeder::class);
        $this->call(CommercialNeedSeeder::class);
        $this->call(CommercialTypeSalesSeeder::class);
        $this->call(TranslationCatalogsSeeder::class);
        $this->call(ExecutiveProfileSeeder::class);
        
    }
}
