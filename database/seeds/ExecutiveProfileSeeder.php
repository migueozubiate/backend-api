<?php
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\ExecutiveProfile;
use App\Roipal\Eloquent\User;
use Carbon\Carbon;
use Grimzy\LaravelMysqlSpatial\Types\Point;
use Illuminate\Database\Seeder;
use Ramsey\Uuid\Uuid;
use Tests\Support\UsersTableSeederTrait;

class ExecutiveProfileSeeder extends Seeder
{
    use UsersTableSeederTrait;

    public function run()
    {
        $locations = $this->getLocations();
        $faker     = Faker\Factory::create('es_PE');

        foreach ($locations as $location) {
            $user = new User([
                'name' => $faker->name,
                'company_id' => 1,
                'email' => $faker->unique()->email,
                'password' => bcrypt('123456789'),
                'classification' => 'executive',
            ]);

            $user->uuid = Uuid::uuid4()->toString();
            $user->email_verified_at = Carbon::now();
            $user->save();

            $executive = Executive::find($user->id);

            $profile = new ExecutiveProfile([
                'bio'      => '',
                'website'  => '',
                'address'  => $location['address'],
                'position' => new Point($location['latitude'], $location['longitude']),
                'country' => 'México',
                'registered_payment' => 0,
                'invitations_received' => 0,
                'verification_status' => 0 
            ]);

            $executive->profile()->save($profile);

        }
    }
}