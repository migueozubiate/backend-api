<?php

use Illuminate\Database\Seeder;
use Waavi\Translation\Models\Language; 

class LanguageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $languageEs = [
            'locale' => 'es',
            'name' => 'Spanish'
        ];

        $language = new Language($languageEs);
        $language->save();

        $languageEn = [
            'locale' => 'en',
            'name' => 'English'
        ];

       $language = new Language($languageEn);
       $language->save();

       $languageFr = [
            'locale' => 'fr',
            'name' => 'French'
       ];

       $language = new Language($languageFr);
       $language->save();

       \Artisan::call('translator:load');
    }
}
