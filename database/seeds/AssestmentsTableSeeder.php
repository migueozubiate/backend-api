<?php

require_once __DIR__ . '/AssestmentsTraitRoipal.php';
require_once __DIR__ . '/DiscAnswersPollValuesTrait.php';

use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\Assestment;
use App\Roipal\Eloquent\AssestmentQuestion;
use App\Roipal\Eloquent\AssestmentAnswersPoll;

class AssestmentsTableSeeder extends Seeder
{

    use AssestmentsTraitRoipal;
    use DiscAnswersPollValuesTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AssestmentAnswersPoll::truncate();
        AssestmentQuestion::truncate();
        Assestment::truncate();

        $assestments = $this->getAssestments();

        foreach ($assestments as $assetment_data) {
            $language = $assetment_data['lang'];

            $assestment = new Assestment();
            $assestment->uuid = $assetment_data['uuid'];
            $assestment->name = $assetment_data['name'];
            $assestment->lang = $assetment_data['lang'];
            $assestment->summary = $assetment_data['summary'] ?? '';

            $answersMethod = "get{$assestment->name}Answers";
            $questions = $this->{"get{$assestment->name}Questions"}($assestment->lang);
            $assestment->total_questions = count($questions);

            $assestment->save();

            foreach ($questions as $index => $question) {
                $number = $index + 1;

                $question['number'] = $number;
                $question['type'] = $question['type'] ?? 'unknown';
                $question = new AssestmentQuestion($question);
                $question->question = $question->question ?? 'Group ' . $number;

                $assestment->questions()->save($question);
            }

            if (method_exists($this, $answersMethod)) {
                $combinations = $this->{$answersMethod}($language);

                foreach ($combinations as $number => $group) {
                    foreach ($group as $combination) {
                        $result = array_pull($combination, 'result');
                        $split = str_split($result);
                        $poll = new AssestmentAnswersPoll($combination);
                        $poll->number = $number + 1;

                        if (trim($result) !== 'indefinido') {
                            $poll->profile = array_shift($split);
                            $poll->points = implode('', $split);
                        }

                        $poll->answer = implode('', array_keys($combination['meaning']));

                        $assestment->poll()->save($poll);
                    }
                }
            }
        }
    }

    protected function getAssestments()
    {
        $assessmentDisc = [
            [
                'uuid' => 'e1f41eb2-2bb2-4515-8820-927c50dd2121',
                'lang' => 'es',
                'name' => 'DISC'
            ],
            [
                'uuid' => 'f2f70837-6ea8-4994-893e-5abc321b6ad4',
                'lang' => 'en',
                'name' => 'DISC'
            ],
            [
                'uuid' => 'c22eac80-c7aa-4e67-a5b3-1fc5947c5034',
                'lang' => 'fr',
                'name' => 'DISC'
            ],
        ];

        $getAssessmentRoipal = $this->getROIPALAssetment();

        foreach ($getAssessmentRoipal as $value) {
            $assessmentDisc[] = $value;
        }

        return $assessmentDisc;
    }

    protected function getDISCAnswers($language)
    {
        $original = $this->getAnswersDisc($language);
        $answers = [];

        $questions = count($original[0]) - 1;

        foreach (range(0, $questions) as $number) {
            foreach ($original as $index => $value) {
                $answers[$number][] = $value[$number];
            }
        }

        return $answers;
    }

    protected function getDISCQuestions($language)
    {
        switch ($language) {
            case 'es':
                return $this->getDISCQuestionsInSpanish();
                break;

            case 'en':
                return $this->getDISCQuestionsInEnglish();
                break;

            case 'fr':
                return $this->getDISCQuestionsInFrench();
                break;
            
            default:
                # code...
                break;
        }
    }

    private function getDISCQuestionsInSpanish()
    {
        return [
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Exacto/a',
                    'B' => 'Seguro de sí mismo/a',
                    'C' => 'Considerado/a',
                    'D' => 'Firme'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Inspirador',
                    'B' => 'Reflexivo/a',
                    'C' => 'Moderado/a',
                    'D' => 'Tenaz'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Agresivo/a',
                    'B' => 'Prudente',
                    'C' => 'Encantador',
                    'D' => 'Generoso/a'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Determinado/a',
                    'B' => 'Buen vecino',
                    'C' => 'Metódico/a',
                    'D' => 'Juguetón'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Firme',
                    'B' => 'Alentador/a',
                    'C' => 'Disciplinado/a',
                    'D' => 'Sabe escuchar'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Audaz',
                    'B' => 'Divertido/a',
                    'C' => 'De voz suave',
                    'D' => 'Simpático/a'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Cuidadoso/a',
                    'B' => 'Competitivo/a',
                    'C' => 'Entusiasta',
                    'D' => 'Ecuánime'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Controlado/a',
                    'B' => 'Pionero',
                    'C' => 'Con aplomo (Seductor/a)',
                    'D' => 'Estable'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Compasivo/a',
                    'B' => 'Correcto/a',
                    'C' => 'Atrevido/a',
                    'D' => 'Expresivo/a'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Acomodadizo/a',
                    'B' => 'Con carácter',
                    'C' => 'Moderado/a',
                    'D' => 'Conservador'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Franco/a',
                    'B' => 'Amistoso/a',
                    'C' => 'Diplomático/a',
                    'D' => 'Resignado/a'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Calmado/a',
                    'B' => 'Influyente',
                    'C' => 'Imparcial',
                    'D' => 'Dinámico/a'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Constante',
                    'B' => 'Impaciente',
                    'C' => 'Cooperativo',
                    'D' => 'Sociable'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Con mucho tacto',
                    'B' => 'Estable',
                    'C' => 'Persuasivo/a',
                    'D' => 'Extrovertido/a'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Atractivo/a',
                    'B' => 'Colaborador/a',
                    'C' => 'Simpático/a',
                    'D' => 'Obstinado/a'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Seguro/a',
                    'B' => 'Comprensivo/a',
                    'C' => 'Abierto/a',
                    'D' => 'Tolerante/a'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Respetuoso/a',
                    'B' => 'Emprendedor/a',
                    'C' => 'Optimista',
                    'D' => 'Servicial'
                ]
            ]
        ];
    }

    private function getDISCQuestionsInEnglish()
    {
        return [
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Precise',
                    'B' => 'Confident',
                    'C' => 'Thoughtfull',
                    'D' => 'Decisive'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Inspiring',
                    'B' => 'Self-analyzing',
                    'C' => 'Controlled',
                    'D' => 'Stong-willed'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Combative',
                    'B' => 'Cautious',
                    'C' => 'Charming',
                    'D' => 'Magnanimous'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Determined',
                    'B' => 'Good-natured',
                    'C' => 'Meticulous',
                    'D' => 'Playful'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Self-assertive',
                    'B' => 'Pleasant',
                    'C' => 'Disciplined',
                    'D' => 'Good Listener'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Impactful',
                    'B' => 'Entertaining',
                    'C' => 'Soft Spoken',
                    'D' => 'Sensitive'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Attentive',
                    'B' => 'Competitive',
                    'C' => 'Enthusiastic',
                    'D' => 'Even-tempered'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Controlled',
                    'B' => 'Pioneering',
                    'C' => 'Poised',
                    'D' => 'Safe'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Compassionate',
                    'B' => 'Correct',
                    'C' => 'Daring',
                    'D' => 'Expressive'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Adaptable',
                    'B' => 'Forceful',
                    'C' => 'Restrained',
                    'D' => 'Talkative'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Outspoken',
                    'B' => 'Friendly',
                    'C' => 'Diplomatic',
                    'D' => 'Accepting'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Calm',
                    'B' => 'Influential',
                    'C' => 'Neutral',
                    'D' => 'Vigorous'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Constant',
                    'B' => 'Impatient',
                    'C' => 'Cooperative',
                    'D' => 'Convivial'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Delicate',
                    'B' => 'Stable',
                    'C' => 'Persuasive',
                    'D' => 'Extroverted'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Attractive',
                    'B' => 'Collaborator',
                    'C' => 'Sympathetic',
                    'D' => 'Stubborn'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Self-confident',
                    'B' => 'Comprehensive',
                    'C' => 'Open Mind',
                    'D' => 'Tolerant'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Respectful',
                    'B' => 'Entrepreneur',
                    'C' => 'Optimistic',
                    'D' => 'Helpful'
                ]
            ]
        ];
    }

    private function getDISCQuestionsInFrench()
    {
        return [
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Exact',
                    'B' => 'Confiant',
                    'C' => 'Aimable',
                    'D' => 'Décidé'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Inspirant',
                    'B' => 'Introspectif',
                    'C' => 'Raisonnable',
                    'D' => 'Déterminé'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Agressif',
                    'B' => 'Prudent',
                    'C' => 'Charmant',
                    'D' => 'Généreux'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Obstiné',
                    'B' => 'Amical',
                    'C' => 'Méthodique',
                    'D' => 'Enjoué'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Sûr de soi',
                    'B' => 'Joyeux',
                    'C' => 'Discipliné',
                    'D' => 'Attentif'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Audacieux',
                    'B' => 'Amusant',
                    'C' => 'À la vois douce',
                    'D' => 'Sympathique'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Soigneux',
                    'B' => 'Compétitif',
                    'C' => 'Enthousiaste',
                    'D' => 'Serein'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Maître se soi',
                    'B' => 'Innovateur',
                    'C' => 'Posé',
                    'D' => 'Régulier'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Compatissant',
                    'B' => "Correct",
                    'C' => 'Hardi',
                    'D' => 'Expressif'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Conciliant',
                    'B' => 'Energique',
                    'C' => 'Modéré',
                    'D' => 'Bavard'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Franc',
                    'B' => 'Amical',
                    'C' => 'Diplomate',
                    'D' => 'Tolérant'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Calme',
                    'B' => 'Influent',
                    'C' => 'Impartial',
                    'D' => 'Vigoureux'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Constant',
                    'B' => 'Impatient',
                    'C' => 'Coopératif',
                    'D' => 'Sociable'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Plein de tact',
                    'B' => 'Stable',
                    'C' => 'Persuasif',
                    'D' => 'Extraverti'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Attrayant',
                    'B' => 'Collaborateur',
                    'C' => 'Sympathique',
                    'D' => 'Têtu'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Sûr de soi',
                    'B' => 'Compréhensif',
                    'C' => 'Ouvert',
                    'D' => 'Tolérant'
                ]
            ],
            [
                'type' => 'order',
                'options' => [
                    'A' => 'Respectueux',
                    'B' => 'Entrepreneur',
                    'C' => 'Optimiste',
                    'D' => 'Utile'
                ]
            ]
        ];
    }
}
