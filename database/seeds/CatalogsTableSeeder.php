<?php

use Carbon\Carbon;
use App\Roipal\Eloquent\User;
use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\Executive;
use App\Roipal\Eloquent\ExecutiveProfile;
use App\Roipal\Eloquent\Catalog;

class CatalogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $catalogs = [
            [
                'language' => 'es',
                'type' => 'mission-types',
                'key' => 'Promoción',
                'value' => 'Promoción',
                'icon' => 'ios-pricetags'
            ],
            [
                'language' => 'es',
                'type' => 'mission-types',
                'key' => 'B2B',
                'value' => 'B2B',
                'icon' => 'ios-pricetags'
            ],
            [
                'language' => 'es',
                'type' => 'mission-types',
                'key' => 'Cambaceo',
                'value' => 'Cambaceo',
                'icon' => 'ios-pricetags'
            ],
            [
                'language' => 'es',
                'type' => 'mission-types',
                'key' => 'Farma',
                'value' => 'Farma',
                'icon' => 'ios-pricetags'
            ],
            [
                'language' => 'es',
                'type' => 'mission-types',
                'key' => 'Piso',
                'value' => 'Piso',
                'icon' => 'ios-pricetags'
            ],
            [
                'language' => 'es',
                'type' => 'mission-types',
                'key' => 'Telemarketing',
                'value' => 'Telemarketing',
                'icon' => 'ios-pricetags'
            ],
        ];

        foreach ($catalogs as $value) {
            $catalog = new Catalog($value);
            $catalog->save();
        }
    }
}
