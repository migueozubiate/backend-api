<?php

use App\Roipal\Eloquent\ExecutiveAssestment;
use Illuminate\Database\Seeder;

class ExecutiveAssestmentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $assestment = new ExecutiveAssestment([
            'id' => 1,
            'uuid' => 'ccfc09ee-82a8-45e3-a658-7db8697594ee',
            'user_id' => 1,
            'mission_id' => 1,
            'name' => 'DISC',
            'total_questions' => 17,
            'profile' => 'P',
            'points' => 170,

        ]);

        $assestment->save();

    }
}
