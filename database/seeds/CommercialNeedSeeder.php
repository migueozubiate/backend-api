<?php

use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\CommercialNeed;

class CommercialNeedSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CommercialNeed::truncate();

        $commercialNeedSeed = [
            [
                'commercial_need' => 'Standard types',
                'commercial_item' => 'standard_types'
            ],
            [
                'commercial_need' => 'Specialized',
                'commercial_item' => 'specialized'
            ],
            [
                'commercial_need' => 'Telemarketing',
                'commercial_item' => 'telemarketing'
            ],
            [
                'commercial_need' => 'E-commerce',
                'commercial_item' => 'e_commerce'
            ],
            [
                'commercial_need' => 'Promotion',
                'commercial_item' => 'promotion'
            ],
            [
                'commercial_need' => 'Custom',
                'commercial_item' => 'custom'
            ]
        ];

        foreach ($commercialNeedSeed as $value) {
            $commercialNeed = new CommercialNeed($value);
            $commercialNeed->save();
        }
    }
}
