<?php

use Laravel\Passport\Passport;
use Illuminate\Database\Seeder;

class PassportClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (!App::environment('production')) {
            $client = Passport::client()->forceFill([
                'user_id' => null,
                'name' => 'Roipal API Password Client',
                'secret' => 'owQmd6i7YM9ofDS41gZ8bJN8E9tBta5ZVshom5l6',
                'redirect' => 'http://localhost',
                'personal_access_client' => false,
                'password_client' => true,
                'revoked' => false,
            ]);


            $client->save();

            $client->id = '923a69f6-c2c7-401c-802c-0fff3c67e448';
            $client->save();
        }
    }
}
