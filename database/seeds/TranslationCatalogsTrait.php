<?php

trait TranslationCatalogsTrait
{
    protected function getCatalogs()
    {
        return [
            [
                'locale' => 'en',
                'namespace' => 'catalogs',
                'group' => 'currency',
                'item' => 'currencies',
                'text' => json_encode([
                    [
                        'currency' => 'MXN',
                        'symbol' => '$',
                        'name' => 'Mexican Peso'
                    ],
                    [
                        'currency' => 'USD',
                        'symbol' => '$',
                        'name' => 'US Dollar'
                    ],
                    [
                        'currency' => 'EUR',
                        'symbol' => '€',
                        'name' => 'Euro'
                    ]
                ])
            ],
            [
                'locale' => 'es',
                'namespace' => 'catalogs',
                'group' => 'currency',
                'item' => 'currencies',
                'text' => json_encode([
                    [
                        'currency' => 'MXN',
                        'symbol' => '$',
                        'name' => 'Peso Mexicano'
                    ],
                    [
                        'currency' => 'USD',
                        'symbol' => '$',
                        'name' => 'Dolar Americano'
                    ],
                    [
                        'currency' => 'EUR',
                        'symbol' => '€',
                        'name' => 'Euro'
                    ]
                ]) 
            ],
            [
                'locale' => 'fr',
                'namespace' => 'catalogs',
                'group' => 'currency',
                'item' => 'currencies',
                'text' => json_encode([
                    [
                        'currency' => 'MXN',
                        'symbol' => '$',
                        'name' => 'Pesos Mexicains'
                    ],
                    [    
                        'currency' => 'USD',
                        'symbol' => '$',
                        'name' => 'Dollar US'
                    ],
                    [
                        'currency' => 'EUR',
                        'symbol' => '€',
                        'name' => 'Euro'
                    ]
                ]) 
            ],
            /**
             * Catalogs of Line Business
             */
            [
                'locale' => 'en',
                'namespace' => 'catalogs',
                'group' => 'line_business',
                'item' => 'line_business',
                'text' => json_encode([ 
                    "Mining.", 
                    "Fishing.",
                    "Real estate.",
                    "Constructing.",
                    "Livestock.",
                    "Tourism.",
                    "Information Technology.",
                    "Agriculture.",
                    "Electricity.",
                    "Drinking Water Service.",
                    "Metallurgy.",
                    "Collection.",
                    "Monitoring Service.",
                    "Transport and Storage.",
                    "Culture & Entertainment.",
                    "Health.",
                    "Education.",
                    "Finance and Insurance.",
                    "Wholesale Market.",
                    "Law Firm.",
                    "Commerce.",
                    "Hotels and restaurants"
                ]),
            ],
            [
                'locale' => 'es',
                'namespace' => 'catalogs',
                'group' => 'line_business',
                'item' => 'line_business',
                'text' => json_encode([
                    "Minería.",
                    "Pesca.",
                    "Bienes raíces.",
                    "Construcción.",
                    "Ganadería.",
                    "Turismo.", 
                    "Tecnologías de la Información.",
                    "Agricultura.",
                    "Electricidad",
                    "Agua potable.", 
                    "Metalurgia.",
                    "Cobranza.",
                    "Vigilancia.",
                    "Transporte y Almacenamiento.",
                    "Cultura y entretenimiento.",
                    "Salud.",
                    "Educación.",
                    "Finanzas y/o Seguros.",
                    "Mercados mayoristas.",
                    "Derecho.",
                    "Comercio.",
                    "Hoteles y Restaurantes",
                ])
            ],
            [
                'locale' => 'fr',
                'namespace' => 'catalogs',
                'group' => 'line_business',
                'item' => 'line_business',
                'text' => json_encode([
                    "Industrie Minière.",
                    "Pêche.",
                    "Biens Immobiliers.",
                    "Construction.",
                    "Élevage.",
                    "Tourisme.", 
                    "Technologies de l'information.",
                    "Agriculture.",
                    "Électricité",
                    "Eau potable.",
                    "Métallurgie.", 
                    "Collecte.",
                    "Surveillance.",
                    "Transports et aux stockages .",
                    "Culture et de divertissement.",
                    "Santé.",
                    "Éducation.",
                    "L'assurance et des finances.",
                    "Marché en gros .",
                    "Cabinet d'avocats.",
                    "commerce.",
                    "Hôtels et les restaurants",
                ])
            ],
            [
                'locale' => 'en',
                'namespace' => 'catalogs',
                'group' => 'mexico',
                'item' => 'banks_mexico',
                'text' => json_encode([
                    'BBVA Bancomer',
                    'citibanamex',
                    'Santander',
                    'BANORTE',
                    'HSBC'
                ]) 
            ],
            [
                'locale' => 'es',
                'namespace' => 'catalogs',
                'group' => 'mexico',
                'item' => 'banks_mexico',
                'text' => json_encode([
                    'BBVA Bancomer',
                    'citibanamex',
                    'Santander',
                    'BANORTE',
                    'HSBC'
                ]) 
            ],
            [
                'locale' => 'fr',
                'namespace' => 'catalogs',
                'group' => 'mexico',
                'item' => 'banks_mexico',
                'text' => json_encode([
                    'BBVA Bancomer',
                    'citibanamex',
                    'Santander',
                    'BANORTE',
                    'HSBC'
                ]) 
            ],
            [
                'locale' => 'en',
                'namespace' => 'catalogs',
                'group' => 'countries',
                'item' => 'countries',
                'text' => json_encode([
                    'Mexico',
                    'France',
                    'United States of America'
                ])
            ],
            [
                'locale' => 'es',
                'namespace' => 'catalogs',
                'group' => 'countries',
                'item' => 'countries',
                'text' => json_encode([
                    'México',
                    'Francia',
                    'Estados Unidos de América'
                ])
            ],
            [
                'locale' => 'fr',
                'namespace' => 'catalogs',
                'group' => 'countries',
                'item' => 'countries',
                'text' => json_encode([
                    'Mexique',
                    'France',
                    "États-Unis d'Amérique"
                ])
            ]
        ]; 
    }
}

