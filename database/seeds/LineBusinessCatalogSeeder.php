<?php

require_once __DIR__ . '/LineBusinessCatalogTrait.php';

use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\LineBusiness;

class LineBusinessCatalogSeeder extends Seeder
{
    use LineBusinessCatalogTrait; 
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $lineBusinessCatalog = $this->getLineBusinessCatalog();

        foreach ($lineBusinessCatalog as $value) {
            $lineBusiness = new LineBusiness($value);
            $lineBusiness->save();
        }
    }
}
