<?php

use App\Roipal\Eloquent\User;
use Illuminate\Database\Seeder;
use App\Roipal\Eloquent\Company;
use App\Roipal\Eloquent\Mission;

class MissionTableSeeder extends Seeder
{
    public function run()
    {
        $missionSeed = [
            'name' => 'Mission',
            'description' => 'Promoción en bla bla bla .... zzzzzz',
            'video' => 0,
            'type' => 'Promoción',
            'profile' => 'promotor',
            'time' => 4,
            'time_unit' => 'hrs',
            'fare_by_time' => 200.00,
            'subtotal' => 800.00,
            'status' => 0,
            'total' => 800.00,
            'executives_requested' => 2,
            'invitations_sent' => 0,
            'accepted' => 0,
            'rejected' => 0,
            'score_mission' => 0,
            'type_sale_id' => 1,
            'commercial_need' => 1,
            'type_sale_activities' => [
                'Prospect: Sourcing new, early stage leads to begin a sales process with.',
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ],
            'currency' => 'MXN',
            'country' => 'MX'
        ];

        $company = Company::find(1);

        $user = User::find(1);

        $mission = new Mission($missionSeed);
        $mission->user_id = $user->id;
        $mission = $company->missions()->save($mission);   
        $mission->uuid = 'a57f53c8-4d6c-45d1-85c8-9db9f249b678';
        $mission->save();
    }
}
