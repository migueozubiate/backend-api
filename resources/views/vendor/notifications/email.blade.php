<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta x-preferred-language="es">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Roipal</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
  
  <style>

    .master{
    max-width: 650px;
        border-collapse: collapse;
        border-spacing: 0px;
        border-color: transparent;

    }

    .content{
    color: #FFFFFF;
    text-align: center;
    background-image:url('https://api-roipal.nulldata.com/assets/background.png');
    background-size: cover;
    padding: 25% 15%;
   }
  
    .text{
        color:#ffffff;
        text-align : center;
    }

    a:link{
     color : white;
    }
    

  </style>
</head>

<body>
  <center>
	<table class="master">
  	<tbody>
    	<tr>
      	<td>
        	<div class="content">
          	<center>
            	<img src="https://api-roipal.nulldata.com/assets/roipal.png" alt="Roipal" class="logo">
          	</center>
          	
            {{-- Greeting --}}
            @if (! empty($greeting))
            	{{ $greeting }}
            @else
                @if ($level === 'error')
                    @lang('Whoops!')
                @else
                    <h1 class="text">
                    @isset($actionText)
                        {{ $actionText }}
                    @endisset
                    </h1>
                @endif
            @endif
          	
          	<p class="text">
            	{{-- Intro Lines --}}
            	@foreach ($introLines as $line)
            	    {{ $line }}
            	@endforeach
            </p>
               
            {{-- Action Button --}}
            @isset($actionText)
            	<?php
                	switch ($level) {
                    	case 'success':
                    	case 'error':
                        	$color = $level;
                        	break;
                    	default:
                        	$color = 'primary';
                	}
            	?>
                @component('mail::button', ['url' => $actionUrl, 'color' => $color ])
            	    {{ $actionText }}
                @endcomponent
            @endisset

            <p class="text">
            	{{-- Outro Lines --}}
            	@foreach ($outroLines as $line)
            	    {{ $line }}
            	@endforeach
            </p>
              
            <p class="text">
                {{-- Salutation --}}
                @if (! empty($salutation))
                    {{ $salutation }}
                @else
                    @lang(trans("mail.email_salutation")) <br>{{ config('app.name') }}
                @endif
            </p>

              
            {{-- Subcopy --}}
            @isset($actionText)
                @component('mail::subcopy')
                    <p class="text">
                        @lang(
                            trans('mail.email_subcopy_first_line',['actionText' => ":actionText",'actionURL' => ":actionURL"]),
                            [
                                'actionText' => $actionText,
                                'actionURL' => $actionUrl,
                            ]
                        )
                    </p>
                @endcomponent
            @endisset
        
        	</div>
      	</td>
    	</tr>
  	</tbody>
	</table>
  </center>
</body>

</html>

