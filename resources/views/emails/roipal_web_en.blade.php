<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Roipal</title>
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
</head>

<body style="margin: 0;padding: 0 15%;font-family: Roboto;">
  <center>
    <table class="master" style="max-width: 650px;border-collapse: collapse;border-spacing: 0px;
      border-color: transparent;">
      <tbody>
        <tr>
          <td>
            <div class="content" style="color: white;text-align: center;
              background-image:url('https://api-roipal.nulldata.com/assets/background.png');background-size: cover;
              padding: 25% 5%;">
              <center>
              <img src="https://api-roipal.nulldata.com/assets/roipal.png" alt="Roipal" class="logo">
              </center>
              <h1 style="font-size: 2.2rem;">
                Thanks for your interest into ROIPAL!
              </h1>
              <p style="font-size: 1rem;">
                You’ll be informed very soon about our oficial launch, stay tuned...
              </p>
            </div>
          </td>
        </tr>
      </tbody>
    </table>
  </center>
</body>

</html>