<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Roipal</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        body
        {
            font-family:Roboto;
            color:#fff;
            background-image:url({{ asset('assets/background.png') }});background-size:cover
        }
        center
        {
            min-height:230px;
            margin-top:calc(50vh - 115px)
        }
        h1
        {
            font-size:2.2rem
        }
        p
        {
            font-size:1.8rem;
            margin:0
        }
    </style>
</head>


<body>
  <center>
        
      <div>
        <div style="margin-bottom: 2em">
            <img src="{{ asset('assets/roipal.png') }}" alt="Roipal" class="logo">
        </div>
        <div style="margin-bottom:1em">
            <a href="itms-services://?action=download-manifest&url=https://api-roipal.nulldata.com/application/manifest/Roipal-business.plist" 
                class="btn btn-info" style="color:white">Download Business
            </a>
        </div>
        <div>
            <a href="itms-services://?action=download-manifest&url=https://api-roipal.nulldata.com/application/manifest/Roipal-executive.plist" 
                class="btn btn-info" style="color:white">Download Executive
            </a>
        </div>
      </div>
  </center>
</body>

</html>