<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="icon" href="img/favicon.ico">
    <title>Roipal</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
    <style>
       body{font-family:Roboto;color:#fff;background-image:url({{ asset('assets/background.png') }});background-size:cover}center{min-height:230px;margin-top:calc(50vh - 115px)}h1{font-size:2.2rem}p{font-size:1.8rem;margin:0}
    </style>
</head>
<body>
    <center>
        <a href="#section01"><img src="{{ asset('assets/roipal.png') }}" alt="Roipal" class="logo"></a>
        <h1 class="title"> {{ $email_verify_title}}</h1>
        <p class="description"> {!! $email_verify_description !!}</p>
    </script>
</body>

</html>