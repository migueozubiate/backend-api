<?php

return [
    'mission_created' => 'Mision creada',
    'mission_started' => 'Mision iniciada',
    'mission_finished' => 'Mision finalizada',
    'mission_canceled' => 'Mision cancelada',
    'mission_goal' => 'Objetivo',
    'mission_missed' => 'Perdido',
    'mission_accepted' => 'Mision :name fue aceptada por :executive',
    'mission_rejected' => 'Mision :name no fue aceptada por :executive',
    'mission_been_started' => 'La mision :name ha sido iniciada',
    'mission_been_finished' => 'La mision :name ha sido terminada',
    'mission_been_canceled' => 'La mision :name ha sido cancelada', 
    'mission_executive_canceled' => 'Executivo de mission cancelado',
    'mission_qualify_once' => 'Sólo se puede calificar la misión una vez.',
    'mission_chat_notification' => 'Nuevo mensaje de :name',
    'mission_necessary_activity' => 'La misión debe tener al menos una actividad',
    'mission_time_unit' => 'horas'
 ];

