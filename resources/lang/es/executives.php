<?php

return [
    'executive_canceled' => 'El ejecutivo :executive ha cancelado la mision :name',
    'executive_finished' => 'El ejecutivo :executive ha finalizado la mision :name',
    'executive_search_mission' => 'Se inició la búsqueda de ejecutivos para su misión :title',
    'executive_searching' => 'Buscando...',
    'executive_activity_started' => 'Actividad iniciada',
    'executive_been_activity_started' => 'La actividad :title ha sido iniciada por :executive',
    'executive_activity_finished' => 'Actividad finalizada',
    'executive_been_activity_finished' => 'La actividad :title ha sido finalizada por :executive',
    'executive_rejected' => 'Lo sentimos, has sido rechazado',
    'executive_calculator' => 'CALCULADOR',
    'executive_hunter' => 'CAZADOR',
    'executive_farmer' => 'RELACIONADOR',
    'executive_promotor' => 'PROMOTOR',
 ];

