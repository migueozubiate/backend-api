<?php

return [
    'standard_types' => 'Ventas tradicionales',
    'specialized' => 'Ventas especializadas',
    'telemarketing' => 'Telemarketing',
    'e_commerce' => 'Comercio electrónico',
    'promotion' => 'Promoción',
    'custom' => 'Personalizado'
];