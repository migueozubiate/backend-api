<?php

return [
    'physical_visual' => 'Limitaciones Visuales',
    'physical_mobility' => 'Limitaciones de Movilidad',
    'physical_verbal' => 'Limitaciones Verbales',
    'not_physical_limitation' => 'No tengo ninguna limitación'
 ];

