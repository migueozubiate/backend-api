<?php

return [
    'account_activated' => 'Cuenta activada',
    'mail_verification' => 'Querido(a) :name, tu email :email fue verificado exitosamente.',
    'email_verification_subject' => 'Verificacion de correo electronico',
    'email_verification_first_line' => 'Por favor, haga clic en el botón de abajo para verificar su dirección de correo electrónico.',
    'email_verification_action' => 'Verificacion de correo electronico',
    'email_verification_second_line' => 'Si no ha creado una cuenta, no es necesario realizar ninguna otra acción.',
    'reset_password_subject' => 'Restablecer Contraseña.',
    'reset_password_first_line' => 'Está recibiendo este correo electrónico porque recibimos una solicitud de restablecimiento de contraseña para su cuenta.',
    'reset_password_action' => 'Restablecer Contraseña.',
    'reset_password_second_line' => 'Este enlace de restablecimiento de contraseña expirará en :count minutos',
    'reset_password_thrid_line' => 'Si no ha solicitado un restablecimiento de contraseña, no es necesario realizar ninguna otra acción.',
    'email_subcopy_first_line' => 'Si tiene problemas para hacer clic en el botón :actionText, copie y pegue la URL a continuación.<br> en su navegador web <a href=":actionURL"> :actionURL </a>',
    'email_salutation' => 'Saludos',
    'email_verify_title' => 'CUENTA ACTIVADA',
    'email_verify_description' => 'tu cuenta de email <br/> ha sido confirmada'
];