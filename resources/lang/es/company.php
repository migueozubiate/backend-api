<?php

return [
    'company_executive_mission_canceled' => 'La compañia :company te canceló la misión :name',
    'company_executive_mission_finished' => 'La compañia :company te finalizó la misión :name',
    'company_executive_rejected' => 'La compañia :company te ha rechazado para la misión :mission'
];