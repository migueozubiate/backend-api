<?php

return [
        'type_insideSales' => json_encode([
            'type' => 'Atención a prospectos y clientes',
            'description' => 'Es responsable de mantener las relaciones existentes con los clientes, y también atiende a las personas que contactan la empresa en búsqueda de información y/o de una compra en especíﬁca.',
            'activities' => [
                'Entrar en contacto: Buscar un contacto directo con el prospecto a travez de una junta presencial o telefónica.',
                'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                'Presentar: Hacer una presentación formal o demostración de lo que se ofrece.',
                'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.'
            ]
        ]),
        'type_outsideSales' => json_encode([
            'type' => 'Desarrollo de nuevos clientes',
            'description' => 'Bajo un método tradicional de ventas, es prospección en frío o a través de una base de datos pre caliﬁcada, las actividades se realizan principalmente fuera de la oﬁcina en interacción directa con los prospectos.',
            'activities' => [
                'Prospectar: Obtener nuevos clientes potenciales para comenzar un proceso de ventas.',
                'Entrar en contacto: Buscar un contacto directo con el prospecto a travez de una junta presencial o telefónica.',
                'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                'Presentar: Hacer una presentación formal o demostración de lo que se ofrece.',
                'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.'
            ]
        ]),
        'type_lead' => json_encode([
            'type' => 'Generación de contactos',
            'description' => 'Es responsable de estudiar el mercado y establecer redes para hacer nuevos contactos de negocios. También será responsable de evaluar la viabilidad de dicho contacto y pronosticar resultados potenciales.',
            'activities' => [
                'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                'Trabajo en red: Iniciar una red de contactos, potenciales prospectos a través de relaciones públicas presenciales y/o investigación sobre internet, plataformas digitales, redes sociales entre otros medios electrónicos.',
                'Evaluar: La viabilidad del prospecto en términos de necesidades.'
            ]
        ]),
        'type_pharma' => json_encode([
            'type' => 'Farma',
            'description' => 'El representante médico es responsable de presentar nuevos medicamentos, y/o mejoras sobre patentes existentes a los médicos y a otros precursores del sector salud, privado y/o público.',
            'activities' => [
                'Prospectar: Obtener una base de datos de los médicos y otros interlocutores del sector salud, privado o público.',
                'Entrar en contacto: Buscar la junta presencial o telefónica.',
                'Presentar: Hacer una presentación formal o demostración de lo que se ofrece.'
            ]
        ]),
        'type_real' => json_encode([
            'type' => 'Bienes raíces',
            'description' => 'Los agentes de ventas de bienes raíces ayudan a los clientes a vender o rentar propiedades.',
            'activities' => [
                'Entrar en contacto: Buscar un contacto directo con el prospecto a travez de una junta presencial o telefónica.',
                'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                'Presentar: Hacer visitar el inmueble a la venta o en renta.',
                'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.'
            ]
        ]),
        'type_retail' => json_encode([
            'type' => 'Venta de piso',
            'description' => 'El vendedor de piso ayuda a los clientes a encontrar los productos que desean y facilitan el cobro de los pagos de los clientes. Pueden vender mercancía al por menor, como ropa, muebles, o automóviles ...',
            'activities' => [
                'Presentar: Hacer una presentación formal o demostración de lo que se ofrece.',
                'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.'
            ]
        ]),
        'type_insurance' => json_encode([
            'type' => 'Seguros',
            'description' => 'El agente de seguros contacta a los clientes potenciales y ofrece uno o más tipos de pólizas. Explica las diferencias entre varias pólizas de seguro y ayudan a los clientes a elegir los planes que mejor les convengan.',
            'activities' => [
                'Prospectar: Obtener nuevos clientes potenciales para comenzar un proceso de ventas.',
                'Entrar en contacto: Buscar un contacto directo con el prospecto a travez de una junta presencial o telefónica.',
                'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                'Presentar: Hacer una presentación formal o demostración de lo que se ofrece.',
                'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.'
            ]
        ]),
        'type_inbound' => json_encode([
            'type' => 'Inbound Telemarketing',
            'description' => 'Un ejecutivo de telemarketing Inbound toma llamadas entrantes de clientes actuales y potenciales. Registra los datos de la persona que llama e informa sobre los productos o servicios solicitados. De no contar con la información, servirá de enlace para una asesoría posterior, más detallada. En ocasiones, intentan vender nuevos productos a los clientes, como actualizaciones, mejoras, etc… Manejan las quejas y ayudan a los clientes con problemas.',
            'activities' => [
                'Entrar en contacto: Atender la llamada del busca información.',
                'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                'Presentar: Hacer una presentación formal de lo que se ofrece.',
                'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.'
            ]
        ]),
        'type_outbound' => json_encode([
            'type' => 'Outbound Telemarketing',
            'description' => 'Un ejecutivo de telemarketing Outbound llama a clientes actuales, anteriores, o llama a clientes potenciales desde una lista de teléfonos, base de datos. Normalmente, trata de vender un producto o un servicio mediante la lectura de un guión proporcionado por la empresa. Responde las preguntas que los clientes pueden tener sobre productos o servicios, y registra las ventas.',
            'activities' => [
                'Prospectar: Obtener nuevos clientes potenciales para comenzar un proceso de ventas.',
                'Entrar en contacto: Buscar un contacto directo con el prospecto a travez de una junta telefónica.',
                'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                'Presentar: Hacer una presentación formal de lo que se ofrece.',
                'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.'
            ]
        ]),
        'type_boost' => json_encode([
            'type' => 'Aumentar sus ventas',
            'description' => 'El ejecutivo de ventas digitales se pone en contacto con clientes actuales y potenciales, a través de internet, plataformas digitales, redes sociales entre otros medios electrónicos.',
            'activities' => [
                'Prospectar: Obtener nuevos clientes potenciales para comenzar un proceso de ventas.',
                'Entrar en contacto: Buscar un contacto directo con el prospecto a travez de una junta presencial o telefónica.',
                'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                'Presentar: Hacer una presentación formal o demostración de lo que se ofrece.',
                'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.'
            ]
        ]),
        'type_customer' => json_encode([
            'type' => 'Atención a clientes',
            'description' => 'El ejecutivo de atención a clientes « digitales » atiende a los clientes actuales y potenciales, a sus datos y responde por escrito sobre las promociones y/o productos o servicios.',
            'activities' => [
                'Entrar en contacto: Buscar un contacto directo con el prospecto a travez de una junta presencial o telefónica.',
                'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                'Presentar: Hacer una presentación formal o demostración de lo que se ofrece.',
                'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.'
            ]
        ]),
        'type_promotion' => json_encode([
            'type' => 'Promoción',
            'description' => 'Los demostradores y promotores muestran productos o informan sobre servicios y responden preguntas con el ﬁn de generar un interés en la compra del producto o servicio.',
            'activities' => [
                'Informar: Dar información sobre el producto o servicio, y responder a las principales preguntas, ventajas competitivas, características técnicas (para los productos) precios, tarifas y otros aspectos de la transacción comercial.',
                'Persuadir: Convertir una necesidad existente en un deseo para estimular el interés en la compra.',
                'Presencia de marca: Mantener la marca del producto o servicio y en la mente del público.'
            ]
        ]),
        'type_custom' => json_encode([
            'type' => 'Su propio tipo de ventas',
            'description' => 'Usted es libre de explicar y detallar su propio proceso de ventas al representante de ventas, gracias al chat ROIPAL.',
            'activities' => [
                [
                    '1' => 'Prospectar: Obtener nuevos clientes potenciales para comenzar un proceso de ventas.',
                ],
                [
                    '2' => 'Entrar en contacto: Buscar un contacto directo con el prospecto a travez de una junta presencial o telefónica.',
                ],
                [
                    '3' => 'Caliﬁcar y analizar necesidades: Saber más sobre el prospecto y analizar sus necesidades en términos comerciales.',
                ],
                [
                    '4' => 'Presentar: Hacer una presentación formal o demostración de lo que se ofrece.',
                ],
                [
                    '5' => 'Trabajo en red: Iniciar una red de contactos, potenciales prospectos a través de relaciones públicas presenciales y/o investigación sobre internet, plataformas digitales, redes sociales entre otros medios electrónicos.',
                ],
                [
                    '6' => 'Informar: Dar información sobre el producto o servicio, y responder a las principales preguntas, ventajas competitivas, características técnicas (para los productos) precios, tarifas y otros aspectos de la transacción comercial.',
                ],
                [
                    '7' => 'Evaluar: La viabilidad del prospecto en términos de necesidades.',
                ],
                [
                    '8' => 'Persuadir: Convertir una necesidad existente en un deseo para estimular el interés en la compra.',
                ],
                [
                    '9' => 'Presencia de marca: Mantener la marca del producto o servicio y en la mente del público.',
                ],
                [
                    '10' => 'Concluir: La conclusión del proceso es muy variada, desde entregar una cotización o propuesta, iniciar una negociación, hasta lograr la aceptación de los tomadores de decisiones y otras acciones.',
                ]
            ]
        ]),
    ];