<?php

return [
        'type_insideSales' => json_encode([
            'type' => 'Ventes entrantes et service client',
            'description' => "Un agent commercial interne est responsable du maintien des relations avec les clients existants. Il ou elle est le principal point de contact des clients de l'entreprise, et il ou elle doit bâtir une relation de confiance avec ses clients, une relation d'affaires solide.",
            'activities' => [
                "Prise de contact: Entrer en contact avec des personnes à un stade précoce permet de recueillir de l'information et de juger de leur 'valeur' potentielles comme prospects.",
                'Evaluer et analyse de besoins: En savoir plus sur un prospect et son entreprise.',
                'Présenter: Organiser une présentation ou une démonstration formelle de ce qui est offert.',
                "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions."
            ]
        ]),
        'type_outsideSales' => json_encode([
            'type' => 'Recherche de nouveaux clients',
            'description' => "Dans le cadre d'une méthode de vente traditionnelle, que ce soit par prospection à froid ou par le biais d'une base de données préqualifiée, les activités se déroulent principalement en dehors du bureau en action directe avec les prospects.",
            'activities' => [
                'Prospecter: Rechercher et obtenir de nouveaux clients potentiels pour démarrer un processus de vente.',
                "Prise de contact: Entrer en contact avec des personnes à un stade précoce permet de recueillir de l'information et de juger de leur 'valeur' potentielles comme prospects.",
                'Evaluer et analyse de besoins: En savoir plus sur un prospect et son entreprise.',
                'Présenter: Organiser une présentation ou une démonstration formelle de ce qui est offert.',
                "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions."
            ]
        ]),
        'type_lead' => json_encode([
            'type' => 'Recherche de nouveaux contacts',
            'description' => "Il est responsable de la recherche et du réseautage afin d'établir de nouveaux contacts d'affaires. Être également responsable de l'évaluation des prospects potentiels.",
            'activities' => [
                'Evaluer et analyse de besoins: En savoir plus sur un prospect et son entreprise.',
                'Networking ou Réseautage: Établir un réseau de contacts, de prospects potentiels par le biais des relations publiques et/ou de la recherche sur Internet, les plateformes numériques, les réseaux sociaux et autres médias électroniques.',
                'Valoriser: Définir la viabilité du contact et/ou prospect en fonction de ses besoins.'
            ]
        ]),
        'type_pharma' => json_encode([
            'type' => 'Pharma',
            'description' => 'Le visiteur médical indépendants est responsable de la présentation des nouveaux médicaments aux médecins.',
            'activities' => [
                "Prospecter: Obtenir une base de données de médecins et d'autres intervenants du secteur de la santé, privés ou publics.",
                'Prise de contact: Obtenir un rendez-vous.',
                'Présenter: Organiser une présentation ou une démonstration formelle de ce qui est offert.'
            ]
        ]),
        'type_real' => json_encode([
            'type' => 'Immobilier',
            'description' => 'Les agents immobiliers indépendants aident les clients à vendre ou louer des propriétés.',
            'activities' => [
                "Prise de contact: Entrer en contact avec des personnes à un stade précoce permet de recueillir de l'information et de juger de leur 'valeur' potentielles comme prospects.",
                'Evaluer et analyse de besoins: En savoir plus sur un prospect et son entreprise.',
                'Présenter: Organiser une présentation ou une démonstration formelle de ce qui est offert.',
                "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions."
            ]
        ]),
        'type_retail' => json_encode([
            'type' => 'Vente en magasin',
            'description' => "Les vendeurs au détail, en magasin, aident les clients à trouver les produits qu'ils veulent et traitent les paiements de ces derniers. Ils peuvent vendre des produits comme des vêtements, des meubles, voitures, par example, ou des services, abonnements, loisirs, par example.",
            'activities' => [
                'Présenter: Organiser une présentation ou une démonstration formelle de ce qui est offert.',
                "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions."
            ]
        ]),
        'type_insurance' => json_encode([
            'type' => 'Assurances',
            'description' => "Les agents de vente indépendants, d’assurance, communiquent avec des clients potentiels et vendent un ou plusieurs types d'assurance. Ils ou elles expliquent les différentes polices d'assurance et aident dans le choix les régimes qui leur conviennent.",
            'activities' => [
                'Prospecter: Rechercher et obtenir de nouveaux clients potentiels pour démarrer un processus de vente.',
                "Prise de contact: Entrer en contact avec des personnes à un stade précoce permet de recueillir de l'information et de juger de leur 'valeur' potentielles comme prospects.",
                'Evaluer et analyse de besoins: En savoir plus sur un prospect et son entreprise.',
                'Présenter: Organiser une présentation ou une démonstration formelle de ce qui est offert.',
                "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions."
            ]
        ]),
        'type_inbound' => json_encode([
            'type' => 'Telemarketing entrant ',
            'description' => "Un opérateur de telemarketing entrant, indépendant, accepte les appels des clients actuels et potentiels. Il ou elle enregistre le nom et les renseignements de la personne qui appelle et il ou elle lit souvent des messages écrits sur les promotions en cours. Certains essaient de vendre de nouveaux produits ou services. D'autres traitent les plaintes et aident les clients à régler leurs problèmes, tout en essayant d'améliorer la qualité du service.",
            'activities' => [
                'Premier contact: Répondre au appels.',
                'Evaluer et analyse de besoins: En savoir plus sur un prospect ou un client actif, demandant des informations ou voulant formuler une plainte.',
                'Présenter: Faire une présentation de ce qui est offert.',
                "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions."
            ]
        ]),
        'type_outbound' => json_encode([
            'type' => 'Telemarketing sortant',
            'description' => "Un opérateur de telemarketing sortant, indépendant, appelle des clients actuels et anciens, ou des clients potentiels à partir d'une base de donnée fournie. Généralement, il ou elle appelle à froid un client et tente de vendre un produit ou un service, grâce a un script fourni par l'entreprise. Il ou elle répond également aux questions que les clients peuvent avoir sur les produits ou les services et enregistre les ventes éventuels dans un programme informatique fourni.",
            'activities' => [
                "Prospecter: Rechercher et obtenir de nouveaux clients potentiels pour démarrer un processus de vente.",
                "Prise de contact: Entrer en contact avec des personnes à un stade précoce permet de recueillir de l'information et de juger de leur 'valeur' potentielles comme prospects.",
                'Evaluer et analyse de besoins: En savoir plus sur un prospect et son entreprise',
                'Présenter: Faire une présentation de ce qui est offert.',
                "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions"
            ]
        ]),
        'type_boost' => json_encode([
            'type' => 'Booster les ventes',
            'description' => "Le vendeur digital, indépendant, entre en contact avec des clients actuels et anciens, ou avec des clients potentiels à froid à partir d'outils numériques, d'un site de e-commerce ou de réseaux sociaux.",
            'activities' => [
                'Prospecter: Rechercher et obtenir de nouveaux clients potentiels pour démarrer un processus de vente',
                "Prise de contact: Entrer en contact avec des personnes à un stade précoce permet de recueillir de l'information et de juger de leur 'valeur' potentielles comme prospects.",
                "Evaluer et analyse de besoins: En savoir plus sur un prospect et son entreprise.",
                'Présenter: Organiser une présentation ou une démonstration formelle de ce qui est offert.',
                "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions."
            ]
        ]),
        'type_customer' => json_encode([
            'type' => 'Service client',
            'description' => 'Le responsable du service clientèle « digital », indépendant, sert les clients actuels et potentiels, et répond par écrit aux questions, plaintes et autres messages reçus directement de la clientèle.',
            'activities' => [
                "Prise de contact: Entrer en contact avec des personnes à un stade précoce permet de recueillir de l'information et de juger de leur 'valeur' potentielles comme prospects.",
                'Evaluer et analyse de besoins: En savoir plus sur un prospect et son entreprise.',
                'Présenter: Organiser une présentation ou une démonstration formelle de ce qui est offert.',
                "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions."
            ]
        ]),
        'type_promotion' => json_encode([
            'type' => 'Promotion',
            'description' => "Les démonstrateurs et promoteurs font la démonstration de marchandises et répondent à des questions dans le but de susciter l'intérêt du public pour l'achat du produit ou du service.",
            'activities' => [
                'Informer: Donner des informations sur le produit ou le service, et répondre aux principales questions, avantages concurrentiels, caractéristiques techniques (pour les produits), prix et autres aspects de la transaction commerciale.',
                'Convaincre: Transformer un besoin existant en une volonté d’achat.',
                "Image de marque: Renforcer la présence d’une marque, d’un produit ou d’un service dans l'esprit du public."
            ]
        ]),
        'type_custom' => json_encode([
            'type' => 'Votre propre type de vente',
            'description' => "Vous êtes libre d'expliquer et de détailler votre propre processus de vente au représentant commercial, grâce au chat ROIPAL.",
            'activities' => [
                [
                    '1' => 'Prospecter: Rechercher et obtenir de nouveaux clients potentiels pour démarrer un processus de vente',
                ],
                [
                    '2' => "Prise de contact: Entrer en contact avec des personnes à un stade précoce permet de recueillir de l'information et de juger de leur 'valeur' potentielles comme prospects.",
                ],
                [
                    '3' => 'Evaluer et analyse de besoins: En savoir plus sur un prospect et son entreprise.',
                ],
                [
                    '4' => 'Présenter: Organiser une présentation ou une démonstration formelle de ce qui est offert.',
                ],
                [
                    '5' => 'Networking ou Réseautage: Établir un réseau de contacts, de prospects potentiels par le biais des relations publiques et/ou de la recherche sur Internet, les plateformes numériques, les réseaux sociaux et autres médias électroniques.',
                ],
                [
                    '6' => 'Valoriser: La viabilité de la connexion.',
                ],
                [
                    '7' => 'Informer: Donner des informations sur le produit ou le service, et répondre aux principales questions, avantages concurrentiels, caractéristiques techniques (pour les produits), prix et autres aspects de la transaction commerciale.',
                ],
                [
                    '8' => 'Convaincre: Transformer un besoin existant en une volonté d’achat.',
                ],
                [
                    '9' => "Image de marque: Renforcer la présence d’une marque, d’un produit ou d’un service dans l'esprit du public.",
                ],
                [
                    '10' => "Conclure: Ultime étape à la fin de l’action commerciale. Elle varie considérablement d'une affaire à l'autre et peut inclure la simple présentation d'une proposition, le début d'une négociation, l'obtention du oui ou non de la part des décideurs entres autres actions.",
                ]
            ]
        ]),
    ];