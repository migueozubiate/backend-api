<?php

return [
    'mission_created' => 'Mission créée',
    'mission_started' => 'Mission initiée',
    'mission_finished' => 'Mission terminée',
    'mission_canceled' => 'Mission annulée',
    'mission_goal' => 'Un objectif',
    'mission_missed' => 'Perdu',
    'mission_accepted' => 'Mission :name accepté par :executive',
    'mission_rejected' => "Mission :name il n'a pas été accepté par :executive",
    'mission_been_started' => 'Le mission :name a été lancé',
    'mission_been_finished' => 'Le mission :name  a été terminée',
    'mission_been_canceled' => 'Le mission :name a été annulée',
    'mission_executive_canceled' => 'cadre de mission annulé',
    'mission_qualify_once' => "La mission ne peut être qualifiée qu'une seule fois.",
    'mission_chat_notification' => 'Nouveau message de :name',
    'mission_necessary_activity' => 'La mission doit avoir au moins une activité',
    'mission_time_unit' => 'heures'
 ];

