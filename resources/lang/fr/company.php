<?php

return [
    'company_executive_mission_canceled' => 'La compagnie :company a annulé votre mission :name',
    'company_executive_mission_finished' => 'La compagnie :company a terminé votre mission :name',
    'company_executive_rejected' => "La compagnie :company t'a refusé la mission :mission",
];