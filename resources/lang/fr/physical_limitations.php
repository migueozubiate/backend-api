<?php

return [
    'physical_visual' => 'Limitations visuelles',
    'physical_mobility' => 'Limitations de mobilité',
    'physical_verbal' => 'Limitations verbales',
    'not_physical_limitation' => "Je n'ai aucune limitation"
 ];