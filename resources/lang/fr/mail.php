<?php

return [
    'account_activated' => 'Compte activé',
    'mail_verification' => 'Cher :name, votre email :email a été vérifiée avec succès.',
    'email_verification_subject' => "Vérification de l'email",
    'email_verification_first_line' => 'Veuillez cliquer sur le bouton ci-dessous pour vérifier votre adresse e-mail.',
    'email_verification_action' => "Vérification de l'email",
    'email_verification_second_line' => "Si vous n'avez pas créé de compte, aucune autre action n'est requise.",
    'reset_password_subject' => "Réinitialiser le mot de l'email",
    'reset_password_first_line' => 'Vous recevez ce courriel parce que nous avons reçu une demande de réinitialisation de mot de passe pour votre compte.',
    'reset_password_action' => "Réinitialiser le mot de l'email",
    'reset_password_second_line' => 'Ce lien de réinitialisation de mot de passe expirera en :count de minutes',
    'reset_password_thrid_line' => "Si vous n'avez pas demandé de réinitialisation de mot de passe, aucune autre action n'est requise.",
    'email_subcopy_first_line' => "Si vous rencontrez des problèmes pour cliquer sur le bouton :actionText, copiez et collez l'URL ci-dessous.<br> dans votre navigateur web <a href=':actionURL'> :actionURL </a>'",
    'email_salutation' => 'salutations',
    'email_verify_title' => 'COMPTE ACTIVÉ',
    'email_verify_description' => 'votre compte email <br/> a été confirmée'
];