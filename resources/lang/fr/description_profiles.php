<?php

return [
    "description_C" => json_encode([
        'profile' => 'Calculator',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Aptitude à bien exécuter les tâches difficiles du premier coup.",
            "Il est sensible aux erreurs lorsque la précision et l'exactitude sont requises.",
            "Lorsqu'il travaille dans sa spécialité, il adopte une approche professionnelle et disciplinée.",
            "Il possède un sens de l'organisation et gère bien son temps."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Dehors, moins perfectionniste." ,
            "Vous oublierez d'autres 'manuels'",
            "Il sera moins attentif aux détails et plus enthousiaste."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Précision et qualité.",
            "search_in_others" => "Rechercher dans les autres: Résultats corrects, présentation des preuves et des faits.",
            "influence" => "Influence sur les autres par: L'utilisation des données et leur exactitude.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Des normes élevées pour lui/elle et son équipe ; une grande discipline.",
            "excessive" => "Excessive avec: Les règles et les normes.",
            "stress" => "Faible stress: Devient excessivement critique à son égard et à l'égard des autres.",
            "fears" => " Les peurs: Décisions à haut risque."
        ]
    ]),
    "description_CF" =>  json_encode([
        'profile' => 'Calculator et Farmer',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Aptitude à établir et à atteindre des normes élevées de comportement et de travail.",
            "Sensible aux problèmes, règles, erreurs et procédures.",
            "Capacité à prendre des décisions difficiles sans laisser les émotions interférer.",
            "Capacité à comprendre et à préserver le besoin de systèmes de qualité."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Reflète tes vrais sentiments.",
            "Il sera moins préoccupé par l'impact du changement sur les relations et la qualité.", 
            "aurait plus de confiance et d'interdépendance"
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Sécurité et dextérité.",
            "search_in_others" => "Rechercher dans les autres: La précision qu'ils ont dans les normes.",
            "influence" => "Influence sur les autres par: Votre responsabilité et votre souci du détail.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Persévérance ; maintien des normes.",
            "excessive" => "Excessive avec: Dépendance à l'égard des procédures",
            "stress" => "Faible stress: Devient introverti et obstiné",
            "fears" => " Les peurs: Antagonisme"
        ]
    ]),
    "description_CH" => json_encode([
        'profile' => 'Calculator et Hunter',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Aptitude à faire un travail de qualité, tout en explorant de nouvelles façons d'améliorer la qualité.",
            "Capacité de prendre des décisions difficiles en utilisant la réflexion et les faits sans se laisser emporter par les émotions.",
            "Capacité de persister jusqu'à ce que des solutions valides et acceptables aux problèmes soient trouvées",
            "Défie l'équipe d'obtenir de meilleurs résultats et espère le meilleur."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Plus sensible aux sentiments des autres.",
            "Dehors, moins direct.", 
            "Se manifestera plus ouvertement."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Conception du système.",
            "search_in_others" => "Rechercher dans les autres: Vos propres exigences élevées.",
            "influence" => "Influence sur les autres par: Votre capacité d'aller de l'avant dans le développement de systèmes.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Précision ; Travailleur persévérant",
            "excessive" => "Excessive avec: Faits, chiffres et faits.",
            "stress" => "Faible stress: Perdez votre sang-froid en toute simplicité.",
            "fears" => " Les peurs: Désorganisation."
        ]
    ]),
    "description_CP" => json_encode([
        'profile' => 'Calculator et Promotor',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Promoteur des systèmes de qualité.",
            "Il équilibre le sens de l'urgence avec le maintien de normes élevées.",
            "Organisé, même dans leurs relations. Apprécie la compagnie de personnes ayant des idées similaires",
            "Elle est sensible aux changements dans l'environnement professionnel et social."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Accepter davantage les idées et les croyances des autres.",
            "Fixez-vous des objectifs réalistes", 
            "Ne vous laissez pas influencer par les commentaires des autres."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Diplomatie.",
            "search_in_others" => "Rechercher dans les autres: Les gens qu'ils connaissent, le prestige et les réalisations qu'ils accomplissent.",
            "influence" => "Influence sur les autres par: Les bonnes relations que vous entretenez.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Crée un bon environnement de travail.",
            "excessive" => "Excessive avec: Le tacte.",
            "stress" => "Faible stress: Devient trop flatteur.",
            "fears" => " Les peurs: Devoir renoncer à la qualité pour maintenir de bonnes relations."
        ]
    ]),
    "description_H" => json_encode([
        'profile' => 'Hunter',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Capacité à faire face à des problèmes impliquant de nombreux facteurs.",
            "Orienté vers l'avenir, agressif et compétitif",
            "Capacité à travailler dans un environnement varié avec des changements fréquents",
            "Favorise les activités et donne le rythme nécessaire pour atteindre les résultats souhaités."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Moins nerveux, têtu et direct.",
            "Vous ne forcerez pas les autres à s'engager dans un projet", 
            "Plus patient, plus humble et plus soucieux des gens."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: La domination et l'indépendance.",
            "search_in_others" => "Rechercher dans les autres: Leur capacité à faire les choses rapidement.",
            "influence" => "Influence sur les autres par: Sa persistance et sa force de caractère.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Montrez aux autres votre attitude.",
            "excessive" => "Excessive avec: Le défi et la compétitivité.",
            "stress" => "Faible stress: Devient silencieux et analytique.",
            "fears" => " Les peurs: Perte de contrôle "
        ]
    ]),
    "description_HP" => json_encode([
        'profile' => 'Hunter et Promotor',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Orienté vers les résultats et avec le sens de l'urgence pour atteindre les objectifs et respecter les délais.",
            "Déterminé et agressif face aux défis",
            "Promouvoir des activités par l'intermédiaire d'autres personnes afin d'obtenir des résultats",
            "Cherche activement des relations avec une grande variété de personnes."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Il ne s'énervera pas si vite si les délais ne sont pas respectés.",
            "N'assumez pas tant de responsabilités en même temps", 
            "Faites plus de suivi et n'ayez pas d'attentes aussi élevées."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: La sécurité pour gagner.",
            "search_in_others" => "Rechercher dans les autres: Leur capacité à communiquer et à penser.",
            "influence" => "Influence sur les autres par: Votre gentillesse et votre désir de résultats.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Bon planificateur, capacité à résoudre les problèmes et à chercher des ressources.",
            "excessive" => "Excessive avec: Votre position et votre propre style.",
            "stress" => "Faible stress: Devient nerveux, impatient et insensible.",
            "fears" => " Les peurs: Perdre et faire des erreurs."
        ]
    ]),
    "description_HC" => json_encode([
        'profile' => 'Hunter et Calculator',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Elle se fixe des normes élevées pour elle-même et pour les autres en termes de performance et de travail d'équipe.",
            "Il s'inquiète du coût des erreurs et des erreurs.",
            "Structuré dans l'utilisation du temps.",
            "Résout systématiquement les problèmes sans être influencé par les émotions."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Nous sommes plus accueillants et reconnaissants envers les membres de l'équipe.",
            "Vous équilibrerez mieux les facteurs de qualité et de quantité dans vos décisions", 
            "Il sera moins direct et moins critique à l'égard des personnes qui ne satisfont pas à ses normes."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Dominance et promotion de projets, d'idées et/ou de situations.",
            "search_in_others" => "Rechercher dans les autres: Leurs propres normes et les idées progressistes qu'ils apportent.",
            "influence" => "Influence sur les autres par: Votre compétitivité et votre capacité à relever des défis.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Favorise le changement.",
            "excessive" => "Excessive avec: Ouverture et critique.",
            "stress" => "Faible stress: Devient autoritaire et exigeant.",
            "fears" => " Les peurs: Ne pas avoir d'influence."
        ]
    ]),
    "description_HF" => json_encode([
        'profile' => 'Hunter et Farmer',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Capacité à générer de nouvelles idées et à les mettre en pratique jusqu'à leur réalisation.",
            "Appréciez ceux qui sont des joueurs d'équipe",
            "Capacité d'analyser les questions d'un point de vue à la fois global et détaillé",
            "Détermination et persévérance"
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Ne te concentre pas tant sur un aspect, oubliant d'autres opportunités.",
            "Ne t'inquiète pas tant de tes propres normes", 
            "Revoyez vos priorités avec les autres."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Avoir la décision d'agir.",
            "search_in_others" => "Rechercher dans les autres: La quantité de travail qu'ils terminent.",
            "influence" => "Influence sur les autres par: Sa ténacité et sa persistance.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Orientation vers les résultats en recherchant la cohérence.",
            "excessive" => "Excessive avec: L'autosuffisance.",
            "stress" => "Faible stress: Devient têtu, calme et inexpressif.",
            "fears" => " Les peurs: Etre impliqué avec trop de gens."
        ]
    ]),
    "description_P" => json_encode([
        'profile' => 'Promotor',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" => [
            "Très optimiste et avec un sens de l'humour positif.",
            "Il se concentre sur les gens et a beaucoup de confiance dans les relations.",
            "Aime le réseautage et établit facilement de bonnes relations",
            "Cherche le consensus dans la prise de décision."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" => [
            "Il se concentrera un peu plus sur les objectifs.",
            "Ne t'inquiète pas trop des sentiments des autres.", 
            "Sors mieux organisé et adopte une attitude plus réaliste."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" => [
            "peculiarity" => "Particularité: Soyez serviable et accommodant.",
            "search_in_others" => "Rechercher dans les autres: La chaleur humaine qu'ils montrent.",
            "influence" => "Influence sur les autres par: Sa gentillesse et ses qualités relationnelles.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Transmettre l'objectif global et créer une équipe.",
            "excessive" => "Excessive avec: Optimisme et dépendance à l'égard des autres.",
            "stress" => "Faible stress: Est très émotif et trop confiant envers les autres.",
            "fears" => " Les peurs: Ne pas être valorisé et apprécié par les autres."
        ]
    ]),
    "description_PH" => json_encode([
        'profile' => 'Promotor et Hunter',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Capacité à convaincre les autres de votre point de vue.",
            "Communiquez ouvertement",
            "Capacité à réduire les tensions dans les situations de conflit",
            "Capacité de promouvoir de nouvelles idées ou de nouveaux produits"
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Ne comptez pas trop sur vos émotions pour prendre des décisions.",
            "Serait prêt à s'engager dans une confrontation quand c'est nécessaire", 
            "Fixer des délais réalistes et mieux gérer le temps."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Entretenir des amitiés.",
            "search_in_others" => "Rechercher dans les autres: L'étendue de leurs contacts et leur engagement.",
            "influence" => "Influence sur les autres par: Votre charisme.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Stabilité, responsabilité et un large éventail d'amitiés.",
            "excessive" => "Excessive avec: Enthousiasme.",
            "stress" => "Faible stress: Il parle trop.",
            "fears" => " Les peurs: Échouer ou faire une erreur."
        ]
    ]),
    "description_PF" => json_encode([
        'profile' => 'Promotor et Farmer',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Capacité d'aider les autres par la chaleur, l'empathie et la compréhension.",
            "Les valeurs et les soucis autant des gens que des choses",
            "Bonne capacité d'écoute et de communication."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Plus assertive et déterminée dans certaines situations.",
            "N'évitera pas la confrontation, même dans les situations les plus risquées.",
            "Ayez plus d'initiative et développez votre sens de l'urgence."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Entretenir des amitiés à long terme.",
            "search_in_others" => "Rechercher dans les autres: La loyauté qu'ils vous témoignent",
            "influence" => "Influence sur les autres par: Votre relation personnelle et la démonstration par l'exemple.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Capacité d'écoute et patience avec les autres.",
            "excessive" => "Excessive avec: Tolérance.",
            "stress" => "Faible stress: Devient rancunier et agité.",
            "fears" => " Les peurs:  La confrontation."
        ]
    ]),
    "description_PC" => json_encode([
        'profile' => 'Promotor et Calculator',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Capacité à gérer les situations difficiles avec tact et sensibilité aux besoins des autres.",
            "Capacité à créer une atmosphère agréable et confortable.",
            "Promouvoir efficacement les idées.",
            "Préfère un rythme de travail rapide."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Dehors, moins analytique.",
            "Donner moins d'informations quand il s'agit de vendre des idées ou des produits.",
            "Dehors, plus assertive."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Approbation et acceptation des autres.",
            "search_in_others" => "Rechercher dans les autres: Leur capacité à comprendre les messages verbaux et non verbaux.",
            "influence" => "Influence sur les autres par: Votre aplomb et votre confiance.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Élimine le stress et favorise les projets et les personnes.",
            "excessive" => "Excessive avec: Contrôle de la conversation.",
            "stress" => "Faible stress: Devient ironique avec les autres.",
            "fears" => " Les peurs: Perte de sa propre individualité."
        ] 
    ]),
    "description_F" => json_encode([
        'profile' => 'Farmer',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Capable de projeter une image de calme et de contrôle et de se concentrer pour écouter et apprendre.",
            "Capacité de persévérer dans les tâches qui contribuent le plus à l'organisation",
            "Un membre de l'équipe qui peut être ouvert, patient et tolérant à l'égard des différences",
            "Aime louer les autres."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Démontrer un sens de l'urgence quand l'occasion l'exige.",
            "Il se concentrera moins sur la routine", 
            "Avait plus d'initiative et une meilleure acceptation du changement."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Formalité et stabilité.",
            "search_in_others" => "Rechercher dans les autres: La constance dont ils font preuve.",
            "influence" => "Influence sur les autres par: Votre disposition agréable et facile d'entretien.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Stabilise l'environnement d'une manière amicale.",
            "excessive" => "Excessive avec: Le calme.",
            "stress" => "Faible stress: Il devient inexpressif.",
            "fears" => " Les peurs: L'inconnu et ne pas être apprécié par les autres."
        ]
    ]),
    "description_FC" => json_encode([
        'profile' => 'Farmer et Calculator',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Capacité d'initier des projets et de les mener à terme.",
            "Prêt à travailler pour un leader et une cause",
            "Beaucoup de capacité à chercher des solutions aux problèmes d'une manière logique, en gagnant la compréhension et l'acceptation de tous ceux qui sont impliqués",
            "Faire preuve d'un leadership positif, en tenant compte des sentiments de tous les membres de l'équipe."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Apprendre à avoir plus d'initiative.",
            "Utilisez une approche plus directe.", 
            "Montrez plus de vos préoccupations et de vos sentiments."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Atteindre les normes élevées qu'il établit.",
            "search_in_others" => "Rechercher dans les autres: L'utilisation qu'ils font de leurs connaissances.",
            "influence" => "Influence sur les autres par: Votre capacité de suivi.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Fournit une logique et une concentration des efforts sur les besoins existants.",
            "excessive" => "Excessive avec: Résistance au changement.",
            "stress" => "Faible stress: Devient déterminé et têtu",
            "fears" => " Les peurs: Défaut de répondre aux besoins et/ou aux attentes"
        ]
    ]),
    "description_FP" => json_encode([
        'profile' => 'Farmer et Promotor',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Bonne capacité d'écoute et empathie avec les gens.",
            "Capacité de soutenir et d'aider les autres à atteindre leurs buts et à réaliser leurs aspirations",
            "Acceptez les sentiments, les valeurs et les croyances des autres",
            "Capacité à créer un environnement où les gens se sentent importants."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Sorti plus assertif et déterminé.",
            "Il n'acceptera pas tant que ça le statu quo.", 
            "Soyez plus ferme et plus sûr de vous"
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: Accepter.",
            "search_in_others" => "Rechercher dans les autres: La loyauté, la sincérité et la formalité dont ils font preuve.",
            "influence" => "Influence sur les autres par: Votre compréhension et votre amabilité.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Soutenir, créer la stabilité et l'harmonie lorsqu'il y a de la pression..",
            "excessive" => "Excessive avec: Compassion et gentillesse.",
            "stress" => "Faible stress: Il prend sa retraite.",
            "fears" => " Les peurs: Conflit, désaccord et ne pas être apprécié par les autres."
        ]
    ]),
    "description_FH" => json_encode([
        'profile' => 'Farmer et Hunter',
        "strongholds" => "POINTS FORTS",
        "strongholds_steps" =>  [
            "Capacité de prendre un problème et d'y remédier jusqu'à ce qu'il soit résolu.",
            "Persistant, tenace et logique dans leur recherche de résultats.",
            "Très bon dans le maintien de relations à l'intérieur et à l'extérieur du travail.",
            "Membre de l'équipe qui fera preuve de leadership et défendra ses convictions."
        ],
        "improvement_areas" => "POINTS À AMÉLIORER",
        "improvement_areas_steps" =>  [
            "Adoptez un comportement plus actif, même lorsque votre sécurité est en danger.",
            "Utiliser une pensée plus nouvelle et plus créative pour résoudre les problèmes", 
            "Ne montrez pas tant de résistance aux nouvelles situations qui vous forcent à sortir de votre zone de confort."
        ],
        "big_trends" => "TENDANCES MAJEURES",
        "big_trends_steps" =>  [
            "peculiarity" => "Particularité: : Réalisations personnelles.",
            "search_in_others" => "Rechercher dans les autres: Les succès et les réalisations qu'ils obtiennent.",
            "influence" => "Influence sur les autres par: Votre persévérance.",
            "contribution_company" => "Ce que vous apportez à l'entreprise: Travaille de façon autonome et aime les défis.",
            "excessive" => "Excessive avec: l'ouverture.",
            "stress" => "Faible stress: Il devient obstiné, inflexible et incessant.",
            "fears" => " Les peurs: Ne pas atteindre les résultats escomptés."
        ]
    ])
];
