<?php

return [
    'standard_types' => 'Ventes classiques',
    'specialized' => 'Ventes spécialisées',
    'telemarketing' => 'Telemarketing',
    'e_commerce' => 'e-commerce',
    'promotion' => 'Promotion',
    'custom' => 'Personnalisé'
];