<?php

return [
    'executive_canceled' => "L'exécutif :executive a été annulée le mission :name",
    'executive_finished' => "L'exécutif :executive a été finalisé le mission :name",
    'executive_search_mission' => 'On a commencé à chercher des cadres pour sa mission :title',
    'executive_searching' => 'en cherchant...',
    'executive_activity_started' => 'Activité initiée',
    'executive_been_activity_started' => 'le activité :title a été initié par :executive',
    'executive_activity_finished' => 'Activité terminée',
    'executive_been_activity_finished' => 'le activité :title a été terminée par :executive',
    'executive_rejected' => 'Nous sommes désolés, vous avez été rejeté',
    'executive_calculator' => 'CALCULATEUR',
    'executive_hunter' => 'CHASSEUR',
    'executive_farmer' => 'CULTIVATEUR',
    'executive_promotor' => 'PROMOTEUR',      
 ];

