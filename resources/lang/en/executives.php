<?php

return [
    'executive_canceled' => 'The executive :executive has been canceled the mission :name',
    'executive_finished' => 'The executive :executive has been finished the mission :name',
    'executive_search_mission' => 'Searching executives for your mission :title is started',
    'executive_searching' => 'Searching...',
    'executive_activity_started' => 'Activity started',
    'executive_been_activity_started' => 'The activity :title has been started by :executive',
    'executive_activity_finished' => 'Activity finished',
    'executive_been_activity_finished' => 'The activity :title has been finished by :executive',
    'executive_rejected' => 'Sorry, has been rejected',
    'executive_calculator' => 'CALCULATOR',
    'executive_hunter' => 'HUNTER',
    'executive_farmer' => 'FARMER',
    'executive_promotor' => 'PROMOTOR',
 ];

