<?php

return [
    'company_executive_mission_canceled' => 'The company :company canceled your the mission :name',
    'company_executive_mission_finished' => 'The company :company finished the mission :name',
    'company_executive_rejected' => 'The company :company turned you down for the mission :mission'
];