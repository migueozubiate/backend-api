<?php

return [
    'physical_visual' => 'Visual Limitations',
    'physical_mobility' => 'Mobility Limitations',
    'physical_verbal' => 'Verbal Limitations',
    'not_physical_limitation' => "I don't have any limitation"
 ];

