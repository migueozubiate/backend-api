<?php

return [
    'standard_types' => 'Standard Types',
    'specialized' => 'Specialized',
    'telemarketing' => 'Telemarketing',
    'e_commerce' => 'E-commerce',
    'promotion' => 'Promotion',
    'custom' => 'Custom'
];