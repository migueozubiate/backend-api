<?php

return [
    "description_C" => json_encode([
        'profile' => 'Calculator',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to perform difficult tasks right the first time.",
            "It is sensitive to errors when precision and accuracy are required",
            "When he works in his specialization, he adopts a professional and disciplined approach",
            "He possesses organizational skills and manages time well."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Outside less perfectionist." ,
            'You will forget more of the "manuals".',
            "You wll look less at the details and be more enthusiastic."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Precision and Quality.",
            "search_in_others" => "Search in others: Correct results, presentation of evidence and facts.",
            "influence" => "Influence on others by: The use of data and its accuracy.",
            "contribution_company" => "What you bring to the company: High standards for him/her and his/her team; highly disciplined.",
            "excessive" => "Excessive with: The rules and regulations.",
            "stress" => "Low stress: Becomes excessively critical of self and others.",
            "fears" => "Fears: High-risk decisions."
        ]
    ]),
    "description_CF" =>  json_encode([
        'profile' => 'Calculator and Farmer',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to set and achieve high standards of behavior and work.",
            "Sensitive to problems, norms, errors and procedures",
            "Ability to make difficult decisions without letting emotions interfere.",
            "Ability to understand and preserve the need for quality systems."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Reflect your true feelings.",
            "It will be less concerned with the impact of change on relationships and quality.", 
            "Would have more trust and interdependence."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Safety and dexterity.",
            "search_in_others" => "Search in others: The precision they have in the standards.",
            "influence" => "Influence on others by: Your responsibility and attention to detail.",
            "contribution_company" => "What you bring to the company: Perseverance; maintains standards.",
            "excessive" => "Excessive with: Dependence on procedures",
            "stress" => "Low stress: Becomes introverted and obstinate",
            "fears" => "Fears: Antagonism"
        ]
    ]),
    "description_CH" => json_encode([
        'profile' => 'Calculator and Hunter',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to do quality work, while exploring new ways to increase quality.",
            "Ability to make difficult decisions using reflection and facts without being carried away by emotions.",
            "Ability to persist until valid and acceptable solutions to problems are found.",
            "Challenges the team to achieve better results and hopes for the best."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Out more sensitive to other people's feelings.",
            "Out less direct.", 
            "Will manifest more openly."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Systems design.",
            "search_in_others" => "Search in others: Your own high standards.",
            "influence" => "Influence on others by: Your ability to move forward in systems development.",
            "contribution_company" => "What you bring to the company: Accuracy; Persevering worker",
            "excessive" => "Excessive with: Facts, numbers and facts.",
            "stress" => "Low stress: He loses his temper easily.",
            "fears" => "Fears: Disorganization."
        ]
    ]),
    "description_CP" => json_encode([
        'profile' => 'Calculator and Promotor',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Promoter of Quality Systems.",
            "It balances the sense of urgency with the maintenance of high standards.",
            "Organized even in their relationships. Appreciates the company of people with similar ideas.",
            "It is sensitive to change in the working and social environment."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Accept more of other people's ideas and beliefs.",
            "Set realistic goals.", 
            "Don't be influenced so much by others' comments."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Diplomacy.",
            "search_in_others" => "Search in others: The people they know, the prestige and the achievements they achieve.",
            "influence" => "Influence on others by: The good relations he maintains.",
            "contribution_company" => "What you bring to the company: Creates a good working environment.",
            "excessive" => "Excessive with: Touch.",
            "stress" => "Low stress: Becomes too flattering.",
            "fears" => "Fears: Having to give up quality in order to maintain good relations."
        ]
    ]),
    "description_H" => json_encode([
        'profile' => 'Hunter',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" =>[
            "Ability to cope with problems involving many factors.",
            "Future-oriented, aggressive and competitive.",
            "Ability to work in a varied environment with frequent changes",
            "Promotes activities and sets the pace necessary to achieve desired results."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Out less nervous, stubborn and direct.",
            "You will not force the commitment of others to a project.", 
            "Out more patient and humble and more concerned about people."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity:  Dominance and independence.",
            "search_in_others" => "Search in others: The ability they have to do things quickly.",
            "influence" => "Influence on others by: His persistence and strength of character.",
            "contribution_company" => "What you bring to the company: Show others your attitude.",
            "excessive" => "Excessive with: The challenge and competitiveness.",
            "stress" => "Low stress: Becomes quiet and analytical.",
            "fears" => "Fears: Losing control."
        ]
    ]),
    "description_HP" => json_encode([
        'profile' => 'Hunter and Promotor',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Results-oriented and with a sense of urgency to achieve objectives and meet deadlines.",
            "Determined and aggressive in the face of challenges.",
            "Promotes activities through others to achieve results.",
            "Actively seeks relationships with a wide variety of people."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "He won't get so irritated when deadlines aren't met.",
            "Don't take on so many responsibilities at the same time.", 
            "Do more follow-up and don't have such high expectations."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Security to win.",
            "search_in_others" => "Search in others: The ability they have to communicate and think.",
            "influence" => "Influence on others by: Your kindness and your desire for results.",
            "contribution_company" => "What you bring to the company: Good planner, ability to solve problems and seek resources.",
            "excessive" => "Excessive with: Your position and your own style.",
            "stress" => "Low stress: Becomes nervous, impatient and insensitive.",
            "fears" => "Fears: Losing and making mistakes."
        ]
    ]),
    "description_HC" => json_encode([
        'profile' => 'Hunter and Calculator',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "It sets high standards for itself and others in terms of performance and teamwork",
            "He worries about the cost of errors and mistakes.",
            "Structured in the use of time.",
            "Solves problems systematically without being influenced by emotions."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Out more welcoming and appreciative of the team members.",
            "You will better balance the factors of quality and quantity in your decisions.", 
            "It will be less direct and critical of people who do not meet its standards."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Dominance and promote projects, ideas and/or situations.",
            "search_in_others" => "Search in others: Their own standards and the progressive ideas they bring.",
            "influence" => "Influence on others by: Your competitiveness and your ability to take on challenges.",
            "contribution_company" => "What you bring to the company: Promotes change.",
            "excessive" => "Excessive with: Openness and criticism.",
            "stress" => "Low stress: Becomes authoritarian and demanding.",
            "fears" => "Fears: Don't be influential."
        ]
    ]),
    "description_HF" => json_encode([
        'profile' => 'Hunter and Farmer',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to generate new ideas and put them into practice until their completion.",
            'Appreciate those who are "team players"',
            "Ability to analyse issues from both a global and detailed point of view",
            "Determination and persistence."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Don't focus so much on one aspect, forgetting other opportunities.",
            "Don't worry so much about your own standards", 
            "Review your priorities with others."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Have a decision to act.",
            "search_in_others" => "Search in others: The amount of work they finish.",
            "influence" => "Influence on others by: His tenacity and persistence.",
            "contribution_company" => "What you bring to the company: Orientation to results looking for consistency.",
            "excessive" => "Excessive with: Self-sufficiency.",
            "stress" => "Low stress: Becomes stubborn, quiet and inexpressive.",
            "fears" => "Fears: Being involved with too many people.."
        ]
    ]),
    "description_P" => json_encode([
        'profile' => 'Promotor',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Very optimistic and with a positive sense of humor.",
            "He focuses on people and has a lot of confidence in relationships.",
            "Enjoys networking and builds good relationships easily.",
            "Seeks consensus in decision making."
        ], 
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "It'll focus a little more on the objectives.",
            "Don't worry so much about other people's feelings." ,
            "Get out more organized and have a more realistic attitude."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Be helpful and accommodating.",
            "search_in_others" =>  "Search in others: The human warmth they show.",
            "influence" => "Influence on others by: His friendliness and interpersonal skills.",
            "contribution_company" => "What you bring to the company: Transmits the overall goal and builds team.",
            "excessive" => "Excessive with: Optimism and dependence on others.",
            "stress" => "Low stress: Very emotional and overconfident in others.",
            "fears" => "Fears: Not being valued and appreciated by others."
        ]
    ]),
    "description_PH" => json_encode([
        'profile' => 'Promotor and Hunter',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to convince others of your point of view.",
            "Communicate openly.",
            "Ability to reduce tension in conflict situations.",
            "Ability to promote new ideas or products"
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Don't rely so much on emotions to make decisions.",
            "Would be willing to engage in confrontation when necessary.", 
            "Set realistic time frames and manage time better."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Maintain friendships.",
            "search_in_others" => "Search in others: The breadth of contacts they have and their commitment.",
            "influence" => "Influence on others by: His charisma",
            "contribution_company" => "What you bring to the company: Stability, responsibility and a wide range of friendships.",
            "excessive" => "Excessive with: The enthusiasm.",
            "stress" => "Low stress: He talks too much.",
            "fears" => "Fears: Fail or make a mistake."
        ]
    ]),
    "description_PF" => json_encode([
        'profile' => 'Promotor and Farmer',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to help others using warmth, empathy and understanding.",
            "Values and cares as much about people as things",
            "Good listening and communication skills."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Out more assertive and determined in certain situations.",
            "Will not avoid confrontation, even in the most risky situations.",
            "Have more initiative and develop more of your sense of urgency."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Maintain long-term friendships",
            "search_in_others" => "Search in others: The loyalty they show you.",
            "influence" => "Influence on others by: Your personal relationship and demonstrating by example.",
            "contribution_company" => "What you bring to the company: Ability to listen and patience with others.",
            "excessive" => "Excessive with: the tolerance.",
            "stress" => "Low stress: Becomes resentful and restless.",
            "fears" => "Fears: The confrontation."
        ]
    ]),
    "description_PC" => json_encode([
        'profile' => 'Promotor and Calculator',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to handle difficult situations with tact and sensitivity to the needs of others",
            "Ability to create a pleasant and comfortable atmosphere.",
            "Effectively promotes ideas.",
            "Prefers a fast pace of work."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Outside less analytical.",
            "Give less information when it comes to selling ideas or products.", 
            "Out more assertive."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: The approval and acceptance of others.",
            "search_in_others" => "Search in others: Their ability to understand verbal and non-verbal messages.",
            "influence" => "Influence on others by: His aplomb and confidence.",
            "contribution_company" => "What you bring to the company: Delete stress and promotes projects and people.",
            "excessive" => "Excessive with: Conversation control.",
            "stress" => "Low stress: Becomes ironic with others.",
            "fears" => "Fears: Loss of one's own individuality."
        ] 
    ]),
    "description_F" => json_encode([
        'profile' => 'Farmer',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to project an image of calm and control and to concentrate in order to listen and learn.",
            "Ability to persist in the tasks that make the greatest contribution to the organization.",
            "Team member who can be open, patient and tolerant of differences.",
            "Enjoys praising others."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Demonstrate a sense of urgency when the occasion requires it.",
            "It will focus less on routine", 
            "Had more initiative and better acceptance of change."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Formality and stability.",
            "search_in_others" => "Search in others: The consistency they show.",
            "influence" => "Influence on others by: Your pleasant disposition and service.",
            "contribution_company" => "What you bring to the company: Stabilizes the environment in a friendly way.",
            "excessive" => "Excessive with: The composure.",
            "stress" => "Low stress: Becomes inexpressive.",
            "fears" => "Fears: The unknown and not to be appreciated by others.."
        ]
    ]),
    "description_FC" => json_encode([
        'profile' => 'Farmer and Calculator',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to initiate projects and work on them to completion.",
            "Willing to work for a leader and a cause",
            "Much ability to seek solutions to problems in a logical way, gaining the understanding and acceptance of all involved",
            "Demonstrates positive leadership, showing consideration for the feelings of all team members."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Learn to have more initiative.",
            "Use a more direct approach.",
            "Show more of your concerns and feelings."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Achieve the high standards it sets",
            "search_in_others" => "Search in others: The use they make of their knowledge.",
            "influence" => "Influence on others by: Their ability to follow up.",
            "contribution_company" => "What you bring to the company: Provides logic and concentration of efforts to existing needs.",
            "excessive" => "Excessive with: Resistance to change.",
            "stress" => "Low stress: Becomes determined and stubborn",
            "fears" => "Fears: Failure to respond to needs and/or expectations"
        ]
    ]),
    "description_FP" => json_encode([
        'profile' => 'Farmer and Promotor',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Good listening skills and empathy with people.",
            "Ability to support and assist others in achieving their goals and aspirations",
            "Accept the feelings, values and beliefs of others",
            "Ability to create an environment where people feel important."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Out more assertive and determined.",
            "He will not accept the status quo so much.", 
            "Out more assertive and self-confident."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Accept.",
            "search_in_others" => "Search in others: The loyalty, sincerity and formality they show.",
            "influence" => "Influence on others by: Your understanding and friendliness.",
            "contribution_company" => "What you bring to the company: Support, creates stability and harmony when there is pressure.",
            "excessive" => "Excessive with: Compassion and kindness.",
            "stress" => "Low stress: It goes.",
            "fears" => "Fears: Conflict, disagreement and not being appreciated by others."
        ]
    ]),
    "description_FH" => json_encode([
        'profile' => 'Farmer and Hunter',
        "strongholds" => "STRENGTHS",
        "strongholds_steps" => [
            "Ability to take a problem and work on it until it's solved.",
            "Persistent, tenacious and logical in their search for results",
            "Very good at maintaining relationships both inside and outside of work",
            "Member of the team who will show leadership skills, aggressively defending what he believes."
        ],
        "improvement_areas" => "AREAS FOR IMPROVEMENT",
        "improvement_areas_steps" => [
            "Demonstrate more active behavior, even when your safety is in jeopardy.",
            "Use newer, more creative thinking when solving problems.", 
            "Don't show so much resistance to new situations that force you out of your comfort zone."
        ],
        "big_trends" => "BIG TRENDS",
        "big_trends_steps" => [
            "peculiarity" => "Particularity: Personal Achievement.",
            "search_in_others" => "Search in others: The successes and achievements they achieve.",
            "influence" => "Influence on others by: Your perseverance..",
            "contribution_company" => "What you bring to the company: Works independently and likes challenges.",
            "excessive" => "Excessive with: The frankness",
            "stress" => "Low stress: Becomes obstinate, inflexible and incessant.",
            "fears" => "Fears: Not achieving the desired results."
        ]
    ])
];
