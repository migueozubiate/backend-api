<?php

return [
    'mission_created' => 'Mission created',
    'mission_started' => 'Mission started',
    'mission_finished' => 'Mission finished',
    'mission_canceled' => 'Mission canceled ',
    'mission_goal' => 'Goal',
    'mission_missed' => 'Missed',
    'mission_accepted' => 'Mission :name accepted by :executive',
    'mission_rejected' => 'Mission :name was not accepted by :executive',
    'mission_been_started' => 'The mission :name  has been started',
    'mission_been_finished' => 'The mission :name has been finished',
    'mission_been_canceled' => 'The mission :name has been canceled',
    'mission_executive_canceled' => 'mission executive canceled',
    'mission_qualify_once' => 'You can only qualify the mission once.',
    'mission_chat_notification' => 'New Message from :name',
    'mission_necessary_activity' => 'The mission must have at least one activity',
    'mission_time_unit' => 'hours'
 ];

