<?php

return [
        'type_insideSales' => json_encode([
            'type' => 'Inside sales/Client services',
            'description' => 'An inside sales rep, is responsible for maintaining existing client relationships. He or she is the main point of contact for the company’s clients, and are expected to retain their business and build a strong business relationship.',
            'activities' => [
                'Prospect: Sourcing new, early stage leads to begin a sales process with.',
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ]
        ]),
        'type_outsideSales' => json_encode([
            'type' => 'Outside sales/Bu.development',
            'description' => 'Outside sales is considered as a traditional method in that it is face-to-face, done primarily outside of the office in direct interaction with their customers.',
            'activities' => [
                'Prospect: Sourcing new, early stage leads to begin a sales process with.',
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Research: Learning more about a prospect and their company.',
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ]
        ]),
        'type_lead' => json_encode([
            'type' => 'Lead generation',
            'description' => 'Is responsible for conducting research and networking to make new business contacts. Also be responsible for assessing the viability of the connection and to what extent it’s worth pursuing and forecasting potential results.',
            'activities' => [
                'Research: Learning more about a prospect and their company.',
                'Networking: To make new business contacts.',
                'Assessing: The viability of the connection.'
            ]
        ]),
        'type_pharma' => json_encode([
            'type' => 'Pharma',
            'description' => 'Pharmaceutical sales representative is responsible for presenting new medications to doctors.',
            'activities' => [
                'Prospect: Sourcing new, early stage leads to begin a sales process with.',
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Present: To run a formal presentation or demonstration of what is being sold.'
            ]
        ]),
        'type_real' => json_encode([
            'type' => 'Real estate',
            'description' => 'Real estate sales agents help clients buy, sell, and rent properties.',
            'activities' => [
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Research: Learning more about a prospect and their company.',
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ]
        ]),
        'type_retail' => json_encode([
            'type' => 'Retail',
            'description' => 'Retail sales workers help customers find products they want and process customers’ payments. They can sell retail merchandise, such as clothing, furniture, automobiles…',
            'activities' => [
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ]
        ]),
        'type_insurance' => json_encode([
            'type' => 'Insurance',
            'description' => 'Insurance sales agents contact potential customers and sell one or more types of insurance. Insurance sales agents explain various insurance policies and help clients choose plans that suit them.',
            'activities' => [
                'Prospect: Sourcing new, early stage leads to begin a sales process with.',
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Research: Learning more about a prospect and their company.',
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ]
        ]),
        'type_inbound' => json_encode([
            'type' => 'Inbound Telemarketing',
            'description' => 'An inbound telemarketer accepts incoming calls from current and potential customers. The telemarketer records the name and information of the person calling and often reads written prompts about current promotions or products. Some inbound telemarketers try to sell new products to customers, such as a cell phone company marketing plan upgrades to customers calling in for other reasons. Other inbound telemarketers handle complaints and help customers with problems, while also trying to sell new products.',
            'activities' => [
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Research: Learning more about a prospect and their company.',
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ]
        ]),
        'type_outbound' => json_encode([
            'type' => 'Outbound Telemarketing',
            'description' => 'Outbound telemarketers call current and former customers, or cold call potential customers from a directory phone listing. Typically, an outbound telemarketer cold calls a customer and tries to sell a product or solicit a donation for a charity by reading a script provided by his company or charitable organization. An outbound telemarketer also answers questions customers may have about products or services, and records sales in a computer program. For example, an outbound telemarketer for a direct mail catalog company may call former customers and promote new products to bring in more sales.',
            'activities' => [
                'Prospect: Sourcing new, early stage leads to begin a sales process with.',
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Research: Learning more about a prospect and their company.',
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ]
        ]),
        'type_boost' => json_encode([
            'type' => 'Boost your sales',
            'description' => 'Digital sales rep make contact with current and former customers, or cold contact potential customers from digital tools, e-commerce website, social media.',
            'activities' => [
                'Prospect: Sourcing new, early stage leads to begin a sales process with, on the net (social media).',
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Research: Learning more about a prospect and their company.',
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ]
        ]),
        'type_customer' => json_encode([
            'type' => 'Customer service',
            'description' => 'Digital client services accepts incoming contact from current and potential customers, records the name and information of the person contacting and often response written prompts about current promotions or products.',
            'activities' => [
                'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                'Research: Learning more about a prospect and their company.',
                'Present: To run a formal presentation or demonstration of what is being sold.',
                'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.'
            ]
        ]),
        'type_promotion' => json_encode([
            'type' => 'Promotion',
            'description' => 'Demonstrators and Product Promoters demonstrate merchandise and answer questions for the purpose of creating public interest in buying the product, or service.',
            'activities' => [
                'Inform: Convert an existing need into a want or to stimulate interest in a new product early stages life cycle.',
                'Persuade: Persuasive promotion is designed to stimulate a purchase or an action.',
                'Remind: To keep the product and brand name in the public mind. crest toothpaste, tide laundry, miller beer, consumer products, maturity phase in product life cycle.'
            ]
        ]),
        'type_custom' => json_encode([
            'type' => 'Your own sales type',
            'description' => 'You are free to explain and detail your own sales process to the sales rep, thanks to the ROIPAL chat.',
            'activities' => [
                [
                    '1' => 'Prospect: Sourcing new, early stage leads to begin a sales process with.',
                ],
                [
                    '2' => 'Connect: Initiating contact with those early stage leads to gather information and judge their worthiness.',
                ],
                [
                    '3' => 'Research: Learning more about a prospect and their company.',
                ],
                [
                    '4' => 'Present: To run a formal presentation or demonstration of what is being sold.',
                ],
                [
                    '5' => 'Networking: To make new business contacts.',
                ],
                [
                    '6' => 'Assessing: The viability of the connection.',
                ],
                [
                    '7' => 'Inform: Convert an existing need into a want or to stimulate interest in a new product early stages life cycle.',
                ],
                [
                    '8' => 'Persuade: Persuasive promotion is designed to stimulate a purchase or an action.',
                ],
                [
                    '9' => 'Remind: To keep the product and brand name in the public mind. crest toothpaste, tide laundry, miller beer, consumer products, maturity phase in product life cycle.',
                ],
                [
                    '10' => 'Close: Late-stage activities that happen as a deal approaches closing. It varies widely from company to company, and may include things like delivering a quote or proposal, negotiation, achieving the buy-in of decision makers, and other actions.',
                ]
            ]
        ]),
    ];