<?php

return [
    'invitation_mission' => 'Invitation to a mission :mission',
    'invitation_company' => 'The company :company invites you to participate in the mission :mission',
 ];

