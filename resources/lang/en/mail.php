<?php

return [
    'account_activated' => 'Account activated',
    'mail_verification' => 'Dear :name, your email :email has been verified successfully',
    'email_verification_subject' => 'Email Verification.',
    'email_verification_first_line' => 'Please click the button below to verify your email address.',
    'email_verification_action' => 'Verify email address',
    'email_verification_second_line' => 'If you have not created an account, no further action is required.',
    'reset_password_subject' => 'Reset Password.',
    'reset_password_first_line' => 'You are receiving this email because we received a password reset request for your account.',
    'reset_password_action' => 'Reset Password.',
    'reset_password_second_line' => 'This password reset link will expire in :count minutes',
    'reset_password_thrid_line' => 'If you have not requested a password reset, no further action is required.',
    'email_subcopy_first_line' => 'If you’re having trouble clicking the :actionText button, copy and paste the URL below. <br> into your web browser <a href=":actionURL"> :actionURL </a>',
    'email_salutation' => 'Regards',
    'email_verify_title' => 'ACTIVATED ACCOUNT',
    'email_verify_description' => 'your email account <br/> has been confirmed'
];