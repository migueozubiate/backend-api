<?php
namespace Tests\Support;

trait ExecutiveAssestmentAnswersTrait
{

    protected function getDiscAssestmentAnswers()
    {
        return [
            "answers" => [
                [
                    "number" => 1,
                    "answer" => "BDCA"
                ],
                [
                    "number" => 2,
                    "answer" => "ADCB"
                ],
                [
                    "number" => 3,
                    "answer" => "CADB"
                ],
                [
                    "number" => 4,
                    "answer" => "DABC"
                ],
                [
                    "number" => 5,
                    "answer" => "BADC"
                ],
                [
                    "number" => 6,
                    "answer" => "BADC"
                ],
                [
                    "number" => 7,
                    "answer" => "CBDA"
                ],
                [
                    "number" => 8,
                    "answer" => "CBDA"
                ],
                [
                    "number" => 9,
                    "answer" => "DCAB"
                ],
                [
                    "number" => 10,
                    "answer" => "ABCD"
                ],
                [
                    "number" => 11,
                    "answer" => "BACD"
                ],
                [
                    "number" => 12,
                    "answer" => "BDCA"
                ],
                [
                    "number" => 13,
                    "answer" => "DBCA"
                ],
                [
                    "number" => 14,
                    "answer" => "DCBA"
                ],
                [
                    "number" => 15,
                    "answer" => "CABD"
                ],
                [
                    "number" => 16,
                    "answer" => "CABD"
                ],
                [
                    "number" => 17,
                    "answer" => "CBDA"
                ]
            ]
        ];
    }

    protected function getRoipalAssessmentAnswers()
    {
        $options = [
            ['1a', '2b', '3c', '4d'],
            ['2a', '3b', '4c', '1d'],
            ['3a', '4b', '1c', '2d'],
            ['4a', '1b', '2c', '3d']
        ];

        $answers = [];
        for ($idx=0; $idx < 54; $idx++) {
            $option_idx = array_rand($options,1);
            $answers[] = [
                'number' => $idx+1,
                'answer' => join('', $options[$option_idx])
            ];
        }
        return [
            'answers' => $answers
        ];
    }
}