<?php
namespace Tests\Support;

trait ExecutiveRegistrationTestTrait
{

    public function makeExecutiveUser($terms = true, $password = '12345678a')
    {
        return [
            'name' => $this->faker()->firstName,
            'email' => $this->faker()->email,
            'password' => $password,
            'terms_accepted' => $terms,
        ];
    }

}