<?php
namespace Tests\Support;

use Faker\Factory as Faker;

trait EmployeerTrait
{
    protected function getDataStoreValid()
    {
        return [
            'name' => 'company',
            'email' => $this->faker()->email,
            'password' => '12345678'
        ];
    }
}