<?php
namespace Tests\Support;

use Faker\Factory as Faker;

trait CompanyTrait
{
    protected function getDataUpdateCompanyValid()
    {
        return [
            'business_name' => 'compañia SA de CV ',
            'name' => 'tiendita de la esquina',
            'vertical' => 'acaba algo',
            'name' => 'jorge',
            'profile' => [
                'video_bio_url' => 'http://www.google.com',
                'bio' => 'test',
                'website' => 'http://www.google.com',
                'vertical' => '12345678',
                'address' => 'addrees',
                'position' => [
                    'latitude' => '19.362722',
                    'longitude' => '-99.179361'
                ]
            ],
            'payment' => [
                'owner' => '2',
                'card_number' => '4444444444444444',
                'exp' => '02/21',
                'ccv' => '123'
            ]
        ];
    }
}