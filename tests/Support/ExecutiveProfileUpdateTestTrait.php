<?php
namespace Tests\Support;

trait ExecutiveProfileUpdateTestTrait
{
    protected function getDatafromUpdateSellerProfile($value = '')
    {
        return [
            'bio' => 'Vendo colchones, refrigeradores, estufas,  microondas o cosas de fierro que quiera comprar.',
            'address' => 'Copal 232',
            'phone' => 'copa',
            'video_bio_url' => 'http://roipal-video.mp4',
            'image_bio_url' => 'http://roipal-image.png',
            'position' => [
                'latitude' => -90.00,
                'longitude' => 45,
            ],
            'website' => 'http=>//www.website.com',
            'limitations' => ['Lim2', 'Lim1'],
        ];
    }
}
