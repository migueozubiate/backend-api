<?php
namespace Tests\Support;

trait CompanyAssessmentTrait
{
    protected function getDataUpdateCompanyAssessmentValid()
    {
        return [
            'assesment' => [
                'name' => 'NameCompanyAssestment',
                'summary' => 'Description of the evaluation',
                'questions' => [
                    [
                        'number' => 1,
                        'question' => 'Group 1',
                        'options' => [
                            'A' => 'Exacto/a',
                            'B' => 'Seguro de sí mismo/a',
                            'C' => 'Considerado/a',
                            'D' => 'Firme',
                        ],
                        'answer' => 'A',
                    ],
                ],
            ],
        ];
    }

    protected function getDataCompanyAssessment()
    {
        return [
            'answers' => [
                [
                    'number' => 1,
                    'answer' => 'D',
                ],
                [
                    'number' => 2,
                    'answer' => 'C',
                ],
                [
                    'number' => 3,
                    'answer' => 'D',
                ],
                [
                    'number' => 4,
                    'answer' => 'B',
                ],
                [
                    'number' => 5,
                    'answer' => 'C',
                ],
            ],
        ];
    }
}
