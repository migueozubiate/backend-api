<?php
namespace Tests\Support\Missions;

use Faker\Factory as Faker;

trait MissionListByExecutivesTrait
{

    protected function dataSended()
    {
          return [
                    'message',
                    'data'=> [
                        '*'=>[
                              'uuid',
                              'name',
                              'description',
                              'video',
                              'video_url',
                              'type',
                              'profile',
                              'time',
                              'time_unit',
                              'fare_by_time',
                              'subtotal',
                              'tax',
                              'status',
                              'total',
                              'executives_requested',
                              'invitations_sent',
                              'accepted',
                              'rejected',
                              'updated_at',
                              'created_at'
                            ]
                         ]
                ];
    }

    protected function dataSendedWithIncludes()
    {
          return [
                    'message',
                    'data'=> [
                        '*'=>[
                              'uuid',
                              'name',
                              'description',
                              'video',
                              'video_url',
                              'type',
                              'profile',
                              'time',
                              'time_unit',
                              'fare_by_time',
                              'subtotal',
                              'tax',
                              'status',
                              'total',
                              'executives_requested',
                              'invitations_sent',
                              'accepted',
                              'rejected',
                              'updated_at',
                              'created_at',
                              'activities'=> [
                                   '*'=> [
                                        'uuid',
                                        'title',
                                        'description',
                                        'status',
                                        'started_at',
                                        'finished_at'
                                    ]
                              ],
                              'locations'=> [
                                  '*'=>[
                                      'uuid',
                                      'name',
                                      'address',
                                      'executives_requested',
                                      'invitations_sent',
                                      'accepted',
                                      'rejected',
                                      'latitude',
                                      'longitude',
                                      'updated_at',
                                      'created_at'
                                  ]
                          ],
                          'company'=> [
                              'uuid',
                              'business_name',
                              'name',
                              'vertical',
                              'terms_accepted',
                              'updated_at',
                              'created_at'
                          ],
                          'invitations'=> [
                              '*'=>[
                                  'uuid',
                                  'status',
                                  'reason',
                                  'notification',
                                  'response_at',
                                  'updated_at',
                                  'created_at'
                              ]
                          ]
                    ]
                  ],
                    'meta'=> [
                        'cursor'=> [
                            'current',
                            'prev',
                            'next',
                            'count'
                        ]
                    ]
                ];
    }

}