<?php
namespace Tests\Support\Missions;

use Faker\Factory as Faker;

trait MissionListByCompanyLocationTrait
{

    protected function dataSended()
    { 
          return [
                  'message',
                  'data'=> [
                      'uuid',
                      'name',
                      'address',
                      'executives_requested',
                      'invitations_sent',
                      'accepted',
                      'rejected',
                      'latitude',
                      'longitude',
                      'updated_at',
                      'created_at'
                      ]
                ];
    }


    protected function dataSendedWithIncludes()
    { 
          return [
                  'message',
                  'data'=> [
                      'uuid',
                      'name',
                      'address',
                      'executives_requested',
                      'invitations_sent',
                      'accepted',
                      'rejected',
                      'latitude',
                      'longitude',
                      'updated_at',
                      'created_at',
                      'company'=>[
                      'uuid',
                      'business_name',
                      'name',
                      'vertical',
                      'terms_accepted',
                      'updated_at',
                      'created_at'
                      ],
                      'executives'=>[
                        '*'=> [
                                'uuid',
                                'name',
                                'email',
                                'classification',
                                'created_at',
                                'updated_at'
                              ]  
                      ],
                      'mission'=> [
                          'uuid',
                          'name',
                          'description',
                          'video',
                          'video_url',
                          'type',
                          'profile',
                          'time',
                          'time_unit',
                          'fare_by_time',
                          'subtotal',
                          'tax',
                          'status',
                          'total',
                          'executives_requested',
                          'invitations_sent',
                          'accepted',
                          'rejected',
                          'updated_at',
                          'created_at',
                          'company'=> [
                              'uuid',
                              'business_name',
                              'name',
                              'vertical',
                              'terms_accepted',
                              'updated_at',
                              'created_at'
                          ]
                      ]

                  ]
                ];
    }

}