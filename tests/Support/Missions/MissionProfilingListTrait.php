<?php
namespace Tests\Support\Missions;

use Faker\Factory as Faker;

trait MissionProfilingListTrait
{
    protected  function dataRetrieve()
    {
      return [
            'mission_type',
            'profile',
            'total_cost',
            'locations'=> [
              [
                'uuid',
                'name',
                'address',
                'latitude',
                'longitude',
                'executives_requested'
              ]
            ]
      ];
    }

    protected function dataSended()
    { 
          return [
                    'message',
                    'data'=> [
                       '*'=> [
                            'uuid',
                            'name',
                            'address',
                            'latitude',
                            'longitude',
                            'executives_requested',
                            'executives'=> [
                                '*'=>[
                                    'uuid',
                                    'name',
                                    'email',
                                    'classification',
                                    'created_at',
                                    'updated_at',
                                    'profile'=> [
                                        'id',
                                        'bio',
                                        'video_bio',
                                        'video_bio_url',
                                        'website',
                                        'terms_accepted',
                                        'limitations'
                                    ],
                                    'address'=> [
                                        'address',
                                        'latitude',
                                        'longitude'
                                    ]
                                ]
                            ]
                        ]
                    ]
                ];
    }

}