<?php
namespace Tests\Support\Missions;

use Faker\Factory as Faker;

trait MissionListTestTrait
{
    protected function dataSended()
    {
        return [
          'message',
          'data' => [
            '*'=> [
                    'uuid',
                    'name',
                    'description',
                    'video',
                    'video_url',
                    'type',
                    'profile',
                    'time',
                    'time_unit',
                    'fare_by_time',
                    'subtotal',
                    'tax',
                    'total',
                    'executives_requested',
                    'invitations_sent',
                    'accepted',
                    'rejected',
                    'updated_at',
                    'created_at'
                  ]
          ]    
        ];
    }

      protected function dataSendedWithIncludes()
    {
        return [
          'message',
          'data' => [
            '*'=> [
                    'uuid',
                    'name',
                    'description',
                    'video',
                    'video_url',
                    'type',
                    'profile',
                    'time',
                    'time_unit',
                    'fare_by_time',
                    'subtotal',
                    'tax',
                    'total',
                    'executives_requested',
                    'invitations_sent',
                    'accepted',
                    'rejected',
                    'updated_at',
                    'created_at',
                    'company'=>[
                      'uuid',
                      'business_name',
                      'name',
                      'vertical',
                      'terms_accepted',
                      'updated_at',
                      'created_at'
                    ],
                    'executives'=>[
                      '*'=> [
                              'uuid',
                              'name',
                              'email',
                              'classification',
                              'created_at',
                              'updated_at'
                            ]  

                    ],
                    'locations'=>[
                      '*'=> [
                              'uuid',
                              'name',
                              'address',
                              'executives_requested',
                              'invitations_sent',
                              'accepted',
                              'rejected',
                              'latitude',
                              'longitude',
                              'updated_at',
                              'created_at'
                            ]
                      ]
                  ]
            ],
            'meta'=> [
            'cursor'=> [
                'current',
                'prev',
                'next',
                'count'
                  ]
              ]    
        ];
    }

}