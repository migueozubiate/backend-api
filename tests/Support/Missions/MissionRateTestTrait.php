<?php
namespace Tests\Support\Missions;

use Faker\Factory as Faker;

trait MissionRateTestTrait
{

    protected function dataSended()
    {
          return [
                    'message',
                    'data'=> [
                        [
                            'id',
                            'uuid',
                            'user_id',
                            'company_id',
                            'name',
                            'description',
                            'video',
                            'video_url',
                            'type',
                            'profile',
                            'time',
                            'time_unit',
                            'fare_by_time',
                            'subtotal',
                            'tax',
                            'status',
                            'total',
                            'executives_requested',
                            'invitations_sent',
                            'accepted',
                            'rejected',
                            'score_mission',
                            'created_at',
                            'updated_at'
                            ]
                         ]
                ];
    }

    protected function wrongInputRetrieve()
    {

    }

    protected  function wrongfieldRetrieve()
    {
      
    }

}