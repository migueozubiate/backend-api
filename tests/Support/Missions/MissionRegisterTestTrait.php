<?php
namespace Tests\Support\Missions;

trait MissionRegisterTestTrait
{
    protected function getData()
    {
        return [
            'name' => 'Mission',
            'date' => '2018-05-09',
            'description' => 'Promoción en bla bla bla .... zzzzzz',
            'type' => 'Promoción',
            'profile' => 'promotor',
            'time' => 4,
            'time_unit' => 'hours',
            'fare_by_time' => 200.00,
            'subtotal' => 800.00,
            'taxes' => 0.00,
            'total' => 800.00,
            'suggested' => [
                'profile' => 'promotor',
                'time' => 2,
                'time_unit' => 'hours',
                'currency' => 'MXN',
                'min_time' => 2,
                'max_time' => 50,
                'fare_by_time' => 200.00,
                'estimations' => [
                    [
                        'min' => 1,
                        'max' => 10,
                        'fare' => 200.00,
                    ],
                ],
            ],
            'locations' => [
                [

                    'name' => 'La Comercial Mexicana',
                    'address' => 'Insurgentes sur 1524',
                    'latitude' => '-19.029372',
                    'longitude' => '99.182737',
                    'executives_requested' => 2,
                ],
            ],
        ];
    }
}
