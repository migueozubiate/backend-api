<?php
namespace Tests\Feature\Assestments;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Tests\Support\ExecutiveAssestmentAnswersTrait;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExecutiveDiscAssestmentAnswerTest extends TestCase
{
    use ExecutiveAssestmentAnswersTrait;

    public function testStoreAnswerForNotLoggedInUser()
    {
        $user = factory(User::class)->create();

        $response = $this->json("POST", "/api/executives/{$user->uuid}/assestments/disc");

        $response->assertStatus(401);

        $response->assertJsonStructure([
            'message',
        ]);
    }

    public function testStoreAnswerWithMissingValues()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $fieldsToForget = [
            'answers'
        ];

        foreach ($fieldsToForget as $field) {
            $data = $this->getDiscAssestmentAnswers();

            array_forget($data, $field);

            $response = $this->json("POST", "/api/executives/{$user->uuid}/assestments/disc", $data);

            $response->assertStatus(422);

            $response->assertJsonStructure([
                'message',
                'code',
                'errors'
            ]);

            $response->assertJsonValidationErrors([$field]);
        }

        $fieldsToForget = [
            'number',
            'answer'
        ];

        foreach ($fieldsToForget as $field) {
            $data = $this->getDiscAssestmentAnswers();

            $answers = array_get($data, 'answers');

            foreach ($answers as $index => $answer) {
                array_forget($data, "answers.{$index}.{$field}");
            }

            $response = $this->json("POST", "/api/executives/{$user->uuid}/assestments/disc", $data);

            $response->assertStatus(422);

            $response->assertJsonStructure([
                'message',
                'code',
                'errors'
            ]);

            $errors = [];

            foreach ($answers as $index => $answer) {
                $errors[] = "answers.{$index}.{$field}";
            }

            $response->assertJsonValidationErrors($errors);
        }
    }

    public function testStoreAnswer()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $data = $this->getDiscAssestmentAnswers();

        $response = $this->json("POST", "/api/executives/{$user->uuid}/assestments/disc", $data);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'message',
            'data' => [
                'uuid',
                'name',
                'total_questions',
                'created_at',
                'updated_at',
            ]
        ]);
    }
}
