<?php

namespace Tests\Feature\Assestments;

use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Tests\TestCase;

class CompanyAssessmentGetTest extends TestCase
{
    public function testNotAuth()
    {
        $response = $this->getJson("/api/assestments?includes=company,questions");
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message',
        ]);
    }

    public function testAllOk()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $response = $this->getJson('/api/assestments/');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "message",
            "data" => [
            '*' => [
                "uuid",
                "name",
                "summary",
                "total_questions",
                "created_at",
                "updated_at",
            ]
          ]
        ]);
    }

    public function testWithIncludeCompany()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $response = $this->getJson('/api/assestments/?includes=company');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "message",
            "data" => [
              '*' =>[
                "uuid",
                "name",
                "summary",
                "total_questions",
                "created_at",
                "updated_at",
                'company' => [
                    "uuid",
                    "business_name",
                    "name",
                    "vertical",
                    "terms_accepted",
                    "updated_at",
                    "created_at",
                ]
              ]
            ]
        ]);
    }

    public function testWithIncludeQuestion()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $response = $this->getJson('/api/assestments/?includes=questions');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "message",
            "data" => [
              '*' => 
            [
                "uuid",
                "name",
                "summary",
                "total_questions",
                "created_at",
                "updated_at",
                'questions' => [
                    '*' => [
                        "number",
                        "type",
                        "group",
                        "question",
                        "options" => [
                            "A",
                            "B",
                            "C",
                            "D",
                        ],
                        "answer",
                        "created_at",
                        "updated_at",
                    ],
                ],
            ],
          ]
        ]);
    }

    public function testWithIncludeCompanyAndQuestion()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $response = $this->getJson('/api/assestments/?includes=company,questions');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "message",
            "data" => [
            '*' => [
                "uuid",
                "name",
                "summary",
                "total_questions",
                "created_at",
                "updated_at",
                'company' => [
                    "uuid",
                    "business_name",
                    "name",
                    "vertical",
                    "terms_accepted",
                    "updated_at",
                    "created_at",
                ],
                'questions' => [
                    '*' => [
                        "number",
                        "type",
                        "group",
                        "question",
                        "options" => [
                            "A",
                            "B",
                            "C",
                            "D",
                        ],
                        "answer",
                        "created_at",
                        "updated_at",
                    ],
                ],
            ],
          ]
        ]);
    }

    public function testFailedUrl()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $response = $this->getJson("/api/assestments/?includes=company,questions");
        $response->assertStatus(500);
        $response->assertJsonStructure([
            'message',
            'code',
            'trace',
        ]);
    }

}
