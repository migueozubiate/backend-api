<?php

namespace Tests\Feature\Assestment;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DicsAssessmentEvaluationGetTest extends TestCase
{
    public function testNotAuth()
    {
        $response = $this->getJson("/api/assestments/disc/evaluations");
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    public function testAllOk()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        ); 

        $response = $this->getJson('/api/assestments/disc/evaluations?download=0&includes=seller');

        $response->assertStatus(200);
        
        $response->assertJsonStructure([
            "message",
            "data" => [
                '*' => [
                "uuid",
                "name",
                "profile",
                "points",
                "validity",
                "total_questions",
                "created_at",
                "updated_at"
                ]
            ],
            "meta" => [
                "pagination" => [
                    "total",
                    "count",
                    "per_page",
                    "current_page",
                    "total_pages",
                    "links" => []
                ]
            ]
        ]);
    }
    
}
