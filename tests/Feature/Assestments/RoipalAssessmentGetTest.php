<?php
namespace Tests\Feature\Assestments;

use Tests\TestCase;

class RoipalAssessmentGetTest extends TestCase
{

    public function testNoAuth()
    {
        $response = $this->getJson('api/assestments/roipal');
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    public function testGetInfo()
    {
        $this->passport(['executive']);
        $response = $this->getJson('api/assestments/roipal?includes=questions');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'message',
            'data' => [
                'uuid',
                'name',
                'summary',
                'total_questions',
                'questions' => [
                    '*' => [
                        'number',
                        'question',
                        'options',
                        'answer'
                    ]
                ]
            ]
        ]);
    }

    public function testAllOk()
    {
        $this->passport(['executive']);
        $response = $this->getJson('api/assestments/roipal');
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'message',
            'data' => [
                'uuid',
                'name',
                'summary',
                'total_questions',
                "created_at",
                "updated_at"
            ]
        ]);   
    }

}