<?php
namespace Tests\Feature\Assestments;

use App\Roipal\Eloquent\User;
use Tests\Support\ExecutiveAssestmentAnswersTrait;
use Tests\TestCase;

class ExecutiveRoipalAssessmentAnswerTest extends TestCase
{

    use ExecutiveAssestmentAnswersTrait;

    public function testNoAuth()
    {
        $user = factory(User::class)->make();
        $response = $this->postJson("api/executives/{$user->uuid}/assestments/roipal");
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    public function testStoreWithMissingFields()
    {
        $user = $this->passport(['executive']);

        /// Case 1 - answers
        $fieldToForget = [
            'answers'
        ];

        foreach ($fieldToForget as $field) {
            $data = $this->getRoipalAssessmentAnswers();
            array_forget($data, $field);
            $response = $this->postJson("api/executives/{$user->uuid}/assestments/roipal", $data);
            $response->assertStatus(422);
            $response->assertJsonStructure([
                'message',
                'code',
                'errors'
            ]);
            $response->assertJsonValidationErrors([$field]);
        }

        /// case 2 - number, answer
        $fieldToForget = [
            'number',
            'answer'
        ];
        foreach ($fieldToForget as $field) {
            $data = $this->getRoipalAssessmentAnswers();
            foreach ($data['answers'] as $idx => $v) {
                array_forget($data, "answers.{$idx}.{$field}");
            }

            $response = $this->postJson("api/executives/{$user->uuid}/assestments/roipal", $data);
            $response->assertStatus(422);
            $response->assertJsonStructure([
                'message',
                'code',
                'errors'
            ]);

            $errors = [];
            foreach ($data['answers'] as $idx => $v) {
                $errors[] = "answers.{$idx}.{$field}";
            }
            $response->assertJsonValidationErrors($errors);
        }

    }

    public function testStoreAnswers()
    {
        $user = $this->passport(['executive']);
        $data = $this->getRoipalAssessmentAnswers();
        $response = $this->postJson("api/executives/{$user->uuid}/assestments/roipal", $data);
        $response->assertOk();
        $response->assertJsonStructure([
            'message',
            'data' => [
                'uuid',
                'name',
                'total_questions',
                'created_at',
                'updated_at'
            ]
        ]);
    }

}