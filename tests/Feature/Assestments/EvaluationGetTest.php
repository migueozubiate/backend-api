<?php

namespace Tests\Feature\Assestments;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EvaluationGetTest extends TestCase
{
    public function testNotAuth()
    {
        $response = $this->getJson("/api/evaluations/ccfc09ee-82a8-45e3-a658-7db8697594ee?includes=answers");
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    public function testWithIncludeAnswers()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        ); 

        $response = $this->getJson('/api/evaluations/ccfc09ee-82a8-45e3-a658-7db8697594ee?includes=answers');

        $response->assertStatus(200);
        
        $response->assertJsonStructure([
            "message",
            "data" => [
                "uuid",
                "name",
                "profile",
                "points",
                "validity",
                "total_questions",
                "created_at",
                "updated_at",
                'answers' => [
                    '*' => [
                    "number",
                    "question",
                    "options" => [
                        "A",
                        "B",
                        "C",
                        "D"
                     ],
                    "answer",
                    "profile",
                    "points",
                    "created_at",
                    "updated_at"
                 ],
                ]
            ]
        ]);
    }

    public function testAllOk()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        ); 

        $response = $this->getJson('/api/evaluations/ccfc09ee-82a8-45e3-a658-7db8697594ee');

        $response->assertStatus(200);
        
        $response->assertJsonStructure([
            "message",
            "data" => [
                "uuid",
                "name",
                "profile",
                "points",
                "validity",
                "total_questions",
                "created_at",
                "updated_at"
            ]
        ]);
    }
}
