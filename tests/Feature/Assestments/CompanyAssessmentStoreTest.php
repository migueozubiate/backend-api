<?php

namespace Tests\Feature;

use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Tests\Support\CompanyAssessmentTrait;
use Tests\TestCase;

class CompanyAssessmentStoreTest extends TestCase
{
    use CompanyAssessmentTrait;

    public function testNotAuth()
    {
        $response = $this->postJson("/api/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/assessments-evaluation");
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message',
        ]);
    }

    public function testAllOk()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $data = $this->getDataCompanyAssessment();

        $response = $this->postJson('/api/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/assessments-evaluation', $data);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "message",
            "data" => [
                "uuid",
                "name",
                "summary",
                "total_questions",
                "created_at",
                "updated_at",
            ],
        ]);
    }

    public function testUpdateOneWithInvalidField()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $data = $this->getDataCompanyAssessment();
        foreach ($data as $field) {
            foreach ($field as $key => $value) {
                foreach ($value as $key => $item) {
                    $item = is_numeric($item) ? 'abcd' : 1234;
                    data_set($value, $key, $item);
                    $response = $this->json("POST", "/api/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/assessments-evaluation", $value);

                    $response->assertStatus(422);

                    $response->assertJsonStructure([
                        'message', 'code', 'errors' => $key
                    ]);

                }
            }
        }
    }

    public function testUuidNotExist()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $response = $this->postJson("/api/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/assessments-evaluation");
        $response->assertStatus(500);
        $response->assertJsonStructure([
            'message',
            'code',
            'trace',
        ]);
    }
}
