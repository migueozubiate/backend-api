<?php
namespace Tests\Feature\Assestments;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class DiscAssestmentGetInfoTest extends TestCase
{
    public function testGetInfoForNotLoggedInUser()
    {
        $response = $this->json('GET', "/api/assestments/disc");

        $response->assertStatus(401);

        $response->assertJsonStructure([
            'message',
        ]);
    }

    public function testAllOk()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $response = $this->json('GET', "/api/assestments/disc");

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'message',
            'data' => [
                'uuid',
                'name',
                'total_questions',
                'created_at',
                'updated_at',
            ]
        ]);
    }

    public function testGetDiscInfo()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $response = $this->json('GET', "/api/assestments/disc?includes=questions");

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'message',
            'data' => [
                'uuid',
                'name',
                'total_questions',
                'created_at',
                'updated_at',
                'questions' => [
                    '*' => [
                        'number',
                        'question',
                        'options',
                        'answer',
                        'created_at',
                        'updated_at',
                    ]
                ]
            ]
        ]);
    }
}
