<?php
namespace Tests\Feature;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserLoginTest extends TestCase
{
    public function testLoginWithEmptyFieldValues()
    {
        $values = [
            'username' => 'rocket@nulldata',
            'password' => 'short'
        ];

        foreach ($values as $key => $value) {
            $data = $this->getOAuthFields([ $key => $value]);

            $response = $this->post('/oauth/token', $data, ['Accept' => 'application/json']);

            $response->assertStatus(422);

            $response->assertJsonStructure([
                'message',
                'code',
                'errors'
            ]);

            $response->assertJsonValidationErrors([$key]);
        }
    }

    public function testLoginWithInvalidFieldValues()
    {
        $values = [
            'username' => 'rocket@nulldata',
            'password' => 'short'
        ];

        foreach ($values as $key => $value) {
            $data = $this->getOAuthFields([ $key => $value]);

            $response = $this->post('/oauth/token', $data, ['Accept' => 'application/json']);

            $response->assertStatus(422);

            $response->assertJsonStructure([
                'message',
                'code',
                'errors'
            ]);

            $response->assertJsonValidationErrors([$key]);
        }
    }

    public function testLoginWithInvalidCredentials()
    {
        $values = [
            'username' => 'pirate@nulldata.com',
            'password' => 'invalid-password'
        ];

        $data = $this->getOAuthFields($values);

        $response = $this->post('/oauth/token', $data, ['Accept' => 'application/json']);

        $response->assertStatus(401);

        $response->assertJsonStructure([
            'message',
            'code',
        ]);
    }

    public function testLoginWithValidCredentials()
    {
        $data = $this->getOAuthFields();

        $response = $this->post('/oauth/token', $data, ['Accept' => 'application/json']);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'message',
            'data' => [
                'token' => [
                    'token_type',
                    'expires_in',
                    'access_token',
                    'refresh_token'
                ],
                'profile'
            ]
        ]);


        $data = json_decode($response->getContent(), true);
        $user = array_get($data, 'data.profile.uuid');

        $response = $this->get("/api/users/${user}", [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . array_get($data, 'data.token.access_token')
        ]);

        $response->assertStatus(200);

        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $response = $this->json('GET', "/api/users/{$user->uuid}");

        $response->assertStatus(200);
    }

    protected function getOAuthFields($data = [])
    {
        return [
            'grant_type' => array_get($data, 'grant_type', 'password'),
            'client_id' => array_get($data, 'client_id', '923a69f6-c2c7-401c-802c-0fff3c67e448'),
            'client_secret' => array_get($data, 'client_secret', 'owQmd6i7YM9ofDS41gZ8bJN8E9tBta5ZVshom5l6'),
            'username' => array_get($data, 'username', 'rocket@nulldata.com'),
            'password' => array_get($data, 'password', 'XaaminRocks'),
            'scope' => array_get($data, 'scope', 'executive'),
        ];
    }
}
