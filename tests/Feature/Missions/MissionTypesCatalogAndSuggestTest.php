<?php
namespace Tests\Feature\Missions;

use Illuminate\Support\Arr;
use Tests\TestCase;

class MissionTypesCatalogAndSuggestTest extends TestCase
{    /** @test */
    public function MissionTypesListCatalogTest()
    {
        $this->passport(['executive']);
        $response = $this->Get('/api/catalogs/mission-types');
        $response->assertStatus(200);
        $response->assertJsonStructure([
                'message',
                'data'=> [
                    [
                        'id',
                        'language',
                        'type',
                        'icon'
                    ],
                    [
                        'id',
                        'language',
                        'type',
                        'icon'
                    ],
                    [
                        'id',
                        'language',
                        'type',
                        'icon'
                    ],
                    [
                        'id',
                        'language',
                        'type',
                        'icon'
                    ],
                    [
                        'id',
                        'language',
                        'type',
                        'icon'
                    ],
                    [
                        'id',
                        'language',
                        'type',
                        'icon'
                    ]
                ]
        ]);
    }

    /** @test */
    public function MissionEstimationTest()
    {
        $type = ['Promoción', 'D2B', 'Cambaceo', 'Farma', 'Piso', 'Telemarketing'];
        $uri = '/api/missions/suggestions?mission_type=';
        $this->passport(['executive']);

        foreach ($type as $key => $value) {
          $uri = $uri . $value;
          $response = $this->Get($uri);

          $response->assertStatus(200);
          $response->assertJsonStructure([
                  'message',
                  'data'=> [
                  'key',
                  'profile',
                  'time',
                  'time_unit',
                  'currency',
                  'min_time',
                  'max_time',
                  'fare_by_time',
                  'estimations'=> [
                      '*'=>[
                            'min',
                            'max',
                            'fare'
                        ]
                  ]
              ]

         ]);

        }
    }

  }