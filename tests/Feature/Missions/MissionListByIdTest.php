<?php
namespace Tests\Feature\Missions;

use Illuminate\Support\Arr;
use Tests\Support\Missions\MissionListByIdTrait;
use Tests\TestCase;

class MissionListByIdTest extends TestCase
{

    use MissionListByIdTrait;

    /** @test */
    public function successMissionListByIdTest()
    {
        $this->passport(['executive']);
        $response = $this->Get(
          '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678'
          );
        $response->assertStatus(200);
        $response->assertJsonStructure($this->retrieveData());
    }

    /** @test */
    public function successMissionListByIdWithIncludesTest()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678?includes=executives,company,locations,activities,invitations';

          $response = $this->Get($uri);
          $response->assertStatus(200);
          $response->assertJsonStructure($this->allRetrieveData());
    }

    /** @test */
    public function successMissionListByIdWithLocations()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678?includes=locations';
        $excludes = [
          'data.executives', 'data.company', 'data.activities', 'data.invitations'
          ];
        $data = Arr::except($this->allRetrieveData(), $excludes);
  
        $response = $this->Get($uri);
        $response->assertStatus(200);
        $response->assertJsonStructure($data);
    }

     /** @test */
    public function successMissionListWithCompany()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678?includes=company';
        $excludes = [
          'data.executives', 'data.locations', 'data.activities', 'data.invitations'
        ];
        $data = Arr::except($this->allRetrieveData(), $excludes);

        $response = $this->Get($uri);
        $response->assertStatus(200);
        $response->assertJsonStructure($data);
    }

    /** @test */
    public function successMissionListWithExecutives()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678?includes=executives';
                $excludes = [
          'data.locations', 'data.company', 'data.activities', 'data.invitations'
          ];
        $data = Arr::except($this->allRetrieveData(), $excludes);

        $response = $this->Get($uri);
        $response->assertStatus(200);
        $response->assertJsonStructure($data);
    }

        /** @test */
    public function successMissionListWithActivities()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678?includes=activities';
                $excludes = [
          'data.executives', 'data.company', 'data.locations', 'data.invitations'
          ];
        $data = Arr::except($this->allRetrieveData(), $excludes);

        $response = $this->Get($uri);
        $response->assertStatus(200);
        $response->assertJsonStructure($data);
    }

        /** @test */
    public function successMissionListWithInvitations()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678?includes=invitations';
               $excludes = [
          'data.executives', 'data.company', 'data.activities', 'data.locations'
          ];
        $data = Arr::except($this->allRetrieveData(), $excludes);

        $response = $this->Get($uri);
        $response->assertStatus(200);
        $response->assertJsonStructure($data);
    }



}