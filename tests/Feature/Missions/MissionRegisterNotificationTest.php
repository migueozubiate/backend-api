<?php
namespace Tests\Feature\Mission;

use Illuminate\Support\Arr;
use Tests\Support\Missions\MissionRegisterTestTrait;
use Tests\TestCase;

class MissionRegisterNotificationTest extends TestCase
{

    use MissionRegisterTestTrait;

    /** @test */
    public function successMissionRegisterTest()
    {

        $this->passport(['executive']);
        $response = $this->postJson(
            '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions',
            $this->getdata()
        );

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'message',
            'data' => [
                'uuid',
                'name',
                'description',
                'video',
                'video_url',
                'type',
                'profile',
                'time',
                'time_unit',
                'fare_by_time',
                'subtotal',
                'tax',
                'status',
                'total',
                'executives_requested',
                'invitations_sent',
                'accepted',
                'rejected',
                'started_at',
                'updated_at',
                'created_at',
            ],
        ]);
    }

    /** @test */
    public function successMissionRegisterWithUuidTest()
    {
        $this->passport(['executive']);
        $data = Arr::except($this->getData(), 'locations');
        $data = Arr::add($data, 'locations.*.uuid', '07ed0980-d125-4e00-bed1-d97a8c0a9889');
        $data = Arr::add($data, 'locations.*.executives_requested', 3);
        $response = $this->postJson('/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions', $data);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'message',
            'data' => [
                'uuid',
                'name',
                'description',
                'video',
                'video_url',
                'type',
                'profile',
                'time',
                'time_unit',
                'fare_by_time',
                'subtotal',
                'tax',
                'status',
                'total',
                'executives_requested',
                'invitations_sent',
                'accepted',
                'rejected',
                'started_at',
                'updated_at',
                'created_at',
            ],
        ]);
    }

}
