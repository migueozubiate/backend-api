<?php
namespace Tests\Feature\Missions;

use Illuminate\Support\Arr;
use Tests\Support\Missions\MissionListByCompanyLocationTrait;
use Tests\TestCase;

class MissionListByCompanyLocationTest extends TestCase
{

    use MissionListByCompanyLocationTrait;

    /** @test */
    public function SuccessMissionListByCompanyLocationTest()
    {
        $this->passport(['executive']);
        $response = $this->Get(
          '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/locations/3e9a3b66-33e3-334e-9a53-713cc6379a44'
          );
        $response->assertStatus(200);
        $response->assertJsonStructure($this->dataSended());
    }

    public function SuccessMissionListByCompanyLocationIncludesTest()
    {
        $this->passport(['executive']);
        $response = $this->Get(
          '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/locations/3e9a3b66-33e3-334e-9a53-713cc6379a44?includes=missions.company,executives'
          );
        $response->assertStatus(200);
        $response->assertJsonStructure($this->dataSendedWithIncludes());
    }

}