<?php
namespace Tests\Feature\Mission;

use Illuminate\Support\Arr;
use Tests\Support\Missions\MissionRegisterTestTrait;
use Tests\TestCase;

class MissionRegisterTest extends TestCase
{

    use MissionRegisterTestTrait;

    /** @test */
    public function successMissionRegisterTest()
    {
        $this->passport(['executive']);
        $response = $this->postJson(
          '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions',
            $this->getdata()
          );

        $response->assertStatus(201);
        $response->assertJsonStructure([
          'message',
          'data' => [
              'uuid',
              'name',
              'description',
              'video',
              'video_url',
              'type',
              'profile',
              'time',
              'time_unit',
              'fare_by_time',
              'subtotal',
              'tax',
              'status',
              'total',
              'executives_requested',
              'invitations_sent',
              'accepted',
              'rejected',
              'updated_at',
              'created_at'
          ]
      ]);
    }

    /** @test */
    public function successMissionRegisterWithUuidTest()
    {
        $this->passport(['executive']);
        $data = Arr::except($this->getdata(),'locations');
        $data = Arr::add($data,'locations.*.uuid', '07ed0980-d125-4e00-bed1-d97a8c0a9889');
        $data = Arr::add($data,'locations.*.executives_requested', 3);
        $response = $this->postJson('/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions', $data);
        
        $response->assertStatus(201);
        $response->assertJsonStructure([
          'message',
          'data' => [
              'uuid',
              'name',
              'description',
              'video',
              'video_url',
              'type',
              'profile',
              'time',
              'time_unit',
              'fare_by_time',
              'subtotal',
              'tax',
              'status',
              'total',
              'executives_requested',
              'invitations_sent',
              'accepted',
              'rejected',
              'updated_at',
              'created_at'
          ]
      ]);
    }

    /** @test */
    public function missingFieldsTest(){

        $this->passport(['executive']);
        $fields = Arr::dot($this->getdata());
         //dd($fields);
        foreach ($fields as $key => $value) {
              $data = Arr::except($this->getdata(), $key);

              $response = $this->postJson(
                '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions',
                $data
              );
              $response->assertStatus(422);
              $response->assertJsonStructure([
                'message',
                'code',
                'errors'=> [$key]
              ]);
        }
    }

    /** @test */   
    public function invalidInputTest()
    {
        $this->passport(['executive']);
        $fields = Arr::dot($this->getdata());

        foreach ($fields as $key => $value) {

            $data = $this->getdata();

            $variable = is_numeric($value)?'Wrong value': 5555;

            Arr::set($data, $key, $variable);
      
            $response = $this->postJson(
              '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions',
               $data
            );

            $response->assertStatus(422);
            $response->assertJsonStructure([
              'message',
              'code',
              'errors'=> [$key]
            ]); 

        }
    }



    public function invalidLocationTest()
    {
        $this->passport(['executive']);
        $fields = $this->getdata();
        $location = Arr::only($fields, 'locations');
        $dotRoot = Arr::dot($location);

        $permutations = $this->array_combinations($dotRoot);

        foreach ($dotRoot as $key => $value) {
            array_fill_keys($key, '923a69f6-c2c7-401c-802c-0fff3c67e448');
            $response = $this->postJson(
              '/api/missions/46a71b89-7e20-48fa-8f51-f01e517560e8/invitations',
              $this->getdata()
            );
            $response->assertStatus(422);
            $response->assertJsonStructure([
              'message',
              'code',
              'errors'=> $key
            ]);
        }
    }



 }