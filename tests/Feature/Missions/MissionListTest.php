<?php
namespace Tests\Feature\Missions;

use Illuminate\Support\Arr;
use Tests\Support\Missions\MissionListTestTrait;
use Tests\TestCase;

class MissionListTes extends TestCase
{

    use MissionListTestTrait;

    /** @test */
    public function successMissionListTest()
    {
        $this->passport(['executive']);
        $response = $this->Get(
          '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions'
          );
        $response->assertStatus(200);
        $response->assertJsonStructure($this->dataSended());
    }

    /** @test */
    public function successMissionListWithIncludesTest()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions?includes=locations,company,executives&limit=1';

          $response = $this->Get($uri);
          $response->assertStatus(200);
          $response->assertJsonStructure($this->dataSendedWithIncludes());
    }

    /** @test */
    public function successMissionListWithLocations()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions?includes=locations&limit=1';
        $excludes = ['data.*.executives', 'data.*.company'];
        $data = Arr::except($this->dataSendedWithIncludes(), $excludes);
        
        $response = $this->Get($uri);
        $response->assertStatus(200);
        $response->assertJsonStructure($data);
    }

        /** @test */
    public function successMissionListWithCompany()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions?includes=company&limit=1';
        $excludes = ['data.*.executives', 'data.*.locations'];
        $data = Arr::except($this->dataSendedWithIncludes(), $excludes);

        $response = $this->Get($uri);
        $response->assertStatus(200);
        $response->assertJsonStructure($data);
    }

        /** @test */
    public function successMissionListWithExecutives()
    {   
        $this->passport(['executive']);
        $uri = '/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions?includes=executives&limit=1';
        $excludes = ['data.*.locations', 'data.*.company'];
        $data = Arr::except($this->dataSendedWithIncludes(), $excludes);

        $response = $this->Get($uri);
        $response->assertStatus(200);
        $response->assertJsonStructure($data);
    }



}