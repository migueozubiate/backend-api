<?php
namespace Tests\Feature\Missions;

use Illuminate\Support\Arr;
use Tests\Support\Missions\MissionListByExecutivesTrait;
use Tests\TestCase;

class MissionListByExecutivesTest extends TestCase
{

    use MissionListByExecutivesTrait;

    /** @test */
    public function SuccessMissionListByCompanyLocationTest()
    {
        $this->passport(['executive']);
        $response = $this->Get('/api/executives/c3a2c6b8-af91-4b05-81e4-5ecb82bcaa0e/missions');
        $response->assertStatus(200);
        $response->assertJsonStructure($this->dataSended());
    }

        /** @test */
    public function SuccessMissionListByCompanyLocationIncludesTest()
    {
        $this->passport(['executive']);
        $response = $this->Get(
          '/api/executives/c3a2c6b8-af91-4b05-81e4-5ecb82bcaa0e/missions?includes=activities,locations,company,invitations'
          );
        $response->assertStatus(200);
        $response->assertJsonStructure($this->dataSendedWithIncludes());
    }

}