<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MissionUpdateStatusTest extends TestCase
{
    public function testNotAuth()
    {
        $response = $this->putJson("/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/status");
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    public function testAllOk()
   {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        ); 

        $data = [
            'status' => 1
        ];

        $response = $this->putJson('/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/status', $data);

        $response->assertStatus(200);
        
        $response->assertJsonStructure([
            "message",
            "data" => [
                "uuid",
                "name",
                "description",
                "video",
                "video_url",
                "type",
                "profile",
                "time",
                "time_unit",
                "fare_by_time",
                "subtotal",
                "tax",
                "status",
                "total",
                "executives_requested",
                "invitations_sent",
                "accepted",
                "rejected",
                "updated_at",
                "created_at"
            ]
        ]);
   }

   public function testUpdateOneWithInvalidField()
   {
       $user = factory(User::class)->create();

       Passport::actingAs(
           $user,
           ['executive']
       ); 

       $fieldToChange = [
           'status',
       ];
       
       foreach ($fieldToChange as $field) {
           $data = $fieldToChange;

           array_set($data, $field, 'prueba');

           $response = $this->json("PUT","/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/status", $data);

           $response->assertStatus(422);

           $response->assertJsonStructure([
               'message',
               'code',
               'errors'
           ]);
       }
    }

    public function testUuidNotExist()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );
        
        $response = $this->getJson("/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b6/status");
        $response->assertStatus(500);
        $response->assertJsonStructure([
            'message',
            'code',
            'trace'
        ]);
    }
}
