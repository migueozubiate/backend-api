<?php
namespace Tests\Feature\Executive;

use App\Roipal\Eloquent\User;
use Illuminate\Support\Arr;
use Tests\Support\ExecutiveProfileUpdateTestTrait;
use Tests\TestCase;

class ExecutiveProfileUpdateTest extends TestCase
{

    use ExecutiveProfileUpdateTestTrait;
    /**
     * El usuario no se encuentra auteticado
     */
    public function testNoAuth()
    {
        $user = factory(User::class)->make();
        $response = $this->putJson("/api/executives/{$user->uuid}");
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message',
        ]);
    }

    // ***********************************************************************
    // ***********************************************************************
    // Datos correctos.

    /**
     * Actualizacion de la biografia, todos los datos correctos en una petición.
     */
    public function testAllOK()
    {
        $this->passport(['executive']);

        $data = $this->makeCorrectDataRequest();
        $response = $this->putJson('/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1?include=profile', $data);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            "data" => [
                "uuid",
                "name",
                "email",
                "classification",
                "created_at",
                "updated_at",
                "profile" => [
                    "bio",
                    "video_bio",
                    "video_bio_url",
                    "website",
                    "address",
                    "score_mission",
                    "terms_accepted",
                    "position" => [
                        "latitude",
                        "longitude",
                    ],
                ],
            ],
        ]);
    }

    /**
     * Actualizaciòn de la biografica, todos los datos correctos de uno en uno.
     */
    public function testUpdateOneByOne()
    {
        $this->passport(['executive']);

        $data = $this->makeCorrectDataRequest();

        foreach ($data as $field => $value) {
            $input = [$field => $value];
            $response = $this->putJson('/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1?include=profile', $input);
            $response->assertStatus(200);

            $response->assertJsonStructure([
                "data" => [
                    "uuid",
                    "name",
                    "email",
                    "classification",
                    "created_at",
                    "updated_at",
                    "profile" => [
                        "bio",
                        "video_bio",
                        "video_bio_url",
                        "website",
                        "terms_accepted",
                    ],
                ],
            ]);
        }

    }

    // public function testUpdateLimitations()
    // {
    //     $this->passport(['executive']);

    //     $cases = [
    //         ['limitations' => null],
    //         $this->makeCorrectDataRequest('limitations'),
    //         ['limitations' => []],
    //     ];

    //     foreach ($cases as $input) {
    //         $response = $this->putJson('/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1', $input);
    //         $response->assertStatus(200);
    //         $limitations = $response->json('data.profile.limitations');
    //         if (null === $input['limitations']) {
    //             $input['limitations'] = [];
    //         }

    //         $this->assertEquals($input['limitations'], $limitations);
    //     }
    // }

    // ***********************************************************************
    // ***********************************************************************
    // Datos incorretos.

    /**
     * Se valida que la posicion se encuentre fuera de rango.
     */
    public function testPositionOutRange()
    {
        $this->passport(['executive']);

        $data = $this->makeCorrectDataRequest('position');

        $cases = [
            ['position.latitude' => -98.4],
            ['position.longitude' => -196],
            ['position.latitude' => 90.1],
            ['position.longitude' => 180.1],
            [
                'position.latitude' => -91,
                'position.longitude' => 190.8,
            ],
        ];

        foreach ($cases as $case) {
            $errors = [];
            foreach ($case as $key => $val) {
                $errors[] = $key;
                Arr::set($data, $key, $val);
            }

            $response = $this->putJson('/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1', $data);
            $response->assertStatus(422);

            $response->assertJsonStructure([
                'message', 'code', 'errors' => $errors,
            ]);
        }

    }

    /**
     * Se valida cunado falta la latitud, longitud y ambas en una actualizacion.
     */
    public function testPositionMissingField()
    {
        $this->passport(['executive']);
        $data = $this->makeCorrectDataRequest('position');
        $cases = [
            ['position.latitude'],
            ['position.longitude'],
        ];

        foreach ($cases as $case) {
            $errors = array_merge($case, ['position']);
            $input = Arr::except($data, $case);

            $response = $this->putJson('/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1', $input);
            $response->assertStatus(422);
            $response->assertJsonStructure([
                'message', 'code', 'errors' => $errors,
            ]);
        }

        $input = ['position' => []];
        $response = $this->putJson('/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1', $input);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message', 'code', 'errors' => ['position'],
        ]);

    }

    public function testWrongData()
    {
        $this->passport(['executive']);

        $cases = [

            'video_bio_url' => 'osdife',
            'address' => [1, 2, 3],
            'position' => 'Posiciones',
            'website' => 254,
            'position.latitude' => 'texto',
            'position.longitude' => 'otro texto',
        ];

        foreach ($cases as $key => $field) {
            $input = $this->getDatafromUpdateSellerProfile();

            Arr::set($input, $key, $field);
            $response = $this->putJson('/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1', $input);
  
            $response->assertStatus(422);
            $response->assertJsonStructure([
                'message', 'code', 'errors' => $key,
            ]);
        }

    }

    /// **********************************************************************
    /// **********************************************************************
    /// Contructor de información

    private function makeCorrectDataRequest($only = null)
    {
        if (null !== $only) {
            $only = !is_array($only) ? [$only] : $only;
        }

        $data = [
            'bio' => $this->faker()->text(52),
            "video_bio" => false,
            'video_bio_url' => $this->faker()->url,
            'website' => $this->faker()->url,
            'address' => $this->faker()->streetAddress,
            'score_mission' => 9,
            'terms_accepted' => null,
            'position' => [
                'latitude' => $this->faker()->latitude,
                'longitude' => $this->faker()->longitude,
            ],
        ];

        return (null === $only) ? $data : Arr::only($data, $only);
    }

}
