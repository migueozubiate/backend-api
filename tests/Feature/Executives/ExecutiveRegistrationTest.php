<?php
namespace Tests\Feature\Executive;

use Illuminate\Support\Arr;
use Tests\Support\ExecutiveRegistrationTestTrait;
use Tests\TestCase;

class ExecutiveRegistrationTest extends TestCase
{

    use ExecutiveRegistrationTestTrait;

    /**
     * Creacion de vendedor correcta.
     */
    public function testCreateSuccess()
    {
        $response = $this->postJson('/api/executives',
            $this->makeExecutiveUser());

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'data' => [
                'token' => [
                    'token_type',
                    'expires_in',
                    'access_token',
                    'refresh_token'
                ],
                'profile' => [
                    'uuid',
                    'name',
                    'email',
                    'created_at',
                    'updated_at'
                ]
            ]
        ]);

    }

    public function testMissingFields(){

        $fields = $this->makeExecutiveUser();
        $keys = array_keys($fields);

        /// Casos donde solo se envia cero a 'n-1' elementos
        $cases = $this->array_combinations($keys);

        $cases = Arr::where($cases, function($case) use($keys) {
            return count($case) !== count($keys);
        });

        foreach ($cases as $case){
            $errors = array_keys(Arr::except($fields, $case));
            $input = Arr::only($fields, $case);

            $response = $this->postJson('/api/executives', $input);
            $response->assertStatus(422);
            $response->assertJsonStructure([
                'message', 'code', 'errors' => $errors
            ]);

        }
    }



    /**
     * Intento de registro de vendedor sin aceptar los terminos
     */
    public function testCreateFailTerms()
    {
        $response = $this->postJson('/api/executives',
            $this->makeExecutiveUser(false));

        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'code',
            'errors' => [
                'terms_accepted'
            ]
        ]);
    }

    /**
     * Intento de registro de vendedor con correo electronico duplicado
     */
    public function testDuplicateUserEmail()
    {
        $data = $this->makeExecutiveUser();
        $response = $this->postJson('/api/executives', $data);
        $response->assertStatus(201);

        $response = $this->postJson('/api/executives', $data);
        $response->assertStatus(422);
        $response->assertJsonStructure([
            'message',
            'code',
            'errors' => [
                'email'
            ]
        ]);
    }

}