<?php

namespace Tests\Feature\Activities;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use App\Roipal\Eloquent\MissionActivity;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ActivityStoreTest extends TestCase
{
    public function testNotAuth()
    {
        $response = $this->json('POST', 
                                '/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/activities');

        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message'
        ]);
    }

    public function testAllOK()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        ); 

        $data = $this->getDataUpdate();

        $response = $this->json('POST', 
                                '/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/activities', 
                                $data);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'message',
            'data' => [
                'uuid',
                'title',
                'description',
                'status',
                'started_at',
                'finished_at'
            ]
        ]);
    }

    public function testStoreOneWithInvalidField()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        ); 

        $fieldToChange = [
            'title',
            'description'
        ];
      
        foreach ($fieldToChange as $field) {
            $data = $this->getDataUpdate();

            array_set($data, $field, 1234);

            $response = $this->json('POST', 
                                    '/api/executives/f4f9df8b-c77b-3354-b77e-acfe5f988cd1/missions/a57f53c8-4d6c-45d1-85c8-9db9f249b678/activities', 
                                    $data);

            $response->assertStatus(422);
            $response->assertJsonStructure([
                'message',
                'code',
                'errors'
            ]);
        }
    }

    public function getDataUpdate()
    {
        $data = [
            'title' => 'Prueba',
	        'description' => 'Description de mi actividad cambio 4'
        ];

        return $data;
    }
}
