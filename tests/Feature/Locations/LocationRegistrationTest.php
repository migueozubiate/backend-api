<?php

namespace Tests\Feature\Locations;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LocationRegistrationTest extends TestCase
{
    public function testNotAuth()
    {
        $response = $this->postJson("/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/locations");
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message'
        ]);
    }

   public function testAllOk()
   {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        ); 

        $data = $this->getDataStore();

        $response = $this->postJson('/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/locations', $data);

        $response->assertStatus(200);
        
        $response->assertJsonStructure([
            "message",
            "data" => [
                "uuid",
                "name",
                "address",
                "latitude",
                "longitude",
                "updated_at",
                "created_at"
            ]
        ]);
   }

   public function testUpdateOneWithInvalidField()
   {
       $user = factory(User::class)->create();

       Passport::actingAs(
           $user,
           ['executive']
       ); 

       $fieldToChange = [
           'name',
           'address',
           'position',
           'position.latitude',
           'position.longitude'
       ];
     
       
       foreach ($fieldToChange as $field) {
           $data = $this->getDataStore();

           array_set($data, $field, is_numeric($data) ? 'abcd':1234);

           $response = $this->json("POST","/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352/locations", $data);

           $response->assertStatus(422);

           $response->assertJsonStructure([
               'message',
               'code',
               'errors'
           ]);
       }
    }

    public function getDataStore()
    {
        $data = [
            "name" => "La Comercial Mexicana Insurgentes",
            "address" => "Insurgentes Sur 1524",
            "position" => [
                    "latitude" => "19.3670318",
                    "longitude" => "-99.1820867"
            ]
        ];

        return $data;
    }

}
