<?php

namespace Tests\Feature\Locations;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LocationGetInfoTest extends TestCase
{
    public function testNotAuth()
    {
        $response = $this->getJson("/api/locations/07ed0980-d125-4e00-bed1-d97a8c0a9889");
        $response->assertStatus(401);
        $response->assertJsonStructure([
            'message'
        ]);
    }
    
    public function testUuidNotExist()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );
        
        $response = $this->getJson("/api/locations/6f63f622-a675-4c23-b9b1-4055da15ff");
        $response->assertStatus(500);
        $response->assertJsonStructure([
            'message',
            'code',
            'trace'
        ]);
    }

    public function testGetLocationsByIdOk()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );
        
        $response = $this->getJson("/api/locations/07ed0980-d125-4e00-bed1-d97a8c0a9889");
        $response->assertStatus(200);
        $response->assertJsonStructure([
            "data" => [
                [
                    "uuid",
                    "name",
                    "address",
                    "latitude",
                    "longitude",
                    "updated_at",
                    "created_at"
                ]
            ]
        ]); 
    }

    public function testGetAllLocationsOk()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );
        
        $response = $this->getJson("/api/locations/");
        $response->assertStatus(200);
        $response->assertJsonStructure([
            'message',
            "data" => [
                [
                    "uuid",
                    "name",
                    "address",
                    "latitude",
                    "longitude",
                    "updated_at",
                    "created_at"
                ]
            ]
        ]); 
    }
}
