<?php

namespace Tests\Feature\Company;

use Tests\TestCase;
use App\Roipal\Eloquent\User;
use Laravel\Passport\Passport;
use Tests\Support\CompanyTrait;

class CompanyUpdateTest extends TestCase
{
    use CompanyTrait;

    public function testUpdateOneWithInvalidField()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $fieldToChange = [
            'business_name',
            'name',
            'vertical',
            'profile',
            'profile.video_bio_url',
            'profile.bio',
            'profile.website',
            'profile.vertical',
            'profile.address',
            'profile.position',
            'payment.owner',
            'payment.card_number',
            'payment.exp',
            'payment.ccv'
        ];

        foreach ($fieldToChange as $field) {
            $data = $this->getDataUpdateCompanyValid();

            array_set($data, $field, 3455333);

            $response = $this->json("PUT", "/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352", $data);

            $response->assertStatus(422);

            $response->assertJsonStructure([
                'message',
                'code',
                'errors'
            ]);

            $response->assertJsonValidationErrors([$field]);
        }
    }

    public function testUpdateWithValidData()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        //caso 1 update business_name, name, vertical
        $fieldToForget = [
            'profile',
            'payment',
        ];

        foreach ($fieldToForget as $field) {
            $data = $this->getDataUpdateCompanyValid();

            array_forget($data, $field);
        }

        $response = $this->json("PUT", "/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352", $data);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'message',
            'data' => [
            ]
        ]);

        //caso 2 update profile
        $fieldToForget = [
            'business_name',
            'name',
            'vertical',
            'payment',
        ];

        foreach ($fieldToForget as $field) {
            $data = $this->getDataUpdateCompanyValid();

            array_forget($data, $field);
        }

        $response = $this->json("PUT", "/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352", $data);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'message',
            'data' => [
            ]
        ]);

        //caso 3 update payment
        $fieldToForget = [
            'business_name',
            'name',
            'vertical',
            'profile',
        ];

        foreach ($fieldToForget as $field) {
            $data = $this->getDataUpdateCompanyValid();

            array_forget($data, $field);
        }

        $response = $this->json("PUT", "/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352", $data);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'message',
            'data' => [
            ]
        ]);
    }

    public function testUpdateWithMissingFieldsPaymentOnly()
    {
        $user = factory(User::class)->create();

        Passport::actingAs(
            $user,
            ['executive']
        );

        $fieldToForget = [
            'payment.owner',
            'payment.card_number',
            'payment.exp',
            'payment.ccv'
        ];

        foreach ($fieldToForget as $field) {
            $data = $this->getDataUpdateCompanyValid();

            array_forget($data, $field);

            $response = $this->json("PUT", "/api/companies/f69f7225-07a3-4caa-88cf-a5f89e84a352", $data);

            $response->assertStatus(422);

            $response->assertJsonStructure([
                'message',
                'code',
                'errors'
            ]);

            $response->assertJsonValidationErrors([$field]);
        }

    }
}
