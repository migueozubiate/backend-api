<?php

namespace Tests\Feature\Assestments;

use Tests\Support\EmployeerTrait;
use Tests\TestCase;

class CompanyRegistrationTest extends TestCase
{

    use EmployeerTrait;

    public function testStoreWithInvalidField()
    {
        //caso 2 update profile
        $fieldToUpdate = [
            'name',
            'email',
            'password'
        ];

        foreach ($fieldToUpdate as $field) {
            $data = $this->getDataStoreValid();

            array_set($data, $field, 3455333);

            $response = $this->json("POST", "/api/companies/", $data);

            $response->assertStatus(422);

            $response->assertJsonStructure([
                'message',
                'code',
                'errors' => [$field]
            ]);
        }
    }

    public function testStoreWithValidData()
    {
        $data = $this->getDataStoreValid();

        $response = $this->json("POST", "/api/companies/", $data);

        $response->assertStatus(201);
        $response->assertJsonStructure([
            'message',
            'data' => [
                'token' => [
                    'token_type',
                    'expires_in',
                    'access_token',
                    'refresh_token'
                ],
                'profile' => [
                    'uuid',
                    'name',
                    'email',
                    'classification',
                    'updated_at',
                    'created_at'
                ],
                'company' => [
                    'uuid',
                    'business_name',
                    'name',
                    'vertical',
                    'terms_accepted',
                    'updated_at',
                    'created_at'
                ]
            ]
        ]);
    }

    public function testStoreEmailDuplicate()
    {
        $data = $this->getDataStoreValid();

        $response = $this->json("POST", "/api/companies/", $data);

        $response->assertStatus(201);

        $response->assertJsonStructure([
            'message',
            'data' => [
                'token' => [
                    'token_type',
                    'expires_in',
                    'access_token',
                    'refresh_token'
                ],
                'profile' => [
                    'uuid',
                    'name',
                    'email',
                    'classification',
                    'updated_at',
                    'created_at'
                ],
                'company' => [
                    'uuid',
                    'business_name',
                    'name',
                    'vertical',
                    'terms_accepted',
                    'updated_at',
                    'created_at'
                ]
            ]
        ]);

        $response = $this->json("POST", "/api/companies/", $data);

        $response->assertStatus(422);

        $response->assertJsonStructure([
            'message',
            'code',
            'errors' => [
                'email'
            ]
        ]);
    }
}
