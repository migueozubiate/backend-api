<?php
namespace Tests;

use App\Roipal\Eloquent\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, WithFaker;

    protected $user_reset = true;
    /** @var User */
    protected $user;

    protected function setUp()
    {
        parent::setUp();
        if ($this->user_reset) {
            $this->user = null;
        }
    }

    protected function passport($scopes = [], User $user = null)
    {
        if (!$this->user && !$user) {
            $user = $this->user = factory(User::class)->create();
        }

        Passport::actingAs($user, $scopes);

        return $this->user;
    }

    protected function unpassport()
    {
        if ($this->user) {
            $this->user->withAccessToken(null);
        }
    }

    /**
     * @param $array
     *
     * @return array
     */
    function array_combinations($array){
        $results = [[]];

        foreach ($array as $element){
            foreach ($results as $combination){
                array_push($results, array_merge(array($element), $combination));
            }
        }
        return $results;
    }

}
